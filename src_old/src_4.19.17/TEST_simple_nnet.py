#%% Imports
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *

import os
import time
import shutil
import numpy as np
import pandas as pd

# The custom imports should be in same folder as this script
import __main__ as main
if not hasattr(main, '__file__'):
    # This means its an iPython instance
    import getpass
    usr = getpass.getuser()
    if usr == 'alexlangMacBookPro':
        path_to_script_folder = '/Users/alexlangMacBookPro/Dropbox/BitBucket/dbm/src/'
        os.chdir(path_to_script_folder)

# import from my files
from run_dbm import train_nnet

#%%
def ais_test(save_folder_base):
    """ Make sure ais and exact match """
    pass

#%%
def default_train_test():
    # parameters to create static dbm
    dbm_init = {
                'layer_size_list'   : [10**2, 4**2],
                'topology_dict'     : {0:{1}},
                'residual_dict'     : {},
                'dropout_prob_list' : [0.0, 0.0],
                'IS_mean_field'     : True,
                'IS_persist'        : True,
                'IS_centered'       : False,
                'IS_std'            : False,
                'IS_whitened'       : False,
                'std_eps'           : None,
                'white_type'        : None,
                'white_eps'         : None, # regularizers whitening
                'weight_file'       : None, # Needed to load existing weights
                }
    
    # parameters to train dbm
    dbm_train = {
                 'n_epoch'      : 100,
                 'batch_size'   : 100, 
                 'n_gibbs_neg'  : 5,
                 'lr_center'    : 0.01,
                 'L1'           : 1e-5,
                 'L2'           : 1e-5,
                 'IS_decay'     : True,
                 'white_update' : None, # empty list will update every epoch, None for no updates
                 'flip_rate'    : 1e-3, # percentage of raw input pixels to flip
                 'IS_fast'      : True, # cuts down on callback time
                 }
                
    # parameters of dataset
    data_details = {
                    'dataset'       : 'MNIST_easy',
                    'data_type'     : 'sampled',
                    'IS_prob_train' : True, # train on prob, regardless of valid type
                    'IS_test_mode'  : False, # whether valid set is actual test set
                    'IS_debug'      : False, # trains a smaller nnet
                    }
    
    optimizer = {
                 'name'         : 'sgd',
                 'lr'           : 0.01,
                 'momentum'     : 0.9,
                 'nesterov'     : True,
                 'beta'         : 1.0,
                 'decay'        : 1.8e-5,
                 'schedule_decay' : 0.004,
                 }
    
    return dbm_init, dbm_train, data_details, optimizer

#%%
def train_test(save_folder_base):
    """ Should be able to do really well on this example.
        Basic setup hits LL = -33 by 100 and LL = -29 by 1000 epochs.
        Idea is to run for 100 epochs which is around 1 min / test.
    """
    
    #test_array = np.asarray([11])
    test_array = np.asarray([9, 13])
    #test_array = np.arange(13) # 17 but not all ready
    #test_array = np.arange(17)
    
    summary = save_folder_base + 'summary.txt'
    
    # these are perfectly good
    cut_best = -35.0 
    # this may just be a poor parameter choice
    # but code implementation is probably good
    cut_okay = -50.0 
    
    all_status = []
    start = time.time()
    
    with open(summary, 'w') as f:
        f.write('Results of TEST_simple_nnet. Expect LL > -35 after 100.\n\n')
    
    def test_fxn(n_test, name_test, summary,
                 dbm_init, dbm_train, data_details, optimizer):

        save_folder = save_folder_base + 'test_'+str(n_test)
        
        try:
            train_nnet(save_folder, dbm_init, dbm_train, data_details, optimizer)
        
            df = pd.read_csv(save_folder+'/history.txt')
            val_prob = df['val_prob'].values
            vp = val_prob[np.logical_not(np.isnan(val_prob))]
            loglike = np.max(vp)
        
            if loglike >= cut_best:
                status = 'PASS'
                out_status = 2
            elif loglike >= cut_okay:
                status = 'OKAY'
                out_status = 1
            else:
                status = 'FAIL'
                out_status = 0
                
        except:
            loglike = np.nan
            status = 'ERROR'
            out_status = -1    
                
    
        with open(summary, 'a') as f:
            f.write('Test {}: {}\n'.format(n_test, name_test))
            f.write('\tLL = {:.1f}.\tSTATUS = {}\n'.format(loglike, status))    
        
        return out_status
    
    ##### Test 0: Baseline
    n_test = 0
    if n_test in test_array:
        name_test = 'basic'
        dbm_init, dbm_train, data_details, optimizer = default_train_test()
        status = test_fxn(n_test, name_test, summary,
                          dbm_init, dbm_train, data_details, optimizer)
        all_status.append(status)
    
    ##### Test 1: Centered
    n_test = 1
    if n_test in test_array:
        name_test = 'center'
        dbm_init, dbm_train, data_details, optimizer = default_train_test()
        dbm_init['IS_centered'] = True
        status = test_fxn(n_test, name_test, summary,
                          dbm_init, dbm_train, data_details, optimizer)
        all_status.append(status)
    
    ##### Test 2: Fliprate
    n_test = 2
    if n_test in test_array:
        name_test = 'fliprate'
        dbm_init, dbm_train, data_details, optimizer = default_train_test()
        dbm_train['flip_rate'] = 0.01
        status = test_fxn(n_test, name_test, summary,
                          dbm_init, dbm_train, data_details, optimizer)
        all_status.append(status)
    
    ##### Test 3: Small dropout
    n_test = 3
    if n_test in test_array:
        name_test = 'small dropout'
        dbm_init, dbm_train, data_details, optimizer = default_train_test()
        dbm_init['dropout_prob_list'] = [0.05, 0.05]
        status = test_fxn(n_test, name_test, summary,
                          dbm_init, dbm_train, data_details, optimizer)
        all_status.append(status)
    
    ##### Test 4: Larger dropout
    n_test = 4
    if n_test in test_array:
        name_test = 'larger dropout'
        dbm_init, dbm_train, data_details, optimizer = default_train_test()
        dbm_init['dropout_prob_list'] = [0.25, 0.25]
        status = test_fxn(n_test, name_test, summary,
                          dbm_init, dbm_train, data_details, optimizer)
        all_status.append(status)
    
    ##### Test 5: std, large eps
    n_test = 5
    if n_test in test_array:
        name_test = 'std large eps'
        dbm_init, dbm_train, data_details, optimizer = default_train_test()
        dbm_init['IS_centered'] = True
        dbm_init['IS_std'] = True
        dbm_init['std_eps'] = 0.5
        status = test_fxn(n_test, name_test, summary,
                          dbm_init, dbm_train, data_details, optimizer)
        all_status.append(status)
    
    ##### Test 6: std, small eps
    n_test = 6
    if n_test in test_array:
        name_test = 'std small eps'
        dbm_init, dbm_train, data_details, optimizer = default_train_test()
        dbm_init['IS_centered'] = True
        dbm_init['IS_std'] = True
        dbm_init['std_eps'] = 0.1
        status = test_fxn(n_test, name_test, summary,
                          dbm_init, dbm_train, data_details, optimizer)
        all_status.append(status)

    ##### Test 7: White, PCA, large eps
    n_test = 7
    if n_test in test_array:
        name_test = 'white PCA large eps'
        dbm_init, dbm_train, data_details, optimizer = default_train_test()
        dbm_init['IS_centered'] = True
        dbm_init['IS_whitened'] = True
        dbm_init['white_type'] = 'PCA'
        dbm_init['white_eps'] = 1e-2
        status = test_fxn(n_test, name_test, summary,
                          dbm_init, dbm_train, data_details, optimizer)
        all_status.append(status)
   
    ##### Test 8: White, PCA, small eps
    n_test = 8
    if n_test in test_array:
        name_test = 'white PCA small eps'
        dbm_init, dbm_train, data_details, optimizer = default_train_test()
        dbm_init['IS_centered'] = True
        dbm_init['IS_whitened'] = True
        dbm_init['white_type'] = 'PCA'
        dbm_init['white_eps'] = 1e-3
        status = test_fxn(n_test, name_test, summary,
                          dbm_init, dbm_train, data_details, optimizer)
        all_status.append(status)

    ##### Test 9: White, ZCA, large eps
    n_test = 9
    if n_test in test_array:
        name_test = 'white ZCA large eps'
        dbm_init, dbm_train, data_details, optimizer = default_train_test()
        dbm_init['IS_centered'] = True
        dbm_init['IS_whitened'] = True
        dbm_init['white_type'] = 'ZCA'
        dbm_init['white_eps'] = 1e-3
        status = test_fxn(n_test, name_test, summary,
                          dbm_init, dbm_train, data_details, optimizer)
        all_status.append(status)
   
    ##### Test 10: White, ZCA, small eps
    n_test = 10
    if n_test in test_array:
        name_test = 'white ZCA small eps'
        dbm_init, dbm_train, data_details, optimizer = default_train_test()
        dbm_init['IS_centered'] = True
        dbm_init['IS_whitened'] = True
        dbm_init['white_type'] = 'ZCA'
        dbm_init['white_eps'] = 1e-5
        status = test_fxn(n_test, name_test, summary,
                          dbm_init, dbm_train, data_details, optimizer)
        all_status.append(status)
        
    ##### Test 11: std and white, ZCA, large eps
    n_test = 11
    if n_test in test_array:
        name_test = 'std, white ZCA, large eps'
        dbm_init, dbm_train, data_details, optimizer = default_train_test()
        dbm_init['IS_centered'] = True
        dbm_init['IS_whitened'] = True
        dbm_init['IS_std'] = True
        dbm_init['std_eps'] = 0.1
        dbm_init['white_type'] = 'ZCA'
        dbm_init['white_eps'] = 1e-3
        optimizer['momentum'] = 0.0
        status = test_fxn(n_test, name_test, summary,
                          dbm_init, dbm_train, data_details, optimizer)
        all_status.append(status)
       
    ##### Test 12: std and White, ZCA, small eps
    n_test = 12
    if n_test in test_array:
        name_test = 'std, white ZCA, small eps'
        dbm_init, dbm_train, data_details, optimizer = default_train_test()
        dbm_init['IS_centered'] = True
        dbm_init['IS_whitened'] = True
        dbm_init['IS_std'] = True
        dbm_init['std_eps'] = 0.1
        dbm_init['white_type'] = 'ZCA'
        dbm_init['white_eps'] = 1e-5
        status = test_fxn(n_test, name_test, summary,
                          dbm_init, dbm_train, data_details, optimizer)
        all_status.append(status)
        
        
    ##### Test 13: White ZCA w/updates, large eps
    n_test = 13
    if n_test in test_array:
        name_test = 'white ZCA w/updates, large eps'
        dbm_init, dbm_train, data_details, optimizer = default_train_test()
        dbm_init['IS_centered'] = True
        dbm_init['IS_whitened'] = True
        dbm_init['white_type'] = 'ZCA'
        dbm_init['white_eps'] = 1e-3
        dbm_train['white_update'] = []
        status = test_fxn(n_test, name_test, summary,
                          dbm_init, dbm_train, data_details, optimizer)
        all_status.append(status)
   
    ##### Test 14: White ZCA w/updates, small eps
    n_test = 14
    if n_test in test_array:
        name_test = 'white ZCA, small eps'
        dbm_init, dbm_train, data_details, optimizer = default_train_test()
        dbm_init['IS_centered'] = True
        dbm_init['IS_whitened'] = True
        dbm_init['white_type'] = 'ZCA'
        dbm_init['white_eps'] = 1e-5
        dbm_train['white_update'] = []
        status = test_fxn(n_test, name_test, summary,
                          dbm_init, dbm_train, data_details, optimizer)
        all_status.append(status)
    
    ##### Test 15: std and white w/updates, ZCA, large eps
    n_test = 15
    if n_test in test_array:
        name_test = 'std, white ZCA w/updates Z, large eps'
        dbm_init, dbm_train, data_details, optimizer = default_train_test()
        dbm_init['IS_centered'] = True
        dbm_init['IS_whitened'] = True
        dbm_init['IS_std'] = True
        dbm_init['std_eps'] = 0.1
        dbm_init['white_type'] = 'ZCA'
        dbm_init['white_eps'] = 1e-3
        dbm_train['white_update'] = []
        status = test_fxn(n_test, name_test, summary,
                          dbm_init, dbm_train, data_details, optimizer)
        all_status.append(status)
       
    ##### Test 16: var and white w/updates, ZCA, small eps
    n_test = 16
    if n_test in test_array:
        name_test = 'std, white ZCA w/updates, small eps'
        dbm_init, dbm_train, data_details, optimizer = default_train_test()
        dbm_init['IS_centered'] = True
        dbm_init['IS_whitened'] = True
        dbm_init['IS_std'] = True
        dbm_init['std_eps'] = 0.1
        dbm_init['white_type'] = 'ZCA'
        dbm_init['white_eps'] = 1e-5
        dbm_train['white_update'] = []
        status = test_fxn(n_test, name_test, summary,
                          dbm_init, dbm_train, data_details, optimizer)
        all_status.append(status)
    
    ##########
    # final count of results
    all_status = np.asarray(all_status)
    n_tests = all_status.size
    n_pass = np.sum(all_status==2).astype(int)
    n_okay = np.sum(all_status==1).astype(int)
    n_fail = np.sum(all_status==0).astype(int)
    n_error = np.sum(all_status == -1).astype(int)
    
    total = time.time()-start

    with open(summary, 'a') as f:
        f.write('\n')
        f.write('Total test time: {:.2f} minutes\n'.format(total/60.0))
        f.write('Time per test: {:.2f} minutes\n'.format(total/(60.0*n_tests)))
        f.write('\n')
        f.write('Passed: {}/{}\n'.format(n_pass, n_tests))
        f.write('Okay:   {}/{}\n'.format(n_okay, n_tests))
        f.write('Failed: {}/{}\n'.format(n_fail, n_tests))
        f.write('Error: {}/{}\n'.format(n_error, n_tests))
        f.write('\n')
        
    
    
#%%
if __name__ == '__main__':
    
    save_folder_base = '../results/test_simple_nnet/'
    if os.path.exists(save_folder_base):
        shutil.rmtree(save_folder_base)
    os.mkdir(save_folder_base)
    
    ais_test(save_folder_base)
    train_test(save_folder_base)
