#%% Imports
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *

import os
import numpy as np
from collections import OrderedDict

from six.moves import zip
import h5py

import theano.tensor as T
from theano.ifelse import ifelse

# The custom imports should be in same folder as this script
import __main__ as main
if not hasattr(main, '__file__'):
    # This means its an iPython instance
    import getpass
    usr = getpass.getuser()
    if usr == 'alexlangMacBookPro':
        path_to_script_folder = '/Users/alexlangMacBookPro/Dropbox/BitBucket/dbm/src/'
        os.chdir(path_to_script_folder)

# import from my files
from utils import make_shared, FLOATX, EPS

#%%
def DefaultParameters(optimizer,
                      n_epoch,
                      n_sample,
                      batch_size,
                      IS_decay):
    
    n_batch = int(np.floor(n_sample/float(batch_size)))
    
    if optimizer['name'] in ['sgd', 'sgdr']:
        
        if IS_decay and (n_epoch >= 100):
            mom_iter_max = n_batch*(n_epoch-50)
            decay = 1.8e-5
        else:
            mom_iter_max = np.inf
            decay = 0.0
    
        default = {'lr' : 0.01,
                   'momentum' : 0.9,
                   'nesterov' : True,
                   'schedule_decay' : 0.004,
                   'beta' : 1.0,
                   'decay' : decay,
                   'mom_iter_max' : mom_iter_max}
        
        if optimizer['name'] == 'sgdr':
            # reaches 0.99 of max within 5 epochs
            schedule_decay = np.log(0.02)/np.log(0.96)/(5.0*n_batch)
            default['schedule_decay'] = schedule_decay
            default['lr_iter_max'] = n_batch*n_epoch
        
    elif optimizer['name'] == 'nadam':
        default = {'lr' : 0.002,
                   'beta_1' : 0.9,
                   'beta_2' : 0.999,
                   'epsilon' : EPS,
                   'schedule_decay' : 0.004,
                   'beta' : 1.0}
  
    else:
        raise NotImplementedError
        
    # prep for optimizer init
    for k, v in default.items():
        if k not in optimizer.keys():
            optimizer[k] = v
    
    return optimizer

#%%
class BaseOptimizer(object):
    """ Basic optimizer framework but needs to be fully implemented """
    def __init__(self, nnet):
        assert nnet is not None
        self.weights = []
        self.nnet = nnet
        self.init_velocity(self.nnet.trainable_weights)
    
    def init_velocity(self, params):
        """ Initialize the velocities """
        if not hasattr(self, 'velocity'):
            self.velocity = []
            for p in params:
                v = make_shared(np.zeros(p.shape.eval()), name='vel_'+p.name)
                self.velocity.append(v)
                self.weights.append(v)
    
    def get_updates(self, params, grads):
        """ Each optimizer needs to implement its own updates"""
        
        self.updates = OrderedDict()

        # need to initialize velocities
        if not hasattr(self, 'velocity'):
            self.init_velocity(params)
    
    def save_weights(self, filepath, overwrite=True):
        """ Dumps all layer weights to a HDF5 file. """
        
        # If file exists and should not be overwritten:
        if not overwrite and os.path.isfile(filepath):
            raise NotImplementedError, 'Cannot overwrite existing weights'
        f = h5py.File(filepath, 'w')
        self.save_weights_to_hdf5_group(f)
        f.flush()
        f.close()

    def save_weights_to_hdf5_group(self, f):
        
        weights = self.weights
        f.attrs['weights'] = [w.name.encode('utf8') for w in weights]

        for w in weights:
            g = f.create_group(w.name)
            weight_values = [w.get_value()]
            weight_names = [w.name.encode('utf8')]
            g.attrs['weight_names'] = weight_names
            for name, val in zip(weight_names, weight_values):
                param_dset = g.create_dataset(name, val.shape,
                                              dtype=val.dtype)
                if not val.shape:
                    # scalar
                    param_dset[()] = val
                else:
                    param_dset[:] = val

#%%
class SGD(BaseOptimizer):
    """ Stochastic gradient descent optimizer.
    
    # Arguments
        lr: float >= 0. Learning rate.
        momentum: float >= 0. Parameter updates momentum.
        decay: float >= 0. Learning rate decay over each update.
        nesterov: boolean. Whether to apply Nesterov momentum.
    """

    def __init__(self,
                 nnet,
                 lr=0.01,
                 beta=1.0,
                 momentum=0.9,
                 nesterov=True,
                 schedule_decay=0.004,
                 decay=1.8e-5,
                 mom_iter_max=500*950):

        super(SGD, self).__init__(nnet)
        
        if beta != 1:
            raise NotImplementedError
        
        self.iterations = make_shared(0, 'iterations')
        self.beta_orig = T.cast(beta, FLOATX)
        self.beta = make_shared(beta, 'beta')
        self.lr_orig = T.cast(lr, FLOATX)
        self.lr = make_shared(lr, 'lr')
        self.momentum_goal = make_shared(momentum, 'momentum_goal')
        self.momentum = make_shared(momentum, 'momentum')
        self.nesterov = nesterov
        
        self.schedule_decay = make_shared(schedule_decay, 'schedule_decay')
        self.decay = make_shared(decay, 'decay')
        self.mom_iter_max = T.cast(mom_iter_max, FLOATX)

        self.weights += [self.iterations, self.beta, self.lr, self.momentum]
        

    def get_updates(self, params, grads):
        
        super(SGD, self).get_updates(params, grads)
        
        lr = self.lr_orig/(1+self.decay*self.iterations)

        momentum = ifelse(T.lt(self.iterations, self.mom_iter_max),
                         (self.momentum_goal
                         *(1. - 0.5 * (T.pow(0.96, self.iterations * self.schedule_decay)))
                         *(1. - 0.5 * (T.pow(0.96, (self.mom_iter_max-self.iterations) * self.schedule_decay)))),
                         0.5*self.momentum_goal)
 
        # update the velocities to reflect new gauge
        velocity = self.nnet.apply_gauge(self.velocity, mode='center_std')
        
        self.updates[self.lr] = lr
        self.updates[self.momentum] = momentum
        self.updates[self.iterations] = self.iterations+1

        for p, g, v_old, v_sh in zip(params, grads, velocity, self.velocity):
            
            v = momentum * v_old - lr * g  # velocity
            self.updates[v_sh] = v

            if self.nesterov:
                del_p = momentum * v - lr * g
            else:
                del_p = v

            self.updates[p] = del_p

        return self.updates

#%%
class SGDR(BaseOptimizer):
    """Stochastic gradient descent optimizer.
    Includes support for momentum,
    learning rate decay, and Nesterov momentum.
    # Arguments
        lr: float >= 0. Learning rate.
        momentum: float >= 0. Parameter updates momentum.
        decay: float >= 0. Learning rate decay over each update.
        nesterov: boolean. Whether to apply Nesterov momentum.
    """

    def __init__(self,
                 nnet,
                 lr=0.01,
                 beta=1.0,
                 momentum=0.9,
                 nesterov=True,
                 schedule_decay=0.004, 
                 mom_iter_max=500*975,
                 lr_iter_decay=500*950,
                 lr_iter_max=500*1000):

        super(SGDR, self).__init__(nnet)
        
        if beta != 1:
            raise NotImplementedError

        self.iterations = make_shared(0, 'iterations')
        self.beta_orig = T.cast(beta, FLOATX)
        self.beta = make_shared(beta, 'beta')
        self.lr_orig = T.cast(lr, FLOATX)
        self.lr = make_shared(lr, 'lr')
        self.momentum_goal = make_shared(momentum, 'momentum_goal')
        self.momentum = make_shared(momentum, 'momentum')
        self.nesterov = nesterov
        
        self.schedule_decay = make_shared(schedule_decay, 'schedule_decay')
        self.mom_iter_max = T.cast(mom_iter_max, FLOATX)
        self.lr_iter_decay = T.cast(lr_iter_decay, FLOATX)
        self.lr_iter_max = T.cast(lr_iter_max, FLOATX)

        self.weights += [self.iterations, self.beta, self.lr, self.momentum]
        

    def get_updates(self, params, grads):

        super(SGDR, self).get_updates(params, grads)
        
        cos_const = T.cast(np.pi/(self.lr_iter_max-self.lr_iter_decay), dtype=FLOATX)
        lr = ifelse(T.lt(self.iterations, self.lr_iter_decay),
                    self.lr_orig,
                    0.5*self.lr_orig*(1.0+T.cos((self.iterations-self.lr_iter_decay)*cos_const))
                    )
        
        momentum = ifelse(T.lt(self.iterations, self.mom_iter_max),
                         (self.momentum_goal
                         *(1. - 0.5 * (T.pow(0.96, self.iterations * self.schedule_decay)))
                         *(1. - 0.5 * (T.pow(0.96, (self.mom_iter_max-self.iterations) * self.schedule_decay)))),
                         0.5*self.momentum_goal)
 
        velocity = self.nnet.apply_gauge(self.velocity, mode='center_std')
        
        self.updates[self.lr] = lr
        self.updates[self.momentum] = momentum
        self.updates[self.iterations] = self.iterations+1

        for p, g, v_old, v_sh in zip(params, grads, velocity, self.velocity):
            
            v = momentum * v_old - lr * g  # velocity
            self.updates[v_sh] = v

            if self.nesterov:
                del_p = momentum * v - lr * g
            else:
                del_p = v

            self.updates[p] = del_p

        return self.updates
    


#%%
class Nadam(BaseOptimizer):
    """Nesterov Adam optimizer.
    Much like Adam is essentially RMSprop with momentum,
    Nadam is Adam RMSprop with Nesterov momentum.
    Default parameters follow those provided in the paper.
    It is recommended to leave the parameters of this optimizer
    at their default values.
    # Arguments
        lr: float >= 0. Learning rate.
        beta_1/beta_2: floats, 0 < beta < 1. Generally close to 1.
        epsilon: float >= 0. Fuzz factor.
    # References
        - [Nadam report](http://cs229.stanford.edu/proj2015/054_report.pdf)
        - [On the importance of initialization and momentum in deep learning](http://www.cs.toronto.edu/~fritz/absps/momentum.pdf)
    """

    def __init__(self,
                 nnet,
                 lr=0.002,
                 beta=1.0,
                 beta_1=0.9,
                 beta_2=0.999,
                 epsilon=EPS,
                 schedule_decay=0.004):
        
        super(Nadam, self).__init__(nnet)
        
        if beta != 1:
            raise NotImplementedError
        
        self.iterations = make_shared(0, 'iterations')
        self.m_schedule = make_shared(1, 'm_schedule')
        self.lr = make_shared(lr, 'lr')
        self.beta = make_shared(beta, 'beta')
        self.beta_1 = make_shared(beta_1, 'beta_1')
        self.beta_2 = make_shared(beta_2, 'beta_2')
        self.epsilon = make_shared(epsilon, 'epsilon')
        self.schedule_decay = make_shared(schedule_decay, 'schedule_decay')

        self.weights += [self.iterations, self.m_schedule, self.lr, self.beta,
                         self.beta_1, self.beta_2, self.epsilon, self.schedule_decay]
        
    
    def get_updates(self, params, grads):

        super(Nadam, self).get_updates(params, grads)
        
        # need to initialize m_velocities
        if not hasattr(self, 'm_velocity'):
            self.m_velocity = []
            for p in params:
                m = make_shared(np.zeros(p.shape.eval()), name='m_vel_'+p.name)
                self.m_velocity.append(m)
                self.weights.append(m)
        
        lr = self.lr        
        
        t = self.iterations + 1

        # Due to the recommendations in [2], i.e. warming momentum schedule
        momentum_cache_t = self.beta_1 * (1. - 0.5 * (T.pow(0.96, t * self.schedule_decay)))
        momentum_cache_t_1 = self.beta_1 * (1. - 0.5 * (T.pow(0.96, (t + 1) * self.schedule_decay)))
        m_schedule_new = self.m_schedule * momentum_cache_t
        m_schedule_next = self.m_schedule * momentum_cache_t * momentum_cache_t_1

        self.updates[self.iterations] = self.iterations+1
        self.updates[self.m_schedule] = m_schedule_new
        
        velocity = self.nnet.apply_gauge(self.velocity, mode='center_std')
        m_velocity = self.nnet.apply_gauge(self.m_velocity, mode='center_std')

        for p, g, m, m_sh, v, v_sh in zip(params, grads,
                                          m_velocity, self.m_velocity,
                                          velocity, self.velocity):
            
            # the following equations given in [1]
            g_prime = g / (1. - m_schedule_new)
            m_t = self.beta_1 * m + (1. - self.beta_1) * g
            m_t_prime = m_t / (1. - m_schedule_next)
            v_t = self.beta_2 * v + (1. - self.beta_2) * T.sqr(g)
            v_t_prime = v_t / (1. - T.pow(self.beta_2, t))
            m_t_bar = (1. - momentum_cache_t) * g_prime + momentum_cache_t_1 * m_t_prime

            self.updates[m_sh] = m_t
            self.updates[v_sh] = v_t

            del_p = - lr * m_t_bar / (T.sqrt(v_t_prime) + self.epsilon)

            self.updates[p] = del_p   
                        
        return self.updates
