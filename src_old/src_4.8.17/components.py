#%% Imports 
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *

import six
import numpy as np
import os
from collections import OrderedDict

import theano.tensor as T
from theano.ifelse import ifelse

# The custom imports should be in same folder as this script
import __main__ as main
if not hasattr(main, '__file__'):
    # This means its an iPython instance
    import getpass
    usr = getpass.getuser()
    if usr == 'alexlangMacBookPro':
        path_to_script_folder = '/Users/alexlangMacBookPro/Dropbox/BitBucket/dbm/src/'
        os.chdir(path_to_script_folder)

# my files
from utils import binomial_sample, make_theano_rng, make_shared, FLOATX, EPS
import my_data

#%%
class NeuralNetParts(object):
    """ Generic object for neural network components """
    
    def __init__(self, name):
        
        assert name.startswith('h')
        self.name = name
        self._updates = OrderedDict()
        self._trainable_weights = []
        self._non_trainable_weights = []
    
    @property
    def updates(self):
        return self._updates
    
    @updates.setter
    def updates(self, updates):
        if updates is None:
            return
        keys = self._updates.keys()
        for k, v in updates.items():
            assert k not in keys, 'Already have update for '+k.name
            self._updates[k] = v

    @property
    def weights(self):
        return self._trainable_weights + self._non_trainable_weights

#%%
#%%
class Synapse(NeuralNetParts):
    def __init__(self,
                 n_in,
                 n_out,
                 name,
                 init_W=None,
                 init_cov=None,
                 init='orthogonal',
                 IS_trainable=True):
                
        super(Synapse, self).__init__(name)
        
        self.n_in = n_in
        self.n_out = n_out
        self.IS_trainable = IS_trainable
        
        # assume False until later initialized
        self.IS_centered = False
        self.IS_std = False
        self.IS_whitened = False
                    
        # Make weight
        if init_W is None:
            shape = (n_in, n_out)
            if init == 'orthogonal':
                W = np.random.normal(size=shape)
                U, _, V = np.linalg.svd(W)
                S = np.zeros(shape)
                np.fill_diagonal(S, 1)
                init_W = U.dot(S).dot(V)
                
            elif init == 'normal':
                init_W = 0.01*np.random.normal(size=shape)
            
            elif init == 'identity':
                assert n_in == n_out
                init_W = np.identity(n_in)
            
            elif init == 'noisy_identity':
                assert n_in == n_out
                init_W = 1e-4*np.random.normal(size=shape)
                np.fill_diagonal(init_W, 1)
            
            elif init == 'zeros':
                init_W = np.zeros(shape)
            
            else:
                raise NotImplementedError
    
        self.W = make_shared(init_W, name=self.name)
    
        if init_cov is None:        
            init_cov = np.zeros((init_W.shape))
        self.cov = make_shared(init_cov, name=self.name+'_cov')
       
        # needed for saving and training weights
        self._trainable_weights.append(self.W)
        self._non_trainable_weights.append(self.cov)                
 
    #%%
    def init_layers(self, layer_in, layer_out):
        """ This allows synapse to know who is connected to it """
        self.layer_in = layer_in
        self.layer_out = layer_out
        
        self.IS_centered = layer_in.IS_centered or layer_out.IS_centered
        self.IS_std = layer_in.IS_std or layer_out.IS_std
        self.IS_whitened = layer_in.IS_whitened or layer_out.IS_whitened
        
        if self.IS_whitened and self.IS_std:
            raise NotImplementedError
    
    #%%
    def norm_cost(self, L1=0.0, L2=0.0):
        
        cost = T.cast(0.0, dtype=FLOATX)
        L1 = T.cast(L1, dtype=FLOATX)
        L2 = T.cast(0.5*L2, dtype=FLOATX)
        
        if self.IS_trainable:
            cost += L1*T.sum(T.abs_(self.W)) + L2*T.sum(T.pow(self.W, 2))

        return cost

    #%%
    def norm_grad(self, L1=0.0, L2=0.0):
        
        g = T.cast(0.0, dtype=FLOATX)
        L1 = T.cast(L1, dtype=FLOATX)
        L2 = T.cast(L2, dtype=FLOATX)
        
        if self.IS_trainable: 
            g += L1*T.sum(T.sgn(self.W)) + L2*T.sum(self.W)

        return g

    #%%
    def grad(self, prob_data, prob_model, L1=0.0, L2=0.0):
               
        g = None
        if self.IS_trainable:
            
            pd0, pd1 = prob_data
            pm0, pm1 = prob_model
            
            n = T.cast(prob_data[0].shape[0], dtype=FLOATX)
            
            g = -1.0/n*(T.dot(pd0.T, pd1) - T.dot(pm0.T, pm1))
            
            g += self.norm_grad(L1, L2)
            
        return g
        
    #%%
    def gauged_W(self, W=None, new=False, change=False):
        
        if W is None:
            W = self.W
        
        if self.IS_std:
            W = self.layer_out.apply_std(W, new=new, change=change, transposed=False)
            W = self.layer_in.apply_std(W, new=new, change=change, transposed=True)
        
        if self.IS_whitened:
            W = self.layer_out.apply_white(W, new=new, change=change, transposed=True)
            W = self.layer_in.apply_white(W.T, new=new, change=change, transposed=False)
            W = W.T
        
        return W
    
    #%%
    def update_weight_gauge(self):
    
        self.W_new = self.gauged_W(change=True)
        self.updates = OrderedDict({self.W : self.W_new})
        
    #%%
    def update_cov(self, center_data, lr):
        if self.IS_whitened:
            cd0, cd1 = center_data
            n = T.cast(cd0.shape[0], dtype=FLOATX)
            cov_up = T.dot(cd0.T, cd1)/(n-1)
            self.cov_new = (1-lr)*self.cov + lr*cov_up
            self.updates = OrderedDict({self.cov : self.cov_new})
        
#%%
#%%
class Layer(NeuralNetParts):
    def __init__(self,
                 name,
                 output_dim,
                 input_dict={},
                 transposed_dict={},
                 IS_persist=False,
                 IS_centered=False,
                 IS_std=False,
                 IS_whitened=False,
                 std_eps=None,
                 white_type=None,
                 white_eps=None,
                 dropout_p=0.0,
                 batch_size=None,
                 theano_rng=None,
                 init_b=None,
                 init_persist=None,
                 init_c=None,
                 init_std=None,
                 init_wh=None,
                 data_mean=None,
                 data_std=None,
                 data_cov=None,
                 activation=T.nnet.sigmoid):
        
        super(Layer, self).__init__(name)
        
        self.my_index = int(self.name[1:])
        self.output_dim = output_dim
        
        self.IS_persist = IS_persist
        self.IS_centered = IS_centered
        self.IS_std = IS_std
        self.IS_whitened = IS_whitened
        self.std_eps = std_eps
        self.white_type = white_type
        self.white_eps = white_eps
        self.IS_datanorm = IS_centered or IS_std or IS_whitened
       
        if IS_whitened or IS_std:
            assert IS_centered
        if IS_whitened and IS_std:
            raise NotImplementedError
        if IS_std:
            assert std_eps is not None
        if IS_whitened:
            assert white_type is not None
            assert white_eps is not None
        
        self.batch_size = batch_size
        self.p = dropout_p
        self.IS_dropout = 1.0*(self.p > 0.0)
        if (0. < self.p < 1.) or self.IS_persist:
            assert self.batch_size is not None
        
        if theano_rng is None:
            theano_rng = make_theano_rng()
        self.theano_rng = theano_rng
        
        # creates a sorted input_dict
        self.input_dict = OrderedDict()
        if len(input_dict) > 0:
            for k in np.sort(input_dict.keys()):
                self.input_dict[k] = input_dict[k]
            
        # creates a sorted transposed_dict
        self.transposed_dict = OrderedDict()
        if len(transposed_dict) > 0:
            for k in np.sort(transposed_dict.keys()):
                self.transposed_dict[k] = transposed_dict[k]
            
        # assign layer properties
        len_in = len(self.input_dict)
        len_tran = len(self.transposed_dict)
        if len_in > 0 and len_tran > 0:
            self.layer_type = 'middle'
            self.direction = 'both'
            # need to compensate for reduced inputs when only going single direction
            self.z_up_adj = 1.0*(len_in+len_tran)/len_in
            self.z_down_adj = 1.0*(len_in+len_tran)/len_tran
        elif len_in > 0 and len_tran == 0:
            self.layer_type = 'output'
            self.direction = 'up'
        elif len_in == 0 and len_tran > 0:
            self.layer_type = 'input'
            self.direction = 'down'
        else:
            raise NotImplementedError
            
        self.activation = activation
            
        self.transposed_index = []
        self.W_T_ls = []
        for key, W in six.iteritems(self.transposed_dict):
            self.W_T_ls.append(W.T)
            self.transposed_index.append(key)
        
        self.input_index = []
        self.W_ls = []
        for key, W in six.iteritems(self.input_dict):
            self.W_ls.append(W)
            self.input_index.append(key)
        
        # create bias
        if init_b is None:
            # create shared variable for visible units bias
            if (self.layer_type == 'input') and (data_mean is not None):
                # tip from ref [2]
                p = np.clip(data_mean, EPS, 1-EPS)
                init_b = np.log(p/(1-p))
            else:            
                init_b = np.zeros(output_dim, dtype=FLOATX)
                
        self.b = make_shared(init_b, name=self.name + '_b')
        self._trainable_weights.append(self.b)
       
        if self.IS_centered:
            if init_c is None:
                if (self.layer_type == 'input') and (data_mean is not None):
                    init_c = np.clip(data_mean, EPS, 1-EPS)
                else:            
                    # assuming these are Bernoulli variables
                    init_c = 0.5*np.ones(output_dim)
                        
            self.c = make_shared(init_c, name=self.name + '_center')
            self._non_trainable_weights.append(self.c)
            
        if self.IS_std:
            if init_std is None:
                if (self.layer_type == 'input') and (data_std is not None):
                    init_std = data_std+100*EPS
                else:           
                    # assuming these are Bernoulli variables
                    #init_std = 0.5*np.ones(output_dim)
                    init_std = np.ones(output_dim)
                        
            self.std = make_shared(init_std, name=self.name + '_std')
            self._non_trainable_weights.append(self.std)
            
        if self.IS_whitened:
            if init_wh is None:
                #if False:
                if (self.layer_type == 'input') and (data_cov is not None):
                    init_wh = self.update_white(cov=data_cov,
                                                return_only=True)
                else:
                    # TODO: init hack?
                    # maybe just init_wh as identity, something on diag of data_cov?
                    init_wh = np.identity(output_dim)
                    data_cov = np.zeros((output_dim, output_dim))
                    #data_cov = 1e-4*np.random.normal(size=(output_dim, output_dim))
                    #np.fill_diagonal(data_cov, 0.025)
                    #data_cov = 0.025*np.identity(output_dim)
                    #init_wh = self.update_white(cov=data_cov,
                    #                            return_only=True)
                        
            self.white = make_shared(init_wh, name=self.name + '_white')
            self._non_trainable_weights.append(self.white)
            
            self.cov = make_shared(data_cov, name=self.name + '_cov')
            self._non_trainable_weights.append(self.cov)
            
        # persistant state of layer
        if self.IS_persist:
            if init_persist is None:
                if (self.layer_type == 'input') and (data_mean is not None):
                    # clipping data_mean to let each neuron have some on variation
                    eps = 0.05
                    dm = np.clip(data_mean, eps, 1-eps)
                    dm *= np.mean(dm)/np.mean(data_mean) # keep same mean prob
                    init_persist = np.repeat(dm.reshape((1, -1)), batch_size, 0)
                else:            
                    init_persist = 0.5*np.ones((batch_size, output_dim))    
            
            self._persist_chain = make_shared(init_persist,
                                              name=self.name+'_persist_chain')
            self._non_trainable_weights.append(self._persist_chain)
        
        if self.IS_dropout:
            
            p = ((1-self.p)*np.ones((self.batch_size, self.output_dim)))
            self._p_matrix = p.astype(FLOATX)
            
            sample = binomial_sample(self.theano_rng, p).eval()
            self._dropout = make_shared(sample, name=self.name + '_dropout')
            self._non_trainable_weights.append(self._dropout)        


    def init_layers(self, layer_in_ls, layer_out_ls):
        """ This allows layer to know who is connected to it """
        self.layer_in_ls = layer_in_ls
        self.layer_out_ls = layer_out_ls
        assert len(self.layer_in_ls) == len(self.W_ls)
        assert len(self.layer_out_ls) == len(self.W_T_ls)

    #%%
    @property
    def persist_chain(self):
        if self.IS_persist:
            return self._persist_chain
        else:
            return None
 
    
    @persist_chain.setter
    def persist_chain(self, persist_chain):
        if self.IS_persist:
            updates = OrderedDict({self._persist_chain : persist_chain})
            self.updates = updates
    
    #%%        
    def update_dropout(self):
        
        if 0. < self.p < 1:
            s = binomial_sample(self.theano_rng, self._p_matrix)
            updates = OrderedDict({self._dropout : s})
            self.updates = updates
        
    def update_center(self, prob_data, lr, IS_full_input=True):
        if self.IS_centered:
            if IS_full_input:
                pd = prob_data[self.my_index]
            else:
                pd = prob_data
                
            self.c_new = (1-lr)*self.c + lr*T.mean(pd, axis=0)
            self.updates = OrderedDict({self.c : self.c_new})
    
    def update_std(self, std_data, lr, IS_full_input=True):
        if self.IS_std:
            if IS_full_input:
                sd = std_data[self.my_index]
            else:
                sd = std_data
            
            n = self.batch_size
            self.std_new = (1-lr)*self.std + lr*T.sqrt(T.sum(sd**2, axis=0)/(n-1))
            self.updates = OrderedDict({self.std : self.std_new})
    
    def update_cov(self, center_data, lr, IS_full_input=True):
        if self.IS_whitened:
            if IS_full_input:
                cd = center_data[self.my_index]
            else:
                cd = center_data
            
            n = T.cast(self.batch_size, FLOATX)
            cov_up = T.dot(cd.T, cd)/(n-1)
            self.cov_new = (1-lr)*self.cov + lr*cov_up
            self.updates = OrderedDict({self.cov : self.cov_new})
    
    def update_white(self, cov=None, return_only=False):
        
        if self.IS_whitened:
            if cov is None:
                cov = self.cov.get_value()
                
            if self.white_type == 'PCA':
                white_new = my_data.make_PCA_matrix(cov, self.white_eps)
            elif self.white_type == 'ZCA':
                white_new = my_data.make_ZCA_matrix(cov, self.white_eps)
            else:
                raise NotImplementedError
                
            if return_only:
                return white_new
            else:
                self.white_new = white_new
                self.white.set_value(self.white_new)
    
    def update_bias_gauge(self):
        
        self.b_new = self.gauged_b(new=False, change=True)
        self.updates = OrderedDict({self.b : self.b_new})
    
    #%%
    def apply_dropout(self, x, IS_dropout):
        if (0. < self.p < 1.) and x is not None:
            x = ifelse(T.eq(IS_dropout, 0),
                       x,
                       x*self._dropout/T.cast(1.0-self.p, dtype=FLOATX)
                       )
        return x
 
    def apply_center(self, x, new=False):
        if self.IS_centered and x is not None:
            if new and hasattr(self, 'c_new'):
                c = self.c_new
            else:
                c = self.c
            x -= c   
        return x
    
    def apply_std(self, x, transposed=False, new=False, change=False):
        if self.IS_std and x is not None:
            
            std_eps = T.cast(self.std_eps, dtype=FLOATX) 
            div = T.cast(1.0 + 2*std_eps, dtype=FLOATX)
            #div = T.cast(0.5+std_eps, dtype=FLOATX)
            
            if change:
                if hasattr(self, 'std_new'):
                    norm = (self.std_new + std_eps)/(self.std + std_eps)
                else:
                    return x
                
            elif new and hasattr(self, 'std_new'):
                norm = div/(self.std_new + std_eps)

            else:
                norm = div/(self.std + std_eps)
                            
            if x.ndim > 1:
                if transposed:
                    norm = T.repeat(norm.reshape((-1, 1)), x.shape[1], 1)
                else:
                    norm = T.repeat(norm.reshape((1, -1)), x.shape[0], 0)
            x *= norm
            
        return x
            
    def apply_white(self, x, transposed=False, new=False, change=False):
        
        if self.IS_whitened and (x is not None):
            
            assert not (new and change)
            
            if change:
                if not hasattr(self, 'white_new'):
                    # assuming this means no change
                    return x
                else:
                    raise NotImplementedError
                    #W_old = self.white
                    #W_new = self.white_new
                    #W = W_old.T.dot(W_new)
            elif new and hasattr(self, 'white_new'):
                W = self.white_new
            else:
                W = self.white

            if transposed:
                W = W.T
            
            x = T.dot(x, W)
            
        return x
    
    def apply_datanorm(self, x, transposed=False, new=False):
        # TODO: update this when have both white and std
        center_data = self.apply_center(x, new)
        if self.IS_whitened:
            output = self.apply_white(center_data, transposed, new)
        else:
            output = self.apply_std(center_data, transposed, new)
            
        return output
    
    #%%
    def grad_b(self, prob_data, prob_model):
        
        pd = prob_data[self.my_index]
        pm = prob_model[self.my_index]
        
        g = -(T.mean(pd, axis=0) - T.mean(pm, axis=0))

        return g
    
    def gauged_b(self, b=None, new=False, change=False):
        
        if b is None:
            b = self.b
        
        if self.IS_std:
            b = self.apply_std(b, new=new, change=change, transposed=False)
        elif self.IS_whitened:
            b = self.apply_white(b, new=new, change=change, transposed=True)

        if self.IS_centered:
            W_ls = self.W_ls + self.W_T_ls
            layer_ls = self.layer_in_ls + self.layer_out_ls
            for W, layer in zip(W_ls, layer_ls):
    
                if change:
                    c = layer.c_new - layer.c
                else:
                    c = layer.c
                
                if not (self.IS_std or self.IS_whitened):
                    b -= T.dot(c, W)
               
                elif self.IS_std:
                    W_std = self.apply_std(W, change=change, transposed=False)
                    W_std = layer.apply_std(W_std, transposed=True)             
                    b -= T.dot(c, W_std)
                    
                elif self.IS_whitened:
                    W_wh = self.apply_white(W, change=change, transposed=True)
                    W_wh = layer.apply_white(W_wh.T, transposed=True)             
                    b -= T.dot(W_wh, c)
            
        return b
    
    #%%
    def needed_inputs(self, mean_field=True, direction=None):
        
        index = []
        if direction is None:
            direction = self.direction
        
        if direction in ['both', 'up']:
            index += self.input_index
        if not mean_field:
            index += [self.my_index]
        if direction in ['both', 'down']:
            index += self.transposed_index
            
        return index
    
    #%%
    def prep_input(self, full_input_ls, mean_field=True, direction=None):
        
        if direction is None:
            direction = self.direction
            
        index = self.needed_inputs(mean_field, direction)
        
        input_ls = [full_input_ls[i] for i in index]
        
        return input_ls

    #%%
    def get_input(self, input_ls, mean_field=True,
                  direction=None, full_input=True, gauged=True):
        
        if direction is None:
            direction = self.direction
        assert direction in ['both', 'up', 'down']
        
        if full_input:
            input_ls = self.prep_input(input_ls, mean_field, direction)
        
        # Controls which interactions to include    
        IS_up, IS_down = True, True
        if direction == 'up' or self.direction == 'up':
            IS_down = False            
        elif direction == 'down' or self.direction == 'down':
            IS_up = False
        
        start, stop = 0, 0
        current_W_ls, current_input = [], []
        
        if IS_up:
            stop = len(self.W_ls)
            current_input += input_ls[start:stop]
            current_W_ls += self.W_ls
        if not mean_field:
            input_self = input_ls[stop]
            stop += 1
        if IS_down:
            current_input += input_ls[stop:]
            current_W_ls += self.W_T_ls
              
        # Calculate activation input
        z = self.b
        for data, W in zip(current_input, current_W_ls):
            z += T.dot(data, W)
            if not mean_field:
                if self.IS_datanorm:
                    raise NotImplementedError
                z += -0.5*T.dot(data-data**2, W**2)*(input_self-0.5)
                
        # need to compensate for reduced input        
        if (self.layer_type == 'middle') and (direction == 'up'):
            z *= self.z_up_adj  
        elif (self.layer_type == 'middle') and (direction == 'down'):
            z *= self.z_down_adj
            
        if self.IS_std and gauged:
            z = self.apply_std(z)
            
        if self.IS_whitened and gauged:
            z = self.apply_white(z, transposed=True)
            
        return z

    #%%
    def get_output(self, input_ls, beta=1.0, mean_field=True, direction=None,
                   full_input=True, gauged=True):
        
        beta = T.cast(beta, FLOATX)
        z = self.get_input(input_ls, mean_field, direction, full_input, gauged)
        prob = self.activation(beta*z)   
        
        return prob
  