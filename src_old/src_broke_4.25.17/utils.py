"""
Miscellaneous functions for neural networks

Original Code Source: 
    [1] http://deeplearning.net/tutorial/code/utils.py
Modified by: Alex Lang (alexhunterlang@gmail.com)
License: BSD-3
"""

#%%
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *

import numpy as np
import os
import six
import h5py

import theano
from theano.sandbox.rng_mrg import MRG_RandomStreams as RandomStreams

FLOATX = theano.config.floatX # should be float32 for GPU
EPS = np.finfo(FLOATX).eps

#%%
def load_weights_hd5f(filename):
    """ Loads the weights that are saved in hd5f format and returns a dictionary"""
    
    with h5py.File(filename, 'r') as f:
        keys_ls = f.keys()
        weights = {}
        for ll in keys_ls:
            for k in f[ll].keys():
                if len(f[ll][k]) > 0:
                    weights[k] = f[ll][k][:]
    
    return weights 

#%%
def standard_folder(folder):
    """ Just ensures that all folders end with a /"""
    if not folder.endswith('/'):
        folder += '/'
    return folder

#%%
def prep_topology(topology_dict):
    
    # First check that parity is consistent with current Gibbs sampler
    parity_violation = 0
    for key, value in six.iteritems(topology_dict):
        parity = key % 2
        IS_diff = np.array([parity != (v % 2) for v in value])
        if not np.all(IS_diff):
            parity_violation += 1
    
    if parity_violation:
        raise ValueError('Current DBM Gibbs updates assumes odd / even states are'+
                         ' conditionally independent.')
    
    # This finds the inputs to each layer
    topology_input_dict = {}
    for  key, value in six.iteritems(topology_dict):
        for i in list(value):
            if i not in topology_input_dict.keys():
                topology_input_dict[i] = [key]
            else:
                topology_input_dict[i] += [key]
    # convert to a set to have same format as topology_dict
    for key, value in six.iteritems(topology_input_dict):
        topology_input_dict[key] = set(value)
    
    pairs = []
    for k, v in topology_dict.items():
        for i in list(v):
            pairs.append((k, i))
    
    return pairs, topology_input_dict

#%%
def make_shared(value, name=None):
    
    if not isinstance(value, np.ndarray):
        value = np.asarray(value)
    
    value = value.astype(FLOATX)
    return theano.shared(value=value, name=name, borrow=True)

#%%
def make_np_rng(seed=None):
    if seed is None:
        seed = 1234
    return np.random.RandomState(seed)

#%%
def make_theano_rng(np_rng=None, seed=None):
    
    if np_rng is None:
        np_rng = make_np_rng(seed)
    
    return RandomStreams(np_rng.randint(2 ** 30))

#%%
def binomial_sample(theano_rng, prob):
    """Binomial sample in theano """  
    return theano_rng.binomial(size=prob.shape, n=1, p=prob, dtype=FLOATX)

#%%
def save_dict(filename, dd):
    """ Given dictionary, saves file with keys and values """
    with open(filename, 'w') as f:
        for key in dd.keys():
            f.write(unicode(key) + u' : ' + unicode(dd[key])+'\n')

#%% 
def find_display_dim(dim):
    """ Finds dimensions for tiling images """
    x = np.floor(np.sqrt(dim))
    IS_bad = True
    while IS_bad:
        assert x > 0
        y = dim/x
        if np.remainder(dim, x) == 0:
            IS_bad = False
        else:
            x = x-1
            
    return (int(x), int(y))

#%% 
def safe_make_folders(save_folder):
    """ Safely creates folder without overwriting data """

    # If folder doesn't exist, make it
    if not os.path.isdir(save_folder):
        os.makedirs(save_folder)
    else:
        # If folder does exist, check if has other folders
        # other files are okay, for example > out.txt
        dir_list = [dd for dd in os.listdir(save_folder) 
                    if os.path.isdir(os.path.join(save_folder, dd))]
        
        if len(dir_list) > 0:
            # Would rather have program terminate than overwrite data
            os.makedirs(save_folder)

#%%
def scale_to_unit_interval(ndar, eps=EPS):
    """ 
    Scales all values in the ndarray ndar to be between 0 and 1 
    Copied from http://deeplearning.net/tutorial/code/utils.py     
    """
    ndar = ndar.copy()
    ndar -= ndar.min()
    ndar *= 1.0 / (ndar.max() + eps)
    return ndar
               
#%%
def tile_raster_images(X, img_shape, tile_shape, tile_spacing=(0, 0),
                       scale_rows_to_unit_interval=True,
                       output_pixel_vals=True):
    """
    Copied from http://deeplearning.net/tutorial/code/utils.py    
    
    Transform an array with one flattened image per row, into an array in
    which images are reshaped and layed out like tiles on a floor.

    This function is useful for visualizing datasets whose rows are images,
    and also columns of matrices for transforming those rows
    (such as the first layer of a neural net).

    :type X: a 2-D ndarray or a tuple of 4 channels, elements of which can
    be 2-D ndarrays or None;
    :param X: a 2-D array in which every row is a flattened image.

    :type img_shape: tuple; (height, width)
    :param img_shape: the original shape of each image

    :type tile_shape: tuple; (rows, cols)
    :param tile_shape: the number of images to tile (rows, cols)

    :param output_pixel_vals: if output should be pixel values (i.e. int8
    values) or floats

    :param scale_rows_to_unit_interval: if the values need to be scaled before
    being plotted to [0,1] or not


    :returns: array suitable for viewing as an image.
    (See:`Image.fromarray`.)
    :rtype: a 2-d array with same dtype as X.

    """

    assert len(img_shape) == 2
    assert len(tile_shape) == 2
    assert len(tile_spacing) == 2

    out_shape = [(ishp + tsp) * tshp - tsp
                 for ishp, tshp, tsp in zip(img_shape, tile_shape, tile_spacing)]

    if isinstance(X, tuple):
        assert len(X) == 4
        # Create an output np ndarray to store the image
        if output_pixel_vals:
            out_array = np.zeros((out_shape[0], out_shape[1], 4), dtype='uint8')
        else:
            out_array = np.zeros((out_shape[0], out_shape[1], 4), dtype=X.dtype)

        #colors default to 0, alpha defaults to 1 (opaque)
        if output_pixel_vals:
            channel_defaults = [0, 0, 0, 255]
        else:
            channel_defaults = [0., 0., 0., 1.]

        for i in range(4):
            if X[i] is None:
                # if channel is None, fill it with zeros of the correct
                # dtype
                dt = out_array.dtype
                if output_pixel_vals:
                    dt = 'uint8'
                out_array[:, :, i] = np.zeros(
                    out_shape,
                    dtype=dt
                ) + channel_defaults[i]
            else:
                # use a recurrent call to compute the channel and store it
                # in the output
                out_array[:, :, i] = tile_raster_images(
                    X[i], img_shape, tile_shape, tile_spacing,
                    scale_rows_to_unit_interval, output_pixel_vals)
        return out_array

    else:
        # if we are dealing with only one channel
        H, W = img_shape
        Hs, Ws = tile_spacing

        # generate a matrix to store the output
        dt = X.dtype
        if output_pixel_vals:
            dt = 'uint8'
        out_array = np.zeros(out_shape, dtype=dt)

        for tile_row in range(tile_shape[0]):
            for tile_col in range(tile_shape[1]):
                if tile_row * tile_shape[1] + tile_col < X.shape[0]:
                    this_x = X[tile_row * tile_shape[1] + tile_col]
                    this_img = this_x.reshape(img_shape)
                    if scale_rows_to_unit_interval:
                        # if we should scale values to be between 0 and 1
                        # do this by calling the `scale_to_unit_interval`
                        # function
                        this_img = scale_to_unit_interval(this_img)
                    
                    # add the slice to the corresponding position in the
                    # output array
                    c = 1
                    if output_pixel_vals:
                        c = 255
                        
                    row_start = tile_row * (H + Hs)
                    row_end = row_start + H
                    col_start = tile_col * (W + Ws)
                    col_end = col_start + W
                    out_array[row_start : row_end,
                              col_start : col_end] = this_img * c
                              
        return out_array
    