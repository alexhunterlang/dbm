"""
Deep Boltzmann Machines in Theano

Original Code Source: 
    [1] http://deeplearning.net/tutorial/rbm.html
    [2] https://github.com/lisa-lab/pylearn2/blob/master/pylearn2/rbm_tools.py
Modified by: Alex Lang (alexhunterlang@gmail.com)
License: BSD-3

The main heart of the code is from [1], while the AIS code is modified from [2]

Useful References:
[1] http://deeplearning.net/tutorial/rbm.html
        - see notes about scan and theano optimization for justification of
            returning pre-sigmoid activation
[2] Hinton Guide to RBM - https://www.cs.toronto.edu/~hinton/absps/guideTR.pdf
[3] Initialization - http://arxiv.org/abs/1312.6120
[4] Nesterov momentum - https://github.com/lisa-lab/pylearn2/issues/677
[5] Neal (2001) Annealed importance sampling
    https://link.springer.com/article/10.1023/A:1008923215028
[6] Salakhutdinov, Murray (2008) On the quantitative analysis of DBNs
    http://www.cs.toronto.edu/~rsalakhu/papers/dbn_ais.pdf
[7] http://www.cs.toronto.edu/~rsalakhu/rbm_ais.html
[8] Deep Boltzmann machines http://machinelearning.wustl.edu/
                            mlpapers/paper_files/AISTATS09_SalakhutdinovH.pdf
                            
"""

#%% 
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *

import numpy as np
import os
from collections import OrderedDict
import h5py
import copy

try:
    import PIL.Image as Image
except ImportError:
    import Image
    
import theano
import theano.tensor as T

# The custom imports should be in same folder as this script
import __main__ as main
if not hasattr(main, '__file__'):
    # This means its an iPython instance
    import getpass
    usr = getpass.getuser()
    if usr == 'alexlangMacBookPro':
        path_to_script_folder = '/Users/alexlangMacBookPro/Dropbox/BitBucket/dbm/src/'
        os.chdir(path_to_script_folder)

# my files
from components import Synapse, Layer
import utils
from utils import binomial_sample, make_theano_rng, prep_topology, FLOATX, EPS

#%%
#%%
class DBM(object):
    """ Deep boltzmann machine """               
    def __init__(self,
                 layer_size_list,
                 topology_dict,
                 residual_dict=None,
                 IS_persist=False,
                 IS_centered=False,
                 std_eps=None,
                 white_type=None,
                 white_eps=None,
                 dropout_prob_list=None,
                 batch_size=None,
                 weights_dict=None,
                 theano_rng=None,
                 init_data=None,
                 init_weights='orthogonal'):
        
        self.layers = []
        self.synapses = []
        self._updates = OrderedDict()
        self.layer_indices = {}
        
        self.layer_size_list = layer_size_list
        self.topology_dict = topology_dict
        if residual_dict is None:
            residual_dict = {}
        self.residual_dict = residual_dict
        
        self.IS_persist = IS_persist
        self.IS_centered = IS_centered
        self.IS_std = (std_eps is not None)
        self.IS_whitened = (white_type is not None) and (white_eps is not None)
        self.std_eps = std_eps
        self.white_type = white_type
        self.white_eps = white_eps
        self.IS_datanorm = self.IS_centered or self.IS_std or self.IS_whitened
    
        if dropout_prob_list is None:
            dropout_prob_list = [0.0]*len(layer_size_list)
        self.dropout_prob_list = dropout_prob_list
        self.IS_dropout = 1.0*(np.sum(self.dropout_prob_list) > 0)
        self.batch_size = batch_size
        
        # Some sanity checks
        for k, v in self.residual_dict.items():
            for vv in list(v):
                assert vv in list(topology_dict[k]), 'residual_dict must be a subset of topology_dict'
        assert len(self.layer_size_list) == len(self.dropout_prob_list)                         
        if self.IS_datanorm and weights_dict is None:
            assert init_data is not None

        data = {}
        if init_data is not None:
            d = copy.deepcopy(init_data)
            data['mean'] = np.clip(np.mean(d, axis=0), EPS, 1-EPS)  
            d -= np.repeat(data['mean'].reshape((1, -1)), d.shape[0], 0)
            data['std'] = np.maximum(np.std(d, axis=0), 100*EPS)
            if self.IS_std:
                div = 1.0 + 2*std_eps
                norm = div/(data['std'] + std_eps)
                norm = np.repeat(norm.reshape((1, -1)), d.shape[0], 0)
                d *= norm
            if self.IS_whitened:
                data['cov'] = np.cov(d.T)

        if theano_rng is None:
            theano_rng = make_theano_rng()
        self.theano_rng = theano_rng
        
        pairs, topology_input_dict = prep_topology(self.topology_dict)
        self.synapse_pairs = pairs
        self.topology_input_dict = topology_input_dict
        
        pairs, residual_input_dict = prep_topology(self.residual_dict)
        self.residual_pairs = pairs
        self.residual_input_dict = residual_input_dict
        
        # first prepare all the weights connecting layers
        self.synapses = []
        for sp in self.synapse_pairs:
            name = 'h{}-{}_W'.format(sp[0], sp[1]) 
            
            if weights_dict is not None:
                init_W = weights_dict[name]
            else:
                init_W = None
            
            if sp in self.residual_pairs:
                init = 'identity'
                IS_trainable = False
            else:
                init = init_weights
                IS_trainable = True    
            
            shape = [self.layer_size_list[s] for s in sp]
            synapse = Synapse(shape,
                              name,
                              init_W,
                              init=init,
                              IS_trainable=IS_trainable)
            self.synapses.append(synapse)
            
        # now make the layers and feed in the given weights
        for index, n_dim in enumerate(self.layer_size_list):
            name = 'h'+str(index)
            if weights_dict is not None:
                init_layer = {}
                keys = [k for k in weights_dict.keys() if k.startswith(name+'_')]                
                init_layer = [weights_dict[k] for k in keys]
            else:
                init_layer = None
            
            if index == 0:
                data_layer = data
            else:
                data_layer = None
                
            input_dict = {}
            output_dict = {}
            for j, sp in enumerate(self.synapse_pairs):
                if index == sp[0]:
                    output_dict[sp[1]] = self.synapses[j].W
                elif index == sp[1]:
                    input_dict[sp[0]] = self.synapses[j].W
                
            layer = Layer(name,
                          index,
                          n_dim,
                          input_dict=input_dict,
                          output_dict=output_dict,
                          IS_persist=self.IS_persist,
                          IS_centered=self.IS_centered,
                          std_eps=self.std_eps,
                          white_type=self.white_type,
                          white_eps=self.white_eps,
                          dropout_p=self.dropout_prob_list[index],
                          batch_size=self.batch_size,
                          theano_rng=self.theano_rng,
                          init=init_layer,
                          data=data_layer)

            self.layers.append(layer)
            self.layer_indices[index] = layer
                      
                                          
        # simplify the sharing of information
        # this lets synapses know connecting layers
        for synapse, sp in zip(self.synapses, self.synapse_pairs):
            layer_in = self.layers[sp[0]]
            layer_out = self.layers[sp[1]]
            synapse.init_layers(layer_in, layer_out)
        # layers know who is prev/next
        for layer in self.layers:
            input_index = layer.input_index
            output_index = layer.output_index

            layers_in = [lay for i, lay in enumerate(self.layers)
                            if i in input_index]
            layers_out = [lay for i, lay in enumerate(self.layers)
                            if i in output_index]

            layer.init_layers(layers_in, layers_out)
            
            
        # better initialize of std and white
        if (weights_dict is None) and (init_data is not None) and (self.IS_std or self.IS_whitened):
            x = T.matrix('x')
            IS_dropout = T.scalar('IS_scalar')
            beta = T.scalar('beta')

            if self.IS_std:   
                prob_data, _, updates = self.pos_stats(x, IS_dropout, beta)
                fn = theano.function([x, IS_dropout, beta], prob_data,
                                     updates=updates,
                                     on_unused_input='ignore')
                pd = fn(init_data, 0, 1.0)
                for i, p in enumerate(pd):
                    if i > 0:
                        layer = self.layers[i]
                        s = np.maximum(np.std(p, axis=0), 100*EPS).astype(FLOATX)
                        layer.std.set_value(s)
                        
            if self.IS_whitened:
                
                prob_data, _, updates = self.pos_stats(x, IS_dropout, beta)
                center_data = self.apply_center(prob_data)
                    
                std_data = self.apply_std(center_data)
                
                fn = theano.function([x, IS_dropout, beta], std_data,
                                     updates=updates,
                                     on_unused_input='ignore')
                wd = fn(init_data, 0, 1.0)

                for i, prob in enumerate(wd):
                    if i > 0:
                        layer = self.layers[i]
                        data_cov = np.cov(prob.T).astype(FLOATX)
                        white, white_inv = layer.update_white(cov=data_cov,
                                                              eps=10*self.white_eps)
                
                        layer.cov.set_value(data_cov)
                        layer.white.set_value(white)
                        layer.white_inv.set_value(white_inv)
                    

    @classmethod
    def init_file(cls, param_npy, weight_file):

        kwargs = {}
        
        param = np.load(param_npy).item()
        
        keys = ['layer_size_list',
                'topology_dict',
                'residual_dict',
                'IS_persist',
                'IS_centered',
                'std_eps',
                'white_type',
                'white_eps',
                'dropout_prob_list',
                'batch_size']

        for k in keys:
            kwargs[k] = param[k]
        
        kwargs['init_data']= None
        kwargs['theano_rng'] = None
        kwargs['init_weights'] = None
        kwargs['weights_dict'] = utils.load_weights_hd5f(weight_file)
        
        return cls(**kwargs)
    
    @classmethod
    def init_non_gauged(cls, dbm):
        # this takes an existing dbm and creates a non gauged dbm
        
        # collect all the weights
        weights_dict = {}
        part_ls = []
        name_ls = []
        for layer in dbm.layers:
            part_ls.append(layer.gauged_b('AIS'))
            name_ls.append(layer.b.name)
        for synapse in dbm.synapses:
            part_ls.append(synapse.gauged_W('AIS'))
            name_ls.append(synapse.W.name)

        for p, n in zip(part_ls, name_ls):
            if isinstance(p, T.TensorVariable):
                p = p.eval()
            else:
                p = p.get_value()
            weights_dict[n] = p
        
        kwargs = {
                 'layer_size_list' : dbm.layer_size_list,
                 'topology_dict' : dbm.topology_dict,
                 'residual_dict' : dbm.residual_dict,
                 'IS_persist' : False,
                 'IS_centered' : False,
                 'std_eps' : None,
                 'white_type' : None,
                 'white_eps' : None,
                 'weights_dict' : weights_dict,
                 'theano_rng' : dbm.theano_rng
                 }
    
        return cls(**kwargs)

    #%%
    @property
    def parts(self):
        return self.layers + self.synapses
    
    @property
    def updates(self):
        updates = self._updates
        parts_ls = self.layers + self.synapses
        
        for part in parts_ls:
            keys = updates.keys()
            for k, v in part.updates.items():
                if k in keys:
                    assert k.name.endswith(('_b', '_W')), 'Already have update for '+k.name
                    v_old = updates[k]
                    updates[k] = v + v_old
                else:
                    updates[k] = v
                       
        return updates
                
    @updates.setter
    def updates(self, updates):
        keys = self._updates.keys()
        for k, v in updates.items():
            if k in keys:
                assert k.name.endswith(('_b', '_W')), 'Already have update for '+k.name
                v_old = updates[k]
                self._updates[k] = v + v_old
            else:
                self._updates[k] = v
    
    @property
    def trainable_weights(self):
        weights = []
        for part in self.parts:
            weights += part.trainable_weights
        return weights

    @property
    def non_trainable_weights(self):
        weights = []
        for part in self.parts:
            weights += part.non_trainable_weights
        return weights

    def get_weights(self):
        """Returns the weights of the model,
        as a flat list of Numpy arrays.
        """
        weights = []
        for part in self.parts:
            weights += part.weights
        return [w.get_value() for w in weights]
 
    def save_weights(self, filepath, overwrite=True):
        """Dumps all layer weights to a HDF5 file.
        The weight file has:
            - `layer_names` (attribute), a list of strings
                (ordered names of model layers).
            - For every layer, a `group` named `layer.name`
                - For every such layer group, a group attribute `weight_names`,
                    a list of strings
                    (ordered names of weights tensor of the layer).
                - For every weight in the layer, a dataset
                    storing the weight value, named after the weight tensor.
        """
        
        # If file exists and should not be overwritten:
        if not overwrite and os.path.isfile(filepath):
            raise NotImplementedError
        f = h5py.File(filepath, 'w')
        self.save_weights_to_hdf5_group(f)
        f.flush()
        f.close()

    def save_weights_to_hdf5_group(self, f):
        
        parts = self.parts
        f.attrs['part_names'] = [p.name.encode('utf8') for p in parts]

        for part in parts:
            g = f.create_group(part.name)
            symbolic_weights = part.weights
            weight_values = [w.get_value() for w in symbolic_weights]
            weight_names = []
            for i, (w, val) in enumerate(zip(symbolic_weights, weight_values)):
                if hasattr(w, 'name') and w.name:
                    name = str(w.name)
                else:
                    name = 'param_' + str(i)
                weight_names.append(name.encode('utf8'))
            g.attrs['weight_names'] = weight_names
            for name, val in zip(weight_names, weight_values):
                param_dset = g.create_dataset(name, val.shape,
                                              dtype=val.dtype)
                if not val.shape:
                    # scalar
                    param_dset[()] = val
                else:
                    param_dset[:] = val

    #%%
    @property
    def persist_chain(self):
        if self.IS_persist:
            persist_chain = []
            for layer in self.layers:
                persist_chain.append(layer.persist_chain)
            return persist_chain
        else:
            return None
        
    
    @persist_chain.setter
    def persist_chain(self, persist_chain):
        if self.IS_persist:
            for p, layer in zip(persist_chain, self.layers):
                layer.persist_chain = p
        
    
    @property
    def center(self):
        if self.IS_centered:
            center_ls = []
            for layer in self.layers:
                center_ls.append(layer.c)
        else:
            center_ls = None
            
        return center_ls
        
    
    @property
    def std(self):
        if self.IS_std:
            std_ls = []
            for layer in self.layers:
                std_ls.append(layer.std)
        else:
            std_ls = None
            
        return std_ls
    
    
    @property
    def white(self):
        if self.IS_whitened:
            white_ls = []
            for layer in self.layers:
                white_ls.append(layer.white)
        else:
            white_ls = None
            
        return white_ls
    
    #%%
    def apply_center(self, prob_ls, new=False):
        
        if self.IS_centered:
            index = 0
            out_ls = len(prob_ls)*[None]
            for p, layer in zip(prob_ls, self.layers):
                out_ls[index] = layer.apply_center(p, new)
                index += 1
        else:
            out_ls = prob_ls
      
        return out_ls
            

    def apply_std(self, prob_ls, new=False, inv=False):
        
        if self.IS_std:
            index = 0
            out_ls = len(prob_ls)*[None]
            for p, layer in zip(prob_ls, self.layers):
                out_ls[index] = layer.apply_std(p, new=new, inv=inv)
                index += 1
        else:
            out_ls = prob_ls
      
        return out_ls
    
    
    def apply_white(self, prob_ls, tran=False, new=False, inv=False):
        
        if self.IS_whitened:
            index = 0
            out_ls = len(prob_ls)*[None]
            for p, layer in zip(prob_ls, self.layers):
                out_ls[index] = layer.apply_white(p, tran=tran, new=new, inv=inv)
                index += 1
        else:
            out_ls = prob_ls
      
        return out_ls
    
    
    def apply_datanorm(self, prob_ls, tran=False, new=False):
        
        if self.IS_datanorm:
            index = 0
            out_ls = len(prob_ls)*[None]
            for p, layer in zip(prob_ls, self.layers):
                out_ls[index] = layer.apply_datanorm(p, tran=tran, new=new)
                index += 1
        else:
            out_ls = prob_ls
      
        return out_ls
            
    
    def apply_gauge(self, params, mode):
        assert mode in ['center_std', 'white']
        
        # TODO: clearer comments of what is going on
        
        b_vel_ls = params[0:len(self.layers)]
        W_vel_ls = params[len(self.layers):]
        if not isinstance(b_vel_ls, list):
            b_vel_ls = [b_vel_ls]
        if not isinstance(W_vel_ls, list):
            W_vel_ls = [W_vel_ls]
        
        gauge_b_vel_ls = []
        gauge_W_vel_ls = []
        
        for b_vel, layer in zip(b_vel_ls, self.layers):
            gb = layer.gauged_b_velocity(b_vel, mode)
            gauge_b_vel_ls.append(gb)
                    
        delta_c_ls = [None]*len(b_vel_ls)
        if mode == 'center_std' and (self.IS_centered or self.IS_std):
            delta_c_ls = self.apply_center(self.center, new=True)
            delta_c_ls = [-dc for dc in delta_c_ls]
            
        b_vel, delta_c = None, None
        for i, pair in enumerate(self.synapse_pairs):
            synapse = self.synapses[i]
            W_vel = W_vel_ls[i]
            if mode == 'center_std' and (self.IS_centered or self.IS_std):
                b_vel = [b_vel_ls[p] for p in pair]
                delta_c = [delta_c_ls[p] for p in pair]
                
            gW = synapse.gauged_W_velocity(mode, W_vel, b_vel, delta_c)
            gauge_W_vel_ls.append(gW)
        
        gauged_params = gauge_b_vel_ls + gauge_W_vel_ls
        
        return gauged_params
 
    
    def apply_dropout(self, prob_ls, IS_dropout):
        
        if self.IS_dropout:
            # TODO: why did I do it this way???
            index = 0
            out_ls = len(prob_ls)*[None]
            for p, layer in zip(prob_ls, self.layers):
                out_ls[index] = layer.apply_dropout(p, IS_dropout)
                index += 1
        else:
            out_ls = prob_ls
      
        return out_ls
    
    #%%
    def norm_cost(self, L1, L2):
        ''' Returns weight costs '''

        cost = T.cast(0.0, dtype=FLOATX)
        for synapse in self.synapses:
            cost += synapse.norm_cost(L1, L2)
        
        return cost

    #%%
    def update_center(self, prob_ls, lr):
        if self.IS_centered:
            for layer in self.layers:
                layer.update_center(prob_ls, lr)

    
    def update_gauge(self, mode, updates=None):
        
        return_updates = isinstance(updates, OrderedDict)
        
        for layer in self.layers:
            if return_updates:
                updates = layer.update_bias_gauge(mode=mode, updates=updates)
            else:
                layer.update_bias_gauge(mode=mode)
                
        for synapse in self.synapses:
            if return_updates:
                updates = synapse.update_weight_gauge(mode=mode, updates=updates)
            else:
                synapse.update_weight_gauge(mode=mode)
            
        if return_updates:
            return updates

    
    def update_std(self, prob_ls, lr):
        if self.IS_std:
            for layer in self.layers:
                layer.update_std(prob_ls, lr)

    
    def update_white(self, updates=None):
        if self.IS_whitened:
            if not isinstance(updates, OrderedDict):
                updates = OrderedDict()
            for layer in self.layers:
                updates = layer.update_white(updates=updates)
            return updates

    
    def update_cov(self, prob_ls, lr):
        if self.IS_whitened:
            for layer in self.layers:
                layer.update_cov(prob_ls, lr)

    
    def update_dropout(self):
        if self.IS_dropout:
            for layer in self.layers:
                layer.update_dropout()
    
    #%%
    def propup(self, x, IS_dropout, beta):
        """ Pass data up through network"""
          
        prob_ls = [x]+[None]*(len(self.layers)-1)
        
        # TODO: why did I need index again?
        index = 1
        for layer in self.layers[1:]:
            current_prob_ls = self.apply_datanorm(prob_ls)
            current_prob_ls = self.apply_dropout(current_prob_ls, IS_dropout)
            prob_ls[index] = layer.get_output(current_prob_ls, direction='up', beta=beta)
            index += 1
        
        # TODO: why did I do this???
        if self.IS_centered or self.IS_std:
            prob_ls[0] = x
        
        return prob_ls    
 
    #%%
    def propdown(self, x, IS_dropout, beta):
        """ Pass data up through network"""
                
        prob_ls = [None]*(len(self.layers)-1)+[x]
        
        index = len(self.layers)-2
        for layer in self.layers[::-1][1:]:
            current_prob_ls = self.apply_datanorm(prob_ls) 
            current_prob_ls = self.apply_dropout(current_prob_ls, IS_dropout)
            prob_ls[index] = layer.get_output(current_prob_ls, direction='down', beta=beta)
            index -= 1
        
        if self.IS_centered or self.IS_std:
            prob_ls[-1] = x
        
        return prob_ls    
    
    #%%
    def free_energy(self, x, IS_dropout, beta):
        ''' Function to compute the free energy of a visible sample '''
        
        prob_ls = self.propup(x, IS_dropout, beta)

        return self.free_energy_given_h(prob_ls, beta)        

    #%%
    def free_energy_given_h(self, prob_ls, beta):
        """ Function for free energy given visible sample and 
        activations of hidden layers        
        """
        beta = T.cast(beta, FLOATX)
    
        if self.IS_datanorm:
            prob_ls = self.apply_datanorm(prob_ls, tran=False)
       
        z_odd_ls = []
        for layer in self.layers[1::2]:
            z = layer.get_input(prob_ls, direction='both', gauged=True)
            z_odd_ls.append(z)

        z = T.concatenate(z_odd_ls, axis=1)
        fe = -T.sum(T.log(1 + T.exp(beta*z)), axis=1)
        
        if self.IS_centered:
            c = self.center
            if self.IS_std:
                c = self.apply_std(c)
            if self.IS_whitened:
                c = self.apply_white(c)
        
        for p, layer in zip(prob_ls[0::2], self.layers[0::2]):
            if self.IS_centered:
                b = layer.get_input(c,
                                    direction='both',
                                    full_input=True,
                                    gauged=False)
            else:
                b = layer.b
                
            fe -= beta*T.dot(p, b)
        
        # NOTE: in centering, there is a constant the depends on odd bias and odd center
        # Ignoring since non-physical and does not affect gradient
        
        return fe
 
    #%%
    def parity_update(self, prob_ls, sample_ls, start, IS_dropout, beta,
                      IS_prob_input=True, hold_constant=[]):
        ''' Updates either even or odd layers'''
       
        index = list(range(start, len(prob_ls), 2))
        index = [i for i in index if i not in hold_constant]
        
        if len(index) == 0:
            # nothing to change, so just pass out the inputs
            prob_out = prob_ls
            sample_out = sample_ls
        else:
            
            prob_out = [None]*len(prob_ls)
            sample_out = [None]*len(sample_ls)
                 
            # fill in the samples if needed
            for i, s in enumerate(sample_ls):
                if s is None:
                    p = prob_ls[i]
                    sample_ls[i] = binomial_sample(self.theano_rng, p)
            
            # this allows implementation of Hintons suggestions of when
            # to use prob updates vs sample updates
            if IS_prob_input:
                input_ls = [pp for pp in prob_ls]
            else:
                input_ls = [s for s in sample_ls]
                
            if self.IS_datanorm:
                input_ls = self.apply_datanorm(input_ls, tran=False)
                
            if self.IS_dropout:
                input_ls = self.apply_dropout(input_ls, IS_dropout)
                
            for i in index:
                layer = self.layers[i]
                p = layer.get_output(input_ls, direction='both', beta=beta) 
                prob_out[i] = p                    
                sample_out[i] = binomial_sample(self.theano_rng, p)
                
            # pass out the unchanged prob / samples
            for i in range(len(prob_out)):
                if prob_out[i] is None:
                    prob_out[i] = prob_ls[i]
                if sample_out[i] is None:
                    sample_out[i] = sample_ls[i]
        
        return prob_out, sample_out
        
    #%%
    def gibbs_odd_even_odd(self, *args):
        ''' This function implements one step of Gibbs sampling,
            starting from the odd hidden states'''

        IS_dropout = args[-2]
        beta = args[-1]
        args = list(args[0:-2])
        mid = int(len(args)/2)
        prob_ls = args[0:mid]
        sample_ls = args[mid:]
 
        # Update even
        prob_ls, sample_ls = self.parity_update(prob_ls,
                                                sample_ls,
                                                0, 
                                                IS_prob_input=False,
                                                hold_constant=[],
                                                IS_dropout=IS_dropout,
                                                beta=beta)
        
        # update odd
        prob_ls, sample_ls = self.parity_update(prob_ls,
                                                sample_ls,
                                                1, 
                                                IS_prob_input=False,
                                                hold_constant=[],
                                                IS_dropout=IS_dropout,
                                                beta=beta)

        return prob_ls+sample_ls+[IS_dropout, beta]           
        

    #%%
    def gibbs_even_odd_even(self, *args):
        ''' This function implements one step of Gibbs sampling,
            starting from the visible state and even hidden states'''
               
        IS_dropout = args[-2]
        beta = args[-1]
        args = list(args[0:-2])
        mid = int(len(args)/2)
        prob_ls = args[0:mid]
        sample_ls = args[mid:]
 
        # Update odd
        prob_ls, sample_ls = self.parity_update(prob_ls,
                                                sample_ls,
                                                1, 
                                                IS_prob_input=False,
                                                hold_constant=[],
                                                IS_dropout=IS_dropout,
                                                beta=beta)
        
        # update even
        prob_ls, sample_ls = self.parity_update(prob_ls,
                                                sample_ls,
                                                0, 
                                                IS_prob_input=False,
                                                hold_constant=[],
                                                IS_dropout=IS_dropout,
                                                beta=beta)

        return prob_ls+sample_ls+[IS_dropout, beta]
                
    #%%
    def gibbs_even_odd_even_given_v(self, *args):
        ''' This function implements one step of Gibbs sampling,
            starting from the visible state and even hidden states
            but with visible state fixed '''
                
        IS_dropout = args[-2]
        beta = args[-1]
        args = list(args[0:-2])
        mid = int(len(args)/2)
        prob_ls = args[0:mid]
        sample_ls = args[mid:]
        
        # Update odd
        prob_ls, sample_ls = self.parity_update(prob_ls,
                                                sample_ls,
                                                1, 
                                                IS_prob_input=True,
                                                hold_constant=[0],
                                                IS_dropout=IS_dropout,
                                                beta=beta)
        
        # update even
        prob_ls, sample_ls = self.parity_update(prob_ls,
                                                sample_ls,
                                                0, 
                                                IS_prob_input=True,
                                                hold_constant=[0],
                                                IS_dropout=IS_dropout,
                                                beta=beta)
        
        return prob_ls+sample_ls+[IS_dropout, beta]
        
    #%%
    def pos_stats(self, x, IS_dropout, beta):
        # compute positive phase

        # these are the number of positive steps
        if len(self.layers) == 2:
            # 1 step is exact
            n_gibbs_pos = 1
        else:
            # when I checked usually converges within 5-10
            n_gibbs_pos = 25

        # use propup to get initial mf activations
        prob_data = self.propup(x, IS_dropout, beta)
        
        sample_data = [binomial_sample(self.theano_rng, p) for p in prob_data]
                
        output_ls = prob_data + sample_data + [IS_dropout, beta]    
        
        scan_out, updates = theano.scan(self.gibbs_even_odd_even_given_v, 
                                        outputs_info=output_ls, 
                                        n_steps=n_gibbs_pos,
                                        name='scan_pos')       
             
        
        num_p = len(prob_data)
        prob_data = scan_out[0:num_p]
        sample_data = scan_out[num_p:2*num_p]
        prob_data = [p[-1] for p in prob_data]
        sample_data = [s[-1] for s in sample_data]
                
        return prob_data, sample_data, updates
        
    #%%
    def neg_stats(self, prob_data, sample_data, IS_dropout, beta, n_gibbs_neg):
               
        # decide how to initialize persistent chain:
        if self.IS_persist:
            # for PCD, we initialize from the old state of the chain
            prob_model = self.persist_chain
            sample_model = [binomial_sample(self.theano_rng, p) for p in prob_model] 
        else:
            # for CD, we use the newly generated hidden sample
            prob_model = prob_data
            sample_model = sample_data
        
        # perform actual negative phase
        output_ls = prob_model+sample_model+[IS_dropout, beta]

        scan_out, updates = theano.scan(self.gibbs_odd_even_odd, 
                                        outputs_info=output_ls, 
                                        n_steps=n_gibbs_neg,
                                        name='scan_neg')

        num_p = len(prob_model)
        prob_model = scan_out[0:num_p]
        sample_model = scan_out[num_p:2*num_p]
        prob_model = [p[-1] for p in prob_model]
        sample_model = [s[-1] for s in sample_model]
                       
        return prob_model, sample_model, updates
        
    #%%
    def contrastive_divergence_cost(self, prob_data, prob_model, beta):
       
        fe_data = T.mean(self.free_energy_given_h(prob_data, beta)) 
        fe_model = T.mean(self.free_energy_given_h(prob_model, beta)) 
        cost = fe_data - fe_model        
                    
        return cost
    
    #%%
    def cd_cost_fxn(self, x, IS_dropout, beta,
                    n_gibbs_neg=5, L1=0.0, L2=0.0):
        
        prob_data, sample_data, updates = self.pos_stats(x, IS_dropout, beta)
        
        prob_model, sample_model, neg_updates = self.neg_stats(prob_data,
                                                               sample_data,
                                                               IS_dropout,
                                                               beta,
                                                               n_gibbs_neg)
         
        data = [sample_data[0]] + prob_data[1:]
        model = [sample_model[0]] + prob_model[1:]
        
        # determine cost
        cost = self.contrastive_divergence_cost(data, model, beta)
        cost += self.norm_cost(L1, L2) 

        if len(updates) == 0:
            updates = OrderedDict()
        
        for k, v in neg_updates.items():
            updates[k] = v

        return cost, updates
    
    #%%
    def grad(self, data, model, L1, L2):
        
        params, grads = [], []
        
        # determine bias grads
        for layer in self.layers:
            params.append(layer.b)
            g = layer.grad_b(data, model)
            grads.append(g)
        
        # determine weight grads
        for i, pair in enumerate(self.synapse_pairs):
            synapse = self.synapses[i]
            W = synapse.W
            params.append(W)
            sd = [data[p] for p in pair]
            sm = [model[p] for p in pair]
            g = synapse.grad(sd, sm, L1, L2)
            grads.append(g)
        
        return params, grads
    
    #%%
    def training_update(self, x, IS_dropout, beta, n_gibbs_neg=5,
                        L1=0.0, L2=0.0, lr=None, flip_rate=0.0):

        # prep for training step        
        if self.IS_datanorm:
            assert lr is not None
            lr = T.cast(lr, dtype=FLOATX)
      
        if flip_rate > 0.0:
            assert self.batch_size is not None
            shape = (self.batch_size, self.layer_size_list[0])
            f_prob = T.cast(flip_rate*np.ones(shape), dtype=FLOATX)
            flip = binomial_sample(self.theano_rng, f_prob)
            x = x*(1-flip)+(1-x)*flip
                  
        # sample the inputs
        # TODO: should I use this?
        #x = binomial_sample(self.theano_rng, x)
        
        # collect probabilities
        prob_data, sample_data, updates = self.pos_stats(x, IS_dropout, beta)
        self.updates = updates
        
        prob_model, sample_model, updates = self.neg_stats(prob_data,
                                                           sample_data,
                                                           IS_dropout,
                                                           beta,
                                                           n_gibbs_neg)
        self.updates = updates 
        
        # preps the next persistent chain
        if self.IS_persist:
            self.persist_chain = prob_model
        
        # determine cost
        cost = self.contrastive_divergence_cost(prob_data, prob_model, beta)
        cost += self.norm_cost(L1, L2)   
        
        # this matches Ruslans code
        # TODO: check on whether this is worth it, see also pylearn2
        #data = [sample_data[0]] + prob_data[1:]
        #model = [sample_model[0]] + prob_model[1:]
        #data = prob_data
        #model = prob_model
        
        # update the center and bias
        if self.IS_centered:
            self.update_center(prob_data, lr)
          
        if self.IS_std or self.IS_whitened:
            std_data = self.apply_center(prob_data, new=True)
            
        # update the std
        if self.IS_std:
            self.update_std(std_data, lr)  
            
        # update the covariance
        if self.IS_whitened:
            cov_data = self.apply_std(std_data, new = True)
            self.update_cov(cov_data, lr) 
            
        # update_gauges (just b,W if no datanorm)
        self.update_gauge(mode='center_std')
    
        # applies newest transforms before taking gradients
        data = self.apply_datanorm(prob_data, new=True)
        model = self.apply_datanorm(prob_model, new=True)
        
        # TODO: should I only do dropout on inputs? not grads?
        #data = self.apply_dropout(data, IS_dropout)
        #model = self.apply_dropout(model, IS_dropout)

        params, grads = self.grad(data, model, L1, L2)

        # update masks for next minibatch
        if self.IS_dropout:
            self.update_dropout()
            
        return cost, params, grads

    #%%
    def sample(self, n_features, filepath, beta, data,
               n_chains=20, n_samples=10, plot_every=2000):
        
        # some assumptions I made
        assert np.mod(n_chains, 5) == 0
        x = int(np.sqrt(n_features))
        assert x**2 == n_features
        y = x
        assert np.mod(x, 2) == 0        
             
                     
        init_v = np.zeros((n_chains, n_features))
        
        num_each = int(n_chains/5)
    
        # make some starting chains
        # these illustrate different types of reconstructions
    
        # white noise examples
        init_v[0:num_each] = np.random.uniform(size=(num_each, n_features))
        
        # actual examples
        index = np.random.choice(np.arange(data.shape[0]), size=num_each,
                                 replace=False) 
        init_v[num_each:2*num_each] = data[index]
        
        # bit flips
        index = np.random.choice(np.arange(data.shape[0]), size=num_each,
                                 replace=False)
        d = data[index]
        f = np.random.uniform(size=d.shape) > 0.5
        init_v[2*num_each:3*num_each] = d*(1-f)+(1-d)*f

        # additive white noise
        index = np.random.choice(np.arange(data.shape[0]), size=num_each,
                                 replace=False)
        d = data[index]
        g = 0.1*np.random.normal(size=d.shape)
        init_v[3*num_each:4*num_each] = np.clip(d+g, 0, 1)

        # masking   
        index = np.random.choice(np.arange(data.shape[0]), size=num_each,
                                 replace=False)
        d = data[index]  
        for i, dd in enumerate(d):
            j = np.random.randint(0, 4, 1)
            if j == 0:
                mask = np.concatenate((np.ones(int(n_features/2)),
                                       np.zeros(int(n_features/2))))
            elif j == 1:
                mask = np.concatenate((np.zeros(int(n_features/2)),
                                       np.ones(int(n_features/2))))
            elif j == 2:
                mask = np.tile(np.concatenate((np.ones((int(x/2))),
                                               np.zeros(int(x/2)))),
                                                x)
            elif j == 3:
                mask = np.tile(np.concatenate((np.zeros((int(x/2))),
                                               np.ones(int(x/2)))),
                                                x)
            init_v[4*num_each+i] = dd*mask
        
        init_v = init_v.astype(FLOATX)
        
        init_v_var = theano.shared(init_v, name='init_v')
        
        IS_dropout = T.scalar('IS_dropout')
        
        prob_ls = self.propup(init_v_var, IS_dropout, beta)
        
        sample_ls = [binomial_sample(self.theano_rng, p) for p in prob_ls]
        
        output_ls = prob_ls+sample_ls+[IS_dropout, beta]   
       
        output_ls, updates = theano.scan(self.gibbs_even_odd_even, 
                                         outputs_info=output_ls, 
                                         n_steps=plot_every,
                                         name='scan_sample')
            
        prob_v = output_ls[0]        
        updates[init_v_var] = prob_v[-1]
    
        # construct the function that implements our persistent chain.
        sample_fxn = theano.function([IS_dropout], prob_v[-1],
                                     updates=updates, name='sample_fxn',
                                     on_unused_input='ignore')
        
        # create a space to store the image for plotting ( we need to leave
        # room for the tile_spacing as well)
        ts = 1 # tile spacing
        xx = x+ts
        yy = y+ts    
        image_data = np.zeros((xx*(n_samples+2)+1, yy*n_chains-1), dtype='uint8')
        npz_data = np.zeros((n_samples+1, n_chains, n_features))
        npz_data[0] = init_v              
                    
        image_data[0:x, :] = utils.tile_raster_images(
                                    X=init_v, img_shape=(x, y),
                                    tile_shape=(1, n_chains), tile_spacing=(ts, ts))
        for idx in range(n_samples):
            # generate `plot_every` intermediate samples that we discard,
            # because successive samples in the chain are too correlated
            # I left a blank row between original images and gibbs samples
            vis_prob = sample_fxn(0)
            image_data[xx*(idx+2) : xx*(idx+2) + x, :] =\
                utils.tile_raster_images(X=vis_prob, img_shape=(x, y),
                    tile_shape=(1, n_chains), tile_spacing=(ts, ts))
            npz_data[idx+1] = vis_prob
        
        # save image
        image = Image.fromarray(image_data)
        image.save(filepath+'.pdf')    
        kwargs = {'samples':npz_data}
        np.savez_compressed(filepath, **kwargs)
        