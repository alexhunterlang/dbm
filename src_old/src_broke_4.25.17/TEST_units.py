#%% Imports
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *

import os
import shutil
import numpy as np
from collections import OrderedDict
import matplotlib.pyplot as plt

import theano.tensor as T
import theano

# The custom imports should be in same folder as this script
import __main__ as main
if not hasattr(main, '__file__'):
    # This means its an iPython instance
    import getpass
    usr = getpass.getuser()
    if usr == 'alexlangMacBookPro':
        path_to_script_folder = '/Users/alexlangMacBookPro/Dropbox/BitBucket/dbm/src/'
        os.chdir(path_to_script_folder)

# import from my files
from utils import make_shared, FLOATX, EPS
from components import Synapse, Layer
from dbm import DBM
from ais import AIS
import my_data

from TEST_simple_nnet import default_train_test

# Use unittest- https://docs.python.org/2/library/unittest.html

#%%
def synapse_tests(save_folder_base):
    """ Test all the functions in synapse """
    pass

#%%
def layer_tests(save_folder_base):
    """ Test all the functions in layer """
    pass

#%%
def dbm_tests(save_folder_base):
    """ Test all the functions in dbm """
    pass

#%%
def dataset_tests(save_folder_base):
    """ make sure matches type, expected stats """
    pass

#%%
def optimizers_tests(save_folder_base):
    """ create and get blank updates """
    pass

#%%
def ais_tests(save_folder_base):
    """ just initialize ais """
    pass

#%%
def std_tests(save_folder_base):
    """ See that std does make stats whiter """
    
    dbm_init, dbm_train, data_details, optimizer = default_train_test()
    dbm_init['IS_centered'] = True
    dbm_init['IS_std'] = True
    dbm_init['std_eps'] = 1e-2
            
    

    # 1. initialize dbm
    # 2. show that stats are whiter
    # 3. train dbm, debug mode
    #       - load up stats, see if gives hint at better init std

#%%
def white_tests(save_folder_base):
    """ See that white does make stats whiter """
    
    dbm_init, dbm_train, data_details, optimizer = default_train_test()
    dbm_init['IS_centered'] = True
    dbm_init['IS_whitened'] = True
    dbm_init['white_type'] = 'ZCA'
    dbm_init['white_eps'] = 1e-3
            
    train_data, valid_data = my_data.create_train_valid(**data_details)
    init_data = train_data.get_value()
            
    dbm_init['batch_size'] = dbm_train['batch_size']
    if not dbm_init['IS_whitened']:
        dbm_init['white_type'] = None
        dbm_init['white_eps'] = None
    if not dbm_init['IS_std']:
        dbm_init['std_eps'] = None
    
    del dbm_init['weight_file']
    dbm_init['init_data'] = init_data
                
    dbm = DBM(**dbm_init)
    
    x = T.matrix()
    IS_dropout = T.scalar()
    beta = T.scalar()
    prob_data, sample_data, updates = dbm.pos_stats(x, IS_dropout, beta)
    center_data = dbm.apply_center(prob_data)
    white_data = dbm.apply_white(center_data)
    
    fn = theano.function([x, IS_dropout, beta], prob_data, updates=updates,
                         on_unused_input='ignore')
    pd = fn(init_data, 0, 1)
    
    fn = theano.function([x, IS_dropout, beta], white_data, updates=updates,
                         on_unused_input='ignore')
    wd = fn(init_data, 0, 1)
    
    p0, p1 = pd
    cov0 = np.cov(p0.T)
    cov1 = np.cov(p1.T)
    w0, w1 = wd
    w_cov0 = np.cov(w0.T)
    w_cov1 = np.cov(w1.T)

    plt.imshow(cov0, cmap='hot', vmin=-1, vmax=1)
    plt.imshow(w_cov0, cmap='hot', vmin=-1, vmax=1)
    plt.imshow(cov1, cmap='hot', vmin=-1, vmax=1)
    plt.imshow(w_cov1, cmap='hot', vmin=-1, vmax=1)

    # 1. initialize dbm
    # 2. show that stats are whiter
    # 3. train dbm, debug mode
    #       - load up stats, see if gives hint at better init cov

#%%
def std_white_tests(save_folder_base):
    """ See that white does make stats whiter """
    
    dbm_init, dbm_train, data_details, optimizer = default_train_test()
    dbm_init['IS_centered'] = True
    dbm_init['IS_std'] = True
    dbm_init['std_eps'] = 1e-1
    dbm_init['IS_whitened'] = True
    dbm_init['white_type'] = 'ZCA'
    dbm_init['white_eps'] = 1e-3
            
    train_data, valid_data = my_data.create_train_valid(**data_details)
    init_data = train_data.get_value()
            
    dbm_init['batch_size'] = dbm_train['batch_size']
    if not dbm_init['IS_whitened']:
        dbm_init['white_type'] = None
        dbm_init['white_eps'] = None
    if not dbm_init['IS_std']:
        dbm_init['std_eps'] = None
    
    del dbm_init['weight_file']
    dbm_init['init_data'] = init_data
                
    dbm = DBM(**dbm_init)
    
    x = T.matrix()
    IS_dropout = T.scalar()
    beta = T.scalar()
    prob_data, sample_data, updates = dbm.pos_stats(x, IS_dropout, beta)
    center_data = dbm.apply_center(prob_data)
    std_data = dbm.apply_std(center_data)
    white_data = dbm.apply_white(std_data)
    
    fn = theano.function([x, IS_dropout, beta], prob_data, updates=updates,
                         on_unused_input='ignore')
    pd = fn(init_data, 0, 1)
    
    fn = theano.function([x, IS_dropout, beta], white_data, updates=updates,
                         on_unused_input='ignore')
    wd = fn(init_data, 0, 1)
    
    p0, p1 = pd
    cov0 = np.cov(p0.T)
    cov1 = np.cov(p1.T)
    w0, w1 = wd
    w_cov0 = np.cov(w0.T)
    w_cov1 = np.cov(w1.T)

    plt.imshow(cov0, cmap='hot', vmin=-1, vmax=1)
    plt.imshow(w_cov0, cmap='hot', vmin=-1, vmax=1)
    plt.imshow(cov1, cmap='hot', vmin=-1, vmax=1)
    plt.imshow(w_cov1, cmap='hot', vmin=-1, vmax=1)

    # 1. initialize dbm
    # 2. show that stats are whiter
    # 3. train dbm, debug mode
    #       - load up stats, see if gives hint at better init cov


#%%
if __name__ == '__main__':
    
    save_folder_base = '../results/test_units/'
    if os.path.exists(save_folder_base):
        shutil.rmtree(save_folder_base)
    os.mkdir(save_folder_base)
    
    synapse_tests(save_folder_base)
    layer_tests(save_folder_base)
    dbm_tests(save_folder_base)
    dataset_tests(save_folder_base)
    optimizers_tests(save_folder_base)
    ais_tests(save_folder_base)
    std_tests(save_folder_base)
    white_tests(save_folder_base)
    