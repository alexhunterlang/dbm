#%% Imports 
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *

import six
import numpy as np
import os
from collections import OrderedDict

import theano.tensor as T
from theano.ifelse import ifelse

# The custom imports should be in same folder as this script
import __main__ as main
if not hasattr(main, '__file__'):
    # This means its an iPython instance
    import getpass
    usr = getpass.getuser()
    if usr == 'alexlangMacBookPro':
        path_to_script_folder = '/Users/alexlangMacBookPro/Dropbox/BitBucket/dbm/src/'
        os.chdir(path_to_script_folder)

# my files
from utils import binomial_sample, make_theano_rng, make_shared, FLOATX, EPS
import my_data

#%%
class NeuralNetParts(object):
    """ Generic object for neural network components """
    
    def __init__(self, name):
        
        assert name.startswith('h')
        self.name = name
        self._updates = OrderedDict()
        self._trainable_weights = []
        self._non_trainable_weights = []
    
    @property
    def updates(self):
        return self._updates
    
    @updates.setter
    def updates(self, updates):
        if updates is None:
            return
        keys = self._updates.keys()
        for k, v in updates.items():
            assert k not in keys, 'Already have update for '+k.name
            self._updates[k] = v

    @property
    def weights(self):
        return self._trainable_weights + self._non_trainable_weights

#%%
#%%
class Synapse(NeuralNetParts):
    def __init__(self,
                 n_in,
                 n_out,
                 name,
                 init_W=None,
                 init_cov=None,
                 init='orthogonal',
                 IS_trainable=True):
                
        super(Synapse, self).__init__(name)
        
        self.n_in = n_in
        self.n_out = n_out
        self.IS_trainable = IS_trainable
                    
        # Make weight
        if init_W is None:
            shape = (n_in, n_out)
            if init == 'orthogonal':
                W = np.random.normal(size=shape)
                U, _, V = np.linalg.svd(W)
                S = np.zeros(shape)
                np.fill_diagonal(S, 1)
                init_W = U.dot(S).dot(V)
                
            elif init == 'normal':
                init_W = 0.01*np.random.normal(size=shape)
            
            elif init == 'identity':
                assert n_in == n_out
                init_W = np.identity(n_in)
            
            elif init == 'noisy_identity':
                assert n_in == n_out
                init_W = 1e-4*np.random.normal(size=shape)
                np.fill_diagonal(init_W, 1)
            
            elif init == 'zeros':
                init_W = np.zeros(shape)
            
            else:
                raise NotImplementedError
    
        self.W = make_shared(init_W, name=self.name)
    
        if init_cov is None:        
            init_cov = np.zeros((init_W.shape))
        self.cov = make_shared(init_cov, name=self.name+'_cov')
       
        # needed for saving and training weights
        self._trainable_weights.append(self.W)
        self._non_trainable_weights.append(self.cov)                
 
    #%%
    def init_layers(self, layer_in, layer_out):
        """ This allows synapse to know who is connected to it """
        self.layer_in = layer_in
        self.layer_out = layer_out
       
    #%%
    def norm_cost(self, L1=0.0, L2=0.0):
        
        cost = T.cast(0.0, dtype=FLOATX)
        L1 = T.cast(L1, dtype=FLOATX)
        L2 = T.cast(0.5*L2, dtype=FLOATX)
        
        if self.IS_trainable:
            cost += L1*T.sum(T.abs_(self.W)) + L2*T.sum(T.pow(self.W, 2))

        return cost

    #%%
    def norm_grad(self, L1=0.0, L2=0.0):
        
        g = T.cast(0.0, dtype=FLOATX)
        L1 = T.cast(L1, dtype=FLOATX)
        L2 = T.cast(L2, dtype=FLOATX)
        
        if self.IS_trainable: 
            g += L1*T.sum(T.sgn(self.W)) + L2*T.sum(self.W)

        return g

    #%%
    def grad(self, prob_data, prob_model, L1=0.0, L2=0.0):
               
        g = None
        if self.IS_trainable:
            
            pd0, pd1 = prob_data
            pm0, pm1 = prob_model
            
            n = T.cast(prob_data[0].shape[0], dtype=FLOATX)
            
            g = -1.0/n*(T.dot(pd0.T, pd1) - T.dot(pm0.T, pm1))
            
            g += self.norm_grad(L1, L2)
            
        return g
        
    #%%
    def gauged_W(self, mode):
    
        assert mode in ['AIS', 'center_var', 'white']
        
        W = self.W
        
        # W01_init = R0 * W01 * R1.T
        W = self.layer_out.apply_white(W, mode='old', tran=True, inv=False)
        W = self.layer_in.apply_white(W.T, mode='old', tran=True, inv=False).T
        
        if mode == 'AIS':    
            # W01_final = p0 * W01_init * p1.T
            W = self.layer_out.apply_var(W, mode='old', tran=True, inv=False)            
            W = self.layer_in.apply_var(W.T, mode='old', tran=True, inv=False).T
            
        elif mode in 'center_var':
            # W01_final = R0^{-1} *P0^{-1} * W01_init * P1.T^{-1} * R1.T^{-1}
            W = self.layer_out.apply_var(W, mode='change', tran=True, inv=True)
            W = self.layer_out.apply_white(W, mode='old', tran=True, inv=True)
            W = self.layer_in.apply_var(W.T, mode='change', tran=True, inv=True).T
            W = self.layer_in.apply_white(W.T, mode='old', tran=True, inv=True).T
            
        elif mode == 'white':
            # W01_final = R0_final^{-1} * W01_init * * R1_final.T^{-1}
            W = self.layer_out.apply_white(W, mode='new', tran=True, inv=False)
            W = self.layer_in.apply_white(W.T, mode='new', tran=True, inv=False).T
        
        return W
    
    #%%
    def gauged_W_velocity(self, mode, W_vel, b_vel=None, delta_c=None):
        assert mode in ['center_var', 'white']
        
        W_vel = self.layer_out.apply_white(W_vel,
                                           mode='old', tran=False, inv=True)
        W_vel = self.layer_in.apply_white(W_vel.T,
                                           mode='old', tran=False, inv=True).T
        
        if mode == 'center_var':

            W_vel = self.layer_out.apply_var(W_vel,
                                             'change', tran=False, inv=False)
            W_vel = self.layer_out.apply_white(W_vel,
                                               mode='old', tran=False, inv=False)
            
            W_vel = self.layer_in.apply_var(W_vel.T,
                                            'change', tran=False, inv=False).T
            W_vel = self.layer_in.apply_white(W_vel.T,
                                               mode='old', tran=False, inv=False).T
        
            if (b_vel is not None) and (delta_c is not None):
                b_in_vel, b_out_vel = b_vel
                dc_in, dc_out = delta_c
            
                # prep the center terms
                dc_in = self.layer_in.apply_var(dc_in, 'old', tran=False, inv=False)
                dc_in = self.layer_in.apply_white(dc_in, 'old', tran=False, inv=False)
            
                dc_out = self.layer_out.apply_var(dc_out, 'old', tran=False, inv=False)
                dc_out = self.layer_out.apply_white(dc_out, 'old', tran=False, inv=False)
                
                dc_in = T.repeat(dc_in.reshape((-1, 1)), self.n_out, 1)
                dc_out = T.repeat(dc_out.reshape((1, -1)), self.n_in, 0)
            
                b_in_vel = T.repeat(b_in_vel.reshape((-1, 1)), self.n_out, 1)
                b_out_vel = T.repeat(b_out_vel.reshape((1, -1)), self.n_in, 0)
            
                W_vel -= dc_in*b_out_vel
                W_vel -= b_in_vel*dc_out
            
        elif mode == 'white':
            W_vel = self.layer_out.apply_white(W_vel,
                                               mode='new', tran=False, inv=False)
            W_vel = self.layer_in.apply_white(W_vel.T,
                                               mode='new', tran=False, inv=False).T
        
        return W_vel
    
    #%%
    def update_weight_gauge(self, mode, updates=None):
        self.W_new = self.gauged_W(mode)
        if updates is None:
            self.updates = OrderedDict({self.W : self.W_new})
        else:
            updates[self.W] = self.W_new
            return updates
        
    #%%
    def update_cov(self, center_data, lr):
        if self.layer_in.IS_whitened or self.layer_out.IS_whitened:
            cd0, cd1 = center_data
            n = T.cast(cd0.shape[0], dtype=FLOATX)
            cov_update = T.dot(cd0.T, cd1)/(n-1)
            self.cov_new = (1-lr)*self.cov + lr*cov_update
            self.updates = OrderedDict({self.cov : self.cov_new})
        
#%%
#%%
class Layer(NeuralNetParts):
    def __init__(self,
                 name,
                 output_dim,
                 input_dict=None,
                 transposed_dict=None,
                 IS_persist=False,
                 IS_centered=False,
                 IS_var=False,
                 IS_whitened=False,
                 var_eps=None,
                 white_type=None,
                 white_eps=None,
                 dropout_p=0.0,
                 batch_size=None,
                 theano_rng=None,
                 init_b=None,
                 init_persist=None,
                 init_c=None,
                 init_var=None,
                 init_wh=None,
                 data_mean=None,
                 data_var=None,
                 data_cov=None,
                 activation=T.nnet.sigmoid):
        
        super(Layer, self).__init__(name)
        
        self.my_index = int(self.name[1:])
        self.output_dim = output_dim
        
        self.IS_persist = IS_persist
        self.IS_centered = IS_centered
        self.IS_var = IS_var
        self.IS_whitened = IS_whitened
        self.var_eps = var_eps
        self.white_type = white_type
        self.white_eps = white_eps
        self.IS_datanorm = IS_centered or IS_var or IS_whitened
        self.batch_size = batch_size
        self.p = dropout_p
        self.IS_dropout = 1.0*(self.p > 0.0)
        
        # Sanity checks 
        if IS_whitened or IS_var:
            assert IS_centered
        if IS_var:
            assert var_eps is not None
        if IS_whitened:
            assert white_type is not None
            assert white_eps is not None
        if (0. < self.p < 1.) or self.IS_persist:
            assert self.batch_size is not None
        
        if theano_rng is None:
            theano_rng = make_theano_rng()
        self.theano_rng = theano_rng
        
        # creates a sorted input_dict
        self.input_dict = OrderedDict()
        if input_dict is not None:
            for k in np.sort(input_dict.keys()):
                self.input_dict[k] = input_dict[k]
            
        # creates a sorted transposed_dict
        self.transposed_dict = OrderedDict()
        if transposed_dict is not None:
            for k in np.sort(transposed_dict.keys()):
                self.transposed_dict[k] = transposed_dict[k]
            
        # assign layer properties
        len_in = len(self.input_dict)
        len_tran = len(self.transposed_dict)
        if len_in > 0 and len_tran > 0:
            self.layer_type = 'middle'
            self.direction = 'both'
            # need to compensate for reduced inputs when only going single direction
            self.z_up_adj = 1.0*(len_in+len_tran)/len_in
            self.z_down_adj = 1.0*(len_in+len_tran)/len_tran
        elif len_in > 0 and len_tran == 0:
            self.layer_type = 'output'
            self.direction = 'up'
        elif len_in == 0 and len_tran > 0:
            self.layer_type = 'input'
            self.direction = 'down'
        else:
            raise NotImplementedError
            
        self.activation = activation
            
        self.transposed_index = []
        self.W_T_ls = []
        for key, W in six.iteritems(self.transposed_dict):
            self.W_T_ls.append(W.T)
            self.transposed_index.append(key)
        
        self.input_index = []
        self.W_ls = []
        for key, W in six.iteritems(self.input_dict):
            self.W_ls.append(W)
            self.input_index.append(key)
        
        if init_b is None:
            if data_mean is not None:
                # tip from ref [2]
                p = np.clip(data_mean, EPS, 1-EPS)
                init_b = np.log(p/(1-p))
            else:            
                init_b = np.zeros(output_dim, dtype=FLOATX)
                
        self.b = make_shared(init_b, name=self.name + '_b')
        self._trainable_weights.append(self.b)
       
        if self.IS_centered:
            if init_c is None:
                if data_mean is not None:
                    init_c = np.clip(data_mean, EPS, 1-EPS)
                else:            
                    # assuming these are Bernoulli variables
                    init_c = 0.5*np.ones(output_dim)
                        
            self.c = make_shared(init_c, name=self.name + '_center')
            self._non_trainable_weights.append(self.c)
            
        if self.IS_var:
            if init_var is None:
                if data_var is not None:
                    init_var = np.maximum(data_var, 100*EPS)
                else:           
                    init_var = np.ones(output_dim)
                        
            self.var = make_shared(init_var, name=self.name + '_var')
            self._non_trainable_weights.append(self.var)
            
        if self.IS_whitened:
            reinit = (init_wh is None) and (data_cov is not None)
            
            if data_cov is None:
                data_cov =  np.identity(output_dim)
            if init_wh is None:
                init_wh = np.identity(output_dim)    
            
            self.white = make_shared(init_wh, name=self.name + '_white')
            self._non_trainable_weights.append(self.white)
            
            self.white_inv = make_shared(init_wh, name=self.name + '_white_inv')
            self._non_trainable_weights.append(self.white_inv)
            
            self.cov = make_shared(data_cov, name=self.name + '_cov')
            self._non_trainable_weights.append(self.cov)
            
            if reinit:
                self.update_white(cov=data_cov)
            
        if self.IS_persist:
            if init_persist is None:
                if data_mean is not None:
                    # clipping data_mean to let each neuron have some variation
                    eps = 0.05
                    dm = np.clip(data_mean, eps, 1-eps)
                    dm *= np.mean(dm)/np.mean(data_mean) # keep same mean prob
                    init_persist = np.repeat(dm.reshape((1, -1)), batch_size, 0)
                else:            
                    init_persist = 0.5*np.ones((batch_size, output_dim))    
            
            self._persist_chain = make_shared(init_persist,
                                              name=self.name+'_persist_chain')
            self._non_trainable_weights.append(self._persist_chain)
        
        if self.IS_dropout:
            
            p = ((1-self.p)*np.ones((self.batch_size, self.output_dim)))
            self._p_matrix = p.astype(FLOATX)
            
            sample = binomial_sample(self.theano_rng, self._p_matrix).eval()
            self._dropout = make_shared(sample, name=self.name + '_dropout')
            self._non_trainable_weights.append(self._dropout)        


    def init_layers(self, layer_in_ls, layer_out_ls):
        """ This allows layer to know who is connected to it """
        self.layer_in_ls = layer_in_ls
        self.layer_out_ls = layer_out_ls
        assert len(self.layer_in_ls) == len(self.W_ls)
        assert len(self.layer_out_ls) == len(self.W_T_ls)

    #%%
    @property
    def persist_chain(self):
        if self.IS_persist:
            return self._persist_chain
        else:
            return None
 
    
    @persist_chain.setter
    def persist_chain(self, persist_chain):
        if self.IS_persist:
            updates = OrderedDict({self._persist_chain : persist_chain})
            self.updates = updates
    
    #%%        
    def update_dropout(self):
        
        if 0. < self.p < 1:
            s = binomial_sample(self.theano_rng, self._p_matrix)
            updates = OrderedDict({self._dropout : s})
            self.updates = updates
        
    def update_center(self, prob_data, lr):
        if self.IS_centered:
            pd = prob_data[self.my_index]
            self.c_new = (1-lr)*self.c + lr*T.mean(pd, axis=0)
            self.updates = OrderedDict({self.c : self.c_new})
    
    def update_var(self, var_data, lr):
        if self.IS_var:
            vd = var_data[self.my_index]
            n = T.cast(self.batch_size, FLOATX)
            self.var_new = (1-lr)*self.var + lr*T.sum(vd**2, axis=0)/(n-1)
            self.updates = OrderedDict({self.var : self.var_new})
    
    def update_cov(self, center_data, lr):
        if self.IS_whitened:
            cd = center_data[self.my_index]
            n = T.cast(self.batch_size, FLOATX)
            cov_update = T.dot(cd.T, cd)/(n-1)
            self.cov_new = (1-lr)*self.cov + lr*cov_update
            self.updates = OrderedDict({self.cov : self.cov_new})
    
    def update_white(self, cov=None, updates=None):
        if self.IS_whitened:
            if cov is None:
                cov = self.cov.get_value()
                
            if self.white_type == 'PCA':
                white_new = my_data.make_PCA_matrix(cov, self.white_eps)
            elif self.white_type == 'ZCA':
                white_new = my_data.make_ZCA_matrix(cov, self.white_eps)
            else:
                raise NotImplementedError
                
            self.white_new = white_new
            self.white_new_inv = np.linalg.inv(white_new)
            
            if updates:
                updates[self.white] = self.white_new
                updates[self.white_inv] = self.white_new_inv
                return updates
    
    def update_bias_gauge(self, mode, updates=None):
        self.b_new = self.gauged_b(mode)
        
        if updates is None:
            self.updates = OrderedDict({self.b : self.b_new})
        else:
            updates[self.b] = self.b_new
            return updates
    
    #%%
    def apply_dropout(self, x, IS_dropout):
        if (0. < self.p < 1.) and x is not None:
            x = ifelse(T.eq(IS_dropout, 0),
                       x,
                       x*self._dropout/T.cast(1.0-self.p, dtype=FLOATX)
                       )
        return x
 
    def apply_center(self, x, mode):
        if self.IS_centered and x is not None:
            assert mode in ['old', 'new']
            if mode == 'new' and hasattr(self, 'c_new'):
                c = self.c_new
            else:
                c = self.c
            x -= c   
        return x
    
    def apply_var(self, x, mode, tran=False, inv=False):
        if self.IS_var and x is not None:
            assert mode in ['old', 'new', 'change']
            
            var_eps = T.cast(self.var_eps, dtype=FLOATX) 
            div = T.cast(1.0 + 4*var_eps, dtype=FLOATX)
            
            if mode == 'change':
                if hasattr(self, 'var_new'):
                    norm = (self.var_new + var_eps)/(self.var + var_eps)
                else:
                    return x
                
            elif mode == 'new' and hasattr(self, 'var_new'):
                norm = div/(self.var_new + var_eps)

            else:
                norm = div/(self.var + var_eps)
                            
            if x.ndim > 1:
                # TODO: fix this
                if tran:
                    norm = T.repeat(norm.reshape((-1, 1)), x.shape[1], 1)
                else:
                    norm = T.repeat(norm.reshape((1, -1)), x.shape[0], 0)
            x *= norm
            
        return x
            
    def apply_white(self, x, mode, tran=False, inv=False):

        if self.IS_whitened and (x is not None):
            assert mode in ['old', 'new']
              
            if not hasattr(self, 'white_new'):
                mode = 'old'
            
            if mode == 'new' and inv:
                W = self.white_new_inv
            elif mode == 'new' and not inv:
                W = self.white_new
            elif mode == 'old' and inv:
                W = self.white_inv
            elif mode == 'old' and not inv:
                W = self.white

            if tran:
                W = W.T
            
            x = T.dot(x, W)
            
        return x
    
    def apply_datanorm(self, x, mode, tran=False, inv=False):
        center_data = self.apply_center(x, mode)
        var_data = self.apply_var(center_data, mode, tran, inv)
        white_data = self.apply_white(var_data, mode, tran, inv)
            
        return white_data
    
    #%%
    def grad_b(self, prob_data, prob_model):
        
        pd = prob_data[self.my_index]
        pm = prob_model[self.my_index]
        
        g = -(T.mean(pd, axis=0) - T.mean(pm, axis=0))

        return g
    
    def gauged_b(self, mode):
        assert mode in ['AIS', 'center_var', 'white']
        
        if self.IS_centered and mode in ['AIS', 'center_var']:
            
            layer_ls = self.layer_in_ls + self.layer_out_ls
            center_ls = [None]*len(layer_ls)
            for i, layer in enumerate(layer_ls):
                if mode == 'AIS':
                    c = -layer.c
                elif mode == 'center_var':
                    c = - layer.apply_center(layer.c, 'new')
                c = layer.apply_var(c, mode='old')
                c = layer.apply_white(c, mode='old')
                center_ls[i] = c
            b = self.get_input(center_ls, full_input=False)
            
        else:
            b = self.b
        
        if mode == 'AIS':    
            b = self.apply_white(b, mode='old', tran=True)
            b = self.apply_var(b, mode='old', tran=True)
           
        elif mode == 'center_var':
            b = self.apply_white(b, mode='old', tran=True)
            b = self.apply_var(b, mode='change', tran=True, inv=True)
            b = self.apply_white(b, mode='old', tran=True, inv=True)
        
        elif mode == 'white':
            b = self.apply_white(b, mode='old', tran=True, inv=False)
            b = self.apply_white(b, mode='new', tran=True, inv=True)
            
        
        return b
        
    
    def gauged_b_velocity(self, b_vel, mode):
        assert mode in ['center_var', 'white']
        
        b_vel = self.apply_white(b_vel, 'old', tran=False, inv=True)
        
        if mode == 'center_var':    
            b_vel = self.apply_var(b_vel, 'change', tran=False, inv=False)
            b_vel = self.apply_white(b_vel, 'old', tran=False, inv=False)
            
        elif mode == 'white':
            b_vel = self.apply_white(b_vel, 'new', tran=False, inv=False)
        
        return b_vel
    
    #%%
    def needed_inputs(self, mean_field=True, direction=None):
        
        index = []
        if direction is None:
            direction = self.direction
        else:
            assert direction in ['both', 'up', 'down']
        
        if direction in ['both', 'up']:
            index += self.input_index
        if not mean_field:
            index += [self.my_index]
        if direction in ['both', 'down']:
            index += self.transposed_index
            
        return index
    
    #%%
    def prep_input(self, full_input_ls, mean_field=True, direction=None):
        
        if direction is None:
            direction = self.direction
            
        index = self.needed_inputs(mean_field, direction)
        
        input_ls = [full_input_ls[i] for i in index]
        
        return input_ls

    #%%
    def get_input(self, input_ls, mean_field=True,
                  direction=None, full_input=True, gauged=True):
        
        if direction is None:
            direction = self.direction
        assert direction in ['both', 'up', 'down']
        
        if full_input:
            input_ls = self.prep_input(input_ls, mean_field, direction)
        
        # Controls which interactions to include    
        IS_up, IS_down = True, True
        if direction == 'up' or self.direction == 'up':
            IS_down = False            
        elif direction == 'down' or self.direction == 'down':
            IS_up = False
        
        start, stop = 0, 0
        current_W_ls, current_input = [], []
        
        if IS_up:
            stop = len(self.W_ls)
            current_input += input_ls[start:stop]
            current_W_ls += self.W_ls
        if not mean_field:
            input_self = input_ls[stop]
            stop += 1
        if IS_down:
            current_input += input_ls[stop:]
            current_W_ls += self.W_T_ls
              
        # Calculate activation input
        z = self.b
        for data, W in zip(current_input, current_W_ls):
            z += T.dot(data, W)
            if not mean_field:
                if self.IS_datanorm:
                    raise NotImplementedError
                z += -0.5*T.dot(data-data**2, W**2)*(input_self-0.5)
                
        # need to compensate for reduced input 
        if (self.layer_type == 'middle') and (direction == 'up'):
            z *= self.z_up_adj  
        elif (self.layer_type == 'middle') and (direction == 'down'):
            z *= self.z_down_adj
        
        # TODO: should this be new???
        if self.IS_whitened and gauged:
            z = self.apply_white(z, mode='old', tran=True)
            
        if self.IS_var and gauged:
            z = self.apply_var(z, mode='old', tran=True)
            
        return z

    #%%
    def get_output(self, input_ls, beta=1.0, mean_field=True, direction=None,
                   full_input=True, gauged=True):
        
        beta = T.cast(beta, FLOATX)
        z = self.get_input(input_ls, mean_field, direction, full_input, gauged)
        prob = self.activation(beta*z)   
        
        return prob
  