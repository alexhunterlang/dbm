#%%
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *

import os
import gzip
import six.moves.cPickle as pickle
from six.moves import urllib
import numpy as np
import scipy.io as io

import theano
import theano.tensor as T

import skimage.filters as filters

try:
    import PIL.Image as Image
except ImportError:
    import Image

import __main__ as main
if not hasattr(main, '__file__'):
    # This means its an iPython instance
    import getpass
    usr = getpass.getuser()
    if usr == 'alexlangMacBookPro':
        path_to_script_folder = '/Users/alexlangMacBookPro/Dropbox/BitBucket/dbm/src/'
        os.chdir(path_to_script_folder)

# import from my files
from utils import FLOATX, EPS
 
#%%
def make_PCA_matrix(cov, eps=1e-3):
    ''' PCA whitening'''

    # want to make sure it is a valid variance estimate
    var = np.diag(cov)
    var = np.maximum(var, 100*EPS)
    np.fill_diagonal(cov, var)

    U, S, V = np.linalg.svd(cov)
    # never trust anything below machine eps
    S = np.maximum(S, EPS)
    
    # also need to regularize the inversion
    PCA = np.diag(1. / np.sqrt(S + eps)).dot(U.T)
    
    PCA = PCA.T.astype(FLOATX)

    return PCA
              
#%%
def make_PCA_corr_matrix(corr, var, eps=1e-3):
    ''' PCA whitening'''

    PCA = make_PCA_matrix(corr, eps)
    sqrt_V = 1.0 / np.sqrt(var + eps)
    
    PCA_corr = (PCA.T*sqrt_V).T.astype(FLOATX)

    return PCA_corr
      
#%%
def make_ZCA_matrix(cov, eps=1e-3):
    ''' ZCA whitening'''

    # want to make sure it is a valid variance estimate
    var = np.diag(cov)
    var = np.maximum(var, 100*EPS)
    np.fill_diagonal(cov, var)

    A = cov.T.dot(cov) + eps*np.identity(cov.shape[0])
    X = np.linalg.solve(A, cov.T)
    
    U, S, V = np.linalg.svd(X)
    # never trust anything below machine eps
    S = np.maximum(S, EPS)

    ZCA = U.dot(np.diag(np.sqrt(S))).dot(U.T)

    ZCA = ZCA.T.astype(FLOATX)

    return ZCA

#%%
def make_ZCA_corr_matrix(corr, var, eps=1e-3):
    ''' ZCA correlation whitening '''

    ZCA = make_ZCA_matrix(corr, eps)
    sqrt_V = 1.0 / np.sqrt(var + eps)
    
    ZCA_corr = (ZCA.T*sqrt_V).T.astype(FLOATX)

    return ZCA_corr
              
#%%
def shared_dataset(data_x, data_y, borrow=True, name=''):
    """ Function that loads the dataset into shared variables

    The reason we store our dataset in shared variables is to allow
    Theano to copy it into the GPU memory (when code is run on GPU).
    Since copying data into the GPU is slow, copying a minibatch everytime
    is needed (the default behaviour if the data is not in a shared
    variable) would lead to a large decrease in performance.
    """
    shared_x = theano.shared(np.asarray(data_x, dtype=FLOATX),
                             name=name, borrow=borrow)
    shared_y = theano.shared(np.asarray(data_y, dtype=FLOATX),
                             name=name, borrow=borrow)
    # When storing data on the GPU it has to be stored as floats
    # therefore we will store the labels as ``floatX`` as well
    # (``shared_y`` does exactly that). But during our computations
    # we need them as ints (we use labels as index, and if they are
    # floats it doesn't make sense) therefore instead of returning
    # ``shared_y`` we will have to cast it to int. This little hack
    # lets ous get around this issue
    return shared_x, T.cast(shared_y, 'int32')              
    
#%%
def load_mnist_easy(data_type='probability'):
    """ Only use a subset of 0 and 1 from MNIST and downsample """
    
    filename = '../data/mnist_easy'
    
    if os.path.isfile(filename+'.npz'):
        
        dataset = np.load(filename+'.npz')
        
        if data_type in ['probability', 'threshold']:
            train_set = (dataset['train_data'], dataset['train_lbl'])
            valid_set = (dataset['valid_data'], dataset['valid_lbl'])
            test_set = (dataset['test_data'], dataset['test_lbl'])
        
        elif data_type == 'sampled':
            train_set = (dataset['train_sample_data'], dataset['train_lbl'])
            valid_set = (dataset['valid_sample_data'], dataset['valid_lbl'])
            test_set = (dataset['test_sample_data'], dataset['test_lbl'])
    
    else:
        
        def shrink_data(dataset, n_sample):
            
            # subsample to 14
            # then just drop first 2, last 2 rows/cols since mainly zero
            
            data, lbl = dataset
            
            new_data = np.zeros((2*n_sample, 10**2))
            new_lbl = np.concatenate((np.zeros((n_sample, )),
                                  np.ones((n_sample, )))).astype('int32')
        
            index0 = np.where(lbl == 0)[0][0:n_sample]
            index1 = np.where(lbl == 1)[0][0:n_sample]    
            index = np.concatenate((index0, index1))
            
            for i in range(new_data.shape[0]):
                img = Image.fromarray(data[index[i]].reshape((28, 28)))
                img_down = img.resize((14, 14))
                temp = np.asarray(img_down)
                temp = temp[:, 2:-2]
                temp = temp[2:-2]
                new_data[i] = temp.flatten()
                
            return (new_data, new_lbl)
        
        train_set, valid_set, test_set = load_mnist('probability')
        
        # Keep 1000, 0's and 1's from train
        train_set = shrink_data(train_set, 1000)
        
        # Keep 100, 0's and 1's from valid
        valid_set = shrink_data(valid_set, 100)
        
        # Keep 100, 0's and 1's from test
        test_set = shrink_data(test_set, 100)
        
        # do the sampling
        np_rng = np.random.RandomState(0)
        
        data = train_set[0]
        train_sample_data = 1.0*(data > np_rng.uniform(size=data.shape))
        
        data = valid_set[0]
        valid_sample_data = 1.0*(data > np_rng.uniform(size=data.shape))
        
        data = test_set[0]
        test_sample_data = 1.0*(data > np_rng.uniform(size=data.shape))
        
        # save dataset
        kwargs = {
                 'train_data' : train_set[0],
                 'train_lbl' : train_set[1],
                 'train_sample_data' : train_sample_data,
                 'valid_data' : valid_set[0],
                 'valid_lbl' : valid_set[1],
                 'valid_sample_data' : valid_sample_data,
                 'test_data' : test_set[0],
                 'test_lbl' : test_set[1],
                 'test_sample_data' : test_sample_data,                       
                 }
        np.savez_compressed(filename, **kwargs)
        
        if data_type == 'sampled':
            train_set = (train_sample_data, train_set[1])
            valid_set = (valid_sample_data, valid_set[1])
            test_set = (test_sample_data, test_set[1])
  
        
    if data_type == 'threshold':
        train_set = (1.0*(train_set[0] > 0.5), train_set[1])
        valid_set = (1.0*(valid_set[0] > 0.5), valid_set[1])
        test_set = (1.0*(test_set[0] > 0.5), test_set[1])
        
    
    return train_set, valid_set, test_set

#%%
def load_mnist(data_type='probability'):
    
    def prob_mnist():
        dataset = 'mnist.pkl.gz'
        filename = '../data/'+dataset
        
        if not os.path.isfile(filename):
            
            origin = 'http://www.iro.umontreal.ca/~lisa/deep/data/mnist/mnist.pkl.gz'
            print('Downloading data from %s' % origin)
            urllib.request.urlretrieve(origin, dataset)
            
        # Load the dataset
        with gzip.open(filename, 'rb') as f:
            try:
                train_set, valid_set, test_set = pickle.load(f, encoding='latin1')
            except:
                train_set, valid_set, test_set = pickle.load(f)
        # train_set, valid_set, test_set format: tuple(input, target)
        # input is a np.ndarray of 2 dimensions (a matrix)
        # where each row corresponds to an example. target is a
        # np.ndarray of 1 dimension (vector) that has the same length as
        # the number of rows in the input. It should give the target
        # to the example with the same index in the input.
        
        return train_set, valid_set, test_set
        
        
    if data_type in ['probability', 'threshold']:
        train_set, valid_set, test_set = prob_mnist()
        if data_type == 'threshold':
            train_X, train_y = train_set
            valid_X, valid_y = valid_set
            test_X, test_y = test_set
            
            train_X = 1.0*(train_X > 0.5)
            valid_X = 1.0*(valid_X > 0.5)
            test_X = 1.0*(test_X > 0.5)
    
            train_set = (train_X, train_y)
            valid_set = (train_X, train_y)
            test_set = (train_X, train_y)
        
    elif data_type == 'sampled':
        dataset = 'mnist_binary.npz'
        filename = '../data/'+dataset
        
        if not os.path.isfile(filename):
            
            basename = 'http://www.cs.toronto.edu/~larocheh/public/datasets/binarized_mnist/'
            file_train = basename+'binarized_mnist_train.amat'
            file_valid = basename+'binarized_mnist_valid.amat'
            file_test = basename+'binarized_mnist_test.amat'
            
            file_ls = [file_train, file_valid, file_test]
            dataset_ls = ['binarized_mnist_train.amat',
                          'binarized_mnist_valid.amat',
                          'binarized_mnist_test.amat']
            dataset_ls = ['../data/'+d for d in dataset_ls]
            
            for f, d in zip(file_ls, dataset_ls):
                print('Downloading data from %s' % f)
                urllib.request.urlretrieve(f, '../data/'+d)
            
            data = {}
            data['train_X'] = np.loadtxt(dataset_ls[0])
            data['valid_X'] = np.loadtxt(dataset_ls[1])
            data['test_X'] = np.loadtxt(dataset_ls[2])
            
            train_set, valid_set, test_set = prob_mnist()
            
            data['train_y'] = train_set[1]
            data['valid_y'] = valid_set[1]
            data['test_y'] = test_set[1]
                        
            np.savez_compressed(filename[:-4], **data)

            for d in dataset_ls:
                os.remove(d)
                
        else:
            
            data = np.load(filename)
            
        train_set = (data['train_X'], data['train_y'])
        valid_set = (data['valid_X'], data['valid_y'])
        test_set = (data['test_X'], data['test_y'])
    
    return train_set, valid_set, test_set
 
#%%
def prep_svhn_prob():
        
    # TODO: probably should make a more descriptive savename
    dataset_ls = ['train_32x32.mat', 'test_32x32.mat']
    file_ls = ['../data/'+d for d in dataset_ls]
    
    for f, d in zip(file_ls, dataset_ls):
        if not os.path.isfile(f):
            origin = 'http://ufldl.stanford.edu/housenumbers/'+d
            print('Downloading data from %s' % origin)
            urllib.request.urlretrieve(origin, f)
        
    train = io.loadmat(file_ls[0])
    test = io.loadmat(file_ls[1])
    
    # nothing super scientific about this choice
    # about half the size of the test data, leaves train as round number
    num_valid = 13257
    num_train = train['X'].shape[3]
    # need random split since original order is correlated
    np_rng = np.random.RandomState(0)
    valid_index = np_rng.choice(np.arange(num_train),
                                size=num_valid, replace=False)
    train_set = set(list(range(num_train)))
    valid_set = set(valid_index)
    train_index = np.array(list(train_set.difference(valid_set)))
    
    train_X = train['X'][:, :, :, train_index].astype(float)
    train_y = train['y'].flatten()[train_index]-1
    valid_X = train['X'][:, :, :, valid_index].astype(float)
    valid_y = train['y'].flatten()[valid_index]-1
    test_X = test['X'].astype(float)
    test_y = test['y'].flatten()-1
    
    # standardize scale
    train_X /= 255.0
    valid_X /= 255.0
    test_X /= 255.0
    
    # convert to samples, features order
    train_X = np.rollaxis(train_X, 3)
    valid_X = np.rollaxis(valid_X, 3)
    test_X = np.rollaxis(test_X, 3)
    train_X = train_X.reshape((-1, 32**2, 3))
    valid_X = valid_X.reshape((-1, 32**2, 3))
    test_X = test_X.reshape((-1, 32**2, 3))
    
    # https://en.wikipedia.org/wiki/Grayscale
    gray = [0.299, 0.587, 0.114]
    train_X = train_X.dot(gray)
    valid_X = valid_X.dot(gray)
    test_X = test_X.dot(gray)

    train_set = (train_X, train_y)
    valid_set = (valid_X, valid_y)
    test_set = (test_X, test_y)
    
    return train_set, valid_set, test_set

#%%
def prep_svhn_threshold():
    
    #https://en.wikipedia.org/wiki/Otsu%27s_method
    # just 0.5 threshold leads to terrible images
    
    filename = '../data/svhn_threshold.npz'
    
    if not os.path.isfile(filename):
        train_set, valid_set, test_set = prep_svhn_prob()
        train_X, train_y = train_set
        valid_X, valid_y = valid_set
        test_X, test_y = test_set
        
        thresh_train = np.zeros((train_X.shape[0], 1))
        thresh_valid = np.zeros((valid_X.shape[0], 1))
        thresh_test = np.zeros((test_X.shape[0], 1))
        
        for i in range(thresh_train.shape[0]):
            img = train_X[i]
            thresh_train[i, 0] = filters.threshold_otsu(img)
            
        for i in range(thresh_valid.shape[0]):
            img = valid_X[i]
            thresh_valid[i, 0] = filters.threshold_otsu(img)
            
        for i in range(thresh_test.shape[0]):
            img = test_X[i]
            thresh_test[i, 0] = filters.threshold_otsu(img)
            
        data = {}
        data['train_X'] = (train_X > thresh_train).astype('uint8')
        data['valid_X'] = (valid_X > thresh_valid).astype('uint8')
        data['test_X'] = (test_X > thresh_test).astype('uint8')
            
        data['train_y'] = train_y
        data['valid_y'] = valid_y
        data['test_y'] = test_y
        
        np.savez_compressed(filename[:-4], **data)
        
    else:
        data = np.load(filename)
    
    train_set = (data['train_X'], data['train_y'])
    valid_set = (data['valid_X'], data['valid_y'])
    test_set = (data['test_X'], data['test_y'])
    
    return train_set, valid_set, test_set
    

#%%
def prep_svhn_binary():
    
    raise NotImplementedError, 'need smarter sampling'
    
    filename = '../data/svhn_binary.npz'
    
    if not os.path.isfile(filename):
        train_set, valid_set, test_set = prep_svhn_prob()
        train_X, train_y = train_set
        valid_X, valid_y = valid_set
        test_X, test_y = test_set
        
        # save the binary version
        np_rng = np.random.RandomState(0)
    
        data = {}
        data['train_X'] = (train_X > np_rng.uniform(size=train_X.shape)).astype('uint8')
        data['valid_X'] = (valid_X > np_rng.uniform(size=valid_X.shape)).astype('uint8')
        data['test_X'] = (test_X > np_rng.uniform(size=test_X.shape)).astype('uint8')
            
        data['train_y'] = train_y
        data['valid_y'] = valid_y
        data['test_y'] = test_y
        
        np.savez_compressed(filename[:-4], **data)

#%%
def load_svhn(data_type='probability'):
    
    if data_type == 'probability':
        train_set, valid_set, test_set = prep_svhn_prob()
        
    elif data_type == 'threshold':
        train_set, valid_set, test_set = prep_svhn_threshold()
        
    elif data_type == 'binary':
        raise NotImplementedError
        #filename = '../data/svhn_binary.npz'
        #if not os.path.isfile(filename):
        #    prep_svhn_binary()
        #data = np.load(filename)
        #train_set = (data['train_X'], data['train_y'])
        #valid_set = (data['valid_X'], data['valid_y'])
        #test_set = (data['test_X'], data['test_y'])
    else:
        raise NotImplementedError
 
    return train_set, valid_set, test_set
  
#%%
def load_omniglot(data_type):
    
    # Omniglot only has thresholded data
    assert data_type == 'threshold'
    
    # see only my_data code (rotations?)
    raise NotImplementedError
    
#%%
def load_data(dataset, data_type):
    
    assert dataset in ['MNIST', 'MNIST_easy',  'SVHN', 'Omniglot']
    assert data_type in ['probability', 'sampled', 'threshold']
    
    if dataset == 'MNIST':
        train_set, valid_set, test_set = load_mnist(data_type)
    elif dataset == 'MNIST_easy':
        train_set, valid_set, test_set = load_mnist_easy(data_type)
    elif dataset == 'SVHN':
        train_set, valid_set, test_set = load_svhn(data_type)
    elif dataset == 'Omniglot':
        train_set, valid_set, test_set = load_omniglot(data_type)
    else:
        raise NotImplementedError
        
    train_x, train_y = train_set
    valid_x, valid_y = valid_set
    test_x, test_y = test_set
        
    train_x = np.clip(train_x, EPS, 1-EPS)
    valid_x = np.clip(valid_x, EPS, 1-EPS)
    test_x = np.clip(test_x, EPS, 1-EPS)
    
    # preps everyone for the output
    train_x, train_y = shared_dataset(train_x, train_y, name='train')    
    valid_x, valid_y = shared_dataset(valid_x, valid_y, name='valid')
    test_x, test_y = shared_dataset(test_x, test_y, name='test')
    
    return [(train_x, train_y), (valid_x, valid_y), (test_x, test_y)]

#%%
def create_train_valid(dataset, data_type,
                       IS_test_mode, IS_prob_train, IS_debug):

    if IS_prob_train:
        data_type_train = 'probability'
    else:
        data_type_train = data_type
    
    if IS_test_mode:
        train_set, valid_set, _ = load_data(dataset, data_type_train)
        td = train_set[0].get_value()
        vd = valid_set[0].get_value()
        train_data = np.concatenate((td, vd), axis=0).astype(FLOATX)
        train_data = theano.shared(train_data, name='train', borrow=True)
        
        # validation depends on options
        _, _, test_set = load_data(dataset, data_type)
        valid_data = test_set[0]
        
    else:
        train_set, _, _ = load_data(dataset, data_type_train)
        train_data = train_set[0]
        
        # validation depends on options
        _, valid_set, _ = load_data(dataset, data_type)
        valid_data = valid_set[0]
    
    if IS_debug:
        train_data = theano.shared(train_data[0:500].eval())
        valid_data = theano.shared(valid_data[0:500].eval())
            
    return train_data, valid_data
