#%%
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *

import os
import numpy as np
import copy
import pandas as pd

# The custom imports should be in same folder as this script
import __main__ as main
if not hasattr(main, '__file__'):
    # This means its an iPython instance
    import getpass
    usr = getpass.getuser()
    if usr == 'alexlangMacBookPro':
        path_to_script_folder = '/Users/alexlangMacBookPro/Dropbox/BitBucket/dbm/src/'
        os.chdir(path_to_script_folder)

# my files
import callbacks
from models import Model
from dbm import DBM
import utils
import my_data
import optimizers
              
#%%
def train_nnet(save_folder, dbm_init, dbm_train, data_details, optimizer):

    ########## makes a bunch of useful folders
    save_folder = utils.standard_folder(save_folder)
    weight_folder = utils.standard_folder(save_folder+'weights')
    optimizer_folder = utils.standard_folder(save_folder+'optimizer')
    sample_folder = utils.standard_folder(save_folder+'samples')
    plot_folder = utils.standard_folder(save_folder+'plots')
    
    utils.safe_make_folders(save_folder)
    os.makedirs(weight_folder)
    os.makedirs(optimizer_folder)
    os.makedirs(sample_folder)
    os.makedirs(plot_folder)
   
    
    ########## get the data and setup model structure
    
    utils.save_dict(save_folder + 'data_details.txt', data_details)
    train_data, valid_data = my_data.create_train_valid(**data_details)
    
    weight_file = dbm_init['weight_file']
    if weight_file is not None:
        weights_dict = utils.load_weights_hd5f(weight_file)
        init_data = None
    else:
        weights_dict = None
        init_data = train_data.get_value()
    
    ########## Create the model
    
    # save the parameters used to initialize
    dbm_init['batch_size'] = dbm_train['batch_size']
    if not dbm_init['IS_whitened']:
        dbm_init['white_type'] = None
        dbm_init['white_eps'] = None
    if not dbm_init['IS_var']:
        dbm_init['var_eps'] = None
    utils.save_dict(save_folder + 'dbm_init.txt', dbm_init)
    np.save(save_folder + 'dbm_init', dbm_init)
    
    del dbm_init['weight_file']
    dbm_init['weights_dict'] = weights_dict
    dbm_init['init_data'] = init_data
                
    dbm = DBM(**dbm_init)


    ########## setup optimizer
    
    n_samples = train_data.shape[0].eval()
    n_epoch = dbm_train['n_epoch']
    optimizer = optimizers.DefaultParameters(optimizer,
                                             n_epoch,
                                             n_samples,
                                             dbm_train['batch_size'],
                                             dbm_train['IS_decay'])
    
    kwargs = copy.deepcopy(optimizer)
    del kwargs['name']
    kwargs['nnet'] = dbm
          
    if optimizer['name'] == 'sgd':
        opt = optimizers.SGD(**kwargs) 
    elif optimizer['name'] == 'sgdr':
        opt = optimizers.SGDR(**kwargs)  
    elif optimizer['name'] == 'nadam':
        opt = optimizers.Nadam(**kwargs)  
    else:
        raise NotImplementedError
   
    utils.save_dict(save_folder + 'optimzier.txt', optimizer)
        
    ########## compile model
    
    utils.save_dict(save_folder + 'dbm_train.txt', dbm_train)
    
    model = Model(dbm,
                  opt,
                  n_gibbs_neg=dbm_train['n_gibbs_neg'],
                  L1=dbm_train['L1'],
                  L2=dbm_train['L2'],
                  lr_center=dbm_train['lr_center'],
                  flip_rate=dbm_train['flip_rate'])

    ########## callbacks
    
    # list of epochs to check on stuff
    # want a few more at start/end
    if dbm_train['IS_fast']:
        temp = [0, 1]
        fixed_ls = temp + [n_epoch-t for t in temp] + [n_epoch-2]
        n_runs = 100
        n_betas = 10000
    else:
        temp = [0, 1, 5, 10]
        fixed_ls = temp + [n_epoch-t for t in temp] + [n_epoch-2]
        # 30000 temps seems to give good results
        n_runs = 1000
        n_betas = 30000
    
    epoch_ls = list(set(list(range(0, n_epoch, 25)) + fixed_ls))
    
    cb_csv = callbacks.CSVLogger(save_folder + 'history.txt', append=False)
        
    cb_ais = callbacks.AISCallback(dbm,
                                   n_runs=n_runs,
                                   n_betas=n_betas,
                                   epoch_ls=epoch_ls)
    
    # long runs of gibbs sampling
    cb_sample1 = callbacks.SampleCallback(dbm,
                                          sample_folder + 'sample',
                                          n_chains=20,
                                          n_samples=10,
                                          plot_every=2000,
                                          epoch_ls=epoch_ls)
    
    # initial steps only 
    cb_sample2 = callbacks.SampleCallback(dbm,
                                          sample_folder + 'sample_init',
                                          n_chains=20,
                                          n_samples=10,
                                          plot_every=1,
                                          epoch_ls=epoch_ls)
 
    # periodically save the weights
    weight_path = weight_folder + 'weights.{epoch:04d}.hdf5'
    opt_path = optimizer_folder + 'opt_weights.{epoch:04d}.hdf5'
    cb_ps = callbacks.PeriodicSave(weight_path=weight_path,
                                   opt_path=opt_path,
                                   epoch_ls=epoch_ls)
    
    # make some plots at the end
    cb_plot = callbacks.PlotCallback(save_folder=plot_folder,
                                     csv_filepath=save_folder + 'history.txt')
    
    # records optimizer parameters
    cb_opt = callbacks.OptimizerSpy() 
    
    callback_list = [cb_plot, cb_ps, cb_opt, cb_csv]
    if not data_details['IS_debug']:
        callback_list = [cb_ais, cb_sample1, cb_sample2] + callback_list


    ########## Training
    history = model.fit(train_data,
                        batch_size=dbm_train['batch_size'],
                        n_epoch=n_epoch,
                        callbacks=callback_list,
                        validation_data=valid_data)
       
    
    ########## summarize the training results
    df = pd.read_csv(save_folder+'history.txt')
    try:
        val_prob = df['val_prob'].values
        HAS_best = True
        vp = val_prob[np.logical_not(np.isnan(val_prob))]
        best_val_prob = np.max(vp)
        best_epoch = np.where(best_val_prob == val_prob)[0].item()
    except KeyError:
        HAS_best = False
    
    with open(save_folder+'summary.txt', 'w') as f:
        total = model.fit_loop_time
        f.write('Number of epochs {}.\n'.format(n_epoch))
        if HAS_best:
            f.write('Best epoch is {}.\n'.format(best_epoch))
            f.write('Best log val prob is {}.\n'.format(best_val_prob))
        f.write('Total fit time was {} minutes.\n'.format(total/60.0))
        f.write('Per epoch total fit time was {} seconds.\n'.format(total/n_epoch))
        f.write('Fit time was {}% callbacks.\n'.format(model.fit_loop_callback_time/total*100))    


#%%
if __name__ == '__main__':
    
    save_folder = '../results/test'
    
    # parameters to create static dbm
    dbm_init = {
                'layer_size_list'   : [1024, 1024],
                'topology_dict'     : {0:{1}},
                'residual_dict'     : {},
                'dropout_prob_list' : [0.0, 0.0],
                'IS_mean_field'     : True,
                'IS_persist'        : True,
                'IS_centered'       : True,
                'IS_var'            : False,
                'IS_whitened'       : True,
                'var_eps'           : 1e-1,
                'white_type'        : 'ZCA',
                'white_eps'         : 1e-3, # regularizers whitening
                'weight_file'       : None, # Needed to load existing weights
                }
    
    # parameters to train dbm
    dbm_train = {
                 'n_epoch'      : 10,
                 'batch_size'   : 100, 
                 'n_gibbs_neg'  : 5,
                 'lr_center'    : 0.01,
                 'L1'           : 0.0,
                 'L2'           : 1e-4,
                 'IS_decay'     : True,
                 'flip_rate'    : 1e-3, # percentage of raw input pixels to flip
                 'IS_fast'      : True, # cuts down on callback time
                 }
                
    # parameters of dataset
    data_details = {
                    'dataset'       : 'SVHN',
                    'data_type'     : 'threshold',
                    'IS_prob_train' : True, # train on prob, regardless of valid type
                    'IS_test_mode'  : False, # whether valid set is actual test set
                    'IS_debug'      : False, # trains a smaller nnet
                    }
    
    optimizer = {
                 'name'         : 'sgd',
                 'lr'           : 0.01,
                 'momentum'     : 0.0,
                 'nesterov'     : True,
                 'beta'         : 1.0,
                 'decay'        : 1.8e-5,
                 'schedule_decay' : 0.004,
                 }
    
    train_nnet(save_folder, dbm_init, dbm_train, data_details, optimizer)
