#%% Imports
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *

import os
import shutil
import numpy as np
from collections import OrderedDict

import theano.tensor as T

# The custom imports should be in same folder as this script
import __main__ as main
if not hasattr(main, '__file__'):
    # This means its an iPython instance
    import getpass
    usr = getpass.getuser()
    if usr == 'alexlangMacBookPro':
        path_to_script_folder = '/Users/alexlangMacBookPro/Dropbox/BitBucket/dbm/src/'
        os.chdir(path_to_script_folder)

# import from my files
from utils import make_shared, FLOATX, EPS
from components import Synapse, Layer
from dbm import DBM
from ais import AIS

from TEST_simple_nnet import default_train_test

# Use unittest- https://docs.python.org/2/library/unittest.html

#%%
def synapse_tests(save_folder_base):
    """ Test all the functions in synapse """
    pass

#%%
def layer_tests(save_folder_base):
    """ Test all the functions in layer """
    pass

#%%
def dbm_tests(save_folder_base):
    """ Test all the functions in dbm """
    pass

#%%
def dataset_tests(save_folder_base):
    """ make sure matches type, expected stats """
    pass

#%%
def optimizers_tests(save_folder_base):
    """ create and get blank updates """
    pass

#%%
def ais_tests(save_folder_base):
    """ just initialize ais """
    pass

#%%
def std_tests(save_folder_base):
    """ See that std does make stats whiter """
    
    dbm_init, dbm_train, data_details, optimizer = default_train_test()
    dbm_init['IS_centered'] = True
    dbm_init['IS_std'] = True
    dbm_init['std_eps'] = 1e-2
            
    

    # 1. initialize dbm
    # 2. show that stats are whiter
    # 3. train dbm, debug mode
    #       - load up stats, see if gives hint at better init std

#%%
def white_tests(save_folder_base):
    """ See that white does make stats whiter """
    
    dbm_init, dbm_train, data_details, optimizer = default_train_test()
    dbm_init['IS_centered'] = True
    dbm_init['IS_whitened'] = True
    dbm_init['white_type'] = 'ZCA'
    dbm_init['white_eps'] = 1e-3


    # 1. initialize dbm
    # 2. show that stats are whiter
    # 3. train dbm, debug mode
    #       - load up stats, see if gives hint at better init cov

#%%
if __name__ == '__main__':
    
    save_folder_base = '../results/test_units/'
    if os.path.exists(save_folder_base):
        shutil.rmtree(save_folder_base)
    os.mkdir(save_folder_base)
    
    synapse_tests(save_folder_base)
    layer_tests(save_folder_base)
    dbm_tests(save_folder_base)
    dataset_tests(save_folder_base)
    optimizers_tests(save_folder_base)
    ais_tests(save_folder_base)
    std_tests(save_folder_base)
    white_tests(save_folder_base)
    