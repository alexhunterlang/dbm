#%% imports
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *

import os
import numpy as np
import theano
import theano.tensor as T

# The custom imports should be in same folder as this script
import __main__ as main
if not hasattr(main, '__file__'):
    # This means its an iPython instance
    import getpass
    usr = getpass.getuser()
    if usr=='alexlangMacBookPro':
        path_to_script_folder = '/Users/alexlangMacBookPro/Dropbox/BitBucket/dbm/src/'
        os.chdir(path_to_script_folder)

from dbm import DBM
import run_dbm
import matplotlib.pyplot as plt

FLOATX = theano.config.floatX # should be FLOATX32 for GPU
EPS = np.finfo(FLOATX).eps
              
#%%
if __name__=='__main__':
    
    param_npy = '/Users/alexlangMacBookPro/Dropbox/BitBucket/dbm/results/test_8/param.npy'
    weight_file = '/Users/alexlangMacBookPro/Dropbox/BitBucket/dbm/results/test_8/weights/weights.0009.hdf5'
    dbm = DBM.init_file(param_npy, weight_file)
    
    train_data, valid_data = run_dbm.prep_data('MNIST', 'probability', True, False, True)
    
    flip_rate = 1e-3
    init_data = train_data.get_value()
    r = np.random.uniform(size=init_data.shape)
    f = (r<flip_rate)
    X = (init_data*(1-f) + (1-init_data)*f).astype(FLOATX)
    
    x = T.matrix()
    IS_dropout = T.scalar()
    beta = T.scalar()
    prob_data, _, updates = dbm.pos_stats(x,IS_dropout,beta)
    norm_data = dbm.apply_datanorm(prob_data)
    center_data = dbm.apply_center(prob_data)
    std_data = dbm.apply_std(prob_data)
    
    fn_p = theano.function([x,IS_dropout,beta],prob_data,updates=updates)
    fn_n = theano.function([x,IS_dropout,beta],norm_data,updates=updates)
    fn_c = theano.function([x,IS_dropout,beta],center_data,updates=updates)
    fn_s = theano.function([x,IS_dropout,beta],std_data,updates=updates)
    norm_ls = fn_n(X,0,1)
    prob_ls = fn_p(X,0,1)
    center_ls = fn_c(X,0,1)
    std_ls = fn_s(X,0,1)
    
    n = np.concatenate((norm_ls),axis=1)
    p = np.concatenate((prob_ls),axis=1)
    c = np.concatenate((center_ls),axis=1)
    s = np.concatenate((std_ls),axis=1)
    
    c_n = np.cov(n.T)
    plt.imshow(c_n,cmap='hot')
    c_p = np.cov(p.T)
    plt.imshow(c_p,cmap='hot')
    
    m_p = np.mean(p,axis=0)
    s_p = np.std(p,axis=0)
    m_n = np.mean(n,axis=0)
    s_n = np.std(n,axis=0)
    m_c = np.mean(c,axis=0)
    s_c = np.std(c,axis=0)
    m_s = np.mean(s,axis=0)
    s_s = np.std(s,axis=0)

    plt.hist(m_p, bins=100)
    plt.hist(m_n, bins=100)
    plt.hist(s_p, bins=100)
    plt.hist(s_n, bins=100)

    plt.scatter(m_p,s_p)
    plt.scatter(m_n,s_n)
    
    plt.scatter(m_p[0:784],s_p[0:784])
    plt.scatter(m_n[0:784],s_n[0:784])
    plt.scatter(m_c[0:784],s_c[0:784])
    plt.scatter(m_s[0:784],s_s[0:784])
    
    plt.scatter(m_p[784:],s_p[784:])
    plt.scatter(m_n[784:],s_n[784:])
    plt.scatter(m_c[784:],s_c[784:])
    plt.scatter(m_s[784:],s_s[784:])
