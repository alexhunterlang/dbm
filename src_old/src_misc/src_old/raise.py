"""Tools for estimating the partition function of an RBM

Author: Alex Lang, alexhunterlang@gmail.com

Based on code from: Ian Goodfellow, https://github.com/lisa-lab/pylearn2

The spiritual ancestor of this code is Pylearn2. First major change was to 
simplify the code to take advantage of my specific dbm structure. Second
major change was to make the for loop through temperatures a scan which
cut runtime in half.
"""
    
#%% imports
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *

import os
import numpy as np

from theano.compat.six.moves import xrange
import theano
import theano.tensor as T

# The custom imports should be in same folder as this script
import __main__ as main
if not hasattr(main, '__file__'):
    # This means its an iPython instance
    import getpass
    usr = getpass.getuser()
    if usr=='alexlangMacBookPro':
        path_to_script_folder = '/Users/alexlangMacBookPro/Dropbox/BitBucket/dbm/src/'
        os.chdir(path_to_script_folder)

from utils import make_shared, binomial_sample
from dbm import DBM

FLOATX = theano.config.floatX
EPS = np.finfo(FLOATX).eps

#%%
def logsum(x):
    # This prevents overflow
    a = np.max(x)
    ls = a + np.log(np.sum(np.exp(x-a)))
    return ls

#%%
def logdiff(x):
    # This prevents overflow
    assert x.shape[0]==2
    a = np.max(x)
    ls = a + np.log(np.diff(np.exp(x-a)))
    return ls.item()

#%%
class RAISE(object):
    """
    Compute the log RAISE weights to approximate a ratio of partition functions.

    """     
    def __init__(self, dbm, n_runs, n_betas, data, betas = None):
        
        self.n_runs = n_runs
        self.n_betas = n_betas
        self.theano_rng = dbm.theano_rng
        
        # this is the final model
        self.dbm_b = DBM(layer_size_list = dbm.layer_size_list,
                         topology_dict = dbm.topology_dict,
                         IS_mean_field = True,
                         IS_persist = False,
                         IS_centered = False,
                         theano_rng = dbm.theano_rng,
                         vis_mean = None)
        
        # transfer all weights and bias for now
        for i, layer in enumerate(self.dbm_b.layers):
            layer.W_ls = dbm.layers[i].W_ls
            layer.W_T_ls = dbm.layers[i].W_T_ls
            layer.b = dbm.layers[i].b
    
        # this corrects for centering
        if dbm.IS_centered:
            for i, layer in enumerate(self.dbm_b.layers):
                try:
                    index_W_T = list(self.dbm_b.topology_dict[i])
                    for j, W in zip(index_W_T, layer.W_T_ls):
                        layer.b -= T.dot(dbm.layers[j].c, W)
                except KeyError:
                    pass
                
                try:
                    index_W = list(self.dbm_b.topology_input_dict[i])
                    for j, W in zip(index_W, layer.W_ls):
                        layer.b -= T.dot(dbm.layers[j].c, W)
                except KeyError:
                    pass
        
        vis_mean = np.clip(np.mean(data,axis=0), EPS, 1-EPS)
        # this is the base rate model Ruslan uses
        p_ruslan = (vis_mean + 0.05)/1.05
        b = np.log(p_ruslan/(1-p_ruslan)).astype(FLOATX)
        self.b0_a = theano.shared(b, name='b0_a', borrow=True)
        
        # make the initial sample
        p0 = np.tile(1. / (1 + np.exp(-b)), (n_runs, 1))
        s0 = np.array(p0 > np.random.random_sample(p0.shape), dtype=FLOATX)
        sample_ls = [s0]
        for n in self.dbm_b.layer_size_list[1:]:
            p = 0.5*np.ones((self.n_runs, n))
            s = binomial_sample(self.theano_rng, p.astype(FLOATX))
            sample_ls.append(theano.shared(s.eval(), borrow=True))
        self.sample_ls = sample_ls
        
        # initialize log importance weights
        self.log_ais_w = np.zeros((n_runs,),dtype=FLOATX)

        if betas is not None:
            assert betas.size == n_betas
        else:
            betas = np.linspace(0,1,n_betas)
            # this is similar to the proportions used by Ruslan
#            num_low = int(0.035*n_betas)
#            num_mid = int(0.275*n_betas)
#            num_high = n_betas - num_low - num_mid
#            betas = np.hstack((np.linspace(0, 0.5, num_low),
#                           np.linspace(0.5, 0.9, num_mid),
#                           np.linspace(0.9, 1, num_high)))
            
        
        self.betas = make_shared(betas, name='betas')

        # utility function for safely computing log-mean of the ais weights
        ais_w = T.vector()
        dlogz = T.log(T.mean(T.exp(ais_w - T.max(ais_w)))) + T.max(ais_w)
        self.log_mean = theano.function([ais_w], dlogz,
                                        allow_input_downcast=False)

    def ais_free_energy(self, beta, sample_ls):
        """
        Computes the free-energy of visible unit configuration `sample_ls`,
        according to the interpolating distribution at temperature beta.
        The interpolating distributions are given by
        :math:`p_a(v)^{1-\\beta} p_b(v)^\\beta`.
        See equation 10, of Salakhutdinov & Murray 2008.
    
        Parameters
        ----------
        beta : int
            Inverse temperature at which to compute the free-energy.
        sample_ls : list of samples
            Matrix whose rows indexes into the minibatch, and columns into
            the data dimensions.
    
        Returns
        -------
        f : float (scalar)
           Free-energy of configuration `sample_ls` given by the
           interpolating distribution at temperature beta.
        """
    
        
        fe_a_odd = np.log(2)*np.sum(self.dbm_b.layer_size_list[1::2])
        fe_a = -(1-beta)*T.dot(sample_ls[0],self.b0_a)-T.cast(fe_a_odd,dtype=FLOATX)
        fe_b = self.dbm_b.free_energy_given_h(sample_ls, beta = beta)
        return fe_a + fe_b
    
    def ais_sample(self, beta, sample_ls):
        """
        Parameters
        ----------        
        beta : theano.shared
            Scalar, represents inverse temperature at which we wish to sample from.
    
        sample_ls : list of samples
            Matrix of shape (n_runs, nvis), state of current particles.
        """
        
        prob_ls = len(sample_ls)*[None]
    
        # equation 15 (Salakhutdinov & Murray 2008)
        # only need an odd parity update
        
        # base rate samples dont matter since multiplied by zero
     
        # only need an odd parity update
        _, sample_b = self.dbm_b.parity_update(prob_ls, sample_ls, 1,
                                               IS_prob_input = False,
                                               hold_constant = [],
                                               IS_dropout = 0.0,
                                               beta = beta)
    
        # equation 17 (Salakhutdinov & Murray 2008)
        # updates the even states
            
        z_b = []
        for layer in self.dbm_b.layers:
            z = layer.get_input(sample_b, mean_field=True, direction='both')
            z_b.append(z)

        sample_ls = len(sample_ls)*[None]
        for i,z in enumerate(z_b):
            if i==0:
                p = T.nnet.sigmoid((1-beta)*self.b0_a + beta*z)
                sample_ls[i] = binomial_sample(self.theano_rng, p)
            elif np.mod(i,2)==1:
                # these get ignored, just need to be a placeholder in list
                sample_ls[i] = sample_b[i]
            else:
                p = T.nnet.sigmoid(beta*z)
                sample_ls[i] = binomial_sample(self.theano_rng, p)
    
        return sample_ls

    def ais_update(self, *args):
        log_ais_w = args[0]
        index = args[1]
        sample_ls = args[2:]
        
        bp, bp1 = self.betas[index], self.betas[index + 1]
        # log-ratio of (free) energies for two nearby temperatures
        fe0 = self.ais_free_energy(bp, sample_ls)
        fe1 = self.ais_free_energy(bp1, sample_ls)
        log_ais_w += fe0 - fe1            
                    
        # generate a new sample at temperature beta_{i+1}
        sample_ls = self.ais_sample(bp1, sample_ls)
        
        index += 1
        
        output = [log_ais_w, index]+sample_ls
        
        return output

    def run(self):
        """
        Performs the grunt-work, implementing

        .. math::

            log\:w^{(i)} += \mathcal{F}_{k-1}(v_{k-1}) - \mathcal{F}_{k}(v_{k-1})

        recursively for all temperatures.
        """
        
        # initial sample
        sample_ls = self.sample_ls
            
        log_ais_w = make_shared(self.log_ais_w, name='log_ais_w')
        index = theano.shared(np.int32(0),name='index', borrow=True)
            
        output_ls, updates = theano.scan(fn = self.ais_update, 
                                        outputs_info = [log_ais_w, index]+sample_ls, 
                                        n_steps = self.n_betas - 1,
                                        name = 'scan_ais')

        ais_fn = theano.function([], output_ls[0][-1], updates=updates)
        
        self.log_ais_w = ais_fn()

    def log_z(self):
        
        # gather statistics
        self.run()
        
        # this is the mean factor from the weights
        #r_AIS = self.log_mean(self.log_ais_w)
        r_AIS = logsum(self.log_ais_w) - np.log(self.n_runs)
        
        # this is the standard deviation
        aa = np.max(self.log_ais_w)
        logstd_AIS = np.log(np.std(np.exp(self.log_ais_w-aa))) + aa - np.log(self.n_runs)/2.0
    
        # log Z = log_za + r_AIS
        self.log_za = np.sum(self.dbm_b.layer_size_list[1:])*np.log(2)
        self.log_za += np.sum(np.log(1 + np.exp(self.b0_a.eval())))
        
        self.logZ_est = self.log_za + r_AIS
        
        #self.logZ_mid = np.median(self.log_ais_w)
        #self.logZ_est_up = np.percentile(self.log_ais_w, 95)
        #self.logZ_est_down = np.percentile(self.log_ais_w, 5)
        
        # find +- 3 std
        l_input = np.asarray([np.log(3)+logstd_AIS, r_AIS])
        self.logZ_est_up = logsum(l_input) + self.log_za
    
        logZ_est_down = logdiff(l_input) + self.log_za
        if np.isnan(logZ_est_down):
            logZ_est_down = 0
        self.logZ_est_down = logZ_est_down
        
        return self.logZ_est, self.logZ_est_up, self.logZ_est_down
