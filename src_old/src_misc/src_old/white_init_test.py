#%%
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *

import os
import numpy as np
import theano
import theano.tensor as T
import copy
import pandas as pd

# The custom imports should be in same folder as this script
import __main__ as main
if not hasattr(main, '__file__'):
    # This means its an iPython instance
    import getpass
    usr = getpass.getuser()
    if usr=='alexlangMacBookPro':
        path_to_script_folder = '/Users/alexlangMacBookPro/Dropbox/BitBucket/dbm/src/'
        os.chdir(path_to_script_folder)

import callbacks
from models import Model
from dbm import DBM
import utils
import my_data
import optimizers
import matplotlib.pyplot as plt

FLOATX = theano.config.floatX # should be FLOATX32 for GPU
EPS = np.finfo(FLOATX).eps

              
#%%
def prep_data(dataset, data_type, IS_test_mode, IS_debug, IS_prob_train):
    
    if isinstance(dataset, dict):
        train_data = dataset['train_data']
        valid_data = dataset['valid_data']
    
    else:
        
        if IS_prob_train:
            data_type_train = 'probability'
        else:
            data_type_train = data_type
        
        if IS_test_mode:
            train_set, valid_set, _ = my_data.load_data(dataset, data_type_train)
            td = train_set[0].get_value()
            vd = valid_set[0].get_value()
            train_data = np.concatenate((td,vd),axis=0).astype(FLOATX)
            train_data = theano.shared(train_data, name = 'train', borrow=True)
            
            # validation depends on options
            _, _, test_set = my_data.load_data(dataset, data_type)
            valid_data = test_set[0]
            
        else:
            train_set, _, _ = my_data.load_data(dataset, data_type_train)
            train_data = train_set[0]
            
            # validation depends on options
            _, valid_set, _ = my_data.load_data(dataset, data_type)
            valid_data = valid_set[0]
        
        if IS_debug:
            train_data = theano.shared(train_data[0:2*batch_size].eval())
            valid_data = theano.shared(valid_data[0:2*batch_size].eval())
            
    return train_data, valid_data
              
              
#%%
if __name__=='__main__':
    
    
    layer_size_list = [784] + [500, 500]
    dropout_prob_list = [0.0]*len(layer_size_list)
    topology_dict = {0:{1}, 1:{2}}
    residual_dict = {}
    
    weight_file = None
    dataset = 'MNIST'
    data_type = 'threshold'
    IS_mean_field = True
    IS_persist = True
    IS_centered = True
    batch_size = 100    
    flip_noise = 1e-3 # 1e-3
    
    IS_debug = False
    IS_test_mode = True
    IS_prob_train = True
    
    
    train_data, valid_data = prep_data(dataset, data_type,
                                       IS_test_mode, IS_debug, IS_prob_train)
    
    # setup model structure
    if weight_file is not None:
        weights_dict = utils.load_weights_hd5f(weight_file)
        init_data = None
    else:
        weights_dict = None
        init_data = train_data.get_value()
    
    ##### Create the model
    dbm = DBM(layer_size_list, topology_dict,
              IS_mean_field = IS_mean_field,
              IS_persist = IS_persist,
              IS_centered = IS_centered,
              dropout_prob_list = dropout_prob_list,
              batch_size = batch_size,
              weights_dict = weights_dict,
              theano_rng = None,
              init_data = init_data,
              init='orthogonal')
    
    ####
    # summary initial stats
    W_orig = dbm.synapses_ls[0].W.get_value()
    
    flip = 1.0*(np.random.uniform(size=init_data.shape)<flip_noise)
    flip_data = init_data*(1-flip) + (1-init_data)*flip
    flip_sample = 1.0*(flip_data>np.random.uniform(size=flip_data.shape))
    flip_data = flip_data.astype(FLOATX)
    flip_sample = flip_sample.astype(FLOATX)
    sample = 1.0*(init_data>np.random.uniform(size=flip_data.shape)).astype(FLOATX)
    
    ####
    # test plain
    x = T.matrix()
    IS_dropout = T.scalar()
    beta = T.scalar()
    prob_data, sample_data, updates = dbm.pos_stats(x, IS_dropout, beta)
    fn = theano.function([x,IS_dropout,beta],prob_data,updates=updates)
    
    out_sample = fn(sample,0,1)
    #out_data = fn(init_data,0,1)
    out_flip_sample = fn(flip_sample,0,1)
    #out_flip_data = fn(flip_data,0,1)
    
    out_sample_all = np.concatenate((out_sample[0],out_sample[1],out_sample[2]),axis=1)
    c_sample = np.cov(out_sample_all.T)    
    c0_sample = c_sample[0:784,0:784]
    c1_sample = c_sample[784:1284,784:1284]
    c2_sample = c_sample[1284:,1284:]
    c01_sample = c_sample[0:784,784:1284]
    c12_sample = c_sample[784:1284,1284:]
    c02_sample = c_sample[0:784,1284:]
    
    plt.imshow(c_sample,cmap='hot',interpolation='none')
    plt.hist(c0_sample.flatten(),bins=100)
    plt.hist(c1_sample.flatten(),bins=100)
    plt.hist(c2_sample.flatten(),bins=100)
    plt.hist(c01_sample.flatten(),bins=100)
    plt.hist(c12_sample.flatten(),bins=100)
    plt.hist(c02_sample.flatten(),bins=100)
    
    out_flip_sample_all = np.concatenate((out_flip_sample[0],
                                          out_flip_sample[1],
                                        out_flip_sample[2]),axis=1)
    c_flip_sample = np.cov(out_flip_sample_all.T)    
    c0_flip_sample = c_flip_sample[0:784,0:784]
    c1_flip_sample = c_flip_sample[784:1284,784:1284]
    c2_flip_sample = c_flip_sample[1284:,1284:]
    c01_flip_sample = c_flip_sample[0:784,784:1284]
    c12_flip_sample = c_flip_sample[784:1284,1284:]
    c02_flip_sample = c_flip_sample[0:784,1284:]
    
    plt.imshow(c_flip_sample,cmap='hot',interpolation='none')
    plt.hist(c0_flip_sample.flatten(),bins=100)
    plt.hist(c1_flip_sample.flatten(),bins=100)
    plt.hist(c2_flip_sample.flatten(),bins=100)
    plt.hist(c01_flip_sample.flatten(),bins=100)
    plt.hist(c12_flip_sample.flatten(),bins=100)
    plt.hist(c02_flip_sample.flatten(),bins=100)
    
    ####
    # test pca
    PCA = my_data.make_PCA_matrix(c0_flip_sample)
    W = PCA.dot(W_orig)
    dbm.synapses_ls[0].W.set_value(W)    
    
    x = T.matrix()
    IS_dropout = T.scalar()
    beta = T.scalar()
    prob_data, sample_data, updates = dbm.pos_stats(x, IS_dropout, beta)
    fn = theano.function([x,IS_dropout,beta],prob_data,updates=updates)
    
    out_sample_pca = fn(sample,0,1)
    #out_data = fn(init_data,0,1)
    out_flip_sample_pca = fn(flip_sample,0,1)
    #out_flip_data = fn(flip_data,0,1)
    
    out_sample_all_pca = np.concatenate((out_sample_pca[0],out_sample_pca[1],out_sample_pca[2]),axis=1)
    c_sample_pca = np.cov(out_sample_all_pca.T)    
    c0_sample_pca = c_sample_pca[0:784,0:784]
    c1_sample_pca = c_sample_pca[784:1284,784:1284]
    c2_sample_pca = c_sample_pca[1284:,1284:]
    c01_sample_pca = c_sample_pca[0:784,784:1284]
    c12_sample_pca = c_sample_pca[784:1284,1284:]
    c02_sample_pca = c_sample_pca[0:784,1284:]
    
    plt.imshow(c_sample_pca,cmap='hot',interpolation='none')
    plt.hist(c0_sample_pca.flatten(),bins=100)
    plt.hist(c1_sample_pca.flatten(),bins=100)
    plt.hist(c2_sample_pca.flatten(),bins=100)
    plt.hist(c01_sample_pca.flatten(),bins=100)
    plt.hist(c12_sample_pca.flatten(),bins=100)
    plt.hist(c02_sample_pca.flatten(),bins=100)
    
    out_flip_sample_all_pca = np.concatenate((out_flip_sample_pca[0],
                                          out_flip_sample_pca[1],
                                        out_flip_sample_pca[2]),axis=1)
    c_flip_sample_pca = np.cov(out_flip_sample_all_pca.T)    
    c0_flip_sample_pca = c_flip_sample_pca[0:784,0:784]
    c1_flip_sample_pca = c_flip_sample_pca[784:1284,784:1284]
    c2_flip_sample_pca = c_flip_sample_pca[1284:,1284:]
    c01_flip_sample_pca = c_flip_sample_pca[0:784,784:1284]
    c12_flip_sample_pca = c_flip_sample_pca[784:1284,1284:]
    c02_flip_sample_pca = c_flip_sample_pca[0:784,1284:]
    
    plt.imshow(c_flip_sample_pca,cmap='hot',interpolation='none')
    plt.hist(c0_flip_sample_pca.flatten(),bins=100)
    plt.hist(c1_flip_sample_pca.flatten(),bins=100)
    plt.hist(c2_flip_sample_pca.flatten(),bins=100)
    plt.hist(c01_flip_sample_pca.flatten(),bins=100)
    plt.hist(c12_flip_sample_pca.flatten(),bins=100)
    plt.hist(c02_flip_sample_pca.flatten(),bins=100)
   
    np.std(c1_flip_sample_pca)
    np.std(c1_flip_sample)
    
    np.std(c2_flip_sample_pca)
    np.std(c2_flip_sample)
    
    np.std(c01_flip_sample_pca)
    np.std(c01_flip_sample)
    
    np.std(c12_flip_sample_pca)
    np.std(c12_flip_sample)
    
    np.std(c02_flip_sample_pca)
    np.std(c02_flip_sample)
    
    ####
    # test pca corr
    corr = np.corrcoef(flip_sample.T)
    var = np.var(flip_sample,axis=0)
    PCA_corr = my_data.make_PCA_corr_matrix(corr, var)
    W = PCA_corr.dot(W_orig)
    dbm.synapses_ls[0].W.set_value(W)    
    
    x = T.matrix()
    IS_dropout = T.scalar()
    beta = T.scalar()
    prob_data, sample_data, updates = dbm.pos_stats(x, IS_dropout, beta)
    fn = theano.function([x,IS_dropout,beta],prob_data,updates=updates)
    
    out_sample_pca_corr = fn(sample,0,1)
    #out_data = fn(init_data,0,1)
    out_flip_sample_pca_corr = fn(flip_sample,0,1)
    #out_flip_data = fn(flip_data,0,1)
    
    out_sample_all_pca_corr = np.concatenate((out_sample_pca_corr[0],
                                              out_sample_pca_corr[1],
                                             out_sample_pca_corr[2]),axis=1)
    c_sample_pca_corr = np.cov(out_sample_all_pca_corr.T)    
    c0_sample_pca_corr = c_sample_pca_corr[0:784,0:784]
    c1_sample_pca_corr = c_sample_pca_corr[784:1284,784:1284]
    c2_sample_pca_corr = c_sample_pca_corr[1284:,1284:]
    c01_sample_pca_corr = c_sample_pca_corr[0:784,784:1284]
    c12_sample_pca_corr = c_sample_pca_corr[784:1284,1284:]
    c02_sample_pca_corr = c_sample_pca_corr[0:784,1284:]
    
    plt.imshow(c_sample_pca_corr,cmap='hot',interpolation='none')
    plt.hist(c0_sample_pca_corr.flatten(),bins=100)
    plt.hist(c1_sample_pca_corr.flatten(),bins=100)
    plt.hist(c2_sample_pca_corr.flatten(),bins=100)
    plt.hist(c01_sample_pca_corr.flatten(),bins=100)
    plt.hist(c12_sample_pca_corr.flatten(),bins=100)
    plt.hist(c02_sample_pca_corr.flatten(),bins=100)
    
    out_flip_sample_all_pca_corr = np.concatenate((out_flip_sample_pca_corr[0],
                                                   out_flip_sample_pca_corr[1],
                                                    out_flip_sample_pca_corr[2]),axis=1)
    c_flip_sample_pca_corr = np.cov(out_flip_sample_all_pca_corr.T)    
    c0_flip_sample_pca_corr = c_flip_sample_pca_corr[0:784,0:784]
    c1_flip_sample_pca_corr = c_flip_sample_pca_corr[784:1284,784:1284]
    c2_flip_sample_pca_corr = c_flip_sample_pca_corr[1284:,1284:]
    c01_flip_sample_pca_corr = c_flip_sample_pca_corr[0:784,784:1284]
    c12_flip_sample_pca_corr = c_flip_sample_pca_corr[784:1284,1284:]
    c02_flip_sample_pca_corr = c_flip_sample_pca_corr[0:784,1284:]
    
    plt.imshow(c_flip_sample_pca_corr,cmap='hot',interpolation='none')
    plt.hist(c0_flip_sample_pca_corr.flatten(),bins=100)
    plt.hist(c1_flip_sample_pca_corr.flatten(),bins=100)
    plt.hist(c2_flip_sample_pca_corr.flatten(),bins=100)
    plt.hist(c01_flip_sample_pca_corr.flatten(),bins=100)
    plt.hist(c12_flip_sample_pca_corr.flatten(),bins=100)
    plt.hist(c02_flip_sample_pca_corr.flatten(),bins=100)
   
    np.std(c1_flip_sample_pca_corr)
    np.std(c1_flip_sample_pca)
    np.std(c1_flip_sample)
    
    np.std(c2_flip_sample_pca_corr)
    np.std(c2_flip_sample_pca)
    np.std(c2_flip_sample)
    
    np.std(c01_flip_sample_pca_corr)
    np.std(c01_flip_sample_pca)
    np.std(c01_flip_sample)
    
    np.std(c12_flip_sample_pca_corr)
    np.std(c12_flip_sample_pca)
    np.std(c12_flip_sample)
    
    np.std(c02_flip_sample_pca_corr)
    np.std(c02_flip_sample_pca)
    np.std(c02_flip_sample)

    