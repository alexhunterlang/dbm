#%% Imports 
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *

import numpy as np
import theano
import os
from collections import OrderedDict
import h5py

# The custom imports should be in same folder as this script
import __main__ as main
if not hasattr(main, '__file__'):
    # This means its an iPython instance
    import getpass
    usr = getpass.getuser()
    if usr=='alexlangMacBookPro':
        path_to_script_folder = '/Users/alexlangMacBookPro/Dropbox/BitBucket/dbm/src/'
        os.chdir(path_to_script_folder)

FLOATX = theano.config.floatX # should be FLOATX32 for GPU
EPS = np.finfo(FLOATX).eps
              
#%%
class NeuralNet(object):
           
    def __init__(self):
        self.layers = []
        self.synapses = []
        self._updates = OrderedDict()
        self.layer_indices = {}
    
    def get_layer(self, name=None, index=None):
        """Returns a layer based on either its name (unique)
        or its index in the graph. Indices are based on
        order of horizontal graph traversal (bottom-up).
        # Arguments
            name: String, name of layer.
            index: Integer, index of layer.
        # Returns
            A layer instance.
        """
        # It would be unreliable to build a dictionary
        # based on layer names, because names can potentially
        # be changed at any point by the user
        # without the container being notified of it.
        if index is not None:
            if len(self.layers) <= index:
                raise ValueError('Was asked to retrieve layer at index ' +
                                 str(index) + ' but model only has ' +
                                 str(len(self.layers)) + ' layers.')
            else:
                return self.layers[index]
        else:
            assert name, 'Provide either a layer name or layer index.'
        layer = None
        for layer in self.layers:
            if layer.name == name:
                return layer
        if not layer:
            raise ValueError('No such layer: ' + name)

    @property
    def updates(self):
        updates = self._updates
        parts_ls = self.layers + self.synapses
        
        for part in parts_ls:
            keys = updates.keys()
            for k,v in part.updates.items():
                if k in keys:
                    assert k.name.endswith(('_b','_W')), 'Already have update for '+k.name
                    v_old = updates[k]
                    updates[k] = v + v_old
                else:
                    updates[k] = v
                       
        return updates
                
    @updates.setter
    def updates(self, updates):
        keys = self._updates.keys()
        for k,v in updates.items():
            if k in keys:
                assert k.name.endswith(('_b','_W')), 'Already have update for '+k.name
                v_old = updates[k]
                self._updates[k] = v + v_old
            else:
                self._updates[k] = v
    
    @property
    def trainable_weights(self):
        weights = []
        for layer in self.layers:
            weights += layer._trainable_weights
        return weights

    @property
    def non_trainable_weights(self):
        weights = []
        for layer in self.layers:
            weights += layer._non_trainable_weights
        return weights

    def get_weights(self):
        """Returns the weights of the model,
        as a flat list of Numpy arrays.
        """
        weights = []
        for layer in self.layers:
            weights += layer.weights
        return [w.get_value() for w in weights]

    def set_weights(self, weights):
        """Sets the weights of the model.
        The `weights` argument should be a list
        of Numpy arrays with shapes and types matching
        the output of `model.get_weights()`.
        """
        tuples = []
        for layer in self.layers:
            nb_param = len(layer.weights)
            layer_weights = weights[:nb_param]
            for sw, w in zip(layer.weights, layer_weights):
                tuples.append((sw, w))
            weights = weights[nb_param:]    
        for x, value in tuples:
             x.set_value(np.asarray(value, dtype=x.dtype))
 
    def save_weights(self, filepath, overwrite=True):
        """Dumps all layer weights to a HDF5 file.
        The weight file has:
            - `layer_names` (attribute), a list of strings
                (ordered names of model layers).
            - For every layer, a `group` named `layer.name`
                - For every such layer group, a group attribute `weight_names`,
                    a list of strings
                    (ordered names of weights tensor of the layer).
                - For every weight in the layer, a dataset
                    storing the weight value, named after the weight tensor.
        """
        
        # If file exists and should not be overwritten:
        if not overwrite and os.path.isfile(filepath):
            raise NotImplementedError
        f = h5py.File(filepath, 'w')
        self.save_weights_to_hdf5_group(f)
        f.flush()
        f.close()

    def save_weights_to_hdf5_group(self, f):
        
        parts = self.layers + self.synapses
        f.attrs['part_names'] = [p.name.encode('utf8') for p in parts]

        for part in parts:
            g = f.create_group(part.name)
            symbolic_weights = part.weights
            weight_values = [w.get_value() for w in symbolic_weights]
            weight_names = []
            for i, (w, val) in enumerate(zip(symbolic_weights, weight_values)):
                if hasattr(w, 'name') and w.name:
                    name = str(w.name)
                else:
                    name = 'param_' + str(i)
                weight_names.append(name.encode('utf8'))
            g.attrs['weight_names'] = weight_names
            for name, val in zip(weight_names, weight_values):
                param_dset = g.create_dataset(name, val.shape,
                                              dtype=val.dtype)
                if not val.shape:
                    # scalar
                    param_dset[()] = val
                else:
                    param_dset[:] = val
