#%%
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *

import os
import numpy as np
import theano
import theano.tensor as T
import copy
import pandas as pd

# The custom imports should be in same folder as this script
import __main__ as main
if not hasattr(main, '__file__'):
    # This means its an iPython instance
    import getpass
    usr = getpass.getuser()
    if usr=='alexlangMacBookPro':
        path_to_script_folder = '/Users/alexlangMacBookPro/Dropbox/BitBucket/dbm/src/'
        os.chdir(path_to_script_folder)

import callbacks
from models import Model
from dbm import DBM
import utils
import my_data
import optimizers

FLOATX = theano.config.floatX # should be FLOATX32 for GPU
EPS = np.finfo(FLOATX).eps

#%%
def prep_data(dataset, data_type, IS_test_mode, IS_debug):
    
    if isinstance(dataset, dict):
        train_data = dataset['train_data']
        valid_data = dataset['valid_data']
    
    else:
        
        if IS_test_mode:
            train_set, valid_set, _ = my_data.load_data(dataset, 'probability')
            td = train_set[0].get_value()
            vd = valid_set[0].get_value()
            train_data = np.concatenate((td,vd),axis=0).astype(FLOATX)
            train_data = theano.shared(train_data, name = 'train', borrow=True)
            
            # validation depends on options
            _, _, test_set = my_data.load_data(dataset, data_type)
            valid_data = test_set[0]
            
        else:
            train_set, _, _ = my_data.load_data(dataset, 'probability')
            train_data = train_set[0]
            
            # validation depends on options
            _, valid_set, _ = my_data.load_data(dataset, data_type)
            valid_data = valid_set[0]
        
        if IS_debug:
            train_data = theano.shared(train_data[0:2*batch_size].eval())
            valid_data = theano.shared(valid_data[0:2*batch_size].eval())
            
    return train_data, valid_data
              
#%%
def train_nnet(layer_size_list, topology_dict, residual_dict, save_folder,
         dataset = 'MNIST', data_type = 'sampled', weight_file = None,
         IS_mean_field = True, IS_persist = True, IS_centered = False,
         neg_steps = 5, L1 = 1e-5, L2 = 1e-5,
         dropout_prob_list = [], batch_size = 100, nb_epochs = 250,
         optimizer={}, IS_debug = False, IS_test_mode = False,
         lr_center = 0.01, reset_chains = True):

    save_folder = utils.standard_folder(save_folder)
    utils.safe_make_folders(save_folder)
    weight_folder = save_folder + 'weights/'
    optimizer_folder = save_folder + 'optimizer/'
    sample_folder = save_folder + 'samples/'
    plot_folder = save_folder + 'plots/'
    os.makedirs(weight_folder)
    os.makedirs(optimizer_folder)
    os.makedirs(sample_folder)
    os.makedirs(plot_folder)
   
    train_data, valid_data = prep_data(dataset, data_type, IS_test_mode, IS_debug)
    
    # setup model structure
    if weight_file is not None:
        print(os.path.abspath(os.path.curdir))
        weights_dict = utils.load_weights_hd5f(weight_file)
        if reset_chains:
            p_keys = [k for k in weights_dict.keys() if k.endswith('_persist_chain')]
            for pk in p_keys:
                pc = weights_dict[pk]
                new_pc = 1.0*(0.5*np.ones(pc.shape) > np.random.uniform(size=pc.shape))
                weights_dict[pk] = new_pc
    else:
        weights_dict = None
    
    ##### Create the model
    dbm = DBM(layer_size_list, topology_dict,
              IS_mean_field = IS_mean_field,
              IS_persist = IS_persist,
              IS_centered = IS_centered,
              dropout_prob_list = dropout_prob_list,
              batch_size = batch_size,
              weights_dict = weights_dict,
              theano_rng = None,
              init_data = train_data.get_value(),
              init='orthogonal')

    # setup optimizer
    num_batches = train_data.get_value().shape[0]/batch_size
#    if optimizer['name'] == 'sgd' and IS_decay:
#        if nb_epochs>=100:
#            mom_iter_max = num_batches*(nb_epochs-50)
#        else:
#            mom_iter_max = np.inf
#        default = {'lr' : 0.01, 'momentum' : 0.9, 'nesterov' : True,
#                   'schedule_decay' : 0.004, 'beta' : 1.0,
#                   'decay' : 1.8e-5, 'mom_iter_max' : mom_iter_max}
#        for k, v in default.items():
#            if k not in optimizer.keys():
#                optimizer[k] = v
#        kwargs = copy.deepcopy(optimizer)
#        del kwargs['name']
#        opt = optimizers.SGD(**kwargs)  
#        
#    elif optimizer['name'] == 'sgd' and not IS_decay:
#        default = {'lr' : 0.01, 'momentum' : 0.9, 'nesterov' : True,
#                   'schedule_decay' : 0.004, 'beta' : 1.0,
#                   'decay' : 0.0, 'mom_iter_max' : np.inf}
#        for k, v in default.items():
#            if k not in optimizer.keys():
#                optimizer[k] = v
#        kwargs = copy.deepcopy(optimizer)
#        del kwargs['name']
#        opt = optimizers.SGD(**kwargs)  
        
    if optimizer['name'] == 'sgd2':
        # momentum will be at 0.99 of max after 5 epochs
        schedule_decay = np.log(0.02)/np.log(0.96)/(5.0*num_batches)
        #if nb_epochs>=100:
        mom_iter_max = num_batches*(nb_epochs-5)
        lr_iter_decay = 0 #num_batches*(nb_epochs-50)
        lr_iter_max = num_batches*nb_epochs
        #else:
        #    mom_iter_max = np.inf
        #    lr_iter_decay = np.inf
        #    lr_iter_max = np.inf
        default = {'lr' : 0.01, 'momentum' : 0.9, 'nesterov' : True,
                   'schedule_decay' : schedule_decay, 'beta' : 1.0,
                   'mom_iter_max' : mom_iter_max,
                   'lr_iter_decay' : lr_iter_decay,
                   'lr_iter_max' : lr_iter_max}
        for k, v in default.items():
            if k not in optimizer.keys():
                optimizer[k] = v
        kwargs = copy.deepcopy(optimizer)
        del kwargs['name']
        opt = optimizers.SGD2(**kwargs)  
        
#    elif optimizer['name'] == 'nadam':
#        default = {'lr' : 0.002, 'beta_1' : 0.9, 'beta_2' : 0.999,
#                    'epsilon' : EPS, 'schedule_decay' : 0.004, 'beta' : 1.0}
#        for k, v in default.items():
#            if k not in optimizer.keys():
#                optimizer[k] = v
#        kwargs = copy.deepcopy(optimizer)
#        del kwargs['name']
#        opt = optimizers.Nadam(**kwargs)  
#  
    else:
        raise NotImplementedError
    
        
    x = T.matrix('x')
    model = Model(x, dbm)
    model.compile(opt,  nb_gibbs_neg = neg_steps,
                  L1 = L1, L2 = L2, lr_center = lr_center)

    # callbacks
    cb_csv = callbacks.CSVLogger(save_folder + 'history.txt', append=False)
        
    # list of epochs to check on stuff
    # want a few more at start/end
    temp = [0,1,5,10]
    fixed_ls = temp + [nb_epochs-t for t in temp] + [nb_epochs-2]
    epoch_ls = list(set(list(range(0, nb_epochs, 25)) + fixed_ls))
    
    cb_ais =  callbacks.AISCallback(dbm, nb_runs = 1000, nb_betas = 30000,
                               epoch_ls = epoch_ls, name='')
    
    cb_sample = callbacks.SampleCallback(dbm, sample_folder,
                                         20, 10, 2000, epoch_ls = epoch_ls)
 
    weight_path = weight_folder + 'weights.{epoch:04d}.hdf5'
    opt_path = optimizer_folder + 'opt_weights.{epoch:04d}.hdf5'
    cb_ps = callbacks.PeriodicSave(weight_path = weight_path,
                                   opt_path = opt_path,
                                   epoch_ls = epoch_ls)
    
    cb_plot = callbacks.PlotCallback(save_folder = plot_folder,
                           csv_filepath = save_folder + 'history.txt')
    
    cb_opt = callbacks.OptimizerSpy()
    
    callback_list = [cb_plot, cb_ps, cb_opt, cb_csv]
    #if dbn_init:
    #    callback_list = [cb_ais]+callback_list
    if not IS_debug:
        callback_list = [cb_ais, cb_sample] + callback_list
    
    if isinstance(dataset,dict):
        dataset = 'dict'
    
    ###### Save parameters used for training
    param = {'layer_size_list' : layer_size_list,
             'topology_dict' : topology_dict,
             'residual_dict' : residual_dict,
             'save_folder' : save_folder,
             'dataset' : dataset,
             'data_type' : data_type,
             'weight_file' : weight_file,
             'IS_mean_field' : IS_mean_field,
             'IS_persist' : IS_persist,
             'IS_centered' : IS_centered,
             'IS_debug' : IS_debug,
             'neg_steps' : neg_steps,
             'L1' : L1,
             'L2' : L2,
             'dropout_prob_list' : dropout_prob_list,
             'batch_size' : batch_size,
             'nb_epochs' : nb_epochs,
             'optimizer' : optimizer,
             'IS_test_mode' : IS_test_mode,
             'lr_center' : lr_center
             }

    utils.save_dict(save_folder + 'param.txt', param)
    np.save(save_folder + 'param', param)
   
    ##### Training
    history = model.fit(train_data, batch_size=batch_size, nb_epoch=nb_epochs,
                        callbacks=callback_list, validation_data=valid_data)
       
    # get some stats about the training     
    df = pd.read_csv(save_folder+'history.txt')
    try:
        val_prob = df['val_prob'].values
        HAS_best = True
        vp = val_prob[np.logical_not(np.isnan(val_prob))]
        best_val_prob = np.max(vp)
        best_epoch = np.where(best_val_prob==val_prob)[0].item()
    except KeyError:
        HAS_best = False
    
    with open(save_folder+'summary.txt', 'w') as f:
        total = model.fit_loop_time
        f.write('Number of epochs {}.\n'.format(nb_epochs))
        if HAS_best:
            f.write('Best epoch is {}.\n'.format(best_epoch))
            f.write('Best log val prob is {}.\n'.format(best_val_prob))
        f.write('Total fit time was {} minutes.\n'.format(total/60.0))
        f.write('Per epoch total fit time was {} seconds.\n'.format(total/nb_epochs))
        f.write('Fit time was {}% callbacks.\n'.format(model.fit_loop_callback_time/total*100))    
        
#%%
def main(layer_size_list, topology_dict, residual_dict,
         save_folder, dataset, data_type,
         IS_mean_field, IS_persist, IS_centered,
         neg_steps_ls, L1, L2, dropout_prob_list,
         batch_size, nb_epochs, optimizer_ls,
         IS_debug, IS_test_mode, lr_center, reset_chains):

    save_folder = utils.standard_folder(save_folder)
    utils.safe_make_folders(save_folder)
    
    for i,opt in enumerate(optimizer_ls):
        if i==0:
            weight_file = None
        else:
            weight_file =  save_folder+'re_'+str(i-1)+'/weights/weights.{:04d}.hdf5'.format(nb_epochs-1)
                
        train_nnet(layer_size_list = layer_size_list,
                   topology_dict = topology_dict,
                   residual_dict = residual_dict,
                   save_folder = save_folder+'re_'+str(i),
                   dataset = dataset,
                   data_type = data_type,
                   weight_file = weight_file,
                   IS_mean_field = IS_mean_field,
                   IS_persist = IS_persist,
                   IS_centered = IS_centered,
                   neg_steps = neg_steps_ls[i],
                   L1 = L1,
                   L2 = L2,
                   dropout_prob_list = dropout_prob_list,
                   batch_size = batch_size,
                   nb_epochs = nb_epochs,
                   optimizer = opt,
                   IS_debug = IS_debug,
                   IS_test_mode = IS_test_mode,
                   lr_center = lr_center,
                   reset_chains = reset_chains)
      
#%%
if __name__ == '__main__':
    
    layer_size_list = [784] + [500, 500]
    dropout_prob_list = [0.0]*len(layer_size_list)
    topology_dict = {0:{1}, 1:{2}}
    residual_dict = {}
    
    save_folder = '../results/sgdr_7'
    
    lr_center = 0.01    
    dataset = 'MNIST'
    data_type = 'threshold'
    IS_mean_field = True
    IS_persist = True
    IS_centered = True    
    L1 = 0.0
    L2 = 2e-4
    batch_size = 100    
    
    IS_test_mode = True
    IS_debug = False
    
    # per sgdr number of epochs
    nb_epochs = 50
    reset_chains = False
    
    
    #lr_ls = [0.1, 0.03, 0.01, 0.003, 0.001]
    #lr_ls = [0.03, 0.01, 0.003, 0.001, 0.0003]
    #lr_ls = [0.1]*4 + [0.03]*4 + [0.01]*4 +[0.003]*4 +[0.001]*4
    #lr_ls = [0.03]*5 + [0.01]*5 +[0.003]*5 +[0.001]*5
    lr_ls = [0.003]*20
    neg_steps_ls = [25]*len(lr_ls)
    
    if IS_test_mode:
        num_batches = 600
    else:
        num_batches = 500
    
    
    schedule_decay = np.log(0.02)/np.log(0.96)/(5.0*num_batches)
    mom_iter_max = num_batches*(nb_epochs-10)
    lr_iter_decay = 0 
    lr_iter_max = num_batches*nb_epochs
    
    opt_default = {'name' : 'sgd2',
                    'lr' : 0.0,
                   'momentum' : 0.9,
                   'nesterov' : True,
                   'schedule_decay' : schedule_decay,
                   'beta' : 1.0,
                   'mom_iter_max' : mom_iter_max,
                   'lr_iter_decay' : lr_iter_decay,
                   'lr_iter_max' : lr_iter_max}
    
    optimizer_ls = []
    for lr in lr_ls:
        opt = copy.deepcopy(opt_default)
        opt['lr'] = lr
        optimizer_ls.append(opt)
        
    
    assert len(optimizer_ls)==len(neg_steps_ls)
    
    main(layer_size_list, topology_dict, residual_dict,
         save_folder, dataset, data_type,
         IS_mean_field, IS_persist, IS_centered,
         neg_steps_ls, L1, L2, dropout_prob_list,
         batch_size, nb_epochs, optimizer_ls,
         IS_debug, IS_test_mode, lr_center, reset_chains)
  