"""
Deep Boltzmann Machines in Theano

Original Code Source: 
    [1] http://deeplearning.net/tutorial/rbm.html
    [2] https://github.com/lisa-lab/pylearn2/blob/master/pylearn2/rbm_tools.py
Modified by: Alex Lang (alexhunterlang@gmail.com)
License: BSD-3

The main heart of the code is from [1], while the AIS code is modified from [2]

Useful References:
[1] http://deeplearning.net/tutorial/rbm.html
        - see notes about scan and theano optimization for justification of
            returning pre-sigmoid activation
[2] Hinton Guide to RBM - https://www.cs.toronto.edu/~hinton/absps/guideTR.pdf
[3] Initialization - http://arxiv.org/abs/1312.6120
[4] Nesterov momentum - https://github.com/lisa-lab/pylearn2/issues/677
[5] Neal (2001) Annealed importance sampling
    https://link.springer.com/article/10.1023/A:1008923215028
[6] Salakhutdinov, Murray (2008) On the quantitative analysis of DBNs
    http://www.cs.toronto.edu/~rsalakhu/papers/dbn_ais.pdf
[7] http://www.cs.toronto.edu/~rsalakhu/rbm_ais.html
[8] Deep Boltzmann machines http://machinelearning.wustl.edu/
                            mlpapers/paper_files/AISTATS09_SalakhutdinovH.pdf
                            
"""

#%% Imports 
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *

import numpy as np
import theano
import theano.tensor as T
from theano.ifelse import ifelse
import os
from collections import OrderedDict

try:
    import PIL.Image as Image
except ImportError:
    import Image

# The custom imports should be in same folder as this script
import __main__ as main
if not hasattr(main, '__file__'):
    # This means its an iPython instance
    import getpass
    usr = getpass.getuser()
    if usr=='alexlangMacBookPro':
        path_to_script_folder = '/Users/alexlangMacBookPro/Dropbox/BitBucket/dbm/src/'
        os.chdir(path_to_script_folder)

from neuralnet import NeuralNet
from components import Synapse, Layer
import utils
from utils import binomial_sample, make_theano_rng, prep_topology
FLOATX = theano.config.floatX # should be FLOATX32 for GPU
EPS = np.finfo(FLOATX).eps

#%%
class DBM(NeuralNet):
    """ Deep boltzmann machine """               
    def __init__(self, layer_size_list, topology_dict, residual_dict = {},
                 IS_mean_field = True, IS_persist = False, IS_centered = False,
                 IS_std = False, dropout_prob_list = None,
                 batch_size = None, weights_dict = None, theano_rng = None,
                 init_data = None, init='orthogonal', flip_rate = 0.01):
        
        super(DBM, self).__init__()
        
        self.layer_size_list = layer_size_list
        self.topology_dict = topology_dict
        self.residual_dict = residual_dict
        for k,v in self.residual_dict.items():
            for vv in list(v):
                assert vv in list(topology_dict[k]), 'residual_dict must be a subset of topology_dict'
        
        self.IS_mean_field = IS_mean_field
        self.IS_persist = IS_persist
        self.IS_centered = IS_centered
        self.IS_std = IS_std
        
        if dropout_prob_list is None:
            dropout_prob_list = [0.0]*len(layer_size_list)
        self.dropout_prob_list = dropout_prob_list
        self.IS_dropout = 1.0*(np.sum(self.dropout_prob_list)>0)
        self.batch_size = batch_size
        
        assert len(self.layer_size_list) == len(self.dropout_prob_list)
        if self.IS_persist or self.IS_dropout:
            assert self.batch_size is not None
        if (self.IS_centered or self.IS_std) and weights_dict is None:
            assert init_data is not None

        if init_data is not None:
            # need to regularize the var with some noise
            # flipping some seems a more reasonable regularizer than just an
            # epsilon cutoff
            if flip_rate  == 0:
                flip_rate = 1e-4
            r = np.random.uniform(size=init_data.shape)
            f = (r<flip_rate)
            X = init_data*(1-f) + (1-init_data)*f
                          
            vis_mean = np.clip(np.mean(X,axis=0),EPS,1-EPS)              
            vis_std = np.maximum(np.std(X, axis=0) , 100*EPS)
        else:
            vis_mean = None
            vis_std = None

        if theano_rng is None:
            theano_rng = make_theano_rng()
        self.theano_rng = theano_rng
        
        self.activation = T.nnet.sigmoid

        pairs, topology_input_dict = prep_topology(self.topology_dict)
        self.synapse_pairs = pairs
        self.topology_input_dict = topology_input_dict
        
        pairs, residual_input_dict = prep_topology(self.residual_dict)
        self.residual_pairs = pairs
        self.residual_input_dict = residual_input_dict
        
        # first prepare all the weights connecting layers
        self.synapses = []
        for sp in self.synapse_pairs:
            name = 'h{}-{}_W'.format(sp[0], sp[1]) 
            
            if weights_dict is not None:
                init_W = weights_dict[name]
            else:
                init_W = None

            if sp in self.residual_pairs:
                synapse = Synapse(self.layer_size_list[sp[0]],
                                  self.layer_size_list[sp[1]],
                                  name, init_W, init = 'identity',
                                  IS_trainable = False)
            else:
                synapse = Synapse(self.layer_size_list[sp[0]],
                                  self.layer_size_list[sp[1]],
                                  name, init_W, init = init,
                                 IS_trainable = True)
            
            self.synapses.append(synapse)
            
        # now make the layers and feed in the given weights
        for i, output_dim in enumerate(self.layer_size_list):
            name = 'h'+str(i)
            
            if weights_dict is not None:
                init_b = weights_dict[name+'_b']
                if self.IS_persist:
                    init_persist = weights_dict[name+'_persist_chain']
                else:
                    init_persist = None
                if self.IS_centered:
                    init_c = weights_dict[name+'_center']
                else:
                    init_c = None
                if self.IS_std:
                    init_std = weights_dict[name+'_std']
                else:
                    init_std = None
            else:
                init_b = None
                init_persist = None
                init_c = None
                init_std = None

            if i==0 and vis_mean is not None:
                data_mean = vis_mean
                data_std = vis_std
            else:
                data_mean = None
                data_std = None

            input_dict = {}
            transposed_dict = {}
            for j, sp in enumerate(self.synapse_pairs):
                if i==sp[0]:
                    transposed_dict[sp[1]] = self.synapses[j].W
                elif i==sp[1]:
                    input_dict[sp[0]] = self.synapses[j].W
                
            layer = Layer(name,
                          self.layer_size_list[i],
                          input_dict = input_dict,
                          transposed_dict = transposed_dict,
                          IS_persist = self.IS_persist,
                          IS_centered = self.IS_centered,
                          IS_std = self.IS_std,
                          dropout_p = self.dropout_prob_list[i],
                          batch_size = self.batch_size,
                          theano_rng = self.theano_rng,
                          init_c = init_c,
                          init_b = init_b,
                          init_persist = init_persist,
                          init_std = init_std,
                          data_mean = data_mean,
                          data_std = data_std,
                          activation = self.activation)
                        
            self.layers.append(layer)
            self.layer_indices[i] = layer
                              
            
        # simplify the sharing of information
        # this lets synapses know connecting layers
        for synapse, sp in zip(self.synapses, self.synapse_pairs):
            layer_in = self.layers[sp[0]]
            layer_out = self.layers[sp[1]]
            synapse.init_layers(layer_in, layer_out)
        # layers know who is prev/next
        for layer in self.layers:
            input_index = layer.input_index
            transposed_index = layer.transposed_index

            layer_in_ls = [lay for i,lay in enumerate(self.layers)
                                    if i in input_index]
            layer_out_ls = [lay for i,lay in enumerate(self.layers)
                                    if i in transposed_index]

            layer.init_layers(layer_in_ls, layer_out_ls)
            
        # need to properly initialize std of higher layers
#        if init_data is not None and (self.IS_centered or self.IS_std):
#            x = T.matrix()
#            IS_dropout = T.scalar()
#            beta = T.scalar()
#            prob_data, _, updates = self.pos_stats(x,IS_dropout,beta)
#            fn = theano.function([x,IS_dropout,beta],prob_data,updates=updates)
#            prob_ls = fn(X.astype(FLOATX),0,1)
#            
#            for p, layer in zip(prob_ls[1:],self.layers[1:]):
#                if self.IS_centered:
#                    c = np.clip(np.mean(p,axis=0),EPS,1-EPS).astype(FLOATX)
#                    layer.c.set_value(c)
#                
#                if self.IS_std:
#                    std = np.std(p,axis=0)
#                    std = np.maximum(std, 100*EPS).astype(FLOATX)
#                    layer.std.set_value(std)

    #%%
    @classmethod
    def init_file(cls, param_npy, weight_file):

        kwargs = {}
        
        param = np.load(param_npy).item()
        
        keys = ['layer_size_list', 'topology_dict', 'residual_dict',
                'IS_mean_field', 'IS_persist', 'IS_centered', 'IS_std',
                'dropout_prob_list', 'batch_size']

        for k in keys:
            kwargs[k] = param[k]
        
        kwargs['init_data']= None
        kwargs['theano_rng'] = None
        kwargs['init'] = 'orthogonal'
        kwargs['weights_dict'] = utils.load_weights_hd5f(weight_file)
        
        return cls(**kwargs)

    #%%
    @property
    def persist_chain(self):
        if self.IS_persist:
            persist_chain = []
            for layer in self.layers:
                persist_chain.append(layer.persist_chain)
            return persist_chain
        else:
            return None
        
    #%%
    @persist_chain.setter
    def persist_chain(self, persist_chain):
        if self.IS_persist:
            for p, layer in zip(persist_chain, self.layers):
                layer.persist_chain = p
        
    #%%
    @property
    def center(self):
        if self.IS_centered:
            center_ls = []
            for layer in self.layers:
                center_ls.append(layer.c)
        else:
            center_ls = None
            
        return center_ls
        
    #%%
    @property
    def std(self):
        if self.IS_std:
            std_ls = []
            for layer in self.layers:
                std_ls.append(layer.std)
        else:
            std_ls = None
            
        return std_ls
    
    #%%
    def apply_center(self, prob_ls, new = False):
        
        if self.IS_centered:
            index = 0
            out_ls = len(prob_ls)*[None]
            for p, layer in zip(prob_ls, self.layers):
                out_ls[index] = layer.apply_center(p, new)
                index += 1
        else:
            out_ls = prob_ls
      
        return out_ls
            
    #%%
    def apply_std(self, prob_ls, transposed = False, new = False):
        
        if self.IS_std:
            index = 0
            out_ls = len(prob_ls)*[None]
            for p, layer in zip(prob_ls, self.layers):
                out_ls[index] = layer.apply_std(p, transposed = transposed,
                                                new = new)
                index += 1
        else:
            out_ls = prob_ls
      
        return out_ls
    
    #%%
    def apply_datanorm(self, prob_ls, transposed = False, new = False):
        
        if self.IS_centered or self.IS_std:
            index = 0
            out_ls = len(prob_ls)*[None]
            for p, layer in zip(prob_ls, self.layers):
                out_ls[index] = layer.apply_datanorm(p, transposed = transposed,
                                                      new = new)
                index += 1
        else:
            out_ls = prob_ls
      
        return out_ls
            
    #%%
    def apply_dropout(self, prob_ls, IS_dropout):
        
        if self.IS_dropout:
            index = 0
            out_ls = len(prob_ls)*[None]
            for p, layer in zip(prob_ls, self.layers):
                out_ls[index] = layer.apply_dropout(p, IS_dropout)
                index += 1
        else:
            out_ls = prob_ls
      
        return out_ls
    
    #%%
    def norm_cost(self, L1, L2):
        ''' Returns weight costs '''

        cost = T.cast(0.0,dtype=FLOATX)
        for synapse in self.synapses:
            cost += synapse.norm_cost(L1, L2)
        
        return cost

    #%%
    def update_center(self, prob_ls, lr):
        if self.IS_centered:
            for layer in self.layers:
                layer.update_center(prob_ls, lr, IS_full_input = True)

    #%%
    def update_gauge(self):
        
        for layer in self.layers:
            layer.update_bias_gauge()
        for synapse in self.synapses:
            synapse.update_weight_gauge()

    #%%
    def update_std(self, prob_ls, lr):
        if self.IS_std:
            for layer in self.layers:
                layer.update_std(prob_ls, lr, IS_full_input = True)

    #%%
    def update_dropout(self):
        if self.IS_dropout:
            for layer in self.layers:
                layer.update_dropout()
    
    #%%
    def propup(self, x, IS_dropout, beta):
        """ Pass data up through network"""
          
        prob_ls = [x]+[None]*(len(self.layers)-1)
        
        index = 1
        for layer in self.layers[1:]:
            current_prob_ls = self.apply_datanorm(prob_ls, transposed = False)
            current_prob_ls = self.apply_dropout(current_prob_ls, IS_dropout)
            prob_ls[index] = layer.get_output(current_prob_ls, 
                                           mean_field = True,
                                           direction = 'up',
                                           beta = beta)
            index += 1
        
        if self.IS_centered or self.IS_std:
            prob_ls[0] = x
        
        return prob_ls    
 
    #%%
    def propdown(self, x, IS_dropout, beta):
        """ Pass data up through network"""
                
        prob_ls = [None]*(len(self.layers)-1)+[x]
        
        index = len(self.layers)-2
        for layer in self.layers[::-1][1:]:
            current_prob_ls = self.apply_datanorm(prob_ls, transposed = False) 
            current_prob_ls = self.apply_dropout(current_prob_ls, IS_dropout)
            prob_ls[index] = layer.get_output(current_prob_ls, 
                                           mean_field = True,
                                           direction = 'down',
                                           beta = beta)
            index -= 1
        
        if self.IS_centered or self.IS_std:
            prob_ls[-1] = x
        
        return prob_ls    
    
    #%%
    def free_energy(self, x, IS_dropout, beta):
        ''' Function to compute the free energy of a visible sample '''
        
        prob_ls = self.propup(x, IS_dropout, beta)

        return self.free_energy_given_h(prob_ls, beta)        

    #%%
    def free_energy_given_h(self, prob_ls, beta):
        """ Function for free energy given visible sample and 
        activations of hidden layers        
        """
        beta = T.cast(beta, FLOATX)
    
        z_odd_ls = []
        
        if self.IS_centered or self.IS_std:
            prob_ls = self.apply_datanorm(prob_ls, transposed = False)
            
        for layer in self.layers[1::2]:
            z = layer.get_input(prob_ls, direction = 'both', mean_field = True)
            if self.IS_std:
                z = layer.apply_std(z, transposed = False)
            z_odd_ls.append(z)

        z = T.concatenate(z_odd_ls, axis=1)
        fe = -T.sum(T.log(1 + T.exp(beta*z)), axis=1)
        
        if self.IS_centered:
            c = self.center
            if self.IS_std:
                c = self.apply_std(c, transposed = False)
        
        for p, layer in zip(prob_ls[0::2], self.layers[0::2]):
            b = layer.b
            if self.IS_centered:
                # this has an unneeded bias term
                b_adj = b - layer.get_input(c, mean_field=True,
                                     direction='bot', full_input = True)
                b += b_adj
                
            fe -= beta*T.dot(p, b)
        
        # NOTE: in centering, there is a constant the depends on odd bias and odd center
        # Ignoring since non-physical and does not affect gradient
        
        return fe
 
    #%%
    def parity_update(self, prob_ls, sample_ls, start, IS_dropout, beta,
                            IS_prob_input = True, hold_constant = []):
        ''' Updates either even or odd layers'''
       
        index = list(range(start,len(prob_ls),2))
        index = [i for i in index if i not in hold_constant]
        
        if len(index)==0:
            # nothing to change, so just pass out the inputs
            prob_out = prob_ls
            sample_out = sample_ls
        else:
            
            prob_out = [None]*len(prob_ls)
            sample_out = [None]*len(sample_ls)
                 
            # fill in the samples if needed
            for i, s in enumerate(sample_ls):
                if s is None:
                    p = prob_ls[i]
                    sample_ls[i] = binomial_sample(self.theano_rng, p)
            
            # this allows implementation of Hintons suggestions of when
            # to use prob updates vs sample updates
            if IS_prob_input:
                input_ls = [pp for pp in prob_ls]
            else:
                input_ls = [s for s in sample_ls]
                
            if self.IS_centered or self.IS_std:
                input_ls = self.apply_datanorm(input_ls, transposed = False)
                
            if self.IS_dropout:
                input_ls = self.apply_dropout(input_ls, IS_dropout)
                
            for i in index:
                layer = self.layers[i]
                p = layer.get_output(input_ls,
                                     direction = 'both',
                                     mean_field = self.IS_mean_field,
                                     beta = beta) 
                prob_out[i] = p                    
                sample_out[i] = binomial_sample(self.theano_rng, p)
                
            # pass out the unchanged prob / samples
            for i in range(len(prob_out)):
                if prob_out[i] is None:
                    prob_out[i] = prob_ls[i]
                if sample_out[i] is None:
                    sample_out[i] = sample_ls[i]
        
        return prob_out, sample_out
        
    #%%
    def gibbs_odd_even_odd(self, *args):
        ''' This function implements one step of Gibbs sampling,
            starting from the odd hidden states'''

        IS_dropout = args[-2]
        beta = args[-1]
        args = list(args[0:-2])
        mid = int(len(args)/2)
        prob_ls = args[0:mid]
        sample_ls = args[mid:]
 
        # Update even
        prob_ls, sample_ls = self.parity_update(prob_ls, sample_ls, 0, 
                                                      IS_prob_input = False,
                                                      hold_constant = [],
                                                      IS_dropout = IS_dropout,
                                                      beta = beta)
        
        # update odd
        prob_ls, sample_ls = self.parity_update(prob_ls, sample_ls, 1, 
                                                      IS_prob_input = False,
                                                      hold_constant = [],
                                                      IS_dropout = IS_dropout,
                                                      beta = beta)

        return prob_ls+sample_ls+[IS_dropout, beta]           
        

    #%%
    def gibbs_even_odd_even(self, *args):
        ''' This function implements one step of Gibbs sampling,
            starting from the visible state and even hidden states'''
               
        IS_dropout = args[-2]
        beta = args[-1]
        args = list(args[0:-2])
        mid = int(len(args)/2)
        prob_ls = args[0:mid]
        sample_ls = args[mid:]
 
        # Update odd
        prob_ls, sample_ls = self.parity_update(prob_ls, sample_ls, 1, 
                                                IS_prob_input = False,
                                                hold_constant = [],
                                                IS_dropout = IS_dropout,
                                                beta = beta)
        
        # update even
        prob_ls, sample_ls = self.parity_update(prob_ls, sample_ls, 0, 
                                                IS_prob_input = False,
                                                hold_constant = [],
                                                IS_dropout = IS_dropout,
                                                beta = beta)

        return prob_ls+sample_ls+[IS_dropout, beta]
                
    #%%
    def gibbs_even_odd_even_given_v(self, *args):
        ''' This function implements one step of Gibbs sampling,
            starting from the visible state and even hidden states
            but with visible state fixed '''
                
        IS_dropout = args[-2]
        beta = args[-1]
        args = list(args[0:-2])
        mid = int(len(args)/2)
        prob_ls = args[0:mid]
        sample_ls = args[mid:]
        
        # Update odd
        prob_ls, sample_ls = self.parity_update(prob_ls, sample_ls, 1, 
                                                IS_prob_input = True,
                                                hold_constant = [0],
                                                IS_dropout = IS_dropout,
                                                beta = beta)
        
        # update even
        prob_ls, sample_ls = self.parity_update(prob_ls, sample_ls, 0, 
                                                IS_prob_input = True,
                                                hold_constant = [0],
                                                IS_dropout = IS_dropout,
                                                beta = beta)
        
        return prob_ls+sample_ls+[IS_dropout, beta]
        
    #%%
    def pos_stats(self, x, IS_dropout, beta):
        # compute positive phase

        # these are the max number of positive steps
        if len(self.layers)==2:
            nb_gibbs_pos = 1
        else:
            # when I checked usually converges within 5-10
            nb_gibbs_pos = 25

        # use propup to get initial mf activations
        prob_data = self.propup(x, IS_dropout, beta)
        
        sample_data = [binomial_sample(self.theano_rng, p) for p in prob_data]
                
        output_ls = prob_data + sample_data + [IS_dropout, beta]    
        
        scan_out, updates = theano.scan(fn = self.gibbs_even_odd_even_given_v, 
                                outputs_info = output_ls, 
                                n_steps = nb_gibbs_pos, name = 'scan_pos')       
             
        
        num_p = len(prob_data)
        prob_data = scan_out[0:num_p]
        sample_data = scan_out[num_p:2*num_p]
        prob_data = [p[-1] for p in prob_data]
        sample_data = [s[-1] for s in sample_data]
                
        return prob_data, sample_data, updates
        
    #%%
    def neg_stats(self, prob_data, sample_data, IS_dropout, beta, nb_gibbs_neg):
               
        # decide how to initialize persistent chain:
        if self.IS_persist:
            # for PCD, we initialize from the old state of the chain
            prob_model = self.persist_chain
            sample_model = [binomial_sample(self.theano_rng, p) for p in prob_model] 
        else:
            # for CD, we use the newly generated hidden sample
            prob_model = prob_data
            sample_model = sample_data
        
        # perform actual negative phase
        output_ls = prob_model+sample_model+[IS_dropout, beta]

        scan_out, updates = theano.scan(self.gibbs_odd_even_odd, 
                                        outputs_info = output_ls, 
                                        n_steps = nb_gibbs_neg, name = 'scan_neg')

        num_p = len(prob_model)
        prob_model = scan_out[0:num_p]
        sample_model = scan_out[num_p:2*num_p]
        prob_model = [p[-1] for p in prob_model]
        sample_model = [s[-1] for s in sample_model]
                       
        return prob_model, sample_model, updates
        
    #%%
    def contrastive_divergence_cost(self, prob_data, prob_model, beta):
       
        fe_data = T.mean(self.free_energy_given_h(prob_data, beta)) 
        fe_model = T.mean(self.free_energy_given_h(prob_model, beta)) 
        cost = fe_data - fe_model        
                    
        return cost
    
    #%%
    def cd_cost_fxn(self, x, IS_dropout, beta,
                        nb_gibbs_neg = 5, L1 = 0.0, L2 = 0.0):
        
        prob_data, sample_data, updates = self.pos_stats(x, IS_dropout, beta)
        
        prob_model, sample_model, neg_updates = self.neg_stats(prob_data, sample_data,
                                                  IS_dropout, beta, nb_gibbs_neg)
         
        data = [sample_data[0]] + prob_data[1:]
        model = [sample_model[0]] + prob_model[1:]
        
        # determine cost
        cost = self.contrastive_divergence_cost(data, model, beta)
        cost += self.norm_cost(L1, L2) 

        if len(updates)==0:
            updates = OrderedDict()
        
        for k,v in neg_updates.items():
            updates[k] = v

        return cost, updates
    
    #%%
    def training_update(self, x, IS_dropout, beta, nb_gibbs_neg = 5,
                        L1 = 0.0, L2 = 0.0, lr = None, flip_noise = 0.0):

        params = []
        grads = []

        # prep for training step        
        if self.IS_centered or self.IS_std:
            assert lr is not None
            lr = T.cast(lr, dtype = FLOATX)
        L1 = T.cast(L1, dtype = FLOATX)
        L2 = T.cast(L2, dtype = FLOATX)
      
        if flip_noise > 0.0:
            assert self.batch_size is not None
            nv = self.layer_size_list[0]
            f_prob = T.cast(flip_noise*np.ones((self.batch_size,nv)),dtype=FLOATX)
            flip = binomial_sample(self.theano_rng, f_prob)
            x = x*(1-flip)+(1-x)*flip
                  
        # sample the inputs
        # TODO: should I use this?
        #x = binomial_sample(self.theano_rng, x)
        
        # collect probabilities
        prob_data, sample_data, updates = self.pos_stats(x, IS_dropout, beta)
        self.updates = updates
        
        prob_model, sample_model, updates = self.neg_stats(prob_data, sample_data,
                                                  IS_dropout, beta, nb_gibbs_neg)
        self.updates = updates 
        
        # determine cost
        cost = self.contrastive_divergence_cost(prob_data, prob_model, beta)
        cost += self.norm_cost(L1, L2)   
        
        # this matches Ruslans code
        # TODO: check on whether this is worth it, see also pylearn2
        #data = [sample_data[0]] + prob_data[1:]
        #model = [sample_model[0]] + prob_model[1:]
        #data = prob_data
        #model = prob_model
        
        # update the center and bias
        if self.IS_centered:
            self.update_center(prob_data, lr)
          
        # update the std
        if self.IS_std:
            std_data = self.apply_center(prob_data, new = True)
            self.update_std(std_data, lr)   
            
        # update_gauges (just b,W if no centering or std)
        self.update_gauge()
    
        # applies newest transforms before taking gradients
        # TODO: check if this matters
        b_data = self.apply_std(prob_data, transposed = False, new = True)
        b_model = self.apply_std(prob_model, transposed = False, new = True)
        
        data = self.apply_datanorm(prob_data, transposed = False, new = True)
        model = self.apply_datanorm(prob_model, transposed = False, new = True)
        
        # TODO: should I only do dropout on inputs? not grads?
        #data = self.apply_dropout(data, IS_dropout)
        #model = self.apply_dropout(model, IS_dropout)
    
        # determine bias grads
        for layer in self.layers:
            params.append(layer.b)
            g = layer.grad_b(b_data, b_model)
            grads.append(g)
        
        # determine weight grads
        for i, pair in enumerate(self.synapse_pairs):
            synapse = self.synapses[i]
            W = synapse.W
            params.append(W)
            sd = [data[p] for p in pair]
            sm = [model[p] for p in pair]
            g = synapse.grad(sd, sm, L1, L2)
            grads.append(g)

        # update masks for next minibatch
        if self.IS_dropout:
            self.update_dropout()
            
        # preps the next persistent chain
        if self.IS_persist:
            self.persist_chain = prob_model
                    
        return cost, params, grads

    #%%
    def sample(self, n_features, filepath, beta,
               n_chains = 20, n_samples = 10, plot_every = 2000):
        
        init_v = 1.0*(np.random.uniform(size = (n_chains, n_features))>0.5)
        init_v = init_v.astype(FLOATX)
        
        init_v_var = theano.shared(init_v, name='init_v')
        
        IS_dropout = T.scalar('IS_dropout')
        
        prob_ls = self.propup(init_v_var, IS_dropout, beta)
        
        sample_ls = [binomial_sample(self.theano_rng, p) for p in prob_ls]
        
        output_ls = prob_ls+sample_ls+[IS_dropout, beta]   
       
        output_ls, updates = theano.scan(self.gibbs_even_odd_even, 
                                           outputs_info = output_ls, 
                                           n_steps = plot_every,
                                           name = 'scan_sample')
            
        prob_v = output_ls[0]        
        updates[init_v_var] = prob_v[-1]
    
        # construct the function that implements our persistent chain.
        sample_fxn = theano.function([IS_dropout], prob_v[-1],
                                     updates=updates, name='sample_fxn')
        
        # create a space to store the image for plotting ( we need to leave
        # room for the tile_spacing as well)
        ts = 1 # tile spacing
        x = int(np.sqrt(n_features))
        y = x
        xx = x+ts
        yy = y+ts    
        image_data = np.zeros((xx*(n_samples+2)+1, yy*n_chains-1), dtype='uint8')              
                    
        image_data[0:x,:] = utils.tile_raster_images(
                                X=init_v, img_shape=(x,y),
                                tile_shape=(1, n_chains), tile_spacing=(ts, ts))
        for idx in range(n_samples):
            # generate `plot_every` intermediate samples that we discard,
            # because successive samples in the chain are too correlated
            # I left a blank row between original images and gibbs samples
            vis_prob = sample_fxn(0)
            image_data[xx*(idx+2) : xx*(idx+2) + x, :] =\
                utils.tile_raster_images(X=vis_prob, img_shape=(x, y),
                    tile_shape=(1, n_chains), tile_spacing=(ts, ts))
        
        # save image
        image = Image.fromarray(image_data)
        image.save(filepath)    
    