#%% Imports 
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *

import six
import numpy as np
import theano
import theano.tensor as T
from theano.ifelse import ifelse
import os
from collections import OrderedDict

# The custom imports should be in same folder as this script
import __main__ as main
if not hasattr(main, '__file__'):
    # This means its an iPython instance
    import getpass
    usr = getpass.getuser()
    if usr=='alexlangMacBookPro':
        path_to_script_folder = '/Users/alexlangMacBookPro/Dropbox/BitBucket/dbm/src/'
        os.chdir(path_to_script_folder)


from utils import binomial_sample, make_theano_rng, orthogonal_init

FLOATX = theano.config.floatX # should be FLOATX32 for GPU
EPS = np.finfo(FLOATX).eps

#%%
class Synapse(object):
    def __init__(self, n_in, n_out, name,
                      init_W = None, init = 'orthogonal', IS_trainable = True):
                
        self.n_in = n_in
        self.n_out = n_out
        self.name = name    
        self.IS_trainable = IS_trainable
                    
        # Make weight
        if init_W is None:
            if init == 'orthogonal':
                init_W = orthogonal_init((n_in, n_out))
            elif init == 'normal':
                init_W = 0.01*np.random.normal(size=(n_in,n_out))
            elif init == 'identity':
                assert n_in == n_out
                init_W = np.identity(n_in)
            elif init == 'zeros':
                init_W = np.zeros((n_in, n_out))
            else:
                raise NotImplementedError
    
        self.W = theano.shared(value = init_W.astype(FLOATX),
                               name = self.name, borrow = True)  
        
        self.weights = [self.W] # needed for saving
        
        self._updates = OrderedDict()
 
    #%%
    def init_layers(self, layer_in, layer_out):
        """ This allows synapse to know who is connected to it """
        self.layer_in = layer_in
        self.layer_out = layer_out
        if self.layer_in.IS_centered or self.layer_out.IS_centered:
            self.IS_centered = True
        else:
            self.IS_centered = False
        if self.layer_in.IS_std or self.layer_out.IS_std:
            self.IS_std = True
        else:
            self.IS_std = False
    
    #%%
    @property
    def updates(self):
        return self._updates
    
    #%%
    @updates.setter
    def updates(self, updates):
        if updates is None:
            return
        keys = self._updates.keys()
        for k,v in updates.items():
            assert k not in keys, 'Already have update for '+k.name
            self._updates[k] = v
    
    #%%
    def norm_cost(self, L1 = 0.0, L2 = 0.0):
        
        cost = T.cast(0.0, dtype=FLOATX)
        L1 = T.cast(L1, dtype=FLOATX)
        L2 = T.cast(0.5*L2, dtype=FLOATX)
        
        if self.IS_trainable and T.gt(L1, 0.0):
            cost += L1*T.sum(T.abs_(self.W))
        if self.IS_trainable and T.gt(L2, 0.0):
            cost += L2*T.sum(T.pow(self.W,2))

        return cost

    #%%
    def norm_grad(self, L1 = 0.0, L2 = 0.0):
        
        g = T.cast(0.0, dtype=FLOATX)
        L1 = T.cast(L1, dtype=FLOATX)
        L2 = T.cast(L2, dtype=FLOATX)
        
        if self.IS_trainable and T.gt(L1, 0.0):
            g += L1*T.sum(T.sgn(self.W))
        if self.IS_trainable and T.gt(L2, 0.0):
            g += L2*T.sum(self.W)

        return g

    #%%
    def grad(self, prob_data, prob_model, L1 = 0.0, L2 = 0.0):
                   
        if self.IS_trainable:
            
            pd0, pd1 = prob_data
            pm0, pm1 = prob_model
            
            n = T.cast(prob_data[0].shape[0], dtype=FLOATX)
            
            g = -1.0/n*(T.dot(pd0.T, pd1) - T.dot(pm0.T, pm1))

            if T.gt(L1, 0.0) or T.gt(L2, 0.0):
                g += self.norm_grad(L1, L2)

            return g

        else:
            return None
        
    #%%
    def gauged_W(self, change = False):
        
        W = self.W
        if self.IS_std:
            W = self.layer_out.apply_std(W, change = change, transposed = False)
            W = self.layer_in.apply_std(W, change = change, transposed = True)
       
        return W
    
    #%%
    def update_weight_gauge(self):
    
        self.W_new = self.gauged_W(change = True)
        self.updates = OrderedDict({self.W : self.W_new})
        
#%%
class Layer(object):
    def __init__(self, name, output_dim,
                 input_dict = {}, transposed_dict = {},
                 IS_persist = False, IS_centered = False, IS_std = False,
                 dropout_p = 0.0, batch_size = None, theano_rng = None,
                 init_b = None, init_persist = None, init_c = None,
                 init_std = None, data_mean = None, data_std = None,
                 activation = T.nnet.sigmoid):
        
        self.name = name
        assert self.name.startswith('h')
        self.my_index = int(self.name[1:])
        self.output_dim = output_dim
        
        self.IS_persist = IS_persist
        self.IS_centered = IS_centered
        self.IS_std = IS_std
        self.batch_size = batch_size
        self.p = dropout_p
        self.IS_dropout = 1.0*(self.p > 0.0)
        if (0. < self.p < 1.) or self.IS_persist:
            assert self.batch_size is not None
        
        if theano_rng is None:
            theano_rng = make_theano_rng()
        self.theano_rng = theano_rng
        
        # creates a sorted input_dict
        self.input_dict = OrderedDict()
        if len(input_dict) > 0:
            for k in np.sort(input_dict.keys()):
                self.input_dict[k] = input_dict[k]
            
        # creates a sorted transposed_dict
        self.transposed_dict = OrderedDict()
        if len(transposed_dict) > 0:
            for k in np.sort(transposed_dict.keys()):
                self.transposed_dict[k] = transposed_dict[k]
            
        # assign layer properties
        len_in = len(self.input_dict)
        len_tran = len(self.transposed_dict)
        if len_in > 0 and len_tran > 0:
            self.layer_type = 'middle'
            self.direction = 'both'
            # need to compensate for reduced inputs
            self.z_up_adj = 1.0*(len_in+len_tran)/len_in
            self.z_down_adj = 1.0*(len_in+len_tran)/len_tran
        elif len_in > 0 and len_tran == 0:
            self.layer_type = 'output'
            self.direction = 'up'
        elif len_in == 0 and len_tran > 0:
            self.layer_type = 'input'
            self.direction = 'down'
        else:
            raise NotImplementedError
            
        self.activation = activation
            
        self._updates = OrderedDict()
        self._trainable_weights = []
        self._non_trainable_weights = []
        
        self.transposed_index = []
        self.W_T_ls = []
        for key, W in six.iteritems(self.transposed_dict):
            self.W_T_ls.append(W.T)
            self.transposed_index.append(key)
        
        self.input_index = []
        self.W_ls = []
        for key, W in six.iteritems(self.input_dict):
            self.W_ls.append(W)
            self.input_index.append(key)
        
        # create bias
        if init_b is None:
            # create shared variable for visible units bias
            if (self.layer_type=='input') and (data_mean is not None):
                # tip from ref [2]
                p = np.clip(data_mean, EPS, 1-EPS)
                init_b = np.log(p/(1-p))
                init_b = init_b.astype(FLOATX)
            else:            
                init_b = np.zeros(output_dim, dtype=FLOATX)
                
        name = self.name + '_b'
        self.b = theano.shared(value=init_b, name=name, borrow=True) 
        self._trainable_weights.append(self.b)
       
        if self.IS_centered:
            if init_c is None:
                if (self.layer_type=='input') and (data_mean is not None):
                    init_c = np.clip(data_mean, EPS, 1-EPS)
                else:            
                    init_c = 0.5*np.ones(output_dim)
            
            init_c = init_c.astype(FLOATX)
            
            name = '{}_center'.format(self.name)
            self.c = theano.shared(value=init_c, name=name, borrow=True)
            self._non_trainable_weights.append(self.c)
            
        if self.IS_std:
            if init_std is None:
                if (self.layer_type=='input') and (data_std is not None):
                    init_std = data_std+100*EPS
                else:           
                    # assuming these are Bernoulli variables
                    init_std = 0.5*np.ones(output_dim)
            
            init_std = init_std.astype(FLOATX)
            
            name = '{}_std'.format(self.name)
            self.std = theano.shared(value=init_std, name=name, borrow=True)
            self._non_trainable_weights.append(self.std)
            
        # persistant state of layer
        if self.IS_persist:
            if init_persist is None:
                if (self.layer_type=='input') and (data_mean is not None):
                    # clipping data_mean to let each neuron have some on prob
                    eps = 0.05
                    dm = np.clip(data_mean, eps, 1-eps)
                    dm *= np.mean(dm)/np.mean(data_mean) # keep same mean prob
                    init_p = np.repeat(dm.reshape((1,-1)),batch_size,0)
                else:            
                    init_p = 0.5*np.ones((batch_size,output_dim))    
            else:
                init_p = init_persist
                
            init_p = init_p.astype(FLOATX)
            
            name = '{}_persist_chain'.format(self.name)
            self._persist_chain = theano.shared(value=init_p, name=name, borrow=True)
            self._non_trainable_weights.append(self._persist_chain)
        
        if self.IS_dropout:
            
            p = T.cast(self.p, dtype=FLOATX)
            p = ((1-p)*np.ones((self.batch_size, self.output_dim))).astype(FLOATX)  
            value = binomial_sample(self.theano_rng, p).eval().astype(FLOATX)
            self._p_matrix = value
            
            name = '{}_dropout'.format(self.name)
            self._dropout = theano.shared(value = value, name = name, borrow=True)
            self._non_trainable_weights.append(self._dropout)
        
    def init_layers(self, layer_in_ls, layer_out_ls):
        """ This allows layer to know who is connected to it """
        self.layer_in_ls = layer_in_ls
        self.layer_out_ls = layer_out_ls
        assert len(self.layer_in_ls)==len(self.W_ls)
        assert len(self.layer_out_ls)==len(self.W_T_ls)
        
    @property
    def weights(self):
        return self._trainable_weights + self._non_trainable_weights
        
    @property
    def updates(self):
        return self._updates
        
    @updates.setter
    def updates(self, updates):
        if updates is None:
            return
        keys = self._updates.keys()
        for k,v in updates.items():
            assert k not in keys, 'Already have update for '+k.name
            self._updates[k] = v
        
    @property
    def persist_chain(self):
        if self.IS_persist:
            return self._persist_chain
        else:
            return None
    
    @persist_chain.setter
    def persist_chain(self, persist_chain):
        if self.IS_persist:
            updates = OrderedDict({self._persist_chain : persist_chain})
            self.updates = updates
                
    def update_dropout(self):
        
        if 0. < self.p < 1:
            s = binomial_sample(self.theano_rng, self._p_matrix)
            updates = OrderedDict({self._dropout : s})
            self.updates = updates
    
    def grad_b(self, prob_data, prob_model):
        
        pd = prob_data[self.my_index]
        pm = prob_model[self.my_index]
        
        g = -(T.mean(pd, axis=0) - T.mean(pm, axis=0))

        return g
        
    def update_center(self, prob_data, lr, IS_full_input = True):
        if self.IS_centered:
            if IS_full_input:
                pd = prob_data[self.my_index]
            else:
                pd = prob_data
            self.c_new = (1-lr)*self.c + lr*T.mean(pd, axis=0)
            self.updates = OrderedDict({self.c : self.c_new})
    
    def update_std(self, std_data, lr, IS_full_input=True):
        if self.IS_std:
            if IS_full_input:
                sd = std_data[self.my_index]
            else:
                sd = std_data
            
            n = self.batch_size
            self.std_new = (1-lr)*self.std + lr*T.sqrt(T.sum(sd**2, axis=0)/(n-1))
            self.updates = OrderedDict({self.std : self.std_new})
    
    def apply_dropout(self, x, IS_dropout):
        if (0. < self.p < 1.) and x is not None:
            x = ifelse(T.eq(IS_dropout, 0),
                       x,
                       x*self._dropout/T.cast(1.0-self.p, dtype=FLOATX)
                       )
        return x
 
    def apply_center(self, x, new = False):
        if self.IS_centered and x is not None:
            if new and hasattr(self, 'c_new'):
                c = self.c_new
            else:
                c = self.c
            x -= c   
        return x
    
    def apply_std(self, x, transposed = False, new = False, change = False):
        if self.IS_std and x is not None:
            
            #delta = T.cast(100,dtype=FLOATX) # same as no std
            delta = T.cast(0.5,dtype=FLOATX) 
            div = T.cast(0.5+delta,dtype=FLOATX)
            
            if change:
                norm_bottom = (self.std + delta)/div
                if hasattr(self, 'std_new'):
                    norm_top = (self.std_new + delta)/div
                    norm  = norm_top/norm_bottom
                else:
                    norm = norm_bottom/norm_bottom
            elif new and hasattr(self, 'std_new'):
                norm  = div/(self.std_new + delta)
            else:
                norm  = div/(self.std + delta)
                            
            if x.ndim > 1:
                if transposed:
                    norm = T.repeat(norm.reshape((-1,1)),x.shape[1],1)
                else:
                    norm = T.repeat(norm.reshape((1,-1)),x.shape[0],0)
            x *= norm
            
        return x
            
    def apply_datanorm(self, x, transposed = False, new = False):
        return self.apply_std(self.apply_center(x, new), transposed, new)
    
    def gauged_b(self, new = False, change = False):
        
        if self.IS_std:
            b = self.apply_std(self.b, new = new,
                               change = change, transposed = False)
        else:
            b = self.b

        W_ls = self.W_ls + self.W_T_ls
        layer_ls = self.layer_in_ls + self.layer_out_ls
        for W, layer in zip(W_ls, layer_ls):

            if change:
                c = layer.c_new-layer.c
            else:
                c = layer.c
            
            if self.IS_centered and not self.IS_std:
                b -= T.dot(c, W)
           
            elif self.IS_centered and self.IS_std:
                W_std = self.apply_std(W, change = change, transposed = False)
                W_std = layer.apply_std(W_std, transposed = True)                
                b -= T.dot(c, W_std)
        
        return b
    
    def update_bias_gauge(self):
        
        self.b_new = self.gauged_b(new = False, change = True)
        self.updates = OrderedDict({self.b : self.b_new})
        
    def needed_inputs(self, mean_field=True, direction = None):
        
        index = []
        if direction is None:
            direction = self.direction
        
        if direction in ['both','up']:
            index += self.input_index
        if not mean_field:
            index += [self.my_index]
        if direction in ['both','down']:
            index += self.transposed_index
            
        return index
    
    def prep_input(self, full_input_ls, mean_field=True, direction=None):
        
        if direction is None:
            direction = self.direction
            
        index = self.needed_inputs(mean_field, direction)
        
        input_ls = [full_input_ls[i] for i in index]
        
        return input_ls

    def get_input(self, input_ls, mean_field=True,
                          direction=None, full_input = True):
        
        if direction is None:
            direction = self.direction
        
        if full_input:
            input_ls = self.prep_input(input_ls, mean_field, direction)
        
        # Controls which interactions to include    
        IS_up, IS_down = True, True
        if direction=='up' or self.direction=='up':
            IS_down = False            
        elif direction=='down' or self.direction=='down':
            IS_up = False
        
        start, stop = 0, 0
        current_W_ls, current_input = [], []
        
        if IS_up:
            stop = len(self.W_ls)
            current_input += input_ls[start:stop]
            current_W_ls += self.W_ls
        if not mean_field:
            input_self = input_ls[stop]
            stop += 1
        if IS_down:
            current_input += input_ls[stop:]
            current_W_ls += self.W_T_ls
              
        # Calculate activation input
        z = self.b
        for data, W in zip(current_input, current_W_ls):
            z += T.dot(data, W)
            if not mean_field:
                z += -0.5*T.dot(data-data**2, W**2)*(input_self-0.5)
                
        # need to compensate for reduced input        
        if (self.layer_type=='middle') and (direction == 'up'):
            z *= self.z_up_adj  
        elif (self.layer_type=='middle') and (direction == 'down'):
            z *= self.z_down_adj
            
        if self.IS_std:
            z = self.apply_std(z)
            
        return z

    def get_output(self, input_ls, beta = 1.0,
                   mean_field=True, direction=None, full_input = True):
        
        beta = T.cast(beta, FLOATX)
        z = self.get_input(input_ls, mean_field, direction, full_input)
        prob = self.activation(beta*z)   
        
        return prob
  