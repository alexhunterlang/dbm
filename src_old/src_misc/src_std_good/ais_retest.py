#%%
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *

import time
import os
import pandas as pd

# The custom imports should be in same folder as this script
import __main__ as main
if not hasattr(main, '__file__'):
    # This means its an iPython instance
    import getpass
    usr = getpass.getuser()
    if usr=='alexlangMacBookPro':
        path_to_script_folder = '/Users/alexlangMacBookPro/Dropbox/BitBucket/dbm/src/'
        os.chdir(path_to_script_folder)

from dbm import DBM
import utils
import my_data
from ais import AIS
      
#%%
if __name__ == '__main__':
    
    save_folder = '../results/center3big_1'
    #prev_epoch = 100 # 100 for center3big_0
    prev_epoch = 190 # 100 for center3big_1
    #weight_file = save_folder + '/weights/weights.0100.hdf5' # center3big_0
    weight_file = save_folder + '/weights/weights.0190.hdf5' # center3big_1
    savename = '/ais_rerun_3.txt'
    
    nb_runs = 1000
    nb_betas = 300000 
    
    # utilizes previous results
    df = pd.read_csv(save_folder+'/csv.txt')    
    logZ_orig = df['logz'].values[prev_epoch]
    if False:
        import numpy as np    
        val_prob = df['val_prob'].values
        epoch = df['epoch'].values
        IS_good = np.logical_not(np.isnan(val_prob))
        vp = val_prob[IS_good]
        ep = epoch[IS_good]
        index = np.where(np.max(vp)==val_prob)
    
    # needed to fill in the dbm
    weights_dict = utils.load_weights_hd5f(weight_file)
    layer_size_list = [784, 1000, 1000]
    topology_dict = {0: {1}, 1: {2}}
    residual_dict = {}
    IS_mean_field = True
    IS_persist = True
    IS_centered = True
    dropout_prob_list = [0.0, 0.0, 0.0]
    residual_dict = {}
    batch_size = 100

    # load the data for the base model    
    data_type = 'sampled'
    dataset = 'MNIST'
    _, valid_set, _ = my_data.load_data(dataset, data_type)
    data = valid_set[0].get_value()
    
    dbm = DBM(layer_size_list, topology_dict, residual_dict,
              IS_mean_field = IS_mean_field,
              IS_persist = IS_persist,
              IS_centered = IS_centered,
              dropout_prob_list = dropout_prob_list,
              batch_size = batch_size,
              weights_dict = weights_dict,
              theano_rng = None,
              vis_mean = None,
              init='orthogonal')
    
    re_ais = AIS(dbm, nb_runs, nb_betas, data)
    start = time.time()
    logZ_est, logZ_est_up, logZ_est_down = re_ais.log_z()
    total = time.time()-start
    
    with open(save_folder+savename, 'w') as f:
        f.write('log Z original = {}\n'.format(logZ_orig))
        f.write('log Z = {}\n'.format(logZ_est))
        f.write('log Z up = {}\n'.format(logZ_est_up))
        f.write('log Z low = {}\n'.format(logZ_est_down))
        f.write('nb_runs = {}\n'.format(nb_runs))
        f.write('nb_betas = {}\n'.format(nb_betas))
        f.write('Total runtime was {} minutes.\n'.format(total/60.0))
    
    