#%%
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *

import os
import numpy as np
import theano
import theano.tensor as T
import copy
import pandas as pd

# The custom imports should be in same folder as this script
import __main__ as main
if not hasattr(main, '__file__'):
    # This means its an iPython instance
    import getpass
    usr = getpass.getuser()
    if usr=='alexlangMacBookPro':
        path_to_script_folder = '/Users/alexlangMacBookPro/Dropbox/BitBucket/dbm/src/'
        os.chdir(path_to_script_folder)

import callbacks
from models import Model
from dbm import DBM
import utils
import my_data
import optimizers

FLOATX = theano.config.floatX # should be FLOATX32 for GPU
EPS = np.finfo(FLOATX).eps

#%%
def main(layer_size_list, topology_dict, residual_dict, save_folder,
         dataset = 'MNIST', data_type = 'sampled', weight_file = None,
         IS_mean_field = True, IS_persist = True, IS_centered = False,
         neg_steps = 5, L1 = 1e-5, L2 = 1e-5,
         dropout_prob_list = [], batch_size = 100, nb_epochs = 250,
         optimizer={}, IS_decay = True, IS_debug = False, IS_test_mode = False,
         lr_center = 0.01):
    
    ##### Prep for the nnet
    
    if IS_decay:
        assert nb_epochs >= 100

    save_folder = utils.standard_folder(save_folder)
    utils.safe_make_folders(save_folder)
    weight_folder = save_folder + 'weights/'
    optimizer_folder = save_folder + 'optimizer/'
    sample_folder = save_folder + 'samples/'
    plot_folder = save_folder + 'plots/'
    os.makedirs(weight_folder)
    os.makedirs(optimizer_folder)
    os.makedirs(sample_folder)
    os.makedirs(plot_folder)

    # training is always done with probabilities
    if IS_test_mode:
        train_set, valid_set, _ = my_data.load_data(dataset, 'probability')
        td = train_set[0].get_value()
        vd = valid_set[0].get_value()
        train_data = np.concatenate((td,vd),axis=0).astype(FLOATX)
        train_data = theano.shared(train_data, name = 'train', borrow=True)
        
        # validation depends on options
        _, _, test_set = my_data.load_data(dataset, data_type)
        valid_data = test_set[0]
        
    else:
        train_set, _, _ = my_data.load_data(dataset, 'probability')
        train_data = train_set[0]
        
        # validation depends on options
        _, valid_set, _ = my_data.load_data(dataset, data_type)
        valid_data = valid_set[0]
    
    if IS_debug:
        train_data = theano.shared(train_data[0:2*batch_size].eval())
        valid_data = theano.shared(valid_data[0:2*batch_size].eval())
    
    # setup model structure
    if weight_file is not None:
        weights_dict = utils.load_weights_hd5f(weight_file)
    else:
        weights_dict = None
    
    ##### Create the model
    dbm = DBM(layer_size_list, topology_dict,
              IS_mean_field = IS_mean_field,
              IS_persist = IS_persist,
              IS_centered = IS_centered,
              dropout_prob_list = dropout_prob_list,
              batch_size = batch_size,
              weights_dict = weights_dict,
              theano_rng = None,
              init_data = train_data.get_value(),
              init='orthogonal')

    x = T.matrix('x')
    model = Model(x, dbm)
    
    # setup optimizer
    if optimizer['name'] == 'sgd' and IS_decay:
        num_batches = train_data.get_value().shape[0]/batch_size
        default = {'lr' : 0.01, 'momentum' : 0.9, 'nesterov' : True,
                   'schedule_decay' : 0.004, 'beta' : 1.0,
                   'decay' : 1.8e-5, 'mom_iter_max' : num_batches*(nb_epochs-50)}
        for k, v in default.items():
            if k not in optimizer.keys():
                optimizer[k] = v
        kwargs = copy.deepcopy(optimizer)
        del kwargs['name']
        opt = optimizers.SGD2(**kwargs)  
        
    elif optimizer['name'] == 'sgd' and not IS_decay:
        default = {'lr' : 0.01, 'momentum' : 0.9, 'nesterov' : True,
                   'schedule_decay' : 0.004, 'beta' : 1.0,
                   'decay' : 0.0, 'mom_iter_max' : np.inf}
        for k, v in default.items():
            if k not in optimizer.keys():
                optimizer[k] = v
        kwargs = copy.deepcopy(optimizer)
        del kwargs['name']
        opt = optimizers.SGD2(**kwargs)  
        
    elif optimizer['name'] == 'nadam':
        default = {'lr' : 0.002, 'beta_1' : 0.9, 'beta_2' : 0.999,
                    'epsilon' : EPS, 'schedule_decay' : 0.004, 'beta' : 1.0}
        for k, v in default.items():
            if k not in optimizer.keys():
                optimizer[k] = v
        kwargs = copy.deepcopy(optimizer)
        del kwargs['name']
        opt = optimizers.Nadam(**kwargs)  
  
    else:
        raise NotImplementedError
        
        
    model.compile(opt,  nb_gibbs_neg = neg_steps,
                  L1 = L1, L2 = L2, lr_center = lr_center)

    ###### Callbacks
    #cb_fe = callbacks.FreeEnergy(dbm.free_energy_fxn())
      
    cb_csv = callbacks.CSVLogger(save_folder + 'history.txt', append=False)
        
    # list of epochs to check on stuff
    fixed_ls = [1,5,10, nb_epochs-10, nb_epochs-1] # want a few more at start/end
    epoch_ls = list(set(list(range(0, nb_epochs, 25)) + fixed_ls))
    
    cb_ais =  callbacks.AISCallback(dbm, nb_runs = 1000, nb_betas = 30000,
                               epoch_ls = epoch_ls, name='')
    
    cb_sample = callbacks.SampleCallback(dbm, sample_folder, 20, 10, 2000,
                                         epoch_ls = epoch_ls)
   
    #cb_es = callbacks.EarlyStopping(monitor='val_prob', min_delta=0,
    #                                      patience=100, mode='max')
    
    cb_ps = callbacks.PeriodicSave(weight_path = weight_folder + 'weights.{epoch:04d}.hdf5',
                                   opt_path = optimizer_folder + 'opt_weights.{epoch:04d}.hdf5',
                                   epoch_ls = epoch_ls)
    
    cb_plot = callbacks.PlotCallback(save_folder = plot_folder,
                           csv_filepath = save_folder + 'history.txt')
    
    cb_opt = callbacks.OptimizerSpy()
    
    if IS_debug:
        callback_list = [cb_plot, cb_ps, cb_opt, cb_csv]
    else:
        callback_list = [cb_ais, cb_sample, cb_plot, cb_ps, cb_opt, cb_csv]

    #beta_fxn = beta_increase(optimizer['beta'], 25, nb_epochs-100, nb_epochs)
    #cb_beta = callbacks.BetaScheduler(beta_fxn)
    #callback_list = [cb_beta]+callback_list

#    if IS_decay:
#        #lr_fxn = lr_decay(optimizer['lr'], nb_epochs-100, nb_epochs)
#        #cb_lr = callbacks.LearningRateScheduler(lr_fxn)
#        #mom_fxn = mom_step(0.5, nb_epochs-100)
#        if optimizer['name']=='sgd':
#            mom = optimizer['momentum']
#        elif optimizer['name']=='nadam':
#            mom = optimizer['beta1']
#        mom_fxn = mom_decay(mom, 0.5, nb_epochs-100, nb_epochs-75)
#        cb_mom = callbacks.MomentumRateScheduler(mom_fxn)
#        callback_list = [cb_mom] + callback_list

    ###### Save parameters used for training
    param = {'layer_size_list' : layer_size_list,
             'topology_dict' : topology_dict,
             'residual_dict' : residual_dict,
             'save_folder' : save_folder,
             'dataset' : dataset,
             'data_type' : data_type,
             'weight_file' : weight_file,
             'IS_mean_field' : IS_mean_field,
             'IS_persist' : IS_persist,
             'IS_centered' : IS_centered,
             'IS_decay' : IS_decay,
             'IS_debug' : IS_debug,
             'neg_steps' : neg_steps,
             'L1' : L1,
             'L2' : L2,
             'dropout_prob_list' : dropout_prob_list,
             'batch_size' : batch_size,
             'nb_epochs' : nb_epochs,
             'optimizer' : optimizer,
             'IS_test_mode' : IS_test_mode,
             'lr_center' : lr_center
             }

    utils.save_dict(save_folder + 'param.txt', param)
    np.save(save_folder + 'param', param)
   
    ##### Training
    history = model.fit(train_data, batch_size=batch_size, nb_epoch=nb_epochs,
                        callbacks=callback_list, validation_data=valid_data)
       
    # get some stats about the training     
    df = pd.read_csv(save_folder+'history.txt')
    try:
        val_prob = df['val_prob'].values
        HAS_best = True
        vp = val_prob[np.logical_not(np.isnan(val_prob))]
        best_val_prob = np.max(vp)
        best_epoch = np.where(best_val_prob==val_prob)[0].item()
    except KeyError:
        HAS_best = False
    
    with open(save_folder+'summary.txt', 'w') as f:
        total = model.fit_loop_time
        f.write('Number of epochs {}.\n'.format(nb_epochs))
        if HAS_best:
            f.write('Best epoch is {}.\n'.format(best_epoch))
            f.write('Best log val prob is {}.\n'.format(best_val_prob))
        f.write('Total fit time was {} minutes.\n'.format(total/60.0))
        f.write('Per epoch total fit time was {} seconds.\n'.format(total/nb_epochs))
        f.write('Fit time was {}% callbacks.\n'.format(model.fit_loop_callback_time/total*100))    
        
#%%
if __name__ == '__main__':
    
    layer_size_list = [784] + [500,500]
    dropout_prob_list = [0.0]*len(layer_size_list)
    #dropout_prob_list = [0.25]+[0.5]*(len(layer_size_list)-1)  
    topology_dict = {0:{1}, 1:{2}}
    residual_dict = {}
    
    save_folder = '../results/melchoir_dbm_17'
    nb_epochs = 200
    
    #optimizer = {'name' : 'nadam'}
    #optimizer = {'name': 'nadam_decay', 'lr' : 0.002, 'decay' : 0.0003}
    optimizer = {'name': 'sgd', 'lr' : 0.01, 'momentum' : 0.9,
                 'schedule_decay' : 0.004, 'nesterov' : True, 'beta' : 1.0,
                 'decay' : 1.8e-5, 'mom_iter_max' : np.inf}
    # either increase lr or schedule_decay (0.02 would be at max mom by 10 epoch)
    IS_decay = True
    
    weight_file = None
    dataset = 'MNIST'
    data_type = 'sampled'
    IS_mean_field = True
    IS_persist = True
    IS_centered = True
    neg_steps = 1
    L1 = 1e-5
    L2 = 1e-4
    batch_size = 100    
    
    
    IS_debug = False
    
    main(layer_size_list, topology_dict, residual_dict, save_folder,
         dataset, data_type, weight_file,
         IS_mean_field, IS_persist, IS_centered,
         neg_steps, L1, L2,
         dropout_prob_list, batch_size, nb_epochs, optimizer, IS_decay, IS_debug)
  