#%%
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *

import os
import gzip
import six.moves.cPickle as pickle
from six.moves import urllib
import theano
import theano.tensor as T
import numpy as np
import scipy.io as io
import skimage.filters as filters

import __main__ as main
if not hasattr(main, '__file__'):
    # This means its an iPython instance
    import getpass
    usr = getpass.getuser()
    if usr=='alexlangMacBookPro':
        path_to_script_folder = '/Users/alexlangMacBookPro/Dropbox/BitBucket/dbm/src/'
        os.chdir(path_to_script_folder)

FLOATX = theano.config.floatX
EPS = np.finfo(FLOATX).eps
           
                         
#%%
def make_eig_matrix(cov):
    ''' PCA whitening'''

    U, S, V = np.linalg.svd(cov)
    
    return U.T              
   
#%%
def make_random_matrix(shape):
    ''' PCA whitening'''

    U, S, V = np.linalg.svd(np.random.normal(size=shape))
    
    return U.T              
           
#%%
def make_PCA_matrix(cov):
    ''' PCA whitening'''

    U, S, V = np.linalg.svd(cov)
    
    S = np.maximum(S, EPS)
    PCA = np.dot(np.diag(1. / np.sqrt(S + 1e-3)), U.T)
    PCA = PCA.astype(FLOATX)

    return PCA.T
              
#%%
def make_PCA_corr_matrix(corr, var):
    ''' PCA whitening'''

    U, S, V = np.linalg.svd(corr)
    
    S = np.maximum(S, EPS)
    PCA_corr = np.dot(np.diag(1. / np.sqrt(S + 1e-4)), U.T)*(1. / np.sqrt(var + 1e-4))
    PCA_corr = PCA_corr.astype(FLOATX)

    return PCA_corr.T
              
#%%
def make_ZCA_matrix(cov):
    ''' ZCA whitening'''

    #U, S, V = np.linalg.svd(cov)
    #S, U = np.linalg.eigh(cov)
    
    #S = np.maximum(S, 10*EPS)
    
    
    #ZCA = np.dot(np.dot(U, np.diag(1. / np.sqrt(S + eps))), U.T)
    #ZCA = ZCA.astype(FLOATX)

    #sqrt_cov = np.dot(np.dot(U, np.diag(np.sqrt(S))), U.T)
    #A = sqrt_cov.T.dot(sqrt_cov) + eps*np.identity(U.shape[0])
    #ZCA2 = np.linalg.solve(A, sqrt_cov.T)
    
    eps = 1e-3
    A = cov.T.dot(cov) + eps*np.identity(cov.shape[0])
    temp = np.linalg.solve(A, cov.T)
    UU, SS, VV = np.linalg.svd(temp)
    ZCA = np.dot(np.dot(UU, np.diag(np.sqrt(SS))), UU.T)

    #d1 = td.dot(ZCA.T)
    #d2 = td.dot(ZCA2.T)
    #d3 = td.dot(ZCA3.T)
    #c1 = np.cov(d1.T)
    #c2 = np.cov(d2.T)
    #c3 = np.cov(d3.T)

    return ZCA.T

#%%
def make_ZCA_corr_matrix(corr, var):
    ''' ZCA correlation whitening '''

    U, S, V = np.linalg.svd(corr)
    
    S = np.maximum(S, EPS)
    ZCA = np.dot(np.dot(U, np.diag(1. / np.sqrt(S + 1e-4))), U.T)
    sqrt_V = (1. / np.sqrt(var + 1e-4))
    ZCA_corr = (ZCA*sqrt_V).astype(FLOATX)

    return ZCA_corr.T
              
#%%
def shared_dataset(data_x, data_y, borrow=True, name = ''):
        """ Function that loads the dataset into shared variables

        The reason we store our dataset in shared variables is to allow
        Theano to copy it into the GPU memory (when code is run on GPU).
        Since copying data into the GPU is slow, copying a minibatch everytime
        is needed (the default behaviour if the data is not in a shared
        variable) would lead to a large decrease in performance.
        """
        shared_x = theano.shared(np.asarray(data_x, dtype=FLOATX),
                                 name = name,
                                 borrow=borrow)
        shared_y = theano.shared(np.asarray(data_y, dtype=FLOATX),
                                 name = name,
                                 borrow=borrow)
        # When storing data on the GPU it has to be stored as floats
        # therefore we will store the labels as ``floatX`` as well
        # (``shared_y`` does exactly that). But during our computations
        # we need them as ints (we use labels as index, and if they are
        # floats it doesn't make sense) therefore instead of returning
        # ``shared_y`` we will have to cast it to int. This little hack
        # lets ous get around this issue
        return shared_x, T.cast(shared_y, 'int32')              
    
#%%
def load_mnist(data_type = 'probability'):
    
    def prob_mnist():
        dataset = 'mnist.pkl.gz'
        filename = '../data/'+dataset
        
        if not os.path.isfile(filename):
            
            origin = 'http://www.iro.umontreal.ca/~lisa/deep/data/mnist/mnist.pkl.gz'
            print('Downloading data from %s' % origin)
            urllib.request.urlretrieve(origin, dataset)
            
        # Load the dataset
        with gzip.open(filename, 'rb') as f:
            try:
                train_set, valid_set, test_set = pickle.load(f, encoding='latin1')
            except:
                train_set, valid_set, test_set = pickle.load(f)
        # train_set, valid_set, test_set format: tuple(input, target)
        # input is a np.ndarray of 2 dimensions (a matrix)
        # where each row corresponds to an example. target is a
        # np.ndarray of 1 dimension (vector) that has the same length as
        # the number of rows in the input. It should give the target
        # to the example with the same index in the input.
        
        return train_set, valid_set, test_set
        
        
    if data_type in ['probability', 'threshold']:
        train_set, valid_set, test_set = prob_mnist()
        if data_type == 'threshold':
            train_X, train_y = train_set
            valid_X, valid_y = valid_set
            test_X, test_y = test_set
            
            train_X = 1.0*(train_X>0.5)
            valid_X = 1.0*(valid_X>0.5)
            test_X = 1.0*(test_X>0.5)
    
            train_set = (train_X, train_y)
            valid_set = (train_X, train_y)
            test_set = (train_X, train_y)
        
    elif data_type == 'sampled':
        dataset = 'mnist_binary.npz'
        filename = '../data/'+dataset
        
        if not os.path.isfile(filename):
            
            file_train = 'http://www.cs.toronto.edu/~larocheh/public/datasets/binarized_mnist/binarized_mnist_train.amat'
            file_valid = 'http://www.cs.toronto.edu/~larocheh/public/datasets/binarized_mnist/binarized_mnist_valid.amat'
            file_test = 'http://www.cs.toronto.edu/~larocheh/public/datasets/binarized_mnist/binarized_mnist_test.amat'
            
            file_ls = [file_train, file_valid, file_test]
            dataset_ls = ['binarized_mnist_train.amat',
                          'binarized_mnist_valid.amat',
                          'binarized_mnist_test.amat']
            dataset_ls = ['../data/'+d for d in dataset_ls]
            
            for f, d in zip(file_ls, dataset_ls):
                print('Downloading data from %s' % f)
                urllib.request.urlretrieve(f, '../data/'+d)
            
            data = {}
            data['train_X'] = np.loadtxt(dataset_ls[0])
            data['valid_X'] = np.loadtxt(dataset_ls[1])
            data['test_X'] = np.loadtxt(dataset_ls[2])
            
            train_set, valid_set, test_set = prob_mnist()
            
            data['train_y'] = train_set[1]
            data['valid_y'] = valid_set[1]
            data['test_y'] = test_set[1]
                        
            np.savez_compressed(filename[:-4], **data)

            for d in dataset_ls:
                os.remove(d)
                
        else:
            
            data = np.load(filename)
            
        train_set = (data['train_X'], data['train_y'])
        valid_set = (data['valid_X'], data['valid_y'])
        test_set = (data['test_X'], data['test_y'])
    
    return train_set, valid_set, test_set
 
#%%
def prep_svhn_prob():
        
    dataset_ls = ['train_32x32.mat', 'test_32x32.mat']
    file_ls = ['../data/'+d for d in dataset_ls]
    
    for f,d in zip(file_ls, dataset_ls):
        if not os.path.isfile(f):
            origin = 'http://ufldl.stanford.edu/housenumbers/'+d
            print('Downloading data from %s' % origin)
            urllib.request.urlretrieve(origin,f)
        
    train = io.loadmat(file_ls[0])
    test = io.loadmat(file_ls[1])
    
    # nothing super scientific about this choice
    # about half the size of the test data, leaves train as round number
    num_valid = 13257
    num_train = train['X'].shape[3]
    # need random split since original order is correlated
    np_rng = np.random.RandomState(0)
    valid_index = np_rng.choice(np.arange(num_train),
                                size=num_valid, replace=False)
    train_set = set(list(range(num_train)))
    valid_set = set(valid_index)
    train_index = np.array(list(train_set.difference(valid_set)))
    
    train_X = train['X'][:,:,:,train_index].astype(float)
    train_y = train['y'].flatten()[train_index]-1
    valid_X = train['X'][:,:,:,valid_index].astype(float)
    valid_y = train['y'].flatten()[valid_index]-1
    test_X = test['X'].astype(float)
    test_y = test['y'].flatten()-1
    
    # standardize scale
    train_X /= 255.0
    valid_X /= 255.0
    test_X /= 255.0
    
    # convert to samples, features order
    train_X = np.rollaxis(train_X,3)
    valid_X = np.rollaxis(valid_X,3)
    test_X = np.rollaxis(test_X,3)
    train_X = train_X.reshape((-1,32**2,3))
    valid_X = valid_X.reshape((-1,32**2,3))
    test_X = test_X.reshape((-1,32**2,3))
    
    # https://en.wikipedia.org/wiki/Grayscale
    gray = [0.299, 0.587, 0.114]
    train_X = train_X.dot(gray)
    valid_X = valid_X.dot(gray)
    test_X = test_X.dot(gray)

    train_set = (train_X, train_y)
    valid_set = (valid_X, valid_y)
    test_set = (test_X, test_y)
    
    return train_set, valid_set, test_set

#%%
def prep_svhn_threshold():
    
    #https://en.wikipedia.org/wiki/Otsu%27s_method
    # just 0.5 threshold leads to terrible images
    
    filename = '../data/svhn_threshold.npz'
    
    if not os.path.isfile(filename):
        train_set, valid_set, test_set = prep_svhn_prob()
        train_X, train_y = train_set
        valid_X, valid_y = valid_set
        test_X, test_y = test_set
        
        thresh_train = np.zeros((train_X.shape[0],1))
        thresh_valid = np.zeros((valid_X.shape[0],1))
        thresh_test = np.zeros((test_X.shape[0],1))
        
        for i in range(thresh_train.shape[0]):
            img = train_X[i]
            thresh_train[i,0] = filters.threshold_otsu(img)
            
        for i in range(thresh_valid.shape[0]):
            img = valid_X[i]
            thresh_valid[i,0] = filters.threshold_otsu(img)
            
        for i in range(thresh_test.shape[0]):
            img = test_X[i]
            thresh_test[i,0] = filters.threshold_otsu(img)
            
        data = {}
        data['train_X'] = (train_X > thresh_train).astype('uint8')
        data['valid_X'] = (valid_X > thresh_valid).astype('uint8')
        data['test_X'] = (test_X > thresh_test).astype('uint8')
            
        data['train_y'] = train_y
        data['valid_y'] = valid_y
        data['test_y'] = test_y
        
        np.savez_compressed(filename[:-4], **data)
        
    else:
        data = np.load(filename)
    
    train_set = (data['train_X'], data['train_y'])
    valid_set = (data['valid_X'], data['valid_y'])
    test_set = (data['test_X'], data['test_y'])
    
    return train_set, valid_set, test_set
    

#%%
def prep_svhn_binary():
    
    raise NotImplementedError, 'need smarter sampling'
    
    filename = '../data/svhn_binary.npz'
    
    if not os.path.isfile(filename):
        train_set, valid_set, test_set = prep_svhn_prob()
        train_X, train_y = train_set
        valid_X, valid_y = valid_set
        test_X, test_y = test_set
        
        # save the binary version
        np_rng = np.random.RandomState(0)
    
        data = {}
        data['train_X'] = (train_X > np_rng.uniform(size=train_X.shape)).astype('uint8')
        data['valid_X'] = (valid_X > np_rng.uniform(size=valid_X.shape)).astype('uint8')
        data['test_X'] = (test_X > np_rng.uniform(size=test_X.shape)).astype('uint8')
            
        data['train_y'] = train_y
        data['valid_y'] = valid_y
        data['test_y'] = test_y
        
        np.savez_compressed(filename[:-4], **data)

#%%
def load_svhn(data_type = 'probability'):
    
    if data_type == 'probability':
        train_set, valid_set, test_set = prep_svhn_prob()
        
    elif data_type == 'threshold':
        train_set, valid_set, test_set = prep_svhn_threshold()
        
    elif data_type == 'binary':
        raise NotImplementedError
        #filename = '../data/svhn_binary.npz'
        #if not os.path.isfile(filename):
        #    prep_svhn_binary()
        #data = np.load(filename)
        #train_set = (data['train_X'], data['train_y'])
        #valid_set = (data['valid_X'], data['valid_y'])
        #test_set = (data['test_X'], data['test_y'])
    else:
        raise NotImplementedError
 
    return train_set, valid_set, test_set
  
#%%
def load_omniglot(data_type):
    
    # Omniglot only has thresholded data
    assert data_type == 'threshold'
    
    # see only my_data code (rotations?)
    raise NotImplementedError
    
#%%
def load_data(dataset, data_type):
    
    assert dataset in ['MNIST', 'SVHN', 'Omniglot']
    assert data_type in ['probability', 'sampled', 'threshold']
    
    if dataset == 'MNIST':
        train_set, valid_set, test_set = load_mnist(data_type)
    elif dataset == 'SVHN':
        train_set, valid_set, test_set = load_svhn(data_type)
    elif dataset == 'Omniglot':
        train_set, valid_set, test_set = load_omniglot(data_type)
    else:
        raise NotImplementedError
        
    train_set_x, train_set_y = train_set
    valid_set_x, valid_set_y = valid_set
    test_set_x, test_set_y = test_set
        
    train_set_x = np.clip(train_set_x, EPS, 1-EPS)
    valid_set_x = np.clip(valid_set_x, EPS, 1-EPS)
    test_set_x = np.clip(test_set_x, EPS, 1-EPS)
    
    # preps everyone for the output
    train_set_x, train_set_y = shared_dataset(train_set_x, train_set_y,
                                              name = 'train')    
    valid_set_x, valid_set_y = shared_dataset(valid_set_x, valid_set_y,
                                              name = 'valid')
    test_set_x, test_set_y = shared_dataset(test_set_x, test_set_y,
                                            name = 'test')
    
    output = [(train_set_x, train_set_y), (valid_set_x, valid_set_y),
            (test_set_x, test_set_y)]

    return output