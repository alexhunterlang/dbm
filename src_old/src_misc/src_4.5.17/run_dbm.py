#%%
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *

import os
import numpy as np
import theano
import theano.tensor as T
import copy
import pandas as pd

# The custom imports should be in same folder as this script
import __main__ as main
if not hasattr(main, '__file__'):
    # This means its an iPython instance
    import getpass
    usr = getpass.getuser()
    if usr=='alexlangMacBookPro':
        path_to_script_folder = '/Users/alexlangMacBookPro/Dropbox/BitBucket/dbm/src/'
        os.chdir(path_to_script_folder)

import callbacks
from models import Model
from dbm import DBM
import utils
import my_data
import optimizers

FLOATX = theano.config.floatX # should be FLOATX32 for GPU
EPS = np.finfo(FLOATX).eps

#%%
def prep_data(dataset, data_type, IS_test_mode, IS_debug, IS_prob_train):
    
    if isinstance(dataset, dict):
        train_data = dataset['train_data']
        valid_data = dataset['valid_data']
    
    else:
        
        if IS_prob_train:
            data_type_train = 'probability'
        else:
            data_type_train = data_type
        
        if IS_test_mode:
            train_set, valid_set, _ = my_data.load_data(dataset, data_type_train)
            td = train_set[0].get_value()
            vd = valid_set[0].get_value()
            train_data = np.concatenate((td,vd),axis=0).astype(FLOATX)
            train_data = theano.shared(train_data, name = 'train', borrow=True)
            
            # validation depends on options
            _, _, test_set = my_data.load_data(dataset, data_type)
            valid_data = test_set[0]
            
        else:
            train_set, _, _ = my_data.load_data(dataset, data_type_train)
            train_data = train_set[0]
            
            # validation depends on options
            _, valid_set, _ = my_data.load_data(dataset, data_type)
            valid_data = valid_set[0]
        
        if IS_debug:
            train_data = theano.shared(train_data[0:2*batch_size].eval())
            valid_data = theano.shared(valid_data[0:2*batch_size].eval())
            
    return train_data, valid_data
              
#%%
def train_nnet(layer_size_list, topology_dict, residual_dict, save_folder,
         dataset = 'MNIST', data_type = 'sampled', weight_file = None,
         IS_mean_field = True, IS_persist = True, IS_centered = False,
         IS_std = False, IS_whitened = False, white_type = None,
         neg_steps = 5, L1 = 1e-5, L2 = 1e-5,
         dropout_prob_list = [], batch_size = 100, nb_epochs = 250,
         optimizer={}, IS_decay = True, IS_debug = False, IS_test_mode = False,
         lr_center = 0.01, dbn_init=False, IS_prob_train = True,
         flip_noise = 0.0, noisy_residual = False):

    save_folder = utils.standard_folder(save_folder)
    utils.safe_make_folders(save_folder)
    weight_folder = save_folder + 'weights/'
    optimizer_folder = save_folder + 'optimizer/'
    sample_folder = save_folder + 'samples/'
    plot_folder = save_folder + 'plots/'
    os.makedirs(weight_folder)
    os.makedirs(optimizer_folder)
    os.makedirs(sample_folder)
    os.makedirs(plot_folder)
   
    train_data, valid_data = prep_data(dataset, data_type,
                                       IS_test_mode, IS_debug, IS_prob_train)
    
    # setup model structure
    if weight_file is not None:
        weights_dict = utils.load_weights_hd5f(weight_file)
        init_data = None
    else:
        weights_dict = None
        init_data = train_data.get_value()
    
    ##### Create the model
    dbm = DBM(layer_size_list, topology_dict,
              IS_mean_field = IS_mean_field,
              IS_persist = IS_persist,
              IS_centered = IS_centered,
              IS_std = IS_std,
              IS_whitened = IS_whitened,
              white_type = white_type,
              dropout_prob_list = dropout_prob_list,
              batch_size = batch_size,
              weights_dict = weights_dict,
              theano_rng = None,
              init_data = init_data,
              init='orthogonal',
              flip_rate = flip_noise,
              noisy_residual = noisy_residual)

    # setup optimizer
    num_batches = train_data.get_value().shape[0]/batch_size
    if optimizer['name'] == 'sgd' and IS_decay:
        if nb_epochs>=100:
            mom_iter_max = num_batches*(nb_epochs-50)
        else:
            mom_iter_max = np.inf
        default = {'lr' : 0.01, 'momentum' : 0.9, 'nesterov' : True,
                   'schedule_decay' : 0.004, 'beta' : 1.0,
                   'decay' : 1.8e-5, 'mom_iter_max' : mom_iter_max}
        for k, v in default.items():
            if k not in optimizer.keys():
                optimizer[k] = v
        kwargs = copy.deepcopy(optimizer)
        del kwargs['name']
        kwargs['nnet'] = dbm
        opt = optimizers.SGD(**kwargs)  
        
    elif optimizer['name'] == 'sgd' and not IS_decay:
        default = {'lr' : 0.01, 'momentum' : 0.9, 'nesterov' : True,
                   'schedule_decay' : 0.004, 'beta' : 1.0,
                   'decay' : 0.0, 'mom_iter_max' : np.inf}
        for k, v in default.items():
            if k not in optimizer.keys():
                optimizer[k] = v
        kwargs = copy.deepcopy(optimizer)
        del kwargs['name']
        kwargs['nnet'] = dbm
        opt = optimizers.SGD(**kwargs)  
        
    elif optimizer['name'] == 'sgdr':
        # momentum will be at 0.99 of max after 5 epochs
        schedule_decay = np.log(0.02)/np.log(0.96)/(5.0*num_batches)
        lr_iter_decay = 0 #num_batches*(nb_epochs-50)
        lr_iter_max = num_batches*nb_epochs
        
        if nb_epochs>=100:
            mom_iter_max = num_batches*(nb_epochs-50)
        else:
            mom_iter_max = np.inf

        default = {'lr' : 0.01, 'momentum' : 0.9, 'nesterov' : True,
                   'schedule_decay' : schedule_decay, 'beta' : 1.0,
                   'mom_iter_max' : mom_iter_max,
                   'lr_iter_decay' : lr_iter_decay,
                   'lr_iter_max' : lr_iter_max}
        for k, v in default.items():
            if k not in optimizer.keys():
                optimizer[k] = v
        kwargs = copy.deepcopy(optimizer)
        del kwargs['name']
        kwargs['nnet'] = dbm
        opt = optimizers.SGDR(**kwargs)  
        
    elif optimizer['name'] == 'nadam':
        default = {'lr' : 0.002, 'beta_1' : 0.9, 'beta_2' : 0.999,
                    'epsilon' : EPS, 'schedule_decay' : 0.004, 'beta' : 1.0}
        for k, v in default.items():
            if k not in optimizer.keys():
                optimizer[k] = v
        kwargs = copy.deepcopy(optimizer)
        del kwargs['name']
        kwargs['nnet'] = dbm
        opt = optimizers.Nadam(**kwargs)  
  
    else:
        raise NotImplementedError
    
        
    x = T.matrix('x')
    model = Model(x, dbm)
    model.compile(opt,  nb_gibbs_neg = neg_steps,
                  L1 = L1, L2 = L2, lr_center = lr_center,
                  flip_noise = flip_noise)

    # callbacks
    cb_csv = callbacks.CSVLogger(save_folder + 'history.txt', append=False)
        
    # list of epochs to check on stuff
    # want a few more at start/end
    temp = [0,1,5,10]
    fixed_ls = temp + [nb_epochs-t for t in temp] + [nb_epochs-2]
    epoch_ls = list(set(list(range(0, nb_epochs, 25)) + fixed_ls))
    
    cb_ais =  callbacks.AISCallback(dbm, nb_runs = 1000, nb_betas = 30000,
                               epoch_ls = epoch_ls, name='')
    
    cb_sample1 = callbacks.SampleCallback(dbm, sample_folder, 'sample',
                                         20, 10, 2000, epoch_ls = epoch_ls)
    cb_sample2 = callbacks.SampleCallback(dbm, sample_folder, 'sample_init',
                                         20, 10, 1, epoch_ls = epoch_ls)
 
    weight_path = weight_folder + 'weights.{epoch:04d}.hdf5'
    opt_path = optimizer_folder + 'opt_weights.{epoch:04d}.hdf5'
    cb_ps = callbacks.PeriodicSave(weight_path = weight_path,
                                   opt_path = opt_path,
                                   epoch_ls = epoch_ls)
    
    cb_plot = callbacks.PlotCallback(save_folder = plot_folder,
                           csv_filepath = save_folder + 'history.txt')
    
    cb_opt = callbacks.OptimizerSpy()
    
    callback_list = [cb_plot, cb_ps, cb_opt, cb_csv]
    if dbn_init:
        callback_list = [cb_ais]+callback_list
    elif not IS_debug:
        callback_list = [cb_ais, cb_sample1, cb_sample2] + callback_list
    
    if isinstance(dataset,dict):
        dataset = 'dict'
    
    ###### Save parameters used for training
    param = {'layer_size_list' : layer_size_list,
             'topology_dict' : topology_dict,
             'residual_dict' : residual_dict,
             'save_folder' : save_folder,
             'dataset' : dataset,
             'data_type' : data_type,
             'weight_file' : weight_file,
             'IS_mean_field' : IS_mean_field,
             'IS_persist' : IS_persist,
             'IS_centered' : IS_centered,
             'IS_std' : IS_std,
             'IS_whitened' : IS_whitened,
             'white_type' : white_type,
             'IS_decay' : IS_decay,
             'IS_debug' : IS_debug,
             'neg_steps' : neg_steps,
             'L1' : L1,
             'L2' : L2,
             'dropout_prob_list' : dropout_prob_list,
             'batch_size' : batch_size,
             'nb_epochs' : nb_epochs,
             'optimizer' : optimizer,
             'IS_test_mode' : IS_test_mode,
             'lr_center' : lr_center,
             'IS_prob_train' : IS_prob_train,
             'flip_noise' : flip_noise,
             'noisy_residual' : noisy_residual
             }

    utils.save_dict(save_folder + 'param.txt', param)
    np.save(save_folder + 'param', param)
   
    ##### Training
    history = model.fit(train_data, batch_size=batch_size, nb_epoch=nb_epochs,
                        callbacks=callback_list, validation_data=valid_data)
       
    # get some stats about the training     
    df = pd.read_csv(save_folder+'history.txt')
    try:
        val_prob = df['val_prob'].values
        HAS_best = True
        vp = val_prob[np.logical_not(np.isnan(val_prob))]
        best_val_prob = np.max(vp)
        best_epoch = np.where(best_val_prob==val_prob)[0].item()
    except KeyError:
        HAS_best = False
    
    with open(save_folder+'summary.txt', 'w') as f:
        total = model.fit_loop_time
        f.write('Number of epochs {}.\n'.format(nb_epochs))
        if HAS_best:
            f.write('Best epoch is {}.\n'.format(best_epoch))
            f.write('Best log val prob is {}.\n'.format(best_val_prob))
        f.write('Total fit time was {} minutes.\n'.format(total/60.0))
        f.write('Per epoch total fit time was {} seconds.\n'.format(total/nb_epochs))
        f.write('Fit time was {}% callbacks.\n'.format(model.fit_loop_callback_time/total*100))    
        
#%%
#def main(layer_size_list, topology_dict, residual_dict,
#         save_folder, dataset, data_type, weight_file,
#         IS_mean_field, IS_persist, IS_centered,
#         neg_steps, L1, L2, dropout_prob_list,
#         batch_size, nb_epochs, optimizer,
#         IS_decay, IS_debug, IS_test_mode,
#         lr_center, dbn_init):
#
#    if dbn_init:
#        assert len(residual_dict)==0
#        assert weight_file is None
#                  
#        save_folder = utils.standard_folder(save_folder)
#        utils.safe_make_folders(save_folder)
#        
#        dbn_topology_dict = {0:{1}}
#        dbn_epochs = 10 # 10 seems to work for lower layered rbms
#        #dbn_epochs = 10 # 10 seems to work for lower layered rbms
#        optimizer_dbn = copy.deepcopy(optimizer)
#        for k in ['decay', 'mom_iter_max']:
#            if k in optimizer_dbn.keys():
#                del optimizer_dbn[k]
#        
#        dbm_weights_dict = {}
#        for i in range(len(layer_size_list)-1):
#           
#            if i==0:
#                train_data, valid_data = prep_data(dataset, data_type,
#                                                   IS_test_mode, IS_debug)
#                dbn_dataset = {'train_data' : train_data,
#                               'valid_data' : valid_data}
#                
#            train_nnet(layer_size_list = layer_size_list[i:i+2],
#                   topology_dict = dbn_topology_dict,
#                   residual_dict = residual_dict,
#                   save_folder = save_folder+'rbm_'+str(i),
#                   dataset = dbn_dataset,
#                   data_type = data_type,
#                   weight_file = None,
#                   IS_mean_field = IS_mean_field,
#                   IS_persist = IS_persist,
#                   IS_centered = IS_centered,
#                   neg_steps = neg_steps,
#                   L1 = L1,
#                   L2 = L2,
#                   dropout_prob_list = dropout_prob_list[i:i+2],
#                   batch_size = batch_size,
#                   nb_epochs = dbn_epochs,
#                   optimizer = optimizer_dbn,
#                   IS_decay = False,
#                   IS_debug = IS_debug,
#                   IS_test_mode = IS_test_mode,
#                   lr_center = lr_center,
#                   dbn_init = True)
#            
#            # prep the weights
#            wf = save_folder+'rbm_'+str(i) + '/weights/weights.{:04d}.hdf5'.format(dbn_epochs-1)
#            weights_dict = utils.load_weights_hd5f(wf)
#            name = 'h{}-{}_W'.format(i,i+1) 
#            dbm_weights_dict[name] = weights_dict['h0-1_W']/2.0
#            name = 'h{}_b'.format(i) 
#            dbm_weights_dict[name] = weights_dict['h0_b']
#            name = 'h{}_center'.format(i) 
#            dbm_weights_dict[name] = weights_dict['h0_center']
#            name = 'h{}_persist'.format(i) 
#            dbm_weights_dict[name] = 0.5*np.ones((batch_size,layer_size_list[i]))
#            
#            # make the dataset for the next rbm
#            if i < len(layer_size_list)-1:
#                dbm = DBM(layer_size_list = layer_size_list[i:i+2],
#                          topology_dict = dbn_topology_dict,
#                          IS_mean_field = IS_mean_field,
#                          IS_persist = IS_persist,
#                          IS_centered = IS_centered,
#                          dropout_prob_list = dropout_prob_list[i:i+2],
#                          batch_size = batch_size,
#                          weights_dict = weights_dict)
#                
#                x = T.matrix('x')
#                IS_dropout = T.scalar('IS_dropout')
#                beta = T.scalar('beta')
#                fn = theano.function([x,IS_dropout, beta],
#                                     dbm.propup(x, IS_dropout, beta))
#                
#                pd = fn(dbn_dataset['train_data'].get_value(), 0 , 1)
#                train_data = pd[1]
#                pd = fn(dbn_dataset['valid_data'].get_value(), 0 , 1)
#                valid_data = pd[1]
#                dbn_dataset= {'train_data' : theano.shared(train_data,
#                                                           name='train_data',
#                                                           borrow=True),
#                              'valid_data' : theano.shared(valid_data,
#                                                           name='valid_data',
#                                                           borrow=True)}
#
#            
#        # also need the top layer
#        name = 'h{}_b'.format(i+1) 
#        dbm_weights_dict[name] = weights_dict['h1_b']
#        name = 'h{}_center'.format(i+1) 
#        dbm_weights_dict[name] = weights_dict['h1_center']
#        name = 'h{}_persist'.format(i) 
#        dbm_weights_dict[name] = 0.5*np.ones((batch_size,layer_size_list[-1]))
#        
#        train_nnet(layer_size_list = layer_size_list,
#                   topology_dict = topology_dict,
#                   residual_dict = residual_dict,
#                   save_folder = save_folder+'dbm',
#                   dataset = dataset,
#                   data_type = data_type,
#                   weight_file = weight_file,
#                   IS_mean_field = IS_mean_field,
#                   IS_persist = IS_persist,
#                   IS_centered = IS_centered,
#                   neg_steps = neg_steps,
#                   L1 = L1,
#                   L2 = L2,
#                   dropout_prob_list = dropout_prob_list,
#                   batch_size = batch_size,
#                   nb_epochs = nb_epochs,
#                   optimizer = optimizer,
#                   IS_decay = IS_decay,
#                   IS_debug = IS_debug,
#                   IS_test_mode = IS_test_mode,
#                   lr_center = lr_center,
#                   dbn_init = False)
#
#    else:
#        ###### This is the easy case
#        train_nnet(layer_size_list = layer_size_list,
#                   topology_dict = topology_dict,
#                   residual_dict = residual_dict,
#                   save_folder = save_folder,
#                   dataset = dataset,
#                   data_type = data_type,
#                   weight_file = weight_file,
#                   IS_mean_field = IS_mean_field,
#                   IS_persist = IS_persist,
#                   IS_centered = IS_centered,
#                   neg_steps = neg_steps,
#                   L1 = L1,
#                   L2 = L2,
#                   dropout_prob_list = dropout_prob_list,
#                   batch_size = batch_size,
#                   nb_epochs = nb_epochs,
#                   optimizer = optimizer,
#                   IS_decay = IS_decay,
#                   IS_debug = IS_debug,
#                   IS_test_mode = IS_test_mode,
#                   lr_center = lr_center,
#                   dbn_init = dbn_init)
#            
#      
#%%
if __name__ == '__main__':
    
    layer_size_list = [1024] + [1024]
    topology_dict = {0:{1}}
    residual_dict = {}
    #layer_size_list = [1024]*4
    #topology_dict = {0:{1}, 1:{2}, 2:{3}}
    #residual_dict = {}
    #residual_dict = {0:{3}}
    noisy_residual = False
    nb_epochs = 500
    
    
    save_folder = '../results/pca_0'
    IS_persist = True
    
    optimizer = {'name': 'sgd', 'lr' : 0.01, 'momentum' : 0.0,
                'schedule_decay' : 0.004, 'nesterov' : True, 'beta' : 1.0,
                #'decay' : 0.0}
                'decay' : 1.8e-5}
                 # 'schedule_decay' : 0.004
    
    neg_steps = 1
    IS_centered = True
    IS_std = False
    IS_whitened = True
    white_type = 'PCA' # ['PCA', 'ZCA', 'eig', 'random']
   
    dropout_prob_list = [0.0]*len(layer_size_list)
    #dropout_prob_list = [0.1]+[0.25]*(len(layer_size_list)-1) 
    
    
    #optimizer = {'name' : 'nadam'}
    #             'decay' : 1.8e-5,
    #             'mom_iter_max' : 600*150}
    #optimizer = {'name': 'sgdr', 'lr' : 0.01, 'momentum' : 0.9,
    #             'nesterov' : True, 'beta' : 1.0}
    
    lr_center = 0.01
    IS_decay = True
    
    weight_file = None
    dataset = 'SVHN'
    data_type = 'threshold'
    IS_mean_field = True
    
    L1 = 0.0
    L2 = 0.0
    batch_size = 100    
    flip_noise = 1e-3 # 1e-3
    
    IS_debug = False
    dbn_init = False
    IS_test_mode = False
    IS_prob_train = True
    
#    main(layer_size_list, topology_dict, residual_dict,
#         save_folder, dataset, data_type, weight_file,
#         IS_mean_field, IS_persist, IS_centered,
#         neg_steps, L1, L2, dropout_prob_list,
#         batch_size, nb_epochs, optimizer,
#         IS_decay, IS_debug, IS_test_mode,
#         lr_center, dbn_init, IS_prob_train)
    
    train_nnet(layer_size_list, topology_dict, residual_dict,
         save_folder, dataset, data_type, weight_file,
         IS_mean_field, IS_persist, IS_centered, IS_std, IS_whitened,
         white_type,
         neg_steps, L1, L2, dropout_prob_list,
         batch_size, nb_epochs, optimizer,
         IS_decay, IS_debug, IS_test_mode,
         lr_center, dbn_init, IS_prob_train, flip_noise, noisy_residual)
