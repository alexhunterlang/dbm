#%%
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *

import os
import numpy as np
from six.moves import zip
import theano.tensor as T
import theano
import h5py
from theano.ifelse import ifelse

from collections import OrderedDict

# The custom imports should be in same folder as this script
import __main__ as main
if not hasattr(main, '__file__'):
    # This means its an iPython instance
    import getpass
    usr = getpass.getuser()
    if usr=='alexlangMacBookPro':
        path_to_script_folder = '/Users/alexlangMacBookPro/Dropbox/BitBucket/dbm/src/'
        os.chdir(path_to_script_folder)

from utils import make_shared

FLOATX = theano.config.floatX
EPS = np.finfo(FLOATX).eps

#%%
class BaseOptimizer(object):
    """ Needs to be fully implemented """
    def __init__(self, nnet):
        self.weights = []
        self.nnet = nnet
        self.init_momentum(self.nnet.trainable_weights)
    
    def init_momentum(self, params):
        # need to initialize velocities
        if not hasattr(self, 'velocity'):
            self.velocity = []
            for p in params:
                v = make_shared(np.zeros(p.shape.eval()),name='vel_'+p.name)
                self.velocity.append(v)
                self.weights.append(v)
    
    #def set_velocity(self, new_velocity):
    #    assert len(new_velocity) == len(self.velocity)
    #    for n_v, v in zip(new_velocity, self.velocity):
    #        if hasattr(n_v, 'eval'):
    #            n_v = n_v.eval()
    #        elif hasattr(n_v, 'get_value'):
    #            n_v = n_v.get_value()
    #        else:
    #            raise NotImplementedError
    #            
    #        self.v.set_value(n_v)
    
    def get_updates(self, params, grads):
        pass
    
    def save_weights(self, filepath, overwrite=True):
        """Dumps all layer weights to a HDF5 file.
        The weight file has:
            - `layer_names` (attribute), a list of strings
                (ordered names of model layers).
            - For every layer, a `group` named `layer.name`
                - For every such layer group, a group attribute `weight_names`,
                    a list of strings
                    (ordered names of weights tensor of the layer).
                - For every weight in the layer, a dataset
                    storing the weight value, named after the weight tensor.
        """
        
        # If file exists and should not be overwritten:
        if not overwrite and os.path.isfile(filepath):
            raise NotImplementedError
        f = h5py.File(filepath, 'w')
        self.save_weights_to_hdf5_group(f)
        f.flush()
        f.close()

    def save_weights_to_hdf5_group(self, f):
        
        weights = self.weights
        f.attrs['weights'] = [w.name.encode('utf8') for w in weights]

        for w in weights:
            g = f.create_group(w.name)
            weight_values = [w.get_value()]
            weight_names = [w.name.encode('utf8')]
            g.attrs['weight_names'] = weight_names
            for name, val in zip(weight_names, weight_values):
                param_dset = g.create_dataset(name, val.shape,
                                              dtype=val.dtype)
                if not val.shape:
                    # scalar
                    param_dset[()] = val
                else:
                    param_dset[:] = val

#%%
class SGD(BaseOptimizer):
    """Stochastic gradient descent optimizer.
    Includes support for momentum,
    learning rate decay, and Nesterov momentum.
    # Arguments
        lr: float >= 0. Learning rate.
        momentum: float >= 0. Parameter updates momentum.
        decay: float >= 0. Learning rate decay over each update.
        nesterov: boolean. Whether to apply Nesterov momentum.
    """

    def __init__(self, lr=0.01, beta = 1.0, momentum=0.9, nesterov=True,
                     schedule_decay = 0.004, decay = 1.8e-5,
                     mom_iter_max = 500*950, nnet = None):

        

        self.updates = OrderedDict()
        self.iterations = make_shared(0, 'iterations')
        self.beta_orig = T.cast(beta, FLOATX)
        self.beta = make_shared(beta, 'beta')
        self.lr_orig = T.cast(lr, FLOATX)
        self.lr = make_shared(lr, 'lr')
        self.momentum_goal = make_shared(momentum, 'momentum_goal')
        self.momentum = make_shared(momentum, 'momentum')
        self.nesterov = nesterov
        
        self.schedule_decay = make_shared(schedule_decay, 'schedule_decay')
        self.decay = make_shared(decay, 'decay')
        self.mom_iter_max = T.cast(mom_iter_max, FLOATX)

        self.weights = [self.iterations, self.beta, self.lr, self.momentum]

        super(SGD, self).__init__(nnet)

    def get_updates(self, params, grads):

        self.updates = OrderedDict()

        # need to initialize velocities
        if not hasattr(self, 'velocity'):
            self.init_momentum(params)
        
        #lr = self.lr/self.beta
        lr = self.lr_orig/(1+self.decay*self.iterations)
        self.updates[self.lr] = lr

        momentum = ifelse(T.lt(self.iterations, self.mom_iter_max),
                        (self.momentum_goal
                         *(1. - 0.5 * (T.pow(0.96, self.iterations * self.schedule_decay)))
                         *(1. - 0.5 * (T.pow(0.96, (self.mom_iter_max-self.iterations) * self.schedule_decay)))),
                        0.5*self.momentum_goal)
 
        self.updates[self.momentum] = momentum
        
        self.updates[self.iterations] = self.iterations+1

        velocity_shared = self.velocity
        velocity = self.nnet.apply_gauge(self.velocity,
                                         new = False, change = True)

        for p, g, v_old, v_sh in zip(params, grads, velocity, velocity_shared):
            
            v = momentum * v_old - lr * g  # velocity
            self.updates[v_sh] = v

            if self.nesterov:
                #new_p = p + momentum * v - lr * g
                del_p = momentum * v - lr * g
            else:
                del_p = v
                #new_p = p + v

            self.updates[p] = del_p

        return self.updates

#%%
class SGDR(BaseOptimizer):
    """Stochastic gradient descent optimizer.
    Includes support for momentum,
    learning rate decay, and Nesterov momentum.
    # Arguments
        lr: float >= 0. Learning rate.
        momentum: float >= 0. Parameter updates momentum.
        decay: float >= 0. Learning rate decay over each update.
        nesterov: boolean. Whether to apply Nesterov momentum.
    """

    def __init__(self, lr=0.01, beta = 1.0, momentum=0.9, nesterov=True,
                     schedule_decay = 0.004, 
                     mom_iter_max = 500*975,
                     lr_iter_decay = 500*950,
                     lr_iter_max = 500*1000, nnet = None):

        self.updates = OrderedDict()
        self.iterations = make_shared(0, 'iterations')
        self.beta_orig = T.cast(beta, FLOATX)
        self.beta = make_shared(beta, 'beta')
        self.lr_orig = T.cast(lr, FLOATX)
        self.lr = make_shared(lr, 'lr')
        self.momentum_goal = make_shared(momentum, 'momentum_goal')
        self.momentum = make_shared(momentum, 'momentum')
        self.nesterov = nesterov
        
        self.schedule_decay = make_shared(schedule_decay, 'schedule_decay')
        self.mom_iter_max = T.cast(mom_iter_max, FLOATX)
        self.lr_iter_decay = T.cast(lr_iter_decay, FLOATX)
        self.lr_iter_max = T.cast(lr_iter_max, FLOATX)

        self.weights = [self.iterations, self.beta, self.lr, self.momentum]

        super(SGDR, self).__init__(nnet)

    def get_updates(self, params, grads):

        self.updates = OrderedDict()

        # need to initialize velocities
        if not hasattr(self, 'velocity'):
            self.init_momentum(params)
        
        #lr = self.lr/self.beta
        #lr = self.lr_orig/(1+self.decay*self.iterations)
        cos_const = T.cast(np.pi/(self.lr_iter_max-self.lr_iter_decay), dtype=FLOATX)
        lr = ifelse(T.lt(self.iterations, self.lr_iter_decay),
                    self.lr_orig,
                    0.5*self.lr_orig*(1.0+T.cos((self.iterations-self.lr_iter_decay)*cos_const))
                    )
        
        self.updates[self.lr] = lr

        momentum = ifelse(T.lt(self.iterations, self.mom_iter_max),
                        (self.momentum_goal
                         *(1. - 0.5 * (T.pow(0.96, self.iterations * self.schedule_decay)))
                         *(1. - 0.5 * (T.pow(0.96, (self.mom_iter_max-self.iterations) * self.schedule_decay)))),
                        0.5*self.momentum_goal)
 
        self.updates[self.momentum] = momentum
        
        self.updates[self.iterations] = self.iterations+1

        velocity_shared = self.velocity
        velocity = self.nnet.apply_gauge(self.velocity,
                                         new = False, change = True)

        for p, g, v_old, v_sh in zip(params, grads, velocity, velocity_shared):
            
            v = momentum * v_old - lr * g  # velocity
            self.updates[v_sh] = v

            if self.nesterov:
                #new_p = p + momentum * v - lr * g
                del_p = momentum * v - lr * g
            else:
                #new_p = p + v
                del_p = v

            self.updates[p] = del_p

        return self.updates
    


#%%
class Nadam(BaseOptimizer):
    """Nesterov Adam optimizer.
    Much like Adam is essentially RMSprop with momentum,
    Nadam is Adam RMSprop with Nesterov momentum.
    Default parameters follow those provided in the paper.
    It is recommended to leave the parameters of this optimizer
    at their default values.
    # Arguments
        lr: float >= 0. Learning rate.
        beta_1/beta_2: floats, 0 < beta < 1. Generally close to 1.
        epsilon: float >= 0. Fuzz factor.
    # References
        - [Nadam report](http://cs229.stanford.edu/proj2015/054_report.pdf)
        - [On the importance of initialization and momentum in deep learning](http://www.cs.toronto.edu/~fritz/absps/momentum.pdf)
    """

    def __init__(self, lr=0.002, beta = 1.0, beta_1=0.9, beta_2=0.999,
                 epsilon=EPS, schedule_decay=0.004, nnet = None):
        self.updates = OrderedDict()
        self.iterations = make_shared(0, 'iterations')
        self.m_schedule = make_shared(1, 'm_schedule')
        self.lr = make_shared(lr, 'lr')
        self.beta = make_shared(beta, 'beta')
        self.beta_1 = make_shared(beta_1, 'beta_1')
        self.beta_2 = make_shared(beta_2, 'beta_2')
        self.epsilon = make_shared(epsilon, 'epsilon')
        self.schedule_decay = make_shared(schedule_decay, 'schedule_decay')

        self.weights = [self.iterations, self.m_schedule, self.lr, self.beta,
                        self.beta_1, self.beta_2, self.epsilon, self.schedule_decay]

        super(Nadam, self).__init__(nnet)
    
    def get_updates(self, params, grads):

        self.updates = OrderedDict()
        
        # need to initialize velocities
        if not hasattr(self, 'velocity'):
            raise NotImplementedError, 'Did not update to rotating variables'
            self.velocity = []
            self.m_velocity = []
            for p in params:
                v = make_shared(np.zeros(p.shape.eval()),name='vel_'+p.name)
                self.velocity.append(v)
                self.weights.append(v)
                m = make_shared(np.zeros(p.shape.eval()),name='m_vel_'+p.name)
                self.m_velocity.append(m)
                self.weights.append(m)
        
        self.updates[self.iterations] = self.iterations+1
        
        t = self.iterations + 1

        # Due to the recommendations in [2], i.e. warming momentum schedule
        momentum_cache_t = self.beta_1 * (1. - 0.5 * (T.pow(0.96, t * self.schedule_decay)))
        momentum_cache_t_1 = self.beta_1 * (1. - 0.5 * (T.pow(0.96, (t + 1) * self.schedule_decay)))
        m_schedule_new = self.m_schedule * momentum_cache_t
        m_schedule_next = self.m_schedule * momentum_cache_t * momentum_cache_t_1
        self.updates[self.m_schedule] = m_schedule_new

        lr = self.lr/self.beta

        for p, g, m, v in zip(params, grads, self.m_velocity, self.velocity):
            # the following equations given in [1]
            g_prime = g / (1. - m_schedule_new)
            m_t = self.beta_1 * m + (1. - self.beta_1) * g
            m_t_prime = m_t / (1. - m_schedule_next)
            v_t = self.beta_2 * v + (1. - self.beta_2) * T.sqr(g)
            v_t_prime = v_t / (1. - T.pow(self.beta_2, t))
            m_t_bar = (1. - momentum_cache_t) * g_prime + momentum_cache_t_1 * m_t_prime

            self.updates[m] = m_t
            self.updates[v] = v_t

            #p_t = p - lr * m_t_bar / (T.sqrt(v_t_prime) + self.epsilon)
            #new_p = p_t
            del_p = - lr * m_t_bar / (T.sqrt(v_t_prime) + self.epsilon)

            self.updates[p] = del_p   
                        
        return self.updates
