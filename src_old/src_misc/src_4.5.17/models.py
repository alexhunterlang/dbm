#%%
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *

import copy
import numpy as np
import os
import theano.tensor as T
import theano
import time

# The custom imports should be in same folder as this script
import __main__ as main
if not hasattr(main, '__file__'):
    # This means its an iPython instance
    import getpass
    usr = getpass.getuser()
    if usr=='alexlangMacBookPro':
        path_to_script_folder = '/Users/alexlangMacBookPro/Dropbox/BitBucket/dbm/src/'
        os.chdir(path_to_script_folder)

import callbacks as cbks

FLOATX = theano.config.floatX # should be FLOATX32 for GPU
EPS = np.finfo(FLOATX).eps

#%%
def make_batches(size, batch_size):
    """Returns a list of batch indices (tuples of indices).
    """
    nb_batch = int(np.ceil(size / float(batch_size)))
    return [(i * batch_size, min(size, (i + 1) * batch_size))
            for i in range(0, nb_batch)]

#%%
class Model(object):

    def __init__(self, inputs, nnet):
        
        self.inputs = inputs
        self.index = T.ivector('index')
        self.beta = T.scalar('beta')
        self.lr = T.scalar('lr')
        self.nnet = nnet
        self.theano_rng = self.nnet.theano_rng
       
        self.layers = self.nnet.layers
        self.layer_indices = self.nnet.layer_indices
        
        self.IS_compiled = False

 
    def compile(self, optimizer, nb_gibbs_neg = 5, L1 = 0.0, L2 = 0.0,
                lr_center = 0.01, flip_noise = 0.0):
        """Configures the model for training.
        """
        
        self.optimizer = optimizer
        self.nb_gibbs_neg = nb_gibbs_neg
        self.L1 = T.cast(L1, FLOATX)
        self.L2 = T.cast(L2, FLOATX)
        self.lr_center = T.cast(lr_center, FLOATX).eval()
        self.flip_noise = T.cast(lr_center, FLOATX).eval()
        
        # initializes the momentum variables with the correct shapes
        #self.optimizer.init_momentum(self.nnet.trainable_weights)
        
        self.IS_compiled = True

    def _make_train_function(self):
        if not self.IS_compiled:
            raise RuntimeError('You must compile your model before using it.')

        # adjust L1, L2 so they are independent of batch size
        norm_adj = self.nb_train_sample/self.batch_size
        L1 = self.L1/norm_adj
        L2 = self.L2/norm_adj

        cost, params, grads = self.nnet.training_update(self.inputs,
                                                        IS_dropout=1,
                                                        beta = self.beta,
                                                        nb_gibbs_neg = self.nb_gibbs_neg,
                                                        L1 = L1,
                                                        L2 = L2,
                                                        lr = self.lr,
                                                        flip_noise = self.flip_noise)

        #new_velocity = self.nnet.apply_gauge(self.optimizer.velocity,
        #                                     new = True)
        #self.optimizer.set_velocity(new_velocity)

        training_updates = self.optimizer.get_updates(params,grads)
        
        self.nnet.updates = training_updates
        updates = self.nnet.updates
        
        # returns cost. Updates weights at each call.
        givens = {self.inputs: self.train_data[self.index]}
        self.train_function = theano.function([self.index, self.beta, self.lr],
                                              cost,
                                              updates=updates,
                                              givens=givens,
                                              on_unused_input='ignore')

    def _make_validation_function(self):
        if not self.IS_compiled:
            raise RuntimeError('You must compile your model before using it.')
        
        # No dropout or norm cost in testing
        cost, updates = self.nnet.cd_cost_fxn(self.inputs,
                                              IS_dropout=0,
                                              beta = self.beta,
                                              nb_gibbs_neg = self.nb_gibbs_neg,
                                              L1 = 0.0,
                                              L2 = 0.0)
        
        # returns cost. Updates weights at each call.
        givens = {self.inputs: self.validation_data[self.index]}
        self.validation_function = theano.function([self.index, self.beta],
                                                   cost,
                                                   updates=updates,
                                                   givens=givens)
        
    def _make_test_function(self):
        if not self.IS_compiled:
            raise RuntimeError('You must compile your model before using it.')
        
        # No dropout or norm cost in testing
        cost, updates = self.nnet.cd_cost_fxn(self.inputs,
                                              IS_dropout=0,
                                              beta = self.beta,
                                              nb_gibbs_neg = self.nb_gibbs_neg,
                                              L1 = 0.0,
                                              L2 = 0.0)
        
        # returns cost. Updates weights at each call.
        givens = {self.inputs: self.test_data[self.index]}
        self.test_function = theano.function([self.index, self.beta],
                                             cost,
                                              updates=updates,
                                              givens=givens)

    def _fit_loop(self, f, out_labels=None, batch_size=100,
                  nb_epoch=100, callbacks=None,
                  val_f=None, shuffle=True,
                  callback_metrics=None, initial_epoch=0):
        """Abstract fit function for f(ins).
        Assume that f returns a list, labeled by out_labels.
        # Arguments
            f: Keras function returning a list of tensors
            ins: list of tensors to be fed to `f`
            out_labels: list of strings, display names of
                the outputs of `f`
            batch_size: integer batch size
            nb_epoch: number of times to iterate over the data
            callbacks: list of callbacks to be called during training
            val_f: Keras function to call for validation
            val_ins: list of tensors to be fed to `val_f`
            shuffle: whether to shuffle the data at the beginning of each epoch
            callback_metrics: list of strings, the display names of the metrics
                passed to the callbacks. They should be the
                concatenation of list the display names of the outputs of
                 `f` and the list of display names of the outputs of `f_val`.
            initial_epoch: epoch at which to start training
                (useful for resuming a previous training run)
        # Returns
            `History` object.
        """
        
        time_start = time.time()
        time_cb = 0
        
        do_validation = False
        if val_f:
            do_validation = True

        index_array = np.arange(self.nb_train_sample, dtype='int32')

        start = time.time()
        self.history = cbks.History()
        callbacks = [cbks.BaseLogger()] + (callbacks or []) + [self.history]
        callbacks = cbks.CallbackList(callbacks)
        out_labels = out_labels or []

        callbacks.set_model(self)
        callbacks.set_params({
            'batch_size': batch_size,
            'nb_epoch': nb_epoch,
            'nb_sample': self.nb_train_sample,
            'do_validation': do_validation,
            'metrics': callback_metrics or [],
        })
        callbacks.on_train_begin()
        self.stop_training = False
        time_cb += time.time()-start

        for epoch in range(initial_epoch, nb_epoch):
            start = time.time()
            callbacks.on_epoch_begin(epoch)
            time_cb += time.time()-start
            if shuffle:
                np.random.shuffle(index_array)

            batches = make_batches(self.nb_train_sample, batch_size)
            epoch_logs = {}
            for batch_index, (batch_start, batch_end) in enumerate(batches):
                batch_ids = index_array[batch_start:batch_end]
                batch_logs = {}
                batch_logs['batch'] = batch_index
                batch_logs['size'] = len(batch_ids)
                start = time.time()
                callbacks.on_batch_begin(batch_index, batch_logs)
                time_cb += time.time()-start
                beta = self.optimizer.beta.get_value()
                lr = self.lr_center
                outs = f(batch_ids, beta, lr)
                if not isinstance(outs, list):
                    outs = [outs]
                for l, o in zip(out_labels, outs):
                    batch_logs[l] = o

                start = time.time()
                callbacks.on_batch_end(batch_index, batch_logs)
                time_cb += time.time() -start

                if batch_index == len(batches) - 1:  # last batch
                    # validation
                    if do_validation:
                        nb_validation_sample = self.validation_data.shape[0].eval()
                        val_outs = self._test_loop(val_f,nb_validation_sample,
                                                   batch_size=batch_size)
                        if not isinstance(val_outs, list):
                            val_outs = [val_outs]
                        # same labels assumed
                        for l, o in zip(out_labels, val_outs):
                            epoch_logs['val_' + l] = o
            start = time.time()
            callbacks.on_epoch_end(epoch, epoch_logs)
            time_cb += time.time()-start
            if self.stop_training:
                break
        start = time.time()
        callbacks.on_train_end()
        time_cb += time.time()-start
        
        self.fit_loop_time = time.time() - time_start
        self.fit_loop_callback_time = time_cb
        self.fit_loop_train_time = self.fit_loop_time
        
        return self.history
    
    def _test_loop(self, f, nb_sample, batch_size=100):
        """Abstract method to loop over some data in batches.
        # Arguments
            f: Keras function returning a list of tensors.
            ins: list of tensors to be fed to `f`.
            batch_size: integer batch size.
            verbose: verbosity mode.
        # Returns
            Scalar loss (if the model has a single output and no metrics)
            or list of scalars (if the model has multiple outputs
            and/or metrics). The attribute `model.metrics_names` will give you
            the display labels for the scalar outputs.
        """
        
        outs = []
        batches = make_batches(nb_sample, batch_size)
        index_array = np.arange(nb_sample, dtype='int32')
        for batch_index, (batch_start, batch_end) in enumerate(batches):
            batch_ids = index_array[batch_start:batch_end]
            beta = self.optimizer.beta.get_value()
            batch_outs = f(batch_ids, beta)
            if isinstance(batch_outs, list):
                if batch_index == 0:
                    for batch_out in enumerate(batch_outs):
                        outs.append(0.)
                for i, batch_out in enumerate(batch_outs):
                    outs[i] += batch_out * len(batch_ids)
            else:
                if batch_index == 0:
                    outs.append(0.)
                outs[0] += batch_outs * len(batch_ids)

        for i, out in enumerate(outs):
            outs[i] /= nb_sample
        if len(outs) == 1:
            return outs[0]
        return outs

    def fit(self, x, batch_size=100, nb_epoch=10, callbacks=None,
            validation_data=None, shuffle=True, initial_epoch=0):
        """Trains the model for a fixed number of epochs (iterations on a dataset).
        # Arguments
            x: Theano shared array of training data
            batch_size: integer. Number of samples per gradient update.
            nb_epoch: integer, the number of times to iterate
                over the training data arrays.
            callbacks: list of callbacks to be called during training.
            validation_data: Theano shared array of data on which to evaluate
                the loss and any model metrics at the end of each epoch.
                The model will not be trained on this data.
            shuffle: boolean, whether to shuffle the training data
                before each epoch.
            initial_epoch: epoch at which to start training
                (useful for resuming a previous training run)
        # Returns
            A `History` instance. Its `history` attribute contains
            all information collected during training.
        """
        self.train_data = x
        self.nb_train_sample = x.shape[0].eval()
        self.validation_data = validation_data
        self.batch_size = batch_size
        
        # havent put in generalized code
        assert np.mod(self.nb_train_sample, batch_size) == 0
        
        if hasattr(self.nnet, 'batch_size'):
            assert self.nnet.batch_size == batch_size, 'Mismatch of batch_size'

        # makes the training functions    
        self._make_train_function()
        f = self.train_function
    
        # preps for validation
        out_labels = ['cost']
        if validation_data:
            callback_metrics = copy.copy(out_labels) + ['val_' + n for n in out_labels]
            self._make_validation_function()
            val_f = self.validation_function
        else:
            callback_metrics = copy.copy(out_labels)
            val_f = None

        # delegate logic to _fit_loop
        return self._fit_loop(f, out_labels=out_labels,
                              batch_size=batch_size, nb_epoch=nb_epoch,
                              callbacks=callbacks,
                              val_f=val_f, shuffle=shuffle,
                              callback_metrics=callback_metrics,
                              initial_epoch=initial_epoch)
