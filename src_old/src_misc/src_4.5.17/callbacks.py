    #%%
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)

import os
import csv

import numpy as np
import time
import warnings
import six

import theano
import theano.tensor as T

import pandas as pd
# Force matplotlib to not use any Xwindows backend.
# Otherwise will get error on linux servers
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

from collections import deque
from collections import OrderedDict
from collections import Iterable

import __main__ as main
if not hasattr(main, '__file__'):
    # This means its an iPython instance
    import getpass
    usr = getpass.getuser()
    if usr=='alexlangMacBookPro':
        path_to_script_folder = '/Users/alexlangMacBookPro/Dropbox/BitBucket/dbm/src/'
        os.chdir(path_to_script_folder)
        
from ais import AIS

FLOATX = theano.config.floatX
EPS = np.finfo(FLOATX).eps

#%%
class CallbackList(object):
    """Container abstracting a list of callbacks.
    # Arguments
        callbacks: List of `Callback` instances.
        queue_length: Queue length for keeping
            running statistics over callback execution time.
    """

    def __init__(self, callbacks=None, queue_length=10):
        callbacks = callbacks or []
        self.callbacks = [c for c in callbacks]
        self.queue_length = queue_length

    def append(self, callback):
        self.callbacks.append(callback)

    def set_params(self, params):
        for callback in self.callbacks:
            callback.set_params(params)

    def set_model(self, model):
        for callback in self.callbacks:
            callback.set_model(model)

    def on_epoch_begin(self, epoch, logs=None):
        """Called at the start of an epoch.
        # Arguments
            epoch: integer, index of epoch.
            logs: dictionary of logs.
        """
        logs = logs or {}
        for callback in self.callbacks:
            callback.on_epoch_begin(epoch, logs)
        self._delta_t_batch = 0.
        self._delta_ts_batch_begin = deque([], maxlen=self.queue_length)
        self._delta_ts_batch_end = deque([], maxlen=self.queue_length)

    def on_epoch_end(self, epoch, logs=None):
        """Called at the end of an epoch.
        # Arguments
            epoch: integer, index of epoch.
            logs: dictionary of logs.
        """
        logs = logs or {}
        for callback in self.callbacks:
            callback.on_epoch_end(epoch, logs)

    def on_batch_begin(self, batch, logs=None):
        """Called right before processing a batch.
        # Arguments
            batch: integer, index of batch within the current epoch.
            logs: dictionary of logs.
        """
        logs = logs or {}
        t_before_callbacks = time.time()
        for callback in self.callbacks:
            callback.on_batch_begin(batch, logs)
        self._delta_ts_batch_begin.append(time.time() - t_before_callbacks)
        delta_t_median = np.median(self._delta_ts_batch_begin)
        if (self._delta_t_batch > 0. and
           delta_t_median > 0.95 * self._delta_t_batch and
           delta_t_median > 0.1):
            warnings.warn('Method on_batch_begin() is slow compared '
                          'to the batch update (%f). Check your callbacks.'
                          % delta_t_median)
        self._t_enter_batch = time.time()

    def on_batch_end(self, batch, logs=None):
        """Called at the end of a batch.
        # Arguments
            batch: integer, index of batch within the current epoch.
            logs: dictionary of logs.
        """
        logs = logs or {}
        if not hasattr(self, '_t_enter_batch'):
            self._t_enter_batch = time.time()
        self._delta_t_batch = time.time() - self._t_enter_batch
        t_before_callbacks = time.time()
        for callback in self.callbacks:
            callback.on_batch_end(batch, logs)
        self._delta_ts_batch_end.append(time.time() - t_before_callbacks)
        delta_t_median = np.median(self._delta_ts_batch_end)
        if (self._delta_t_batch > 0. and
           (delta_t_median > 0.95 * self._delta_t_batch and delta_t_median > 0.1)):
            warnings.warn('Method on_batch_end() is slow compared '
                          'to the batch update (%f). Check your callbacks.'
                          % delta_t_median)

    def on_train_begin(self, logs=None):
        """Called at the beginning of training.
        # Arguments
            logs: dictionary of logs.
        """
        logs = logs or {}
        for callback in self.callbacks:
            callback.on_train_begin(logs)

    def on_train_end(self, logs=None):
        """Called at the end of training.
        # Arguments
            logs: dictionary of logs.
        """
        logs = logs or {}
        for callback in self.callbacks:
            callback.on_train_end(logs)

#%%
class Callback(object):
    """Abstract base class used to build new callbacks.
    # Properties
        params: dict. Training parameters
            (eg. verbosity, batch size, number of epochs...).
        model: instance of `keras.models.Model`.
            Reference of the model being trained.
    The `logs` dictionary that callback methods
    take as argument will contain keys for quantities relevant to
    the current batch or epoch.
    Currently, the `.fit()` method of the `Sequential` model class
    will include the following quantities in the `logs` that
    it passes to its callbacks:
        on_epoch_end: logs include `acc` and `loss`, and
            optionally include `val_loss`
            (if validation is enabled in `fit`), and `val_acc`
            (if validation and accuracy monitoring are enabled).
        on_batch_begin: logs include `size`,
            the number of samples in the current batch.
        on_batch_end: logs include `loss`, and optionally `acc`
            (if accuracy monitoring is enabled).
    """

    def __init__(self):
        pass

    def set_params(self, params):
        self.params = params

    def set_model(self, model):
        self.model = model

    def on_epoch_begin(self, epoch, logs=None):
        pass

    def on_epoch_end(self, epoch, logs=None):
        pass

    def on_batch_begin(self, batch, logs=None):
        pass

    def on_batch_end(self, batch, logs=None):
        pass

    def on_train_begin(self, logs=None):
        pass

    def on_train_end(self, logs=None):
        pass

#%%
class BaseLogger(Callback):
    """Callback that accumulates epoch averages of metrics.
    This callback is automatically applied to every Keras model.
    """

    def on_epoch_begin(self, epoch, logs=None):
        self.seen = 0
        self.totals = {}

    def on_batch_end(self, batch, logs=None):
        logs = logs or {}
        batch_size = logs.get('size', 0)
        self.seen += batch_size

        for k, v in logs.items():
            if k in self.totals:
                self.totals[k] += v * batch_size
            else:
                self.totals[k] = v * batch_size

    def on_epoch_end(self, epoch, logs=None):
        if logs is not None:
            for k in self.params['metrics']:
                if k in self.totals:
                    # Make value available to next callbacks.
                    logs[k] = self.totals[k] / self.seen

#%%
class History(Callback):
    """Callback that records events into a `History` object.
    This callback is automatically applied to
    every Keras model. The `History` object
    gets returned by the `fit` method of models.
    """

    def on_train_begin(self, logs=None):
        self.epoch = []
        self.history = {}

    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}
        self.epoch.append(epoch)
        for k, v in logs.items():
            self.history.setdefault(k, []).append(v)

#%%
class ModelCheckpoint(Callback):
    """Save the model after every epoch.
    `filepath` can contain named formatting options,
    which will be filled the value of `epoch` and
    keys in `logs` (passed in `on_epoch_end`).
    For example: if `filepath` is `weights.{epoch:02d}-{val_loss:.2f}.hdf5`,
    then the model checkpoints will be saved with the epoch number and
    the validation loss in the filename.
    # Arguments
        filepath: string, path to save the model file.
        monitor: quantity to monitor.
        verbose: verbosity mode, 0 or 1.
        save_best_only: if `save_best_only=True`,
            the latest best model according to
            the quantity monitored will not be overwritten.
        mode: one of {auto, min, max}.
            If `save_best_only=True`, the decision
            to overwrite the current save file is made
            based on either the maximization or the
            minimization of the monitored quantity. For `val_acc`,
            this should be `max`, for `val_loss` this should
            be `min`, etc. In `auto` mode, the direction is
            automatically inferred from the name of the monitored quantity.
        save_weights_only: if True, then only the model's weights will be
            saved (`model.save_weights(filepath)`), else the full model
            is saved (`model.save(filepath)`).
        period: Interval (number of epochs) between checkpoints.
    """

    def __init__(self, filepath, monitor, save_best_only=False,
                         mode='min', period=1):
        
        super(ModelCheckpoint, self).__init__()
        self.monitor = monitor
        self.filepath = filepath
        self.save_best_only = save_best_only
        self.save_weights_only = True
        self.period = period
        self.epochs_since_last_save = 0

        assert mode in ['min', 'max']

        if mode == 'min':
            self.monitor_op = np.less
            self.best = np.Inf
        elif mode == 'max':
            self.monitor_op = np.greater
            self.best = -np.Inf

    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}
        self.epochs_since_last_save += 1
        if self.epochs_since_last_save >= self.period:
            self.epochs_since_last_save = 0
            filepath = self.filepath.format(epoch=epoch, **logs)
            if self.save_best_only:
                current = logs.get(self.monitor)
                if current is None:
                    warnings.warn('Can save best model only with %s available, '
                                  'skipping.' % (self.monitor), RuntimeWarning)
                else:
                    if self.monitor_op(current, self.best):
                        self.best = current
                        self.model.nnet.save_weights(filepath, overwrite=True)
            else:
                self.model.nnet.save_weights(filepath, overwrite=True)

#%%
class EarlyStopping(Callback):
    """Stop training when a monitored quantity has stopped improving.
    # Arguments
        monitor: quantity to be monitored.
        min_delta: minimum change in the monitored quantity
            to qualify as an improvement, i.e. an absolute
            change of less than min_delta, will count as no
            improvement.
        patience: number of epochs with no improvement
            after which training will be stopped.
        verbose: verbosity mode.
        mode: one of {auto, min, max}. In `min` mode,
            training will stop when the quantity
            monitored has stopped decreasing; in `max`
            mode it will stop when the quantity
            monitored has stopped increasing; in `auto`
            mode, the direction is automatically inferred
            from the name of the monitored quantity.
    """

    def __init__(self, monitor, min_delta=0, patience=0, mode='min'):
        super(EarlyStopping, self).__init__()

        self.monitor = monitor
        self.patience = patience
        self.min_delta = min_delta
        self.wait = 0
        self.stopped_epoch = 0

        assert mode in ['min', 'max']

        if mode == 'min':
            self.monitor_op = np.less
        elif mode == 'max':
            self.monitor_op = np.greater

        if self.monitor_op == np.greater:
            self.min_delta *= 1
        else:
            self.min_delta *= -1

    def on_train_begin(self, logs=None):
        self.wait = 0  # Allow instances to be re-used
        self.best = np.Inf if self.monitor_op == np.less else -np.Inf

    def on_epoch_end(self, epoch, logs=None):
        current = logs.get(self.monitor)
        if current is None:
            warnings.warn('Early stopping requires %s available!' %
                          (self.monitor), RuntimeWarning)

        if self.monitor_op(current - self.min_delta, self.best):
            self.best = current
            self.wait = 0
        else:
            if self.wait >= self.patience:
                self.stopped_epoch = epoch
                self.model.stop_training = True
            self.wait += 1


#%%
class ReduceLROnPlateau(Callback):
    """Reduce learning rate when a metric has stopped improving.
    Models often benefit from reducing the learning rate by a factor
    of 2-10 once learning stagnates. This callback monitors a
    quantity and if no improvement is seen for a 'patience' number
    of epochs, the learning rate is reduced.
    # Example
        ```python
            reduce_lr = ReduceLROnPlateau(monitor='val_loss', factor=0.2,
                                          patience=5, min_lr=0.001)
            model.fit(X_train, Y_train, callbacks=[reduce_lr])
        ```
    # Arguments
        monitor: quantity to be monitored.
        factor: factor by which the learning rate will
            be reduced. new_lr = lr * factor
        patience: number of epochs with no improvement
            after which learning rate will be reduced.
        verbose: int. 0: quiet, 1: update messages.
        mode: one of {auto, min, max}. In `min` mode,
            lr will be reduced when the quantity
            monitored has stopped decreasing; in `max`
            mode it will be reduced when the quantity
            monitored has stopped increasing; in `auto`
            mode, the direction is automatically inferred
            from the name of the monitored quantity.
        epsilon: threshold for measuring the new optimum,
            to only focus on significant changes.
        cooldown: number of epochs to wait before resuming
            normal operation after lr has been reduced.
        min_lr: lower bound on the learning rate.
    """

    def __init__(self, monitor, factor=0.1, patience=10,
                     mode='min', epsilon=1e-4, cooldown=0, min_lr=0):
        super(ReduceLROnPlateau, self).__init__()

        self.monitor = monitor
        if factor >= 1.0:
            raise ValueError('ReduceLROnPlateau '
                             'does not support a factor >= 1.0.')
        assert mode not in ['min', 'max']
        self.factor = factor
        self.min_lr = min_lr
        self.epsilon = epsilon
        self.patience = patience
        self.cooldown = cooldown
        self.cooldown_counter = 0  # Cooldown counter.
        self.wait = 0
        self.best = 0
        self.mode = mode
        self.monitor_op = None
        self._reset()

    def _reset(self):
        """Resets wait counter and cooldown counter.
        """
        if self.mode == 'min':
            self.monitor_op = lambda a, b: np.less(a, b - self.epsilon)
            self.best = np.Inf
        else:
            self.monitor_op = lambda a, b: np.greater(a, b + self.epsilon)
            self.best = -np.Inf
        self.cooldown_counter = 0
        self.wait = 0
        self.lr_epsilon = self.min_lr * 1e-4

    def on_train_begin(self, logs=None):
        self._reset()

    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}
        logs['lr'] = self.model.optimizer.lr.get_value()
        current = logs.get(self.monitor)
        if current is None:
            warnings.warn('Learning Rate Plateau Reducing requires %s available!' %
                          self.monitor, RuntimeWarning)
        else:
            if self.in_cooldown():
                self.cooldown_counter -= 1
                self.wait = 0

            if self.monitor_op(current, self.best):
                self.best = current
                self.wait = 0
            elif not self.in_cooldown():
                if self.wait >= self.patience:
                    old_lr = float(self.model.optimizer.lr.get_value())
                    if old_lr > self.min_lr + self.lr_epsilon:
                        new_lr = old_lr * self.factor
                        new_lr = max(new_lr, self.min_lr)
                        theano.set_value(self.model.optimizer.lr, new_lr)
                        if self.verbose > 0:
                            print('\nEpoch %05d: reducing learning rate to %s.' % (epoch, new_lr))
                        self.cooldown_counter = self.cooldown
                        self.wait = 0
                self.wait += 1

    def in_cooldown(self):
        return self.cooldown_counter > 0

#%%
class CSVLogger(Callback):
    """Callback that streams epoch results to a csv file.
    Supports all values that can be represented as a string,
    including 1D iterables such as np.ndarray.
    # Example
        ```python
            csv_logger = CSVLogger('training.log')
            model.fit(X_train, Y_train, callbacks=[csv_logger])
        ```
    # Arguments
        filename: filename of the csv file, e.g. 'run/log.csv'.
        separator: string used to separate elements in the csv file.
        append: True: append if file exists (useful for continuing
            training). False: overwrite existing file,
    """

    def __init__(self, filename, separator=',', append=False):
        self.sep = str(separator)
        self.filename = filename
        self.append = append
        self.writer = None
        self.keys = None
        self.append_header = True
        super(CSVLogger, self).__init__()

    def on_train_begin(self, logs=None):
        if self.append:
            if os.path.exists(self.filename):
                with open(self.filename) as f:
                    self.append_header = not bool(len(f.readline()))
            self.csv_file = open(self.filename, 'a')
        else:
            self.csv_file = open(self.filename, 'w')

    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}

        def handle_value(k):
            is_zero_dim_ndarray = isinstance(k, np.ndarray) and k.ndim == 0
            if isinstance(k, Iterable) and not is_zero_dim_ndarray:
                return '"[%s]"' % (', '.join(map(str, k)))
            else:
                return k

        if not self.writer:
            self.keys = sorted(logs.keys())

            class CustomDialect(csv.excel):
                delimiter = self.sep

            self.writer = csv.DictWriter(self.csv_file,
                                         fieldnames=['epoch'] + self.keys, dialect=CustomDialect)
            if self.append_header:
                self.writer.writeheader()

        row_dict = OrderedDict({'epoch': epoch})
        row_dict.update((key, handle_value(logs[key])) for key in self.keys)
        self.writer.writerow(row_dict)
        self.csv_file.flush()

    def on_train_end(self, logs=None):
        self.csv_file.close()
        self.writer = None

#%%
class OptimizerSpy(Callback):
    def __init__(self):
        super(OptimizerSpy, self).__init__()
    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}
        if hasattr(self.model.optimizer, 'lr'):
            logs['lr'] = self.model.optimizer.lr.get_value()
            
        if hasattr(self.model.optimizer, 'beta'):
            logs['beta'] = self.model.optimizer.beta.get_value()
        
        if hasattr(self.model.optimizer, 'momentum'):
            logs['momentum'] = self.model.optimizer.momentum.get_value()
        elif hasattr(self.model.optimizer, 'beta_1'):
            logs['momentum'] = self.model.optimizer.beta_1.get_value()

#%%
class PeriodicSave(Callback):
    
    def __init__(self, weight_path, epoch_ls, opt_path = None):
        super(PeriodicSave, self).__init__()
        self.weight_path = weight_path
        self.opt_path = opt_path
        self.epoch_ls = epoch_ls

    def on_epoch_end(self, epoch, logs=None):        
        # save special epochs
        if epoch in self.epoch_ls:
            filepath = self.weight_path.format(epoch=epoch, **logs)
            self.model.nnet.save_weights(filepath, overwrite=True)
            if self.opt_path is not None:
                filepath = self.opt_path.format(epoch=epoch, **logs)
                self.model.optimizer.save_weights(filepath, overwrite=True)

#%%
class LearningRateScheduler(Callback):
    """Learning rate scheduler.
    # Arguments
        schedule: a function that takes an epoch index as input
            (integer, indexed from 0) and returns a new
            learning rate as output (float).
    """

    def __init__(self, schedule):
        super(LearningRateScheduler, self).__init__()
        self.schedule = schedule

    def on_epoch_begin(self, epoch, logs=None):
        if not hasattr(self.model.optimizer, 'lr'):
            raise ValueError('Optimizer must have a "lr" attribute.')
            
        lr = self.schedule(epoch)
        
        if not isinstance(lr, (float, np.float32, np.float64)):
            raise ValueError('The output of the "schedule" function '
                             'should be float.')
            
        lr = T.cast(lr,dtype=FLOATX).eval()
        self.model.optimizer.lr.set_value(lr)


#%%
class MomentumRateScheduler(Callback):
    """Momentum rate scheduler.
    # Arguments
        schedule: a function that takes an epoch index as input
            (integer, indexed from 0) and returns a new
            momentum rate as output (float).
    """

    def __init__(self, schedule):
        super(MomentumRateScheduler, self).__init__()
        self.schedule = schedule

    def on_epoch_begin(self, epoch, logs=None):
        if hasattr(self.model.optimizer, 'momentum_goal'):
            pass
        elif hasattr(self.model.optimizer, 'beta_1'):
            pass
        else:
            raise ValueError('Optimizer must have a "momentum" or "beta_1" attribute.')
        
        momentum = self.schedule(epoch)
        
        if momentum is not None:
        
            if not isinstance(momentum, (float, np.float32, np.float64)):
                raise ValueError('The output of the "schedule" function '
                                 'should be float.')
            
            momentum = T.cast(momentum,dtype=FLOATX).eval()
            if hasattr(self.model.optimizer, 'momentum_goal'):
                self.model.optimizer.momentum_goal.set_value(momentum)
            elif hasattr(self.model.optimizer, 'beta_1'):
                self.model.optimizer.beta_1.set_value(momentum)
                
#%%
class BetaScheduler(Callback):
    """Beta scheduler.
    # Arguments
        schedule: a function that takes an epoch index as input
            (integer, indexed from 0) and returns a new
            beta rate as output (float).
    """

    def __init__(self, schedule):
        super(BetaScheduler, self).__init__()
        self.schedule = schedule

    def on_epoch_begin(self, epoch, logs=None):
        if hasattr(self.model.optimizer, 'beta'):
            pass
        else:
            raise ValueError('Optimizer must have a "beta" attribute.')
        
        beta = self.schedule(epoch)
                
        if not isinstance(beta, (float, np.float32, np.float64)):
            raise ValueError('The output of the "schedule" function '
                             'should be float.')
        
        beta = T.cast(beta,dtype=FLOATX).eval()
        self.model.optimizer.beta.set_value(beta)

#%%
#class FreeEnergy(Callback):
#    def __init__(self, fe_fxn, epoch_ls = []):
#        self.fe_fxn = fe_fxn
#        self.epoch_ls = epoch_ls
#        super(FreeEnergy, self).__init__()
#        
#    def on_train_begin(self, logs={}):
#        return
#
#    def on_train_end(self, logs={}):
#        return
# 
#    def on_epoch_begin(self, epoch, logs={}):
#        return
# 
#    def on_epoch_end(self, epoch, logs={}):
#
#        if (len(self.epoch_ls)==0) or (epoch in self.epoch_ls):
#            
#            beta = self.model.optimizer.beta.get_value()
#            data = self.model.train_data.get_value()
#            logs['free_energy'] = self.fe_fxn(data,0,beta)
#            data = self.model.validation_data.get_value()
#            logs['val_free_energy'] = self.fe_fxn(data,0,beta)
#        
#        return logs
# 
#    def on_batch_begin(self, batch, logs={}):
#        return
# 
#    def on_batch_end(self, batch, logs={}):
#        return

#%%
#class EpochEnd(Callback):
#    def __init__(self, outputs_dict, epoch_ls = []):
#
#        self.outputs = []
#        self.outputs_names = []
#        for k, v in six.iteritems(outputs_dict):
#            self.outputs.append(v)
#            self.outputs_names.append(k)
#        
#        self.epoch_ls = epoch_ls
#
#        super(EpochEnd, self).__init__()
#    
#    def on_train_begin(self, logs={}):
#        return
#
#    def on_train_end(self, logs={}):
#        return
# 
#    def on_epoch_begin(self, epoch, logs={}):
#        return
# 
#    def on_epoch_end(self, epoch, logs={}):
#
#        inputs = self.model.inputs
#        outputs = [f(inputs) for f in self.outputs]
#        self.fn = theano.function([inputs], outputs)
#        
#        if (len(self.epoch_ls)==0) or (epoch in self.epoch_ls):
#            
#            train_outputs = self.fn(self.model.train_data.get_value())
#            valid_outputs = self.fn(self.model.validation_data.get_value())
#    
#            if not isinstance(train_outputs,list):
#                train_outputs = [train_outputs]
#            if not isinstance(valid_outputs,list):
#                valid_outputs = [valid_outputs]
#            
#            for i in range(len(train_outputs)):
#                key = self.outputs_names[i]
#                logs[key] = train_outputs[i]
#                logs['val_'+key] = valid_outputs[i]
#        
#        return logs
# 
#    def on_batch_begin(self, batch, logs={}):
#        return
# 
#    def on_batch_end(self, batch, logs={}):
#        return

#%%
class AISCallback(Callback):
    def __init__(self, dbm, nb_runs, nb_betas, epoch_ls = [], name='',
                     exact = False):

        self.nb_runs = nb_runs
        self.nb_betas = nb_betas
        self.epoch_ls = epoch_ls
        self.dbm = dbm
        self.exact = exact
        
        if len(name)>0:
            self.name = '_'+name
        else:
            self.name = ''


        super(AISCallback, self).__init__()
    
    def on_train_begin(self, logs={}):
        return
        
    def on_train_end(self, logs={}):
        return
 
    def on_epoch_begin(self, epoch, logs={}):
        return
 
    def on_epoch_end(self, epoch, logs={}):
        
        if (len(self.epoch_ls)==0) or (epoch in self.epoch_ls):
            
            ais = AIS(self.dbm, self.nb_runs, self.nb_betas,
                      self.model.train_data.get_value(),
                     final_beta = self.model.optimizer.beta.get_value())
  
            logZ, logZ_est_up, logZ_est_down = ais.log_z()
             
            beta = self.model.optimizer.beta.get_value()
            fe_fxn = ais.free_energy_fxn()
            
            data = self.model.train_data.get_value()
            fe = fe_fxn(data,0,beta)
            logs['free_energy'] = fe
                
            data = self.model.validation_data.get_value()
            val_fe = fe_fxn(data,0,beta)
            logs['val_free_energy'] = val_fe

            logs['logz'+self.name] = logZ
            logs['logz_high'+self.name] = logZ_est_up
            logs['logz_low'+self.name] = logZ_est_down
            
            logs['prob'+self.name] = -fe - logZ
            logs['prob_high'+self.name] = -fe - logZ_est_down
            logs['prob_low'+self.name] = -fe - logZ_est_up
                
            logs['val_prob'+self.name] = -val_fe - logZ
            logs['val_prob_high'+self.name] = -val_fe - logZ_est_down
            logs['val_prob_low'+self.name] = -val_fe - logZ_est_up
                
            if self.exact:
                logs['logz_exact'+self.name] = self.ais.exact_log_z(self.dbm)
                
        else:
            
            logs['free_energy'] = np.nan
            logs['val_free_energy'] = np.nan
            
            for w1 in ['logz', 'prob', 'val_prob']:
                for w2 in ['', '_high', '_low']:
                    logs[w1+w2+self.name] = np.nan
            
            if self.exact:
                logs['logz_exact'+self.name]  = np.nan
            
        return logs
 
    def on_batch_begin(self, batch, logs={}):
        return
 
    def on_batch_end(self, batch, logs={}):
        return

#%%
class SampleCallback(Callback):
    def __init__(self, dbm, savefolder, savename, n_chains = 20,
                     n_samples = 10, plot_every = 2000, epoch_ls = []):

        self.dbm = dbm
        self.save = savefolder + '/' + savename
        self.n_chains = n_chains
        self.n_samples = n_samples
        self.plot_every = plot_every
        self.epoch_ls = epoch_ls
 
        super(SampleCallback, self).__init__()
    
    def on_train_begin(self, logs={}):
        return
        
    def on_train_end(self, logs={}):
        return
 
    def on_epoch_begin(self, epoch, logs={}):
        return
 
    def on_epoch_end(self, epoch, logs={}):

        if (len(self.epoch_ls)==0) or (epoch in self.epoch_ls):
            
            filepath = self.save + '.{:04d}'.format(epoch)
            self.dbm.sample(self.model.train_data.shape[1].eval(),
                            filepath,
                            self.model.optimizer.beta.get_value(),
                            self.model.train_data.get_value(),
                            self.n_chains, self.n_samples, self.plot_every)
        
        return logs
 
    def on_batch_begin(self, batch, logs={}):
        return
 
    def on_batch_end(self, batch, logs={}):
        return

#%%
class PlotCallback(Callback):
    def __init__(self, save_folder, csv_filepath):

        self.save_folder = save_folder
        self.csv_filepath = csv_filepath
 
        super(PlotCallback, self).__init__()
    
    def on_train_begin(self, logs={}):
        return
        
    def on_epoch_begin(self, epoch, logs={}):
        return
 
    def on_epoch_end(self, epoch, logs={}):
        return
 
    def on_batch_begin(self, batch, logs={}):
        return
 
    def on_batch_end(self, batch, logs={}):
        return
    
    def on_train_end(self, logs={}):
        
        df = pd.read_csv(self.csv_filepath)
        
        data_dict = {}
        for c in df.columns.values:
            data_dict[c] = df[c].values
        
        def plot_multi_cost(x, y_ls, color_ls, lbl_ls, title, savename):  
            fig = plt.figure()
            for y, color, lbl in zip(y_ls, color_ls, lbl_ls):                
                plt.plot(x,y,color=color,label=lbl)   

            plt.xlabel('Epoch')
            #plt.title(title)
            plt.grid(True)
            plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
            fig.savefig(savename, bbox_inches='tight')
            plt.close() 
            
        def plot_errorbars(x_d, y_d, high_d, low_d, color_d, lbl_d,
                           title, savename):
            
            fig = plt.figure()
            low_err = np.array(y_d)-np.array(low_d)
            high_err = np.array(high_d)-np.array(y_d)
            data_err = np.array([low_err,high_err])
            plt.errorbar(x_d, y_d, yerr=data_err,color=color_d,
                         fmt='o',ls='',label=lbl_d, capsize=10)
            plt.xlim([-5,x[-1]+5]) # so you can see the start/end error bars

            plt.xlabel('Epoch')
            plt.title(title)
            plt.grid(True)
            plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
            fig.savefig(savename, bbox_inches='tight')
            plt.close() 
        
       
        # These plots are similar:
        # Loss, pslike, and recon
        
        x = data_dict['epoch']
        color_ls = ['red', 'blue']
        
        for t in ['cost','pslike','recon']:
            if t in data_dict.keys():
                lbl_ls = [t, 'val_'+t]
                y_ls = [data_dict[lbl_ls[0]], data_dict[lbl_ls[1]]]
                title = t
                savename = self.save_folder+t+'.pdf'
                plot_multi_cost(x, y_ls, color_ls, lbl_ls, title, savename)
        
        # These plots are easy:
        # lr, momentum
        if 'lr' in data_dict.keys():
            lbl_ls = ['lr']
            y_ls = [data_dict['lr']]
            title = 'Learning Rate'
            savename = self.save_folder+'learning_rate.pdf'
            plot_multi_cost(x, y_ls, color_ls, lbl_ls, title, savename)
        if 'momentum' in data_dict.keys():
            lbl_ls = ['momentum']
            y_ls = [data_dict['momentum']]
            title = 'Momentum'
            savename = self.save_folder+'momentum.pdf'
            plot_multi_cost(x, y_ls, color_ls, lbl_ls, title, savename)
        if 'beta' in data_dict.keys():
            lbl_ls = ['beta']
            y_ls = [data_dict['beta']]
            title = 'Beta'
            savename = self.save_folder+'beta.pdf'
            plot_multi_cost(x, y_ls, color_ls, lbl_ls, title, savename)
        
        
        # Free energy difference
        if 'fe' in data_dict.keys():
            fe = 'fe'
        else:
            fe = 'free_energy'
        
        if fe in data_dict.keys():
            IS_good = np.logical_not(np.isnan(data_dict[fe]))
            x_d = x[IS_good]
            y_ls = [data_dict[fe][IS_good]-data_dict['val_'+fe][IS_good]]
            color_ls = ['black']
            lbl_ls = ['free_energy_diff']
            title = 'Free energy difference, train minus valid'
            savename = self.save_folder+'free_energy.pdf'
            plot_multi_cost(x_d, y_ls, color_ls, lbl_ls, title, savename)


        if 'logz' in data_dict.keys():
    
            # logz plots
            IS_good = np.logical_not(np.isnan(data_dict['logz']))
            x_d = x[IS_good]
            y_d = data_dict['logz'][IS_good]
            high_d = data_dict['logz_high'][IS_good]
            low_d = data_dict['logz_low'][IS_good]
            
            y_ls = [y_d]
            color_ls = ['red']
            lbl_ls = ['Mean']
            title = 'Log Z. Mean estimate'
            savename = self.save_folder+'logz.pdf'
            plot_multi_cost(x_d, y_ls, color_ls, lbl_ls, title, savename)
            
            title = 'Log Z. Error bars are 3 std.'
            savename = self.save_folder+'logz_errorbars.pdf'
            plot_errorbars(x_d, y_d, high_d, low_d, 'black', 'logz',
                           title, savename)
    
                
            # probability plots
            IS_good = np.logical_not(np.isnan(data_dict['prob']))
            x_d = x[IS_good]
            yt_d = data_dict['prob'][IS_good]
            y_d = data_dict['val_prob'][IS_good]
            high_d = data_dict['val_prob_high'][IS_good]
            low_d = data_dict['val_prob_low'][IS_good]
            
            y_ls = [yt_d, y_d]
            color_ls = ['blue', 'red']
            lbl_ls = ['train', 'valid']
            title = 'Probability of train vs valid. Mean estimate.'
            savename = self.save_folder+'prob.pdf'
            plot_multi_cost(x_d, y_ls, color_ls, lbl_ls, title, savename)
            
            title = 'Probability of valid. Error bars are 3 std.'
            savename = self.save_folder+'prob_errorbars.pdf'
            plot_errorbars(x_d, y_d, high_d, low_d, 'black', 'val_prob',
                           title, savename)
        
        return