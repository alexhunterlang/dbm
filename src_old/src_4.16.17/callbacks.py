#%%
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
# NOTE: do not use from builtins import * since it messes us csv logger

import os
import csv
import pandas as pd
import numpy as np
import time
import warnings
from collections import deque, OrderedDict, Iterable

import theano.tensor as T

# Force matplotlib to not use any Xwindows backend.
# Otherwise will get error on linux servers
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

import __main__ as main
if not hasattr(main, '__file__'):
    # This means its an iPython instance
    import getpass
    usr = getpass.getuser()
    if usr == 'alexlangMacBookPro':
        path_to_script_folder = '/Users/alexlangMacBookPro/Dropbox/BitBucket/dbm/src/'
        os.chdir(path_to_script_folder)
        
# my files
from ais import AIS
from dbm import DBM
from utils import FLOATX, EPS

#%%
class CallbackList(object):
    """Container abstracting a list of callbacks.
    # Arguments
        callbacks: List of `Callback` instances.
        queue_length: Queue length for keeping
            running statistics over callback execution time.
    """

    def __init__(self, callbacks=None, queue_length=10):
        callbacks = callbacks or []
        self.callbacks = [c for c in callbacks]
        self.queue_length = queue_length

    def append(self, callback):
        self.callbacks.append(callback)

    def set_params(self, params):
        for callback in self.callbacks:
            callback.set_params(params)

    def set_model(self, model):
        for callback in self.callbacks:
            callback.set_model(model)

    def on_epoch_begin(self, epoch, logs=None):
        """Called at the start of an epoch.
        # Arguments
            epoch: integer, index of epoch.
            logs: dictionary of logs.
        """
        logs = logs or {}
        for callback in self.callbacks:
            callback.on_epoch_begin(epoch, logs)
        self._delta_t_batch = 0.
        self._delta_ts_batch_begin = deque([], maxlen=self.queue_length)
        self._delta_ts_batch_end = deque([], maxlen=self.queue_length)

    def on_epoch_end(self, epoch, logs=None):
        """Called at the end of an epoch.
        # Arguments
            epoch: integer, index of epoch.
            logs: dictionary of logs.
        """
        logs = logs or {}
        for callback in self.callbacks:
            callback.on_epoch_end(epoch, logs)

    def on_batch_begin(self, batch, logs=None):
        """Called right before processing a batch.
        # Arguments
            batch: integer, index of batch within the current epoch.
            logs: dictionary of logs.
        """
        logs = logs or {}
        t_before_callbacks = time.time()
        for callback in self.callbacks:
            callback.on_batch_begin(batch, logs)
        self._delta_ts_batch_begin.append(time.time() - t_before_callbacks)
        delta_t_median = np.median(self._delta_ts_batch_begin)
        if (self._delta_t_batch > 0. and
           delta_t_median > 0.95 * self._delta_t_batch and
           delta_t_median > 0.1):
            warnings.warn('Method on_batch_begin() is slow compared '
                          'to the batch update (%f). Check your callbacks.'
                          % delta_t_median)
        self._t_enter_batch = time.time()

    def on_batch_end(self, batch, logs=None):
        """Called at the end of a batch.
        # Arguments
            batch: integer, index of batch within the current epoch.
            logs: dictionary of logs.
        """
        logs = logs or {}
        if not hasattr(self, '_t_enter_batch'):
            self._t_enter_batch = time.time()
        self._delta_t_batch = time.time() - self._t_enter_batch
        t_before_callbacks = time.time()
        for callback in self.callbacks:
            callback.on_batch_end(batch, logs)
        self._delta_ts_batch_end.append(time.time() - t_before_callbacks)
        delta_t_median = np.median(self._delta_ts_batch_end)
        if (self._delta_t_batch > 0. and
           (delta_t_median > 0.95 * self._delta_t_batch and delta_t_median > 0.1)):
            warnings.warn('Method on_batch_end() is slow compared '
                          'to the batch update (%f). Check your callbacks.'
                          % delta_t_median)

    def on_train_begin(self, logs=None):
        """Called at the beginning of training.
        # Arguments
            logs: dictionary of logs.
        """
        logs = logs or {}
        for callback in self.callbacks:
            callback.on_train_begin(logs)

    def on_train_end(self, logs=None):
        """Called at the end of training.
        # Arguments
            logs: dictionary of logs.
        """
        logs = logs or {}
        for callback in self.callbacks:
            callback.on_train_end(logs)

#%%
class Callback(object):
    """Abstract base class used to build new callbacks.
    # Properties
        params: dict. Training parameters
            (eg. verbosity, batch size, number of epochs...).
        model: instance of `keras.models.Model`.
            Reference of the model being trained.
    The `logs` dictionary that callback methods
    take as argument will contain keys for quantities relevant to
    the current batch or epoch.
    Currently, the `.fit()` method of the `Sequential` model class
    will include the following quantities in the `logs` that
    it passes to its callbacks:
        on_epoch_end: logs include `acc` and `loss`, and
            optionally include `val_loss`
            (if validation is enabled in `fit`), and `val_acc`
            (if validation and accuracy monitoring are enabled).
        on_batch_begin: logs include `size`,
            the number of samples in the current batch.
        on_batch_end: logs include `loss`, and optionally `acc`
            (if accuracy monitoring is enabled).
    """

    def __init__(self):
        pass

    def set_params(self, params):
        self.params = params

    def set_model(self, model):
        self.model = model

    def on_epoch_begin(self, epoch, logs=None):
        pass

    def on_epoch_end(self, epoch, logs=None):
        pass

    def on_batch_begin(self, batch, logs=None):
        pass

    def on_batch_end(self, batch, logs=None):
        pass

    def on_train_begin(self, logs=None):
        pass

    def on_train_end(self, logs=None):
        pass

#%%
class BaseLogger(Callback):
    """Callback that accumulates epoch averages of metrics.
    This callback is automatically applied to every Keras model.
    """

    def on_epoch_begin(self, epoch, logs=None):
        self.seen = 0
        self.totals = {}

    def on_batch_end(self, batch, logs=None):
        logs = logs or {}
        batch_size = logs.get('size', 0)
        self.seen += batch_size

        for k, v in logs.items():
            if k in self.totals:
                self.totals[k] += v * batch_size
            else:
                self.totals[k] = v * batch_size

    def on_epoch_end(self, epoch, logs=None):
        if logs is not None:
            for k in self.params['metrics']:
                if k in self.totals:
                    # Make value available to next callbacks.
                    logs[k] = self.totals[k] / self.seen

#%%
class History(Callback):
    """Callback that records events into a `History` object.
    This callback is automatically applied to
    every Keras model. The `History` object
    gets returned by the `fit` method of models.
    """

    def on_train_begin(self, logs=None):
        self.epoch = []
        self.history = {}

    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}
        self.epoch.append(epoch)
        for k, v in logs.items():
            self.history.setdefault(k, []).append(v)

#%%
class CSVLogger(Callback):
    """Callback that streams epoch results to a csv file.
    Supports all values that can be represented as a string,
    including 1D iterables such as np.ndarray.
    # Example
        ```python
            csv_logger = CSVLogger('training.log')
            model.fit(X_train, Y_train, callbacks=[csv_logger])
        ```
    # Arguments
        filename: filename of the csv file, e.g. 'run/log.csv'.
        separator: string used to separate elements in the csv file.
        append: True: append if file exists (useful for continuing
            training). False: overwrite existing file,
    """

    def __init__(self, filename, separator=',', append=False):
        super(CSVLogger, self).__init__()
        
        self.sep = str(separator)
        self.filename = filename
        self.append = append
        self.writer = None
        self.keys = None
        self.append_header = True

    def on_train_begin(self, logs=None):
        if self.append:
            if os.path.exists(self.filename):
                with open(self.filename) as f:
                    self.append_header = not bool(len(f.readline()))
            self.csv_file = open(self.filename, 'a')
        else:
            self.csv_file = open(self.filename, 'w')

    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}

        def handle_value(k):
            is_zero_dim_ndarray = isinstance(k, np.ndarray) and k.ndim == 0
            if isinstance(k, Iterable) and not is_zero_dim_ndarray:
                return '"[%s]"' % (', '.join(map(str, k)))
            else:
                return k

        if not self.writer:
            self.keys = sorted(logs.keys())

            class CustomDialect(csv.excel):
                delimiter = self.sep

            self.writer = csv.DictWriter(self.csv_file,
                                         fieldnames=['epoch'] + self.keys, dialect=CustomDialect)
            if self.append_header:
                self.writer.writeheader()

        row_dict = OrderedDict({'epoch': epoch})
        row_dict.update((key, handle_value(logs[key])) for key in self.keys)
        self.writer.writerow(row_dict)
        self.csv_file.flush()

    def on_train_end(self, logs=None):
        self.csv_file.close()
        self.writer = None

#%%
class OptimizerSpy(Callback):
    def __init__(self):
        super(OptimizerSpy, self).__init__()
        
    def on_epoch_end(self, epoch, logs=None):
        logs = logs or {}
        if hasattr(self.model.optimizer, 'lr'):
            logs['lr'] = self.model.optimizer.lr.get_value()
            
        if hasattr(self.model.optimizer, 'beta'):
            logs['beta'] = self.model.optimizer.beta.get_value()
        
        if hasattr(self.model.optimizer, 'momentum'):
            logs['momentum'] = self.model.optimizer.momentum.get_value()
        elif hasattr(self.model.optimizer, 'beta_1'):
            logs['momentum'] = self.model.optimizer.beta_1.get_value()

#%%
class PeriodicSave(Callback):
    
    def __init__(self, weight_path, epoch_ls, opt_path=None):
        super(PeriodicSave, self).__init__()
        
        self.weight_path = weight_path
        self.opt_path = opt_path
        self.epoch_ls = epoch_ls

    def on_epoch_end(self, epoch, logs=None):        
        # save special epochs
        if epoch in self.epoch_ls:
            filepath = self.weight_path.format(epoch=epoch, **logs)
            self.model.nnet.save_weights(filepath, overwrite=True)
            if self.opt_path is not None:
                filepath = self.opt_path.format(epoch=epoch, **logs)
                self.model.optimizer.save_weights(filepath, overwrite=True)

#%%
class LearningRateScheduler(Callback):
    """Learning rate scheduler.
    # Arguments
        schedule: a function that takes an epoch index as input
            (integer, indexed from 0) and returns a new
            learning rate as output (float).
    """

    def __init__(self, schedule):
        super(LearningRateScheduler, self).__init__()
        
        self.schedule = schedule

    def on_epoch_begin(self, epoch, logs=None):
        if not hasattr(self.model.optimizer, 'lr'):
            raise ValueError('Optimizer must have a "lr" attribute.')
            
        lr = self.schedule(epoch)
        
        if not isinstance(lr, (float, np.float32, np.float64)):
            raise ValueError('The output of the "schedule" function '
                             'should be float.')
            
        lr = T.cast(lr, dtype=FLOATX).eval()
        self.model.optimizer.lr.set_value(lr)


#%%
class MomentumRateScheduler(Callback):
    """Momentum rate scheduler.
    # Arguments
        schedule: a function that takes an epoch index as input
            (integer, indexed from 0) and returns a new
            momentum rate as output (float).
    """

    def __init__(self, schedule):
        super(MomentumRateScheduler, self).__init__()
        
        self.schedule = schedule

    def on_epoch_begin(self, epoch, logs=None):
        
        has_mom = hasattr(self.model.optimizer, 'momentum_goal')
        has_beta = hasattr(self.model.optimizer, 'beta_1')
        
        if (not has_mom) and (not has_beta):
            raise ValueError('Optimizer must have a "momentum" or "beta_1" attribute.')
        
        momentum = self.schedule(epoch)
        
        if momentum is not None:
        
            if not isinstance(momentum, (float, np.float32, np.float64)):
                raise ValueError('The output of the "schedule" function '
                                 'should be float.')
            
            momentum = T.cast(momentum, dtype=FLOATX).eval()
            if hasattr(self.model.optimizer, 'momentum_goal'):
                self.model.optimizer.momentum_goal.set_value(momentum)
            elif hasattr(self.model.optimizer, 'beta_1'):
                self.model.optimizer.beta_1.set_value(momentum)
                
#%%
class BetaScheduler(Callback):
    """Beta scheduler.
    # Arguments
        schedule: a function that takes an epoch index as input
            (integer, indexed from 0) and returns a new
            beta rate as output (float).
    """

    def __init__(self, schedule):
        super(BetaScheduler, self).__init__()
        
        self.schedule = schedule

    def on_epoch_begin(self, epoch, logs=None):
        if not hasattr(self.model.optimizer, 'beta'):
            raise ValueError('Optimizer must have a "beta" attribute.')
        
        beta = self.schedule(epoch)
                
        if not isinstance(beta, (float, np.float32, np.float64)):
            raise ValueError('The output of the "schedule" function '
                             'should be float.')
        
        beta = T.cast(beta, dtype=FLOATX).eval()
        self.model.optimizer.beta.set_value(beta)

#%%
class AISCallback(Callback):
    def __init__(self, dbm, n_runs, n_betas, epoch_ls=[], name='',
                 exact=False):
        super(AISCallback, self).__init__()

        self.dbm = dbm
        self.n_runs = n_runs
        self.n_betas = n_betas
        self.epoch_ls = epoch_ls
        self.exact = exact
        
        if len(name) > 0:
            self.name = '_'+name
        else:
            self.name = ''
 
    def on_epoch_end(self, epoch, logs={}):
        
        if (len(self.epoch_ls) == 0) or (epoch in self.epoch_ls):
            
            dbm_ais = DBM.init_non_gauged(self.dbm)
            
            ais = AIS(dbm=dbm_ais,
                      data=self.model.train_data.get_value(),
                      n_runs=self.n_runs,
                      n_betas=self.n_betas,
                      final_beta=self.model.optimizer.beta.get_value())
  
            logz, logz_high, logz_low = ais.log_z()
             
            beta = self.model.optimizer.beta.get_value()
            fe_fxn = ais.free_energy_fxn()
            
            data = self.model.train_data.get_value()
            fe = fe_fxn(data, 0, beta)
            logs['free_energy'] = fe
                
            data = self.model.validation_data.get_value()
            val_fe = fe_fxn(data, 0, beta)
            logs['val_free_energy'] = val_fe

            logs['logz'+self.name] = logz
            logs['logz_high'+self.name] = logz_high
            logs['logz_low'+self.name] = logz_low
            
            logs['prob'+self.name] = -fe - logz
            logs['prob_high'+self.name] = -fe - logz_low
            logs['prob_low'+self.name] = -fe - logz_high
                
            logs['val_prob'+self.name] = -val_fe - logz
            logs['val_prob_high'+self.name] = -val_fe - logz_low
            logs['val_prob_low'+self.name] = -val_fe - logz_high
                
            if self.exact:
                logs['logz_exact'+self.name] = self.ais.exact_log_z(dbm_ais)
                
        else:
            
            keys = ['free_energy', 'val_free_energy']
            
            for w1 in ['logz', 'prob', 'val_prob']:
                for w2 in ['', '_high', '_low']:
                    keys.append(w1+w2+self.name)
            
            if self.exact:
                keys.append('logz_exact'+self.name)
            
            for k in keys:
                logs[k] = np.nan
 
#%%
class SampleCallback(Callback):
    def __init__(self, dbm, savename, n_chains=20,
                 n_samples=10, plot_every=2000, epoch_ls=[]):

        super(SampleCallback, self).__init__()

        self.dbm = dbm
        self.savename = savename
        self.n_chains = n_chains
        self.n_samples = n_samples
        self.plot_every = plot_every
        self.epoch_ls = epoch_ls
 
    def on_epoch_end(self, epoch, logs={}):

        if (len(self.epoch_ls) == 0) or (epoch in self.epoch_ls):
            
            filepath = self.savename + '.{:04d}'.format(epoch)
            self.dbm.sample(self.model.train_data.shape[1].eval(),
                            filepath,
                            self.model.optimizer.beta.get_value(),
                            self.model.train_data.get_value(),
                            self.n_chains,
                            self.n_samples,
                            self.plot_every)
       
#%%
class PlotCallback(Callback):
    def __init__(self, save_folder, csv_filepath):
        super(PlotCallback, self).__init__()
        
        self.save_folder = save_folder
        self.csv_filepath = csv_filepath
    
    def on_train_end(self, logs={}):
        
        df = pd.read_csv(self.csv_filepath)
        
        data_dict = {}
        for c in df.columns.values:
            data_dict[c] = df[c].values
        
        def convert2list(ls):
            if not isinstance(ls, list):
                ls = [ls]
            return ls
        
        def plot_multi_cost(x, y_ls, color_ls, lbl_ls, title, savename):  
            fig = plt.figure()
            
            y_ls = convert2list(y_ls)
            color_ls = convert2list(color_ls)
            lbl_ls = convert2list(lbl_ls)
            
            for y, color, lbl in zip(y_ls, color_ls, lbl_ls):                
                plt.plot(x, y, color=color, label=lbl)   
 
            plt.xlabel('Epoch')
            plt.title(title)
            plt.grid(True)
            plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
            fig.savefig(savename, bbox_inches='tight')
            plt.close() 
            
        def plot_errorbars(x, y, high, low, color, lbl, title, savename):
            fig = plt.figure()
            
            low_err = np.array(y)-np.array(low)
            high_err = np.array(high)-np.array(y)
            data_err = np.array([low_err,high_err])
            
            plt.errorbar(x, y, yerr=data_err, color=color,
                         fmt='o', ls='', label=lbl, capsize=10)
            
            plt.xlim([-5, x[-1]+5]) # so you can see the start/end error bars

            plt.xlabel('Epoch')
            plt.title(title)
            plt.grid(True)
            plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)
            fig.savefig(savename, bbox_inches='tight')
            plt.close() 

        x = data_dict['epoch']
       
        # These plots are similar, 2 lines:
        # Loss, pslike, and recon        
        color_ls = ['red', 'blue']        
        for t in ['cost','pslike','recon']:
            if t in data_dict.keys():
                lbl_ls = [t, 'val_'+t]
                y_ls = [data_dict[lbl_ls[0]], data_dict[lbl_ls[1]]]
                title = t
                savename = self.save_folder+t+'.pdf'
                plot_multi_cost(x, y_ls, color_ls, lbl_ls, title, savename)
        
        # These plots are single lines:
        # lr, momentum, beta
        if 'lr' in data_dict.keys():
            lbl_ls = ['lr']
            y_ls = [data_dict['lr']]
            title = 'Learning Rate'
            savename = self.save_folder+'learning_rate.pdf'
            plot_multi_cost(x, y_ls, color_ls, lbl_ls, title, savename)
        if 'momentum' in data_dict.keys():
            lbl_ls = ['momentum']
            y_ls = [data_dict['momentum']]
            title = 'Momentum'
            savename = self.save_folder+'momentum.pdf'
            plot_multi_cost(x, y_ls, color_ls, lbl_ls, title, savename)
        if 'beta' in data_dict.keys():
            lbl_ls = ['beta']
            y_ls = [data_dict['beta']]
            title = 'Beta'
            savename = self.save_folder+'beta.pdf'
            plot_multi_cost(x, y_ls, color_ls, lbl_ls, title, savename)
 
    
        if 'logz' in data_dict.keys():
    
            IS_good = np.logical_not(np.isnan(data_dict['logz']))
            x_d = x[IS_good]
            
            # Free energy difference
            fe = 'free_energy'
            y = data_dict[fe][IS_good]-data_dict['val_'+fe][IS_good]
            color = 'black'
            lbl = 'free_energy_diff'
            title = 'Free energy difference, train minus valid'
            savename = self.save_folder+'free_energy.pdf'
            plot_multi_cost(x_d, y, color, lbl, title, savename)
            
            
            # logz plots
            y = data_dict['logz'][IS_good]
            high = data_dict['logz_high'][IS_good]
            low = data_dict['logz_low'][IS_good]
            
            color = 'red'
            lbl = 'Mean'
            title = 'Log Z. Mean estimate'
            savename = self.save_folder+'logz.pdf'
            plot_multi_cost(x_d, y, color, lbl, title, savename)
            
            title = 'Log Z. Error bars are 3 std.'
            savename = self.save_folder+'logz_errorbars.pdf'
            plot_errorbars(x_d, y, high, low, 'black', 'logz',
                           title, savename)
    
                
            # probability plots
            yt_d = data_dict['prob'][IS_good]
            y_d = data_dict['val_prob'][IS_good]
            high = data_dict['val_prob_high'][IS_good]
            low = data_dict['val_prob_low'][IS_good]
            
            y_ls = [yt_d, y_d]
            color_ls = ['blue', 'red']
            lbl_ls = ['train', 'valid']
            title = 'Probability of train vs valid. Mean estimate.'
            savename = self.save_folder+'prob.pdf'
            plot_multi_cost(x_d, y_ls, color_ls, lbl_ls, title, savename)
            
            title = 'Probability of valid. Error bars are 3 std.'
            savename = self.save_folder+'prob_errorbars.pdf'
            plot_errorbars(x_d, y_d, high, low, 'black', 'val_prob',
                           title, savename)
