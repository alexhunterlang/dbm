"""Tools for estimating the partition function of an RBM

Author: Alex Lang, alexhunterlang@gmail.com

Based on code from: Ian Goodfellow, https://github.com/lisa-lab/pylearn2

The spiritual ancestor of this code is Pylearn2. First major change was to 
simplify the code to take advantage of my specific dbm structure. Second
major change was to make the for loop through temperatures a scan which
cut runtime in half.
"""
    
#%% imports
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *

import os
import numpy as np

import theano
import theano.tensor as T

# The custom imports should be in same folder as this script
import __main__ as main
if not hasattr(main, '__file__'):
    # This means its an iPython instance
    import getpass
    usr = getpass.getuser()
    if usr == 'alexlangMacBookPro':
        path_to_script_folder = '/Users/alexlangMacBookPro/Dropbox/BitBucket/dbm/src/'
        os.chdir(path_to_script_folder)

# my files
from utils import make_shared, binomial_sample, FLOATX, EPS

#%%
def logsum(x):
    # This prevents overflow
    
    x = x.astype(np.longdouble) # try and get a few extra precision points
    a = np.max(x)
    ls = a + np.log(np.sum(np.exp(x-a)))
    ls = float(ls)
    
    return ls

#%%
def logdiff(x):
    # This prevents overflow
    
    assert x.shape[0] == 2
    x = x.astype(np.longdouble) # try and get a few extra precision points
    a = np.max(x)
    ld = a + np.log(np.diff(np.exp(x-a)))
    ld = ld.item()
    ld = float(ld)
    
    return ld
 
#%%
def exact_log_z(dbm):
    """
    Compute the log partition function of an (binary-binary) RBM.
    
    This function enumerates a sum with exponentially many terms, and
    should not be used with more than a small, toy model.
    """
    assert len(dbm.layer_size_list) == 2
    assert dbm.IS_datanorm is False
    
    nvis, nhid = dbm.layer_size_list
              
    # Pick whether to iterate over visible or hidden states.
    if nvis < nhid:
        width = nvis
        eval_type = 'vis'
    else:
        width = nhid
        eval_type = 'hid'
    
    # Allocate storage for 2**block_bits of the 2**width possible
    # configurations.
    try:
        logz_data_c = np.zeros((2**width, width),
                               order='C', dtype=FLOATX)
    except MemoryError:
        print('Too big')

    # fill in the first block_bits, which will remain fixed for all
    # 2**width configs
    tensor_10D_idx = np.ndindex(*([2] * width))
    for i, j in enumerate(tensor_10D_idx):
        logz_data_c[i, -width:] = j
    try:
        logz_data = np.array(logz_data_c, order='F', dtype=FLOATX)
    except MemoryError:
        print('Too big')
        
        
    inputs = T.matrix('inputs')

    b0 = dbm.layers[0].b
    b1 = dbm.layers[1].b
    W = dbm.layers[1].W_ls[0]  
        
    if eval_type == 'vis':
        bias_term = T.dot(inputs, b0)
        z = b1 + T.dot(inputs, W)
    elif eval_type == 'hid':
        bias_term = T.dot(inputs, b1)
        z = b0 + T.dot(inputs, W.T)
        
    log_term = T.sum(T.log(1+T.exp(z)), axis=1)

    log_prob_vv = bias_term + log_term
        
    fn = theano.function([inputs], log_prob_vv)
        
    log_prob = fn(logz_data)        
                                     
    log_z = logsum(log_prob)
    
    return log_z


#%%
class AIS(object):
    """
    Compute the log AIS weights to approximate a ratio of partition functions.

    """     
    def __init__(self,
                 dbm,
                 data,
                 n_runs,
                 n_betas=None,
                 final_beta=1.0,
                 betas=None):
        
        assert dbm.IS_datanorm is False
        self.dbm_b = dbm
        
        self.n_runs = n_runs
        
        if (n_betas is not None) and (betas is None):
            self.n_betas = n_betas
            self.final_beta = T.cast(final_beta, FLOATX)    
            betas = np.linspace(0, 1, n_betas)*final_beta
        elif (n_betas is None) and (betas is not None):
            self.n_betas = betas.shape[0]
            self.final_beta = T.cast(betas[-1], FLOATX)    
        else:
            raise NotImplementedError        
        self.betas = make_shared(betas, name='betas')
        
        self.theano_rng = dbm.theano_rng
        
        vis_mean = np.clip(np.mean(data, axis=0), EPS, 1-EPS)
        # this is the base rate model Ruslan uses
        p_ruslan = (vis_mean + 0.05)/1.05          
        b = np.log(p_ruslan/(1-p_ruslan)).astype(FLOATX)
        self.b0_a = theano.shared(b, name='b0_a', borrow=True)
        
        # make the initial sample
        p0 = np.tile(1. / (1 + np.exp(-b)), (n_runs, 1))
        s0 = np.array(p0 > np.random.random_sample(p0.shape), dtype=FLOATX)
        sample_ls = [s0]
        for n in self.dbm_b.layer_size_list[1:]:
            p = 0.5*np.ones((self.n_runs, n))
            s = binomial_sample(self.theano_rng, p.astype(FLOATX))
            sample_ls.append(theano.shared(s.eval(), borrow=True))
        self.sample_ls = sample_ls
        
        # initialize log importance weights
        # log Z_init 
        log_Z_init = np.sum(self.dbm_b.layer_size_list[1:])*np.log(2)
        log_Z_init += np.sum(np.log(1 + np.exp(self.b0_a.eval())))
        log_ais_w = log_Z_init*np.ones((self.n_runs,))
        self.log_ais_w = log_ais_w.astype(FLOATX)

        
    def ais_free_energy(self, beta, sample_ls):
        """
        Computes the free-energy of visible unit configuration `sample_ls`,
        according to the interpolating distribution at temperature beta.
        The interpolating distributions are given by
        :math:`p_a(v)^{1-\\beta} p_b(v)^\\beta`.
        See equation 10, of Salakhutdinov & Murray 2008.
    
        Parameters
        ----------
        beta : int
            Inverse temperature at which to compute the free-energy.
        sample_ls : list of samples
            Matrix whose rows indexes into the minibatch, and columns into
            the data dimensions.
    
        Returns
        -------
        f : float (scalar)
           Free-energy of configuration `sample_ls` given by the
           interpolating distribution at temperature beta.
        """
    
        
        fe_a_odd = np.log(2)*np.sum(self.dbm_b.layer_size_list[1::2])
        fe_a = -(self.final_beta-beta)*T.dot(sample_ls[0], self.b0_a)-T.cast(fe_a_odd, dtype=FLOATX)
        fe_b = self.dbm_b.free_energy_given_h(sample_ls, beta=beta)
        return fe_a + fe_b
    
    def ais_sample(self, beta, sample_ls):
        """
        Parameters
        ----------        
        beta : theano.shared
            Scalar, represents inverse temperature at which we wish to sample from.
    
        sample_ls : list of samples
            Matrix of shape (n_runs, nvis), state of current particles.
        """
        
        prob_ls = len(sample_ls)*[None]
    
        # equation 15 (Salakhutdinov & Murray 2008)
        # only need an odd parity update
        
        # base rate samples dont matter since multiplied by zero
     
        # only need an odd parity update
        _, sample_b = self.dbm_b.parity_update(prob_ls,
                                               sample_ls,
                                               1,
                                               IS_prob_input=False,
                                               hold_constant=[],
                                               IS_dropout=0.0,
                                               beta=beta)
    
        # equation 17 (Salakhutdinov & Murray 2008)
        # updates the even states
            
        z_b = []
        for layer in self.dbm_b.layers:
            z = layer.get_input(sample_b, mean_field=True, direction='both')
            z_b.append(z)

        sample_ls = len(sample_ls)*[None]
        for i, z in enumerate(z_b):
            if i == 0:
                p = T.nnet.sigmoid((self.final_beta-beta)*self.b0_a + beta*z)
                sample_ls[i] = binomial_sample(self.theano_rng, p)
            elif np.mod(i, 2) == 1:
                # these get ignored, just need to be a placeholder in list
                sample_ls[i] = sample_b[i]
            else:
                p = T.nnet.sigmoid(beta*z)
                sample_ls[i] = binomial_sample(self.theano_rng, p)
    
        return sample_ls

    def ais_update(self, *args):
        log_ais_w = args[0]
        index = args[1]
        sample_ls = args[2:]
        
        bp, bp1 = self.betas[index], self.betas[index + 1]
        # log-ratio of (free) energies for two nearby temperatures
        fe0 = self.ais_free_energy(bp, sample_ls)
        fe1 = self.ais_free_energy(bp1, sample_ls)
        log_ais_w += fe0 - fe1            
                    
        # generate a new sample at temperature beta_{i+1}
        sample_ls = self.ais_sample(bp1, sample_ls)
        
        index += 1
        
        output = [log_ais_w, index]+sample_ls
        
        return output

    def run(self):
        """
        Performs the grunt-work, implementing

        .. math::

            log\:w^{(i)} += \mathcal{F}_{k-1}(v_{k-1}) - \mathcal{F}_{k}(v_{k-1})

        recursively for all temperatures.
        """
        
        # initial sample
        sample_ls = self.sample_ls
            
        log_ais_w = make_shared(self.log_ais_w, name='log_ais_w')
        index = theano.shared(np.int32(0), name='index', borrow=True)
            
        output_ls, updates = theano.scan(self.ais_update, 
                                         outputs_info=[log_ais_w, index]+sample_ls, 
                                         n_steps=self.n_betas - 1,
                                         name='scan_ais')

        ais_fn = theano.function([], output_ls[0][-1], updates=updates)
        
        self.log_ais_w = ais_fn()

    def log_z(self):
        
        # gather statistics
        self.run()
        
        # this is the mean factor from the weights
        self.logZ_est = logsum(self.log_ais_w) - np.log(self.n_runs)
        
        # this is the standard deviation
        aa = np.max(self.log_ais_w)
        logstd_AIS = np.log(np.std(np.exp(self.log_ais_w-aa))) + aa - np.log(self.n_runs)/2.0

    
        # find +- 3 std
        l_input = np.asarray([np.log(3)+logstd_AIS, self.logZ_est])
        self.logZ_est_up = logsum(l_input)
    
        logZ_est_down = logdiff(l_input)
        if np.isnan(logZ_est_down):
            logZ_est_down = 0.0
        self.logZ_est_down = logZ_est_down
        
        return self.logZ_est, self.logZ_est_up, self.logZ_est_down
    
    def free_energy_fxn(self):
                
        x = T.matrix('x')
        IS_dropout = T.scalar('IS_dropout')
        beta = T.scalar('beta')

        prob_data, sample_data, updates = self.dbm_b.pos_stats(x, IS_dropout, beta)
        
        fe1 = self.dbm_b.free_energy_given_h(prob_data, beta)
        
        fxn = theano.function([x, IS_dropout, beta],
                              T.mean(fe1),
                              updates=updates,
                              on_unused_input='ignore')
        
        return fxn
