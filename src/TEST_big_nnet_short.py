#%% Imports
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *

import os
import time
import shutil
import numpy as np
import pandas as pd

# The custom imports should be in same folder as this script
import __main__ as main
if not hasattr(main, '__file__'):
    # This means its an iPython instance
    import getpass
    usr = getpass.getuser()
    if usr == 'alexlangMacBookPro':
        path_to_script_folder = '/Users/alexlangMacBookPro/Dropbox/BitBucket/dbm/src/'
        os.chdir(path_to_script_folder)

# import from my files
from run_dbm import train_nnet

#%%
def default_train_test():
    # parameters to create static dbm
    dbm_init = {
                'layer_size_list'   : [784, 500],
                'topology_dict'     : {0 : {1}},
                'residual_dict'     : None,
                'dropout_prob_list' : [0.0, 0.0],
                'IS_persist'        : True,
                'IS_centered'       : True,
                'std_eps'           : None,
                'white_type'        : None,
                'white_eps'         : None, # regularizers whitening
                'weight_file'       : None, # Needed to load existing weights
                }
    
    # parameters to train dbm
    dbm_train = {
                 'n_epoch'      : 200,
                 'batch_size'   : 100, 
                 'n_gibbs_neg'  : 25,
                 'lr_center'    : 0.01,
                 'L1'           : 0.0,
                 'L2'           : 2e-4,
                 'IS_decay'     : True,
                 'white_update' : None, # empty list will update every epoch, None for no updates
                 'flip_rate'    : 1e-3, # percentage of raw input pixels to flip
                 'IS_fast'      : False, # cuts down on callback time
                 }
                
    # parameters of dataset
    data_details = {
                    'dataset'       : 'MNIST',
                    'data_type'     : 'sampled',
                    'IS_prob_train' : True, # train on prob, regardless of valid type
                    'IS_test_mode'  : True, # whether valid set is actual test set
                    'IS_debug'      : False, # trains a smaller nnet
                    }
    
    optimizer = {
                 'name'         : 'sgd',
                 'lr'           : 0.01,
                 'momentum'     : 0.9,
                 'nesterov'     : True,
                 'beta'         : 1.0,
                 'decay'        : 1.8e-5,
                 'schedule_decay' : 0.004,
                 }
    
    return dbm_init, dbm_train, data_details, optimizer

#%%
def train_test(save_folder_base):
    """ Should be able to do really well on this example.
    """
    
    test_array = [0]
    #test_array = np.arange(8)
   
    summary = save_folder_base + 'summary.txt'
    
    all_status = []
    start = time.time()
    
    with open(summary, 'w') as f:
        f.write('Results of TEST_simple_nnet. Expect LL > -35 after 100.\n\n')
    
    def test_fxn(n_test, name_test, summary,
                 dbm_init, dbm_train, data_details, optimizer):

        save_folder = save_folder_base + 'test_'+str(n_test)
        
        start_test = time.time()
        
        if data_details['data_type'] == 'sampled':
            # these are perfectly good
            cut_best = -88.0 
            # this may just be a poor parameter choice
            # but code implementation is probably good
            cut_okay = -90.0
            
        elif data_details['data_type'] == 'threshold':
            # these are perfectly good
            cut_best = -74.0 
            # this may just be a poor parameter choice
            # but code implementation is probably good
            cut_okay = -77.0 
            
        else:
            raise NotImplementedError
               
        try:
            train_nnet(save_folder, dbm_init, dbm_train, data_details, optimizer)
        
            df = pd.read_csv(save_folder+'/history.txt')
            val_prob = df['val_prob'].values
            vp = val_prob[np.logical_not(np.isnan(val_prob))]
            loglike = np.max(vp)
        
            if loglike >= cut_best:
                status = 'PASS'
                out_status = 2
            elif loglike >= cut_okay:
                status = 'OKAY'
                out_status = 1
            else:
                status = 'FAIL'
                out_status = 0
                
        except:
            loglike = np.nan
            status = 'ERROR'
            out_status = -1    
                
        runtime = (time.time() - start_test)/60.0
    
        with open(summary, 'a') as f:
            f.write('Test {}: {}\n'.format(n_test, name_test))
            f.write('\tLL = {:.1f}.\tSTATUS = {}\n'.format(loglike, status))
            f.write('\tRuntime = {:.2f} minutes\n'.format(runtime))
        
        return out_status
    
    ##### Test 0: Sampled baseline
    n_test = 0
    if n_test in test_array:
        name_test = 'sampled, basic'
        dbm_init, dbm_train, data_details, optimizer = default_train_test()
        status = test_fxn(n_test, name_test, summary,
                          dbm_init, dbm_train, data_details, optimizer)
        all_status.append(status)
    
    ##### Test 1: Sampled, std
    n_test = 1
    if n_test in test_array:
        name_test = 'sampled, std'
        dbm_init, dbm_train, data_details, optimizer = default_train_test()
        dbm_init['std_eps'] = 0.1
        status = test_fxn(n_test, name_test, summary,
                          dbm_init, dbm_train, data_details, optimizer)
        all_status.append(status)
    
    ##### Test 2: Sampled, White ZCA
    n_test = 2
    if n_test in test_array:
        name_test = 'sampled, white ZCA'
        dbm_init, dbm_train, data_details, optimizer = default_train_test()
        dbm_init['white_type'] = 'ZCA'
        dbm_init['white_eps'] = 1e-3
        status = test_fxn(n_test, name_test, summary,
                          dbm_init, dbm_train, data_details, optimizer)
        all_status.append(status)
   
    ##### Test 3: Sampled, White ZCA w/updates
    n_test = 3
    if n_test in test_array:
        name_test = 'sampled, white ZCA w/updates'
        dbm_init, dbm_train, data_details, optimizer = default_train_test()
        dbm_init['white_type'] = 'ZCA'
        dbm_init['white_eps'] = 1e-3
        dbm_train['white_update'] = []
        status = test_fxn(n_test, name_test, summary,
                          dbm_init, dbm_train, data_details, optimizer)
        all_status.append(status)
   
    ##### Test 4: Threshold baseline
    n_test = 4
    if n_test in test_array:
        name_test = 'threshold, basic'
        dbm_init, dbm_train, data_details, optimizer = default_train_test()
        data_details['data_type'] = 'threshold'
        status = test_fxn(n_test, name_test, summary,
                          dbm_init, dbm_train, data_details, optimizer)
        all_status.append(status)
    
    ##### Test 5: Threshold, std
    n_test = 5
    if n_test in test_array:
        name_test = 'threshold, std'
        dbm_init, dbm_train, data_details, optimizer = default_train_test()
        data_details['data_type'] = 'threshold'
        dbm_init['std_eps'] = 0.1
        status = test_fxn(n_test, name_test, summary,
                          dbm_init, dbm_train, data_details, optimizer)
        all_status.append(status)
    
    ##### Test 6: Threshold, White ZCA
    n_test = 6
    if n_test in test_array:
        name_test = 'threshold, white ZCA'
        dbm_init, dbm_train, data_details, optimizer = default_train_test()
        data_details['data_type'] = 'threshold'
        dbm_init['white_type'] = 'ZCA'
        dbm_init['white_eps'] = 1e-3
        status = test_fxn(n_test, name_test, summary,
                          dbm_init, dbm_train, data_details, optimizer)
        all_status.append(status)
   
    ##### Test 7: Threshold, White ZCA w/updates
    n_test = 7
    if n_test in test_array:
        name_test = 'threshold, white ZCA w/updates'
        dbm_init, dbm_train, data_details, optimizer = default_train_test()
        data_details['data_type'] = 'threshold'
        dbm_init['white_type'] = 'ZCA'
        dbm_init['white_eps'] = 1e-3
        dbm_train['white_update'] = []
        status = test_fxn(n_test, name_test, summary,
                          dbm_init, dbm_train, data_details, optimizer)
        all_status.append(status)

    ##########
    # final count of results
    all_status = np.asarray(all_status)
    n_tests = all_status.size
    n_pass = np.sum(all_status==2).astype(int)
    n_okay = np.sum(all_status==1).astype(int)
    n_fail = np.sum(all_status==0).astype(int)
    n_error = np.sum(all_status == -1).astype(int)
    
    total = time.time()-start

    with open(summary, 'a') as f:
        f.write('\n')
        f.write('Total test time: {:.2f} minutes\n'.format(total/60.0))
        f.write('Average time per test: {:.2f} minutes\n'.format(total/(60.0*n_tests)))
        f.write('\n')
        f.write('Passed: {}/{}\n'.format(n_pass, n_tests))
        f.write('Okay:   {}/{}\n'.format(n_okay, n_tests))
        f.write('Failed: {}/{}\n'.format(n_fail, n_tests))
        f.write('Error: {}/{}\n'.format(n_error, n_tests))
        f.write('\n')
        
    
    
#%%
if __name__ == '__main__':
    
    save_folder_base = '../results/test_big_nnet_short_no_up/'
    if os.path.exists(save_folder_base):
        shutil.rmtree(save_folder_base)
    os.mkdir(save_folder_base)
    
    train_test(save_folder_base)
