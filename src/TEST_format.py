Quick Guide to Formatting:

1. Documentation:
	- Docstring for each file
	- Docstring for each function
		- especially input description
	- Notes on unusual code

2. Static Code Analyis
	pylint
		- ignore trailing whitespace
		- should be be able to get at least 4/10
	
	pylint --disable=C0303 XXX.py