#%% Imports 
from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
from builtins import *

import six
import numpy as np
import os
from collections import OrderedDict

import theano.tensor as T
from theano.ifelse import ifelse
from theano.tensor import nlinalg

# The custom imports should be in same folder as this script
import __main__ as main
if not hasattr(main, '__file__'):
    # This means its an iPython instance
    import getpass
    usr = getpass.getuser()
    if usr == 'alexlangMacBookPro':
        path_to_script_folder = '/Users/alexlangMacBookPro/Dropbox/BitBucket/dbm/src/'
        os.chdir(path_to_script_folder)

# my files
from utils import binomial_sample, make_theano_rng, make_shared, FLOATX, EPS
import my_data

#%%
class NeuralNetParts(object):
    """ Generic object for neural network components """
    
    def __init__(self, name):
        
        self.name = name
        self._updates = OrderedDict()
        self._trainable_weights = []
        self._non_trainable_weights = []
    
    @property
    def updates(self):
        return self._updates
    
    @updates.setter
    def updates(self, updates):
        if updates is None:
            return
        keys = self._updates.keys()
        for k, v in updates.items():
            assert k not in keys, 'Already have update for '+k.name
            self._updates[k] = v

    @property
    def weights(self):
        return self._trainable_weights + self._non_trainable_weights
    
    @property
    def trainable_weights(self):
        return self._trainable_weights
    
    @trainable_weights.setter
    def trainable_weights(self, new_weights):
        if not isinstance(new_weights, list):
            new_weights = [new_weights]
        self._trainable_weights += new_weights
        
    @property
    def non_trainable_weights(self):
        return self._non_trainable_weights
    
    @non_trainable_weights.setter
    def non_trainable_weights(self, new_weights):
        if not isinstance(new_weights, list):
            new_weights = [new_weights]
        self._non_trainable_weights += new_weights

#%%
#%%
class Synapse(NeuralNetParts):
    def __init__(self,
                 shape,
                 name,
                 init_W=None,
                 init='orthogonal',
                 IS_trainable=True):
                
        super(Synapse, self).__init__(name)
        
        self.n0 = shape[0]
        self.n1 = shape[1]
        self.IS_trainable = IS_trainable
                    
        # Make weight
        if init_W is None:
            if init == 'orthogonal':
                W = np.random.normal(size=shape)
                U, _, V = np.linalg.svd(W)
                S = np.zeros(shape)
                np.fill_diagonal(S, 1)
                init_W = U.dot(S).dot(V)
                
            elif init == 'normal':
                init_W = 0.01*np.random.normal(size=shape)
            
            elif init == 'identity':
                assert self.n0 == self.n1
                init_W = np.identity(self.n0)
            
            elif init == 'zeros':
                init_W = np.zeros(shape)
            
            else:
                raise NotImplementedError
    
        self.W = make_shared(init_W, name=self.name)
       
        # needed for saving and training weights
        self.trainable_weights = self.W
 
    #%%
    def init_layers(self, layer0, layer1):
        """ This allows synapse to know who is connected to it """
        self.layer0 = layer0
        self.layer1 = layer1
    
    #%%
    def norm_cost(self, L1=0.0, L2=0.0):
        
        cost = T.cast(0.0, dtype=FLOATX)
        L1 = T.cast(L1, dtype=FLOATX)
        L2 = T.cast(0.5*L2, dtype=FLOATX)
        
        if self.IS_trainable:
            cost += L1*T.sum(T.abs_(self.W)) + L2*T.sum(T.pow(self.W, 2))

        return cost

    #%%
    def norm_grad(self, L1=0.0, L2=0.0):
        
        g = T.cast(0.0, dtype=FLOATX)
        L1 = T.cast(L1, dtype=FLOATX)
        L2 = T.cast(L2, dtype=FLOATX)
        
        if self.IS_trainable: 
            g += L1*T.sum(T.sgn(self.W)) + L2*T.sum(self.W)

        return g

    #%%
    def grad(self, prob_data, prob_model, L1=0.0, L2=0.0):
               
        g = None
        if self.IS_trainable:
            
            pd0, pd1 = prob_data
            pm0, pm1 = prob_model
            
            n = T.cast(prob_data[0].shape[0], dtype=FLOATX)
            
            g = -1.0/n*(T.dot(pd0.T, pd1) - T.dot(pm0.T, pm1))
            
            g += self.norm_grad(L1, L2)
            
        return g
        
    def gauged_W(self, mode):
    
        assert mode in ['AIS', 'center_std', 'white']
        
        W = self.W
        
        # W01_init = R0 * W01 * R1.T
        W = self.layer1.apply_white(W, tran=True)
        W = self.layer0.apply_white(W, x_right=True)
        
        if mode == 'AIS':    
            # W01_final = p0 * W01_init * p1
            W = self.layer1.apply_std(W)  
            W = self.layer0.apply_std(W, x_right=True)
            
        elif mode == 'center_std':
            # W01_final = R0^{-1} * P0^{-1} * W01_init * P1^{-1} * R1.T^{-1}
            W = self.layer1.apply_std(W, change=True, inv=True)
            W = self.layer1.apply_white(W, tran=True, inv=True)
            W = self.layer0.apply_std(W, change=True, x_right=True, inv=True)
            W = self.layer0.apply_white(W, inv=True, x_right=True)
            
        elif mode == 'white':
            # W01_final = R0_final^{-1} * W01_init * R1_final.T^{-1}
            W = self.layer1.apply_white(W, new=True, tran=True, inv=True)
            W = self.layer0.apply_white(W, new=True, inv=True, x_right=True)
        
        return W
    
    def gauged_W_velocity(self, mode, W_vel, b_vel=None, delta_c=None):
                
        assert mode in ['center_std', 'white']
        
        # Need both b_vel and delta_c
        if b_vel is not None:
            assert delta_c is not None
        if delta_c is not None:
            assert b_vel is not None
        
        # W_vel_init = R0.T^{-1} * W_vel * R1^{-1}
        W_vel = self.layer1.apply_white(W_vel, inv=True)
        W_vel = self.layer0.apply_white(W_vel, tran=True, inv=True, x_right=True)
        
        if mode == 'center_std':
            
            # W_vel = R0.T * P0 * W_vel_init * P1 * R1
            W_vel = self.layer1.apply_std(W_vel, change=True)
            W_vel = self.layer1.apply_white(W_vel)
            W_vel = self.layer0.apply_std(W_vel, change=True, x_right=True)
            W_vel = self.layer0.apply_white(W_vel, tran=True, x_right=True)
        
            if (b_vel is not None) and (delta_c is not None):
                bv0, bv1 = b_vel
                dc0, dc1 = delta_c
            
                # bv0_new.T = R0.T * P0 * R0.T^{-1} * bv0
                bv0 = T.repeat(bv0.reshape((-1, 1)), self.n1, 1)
                bv0 = self.layer0.apply_white(bv0, tran=True, inv=True, x_right=True)
                bv0 = self.layer0.apply_std(bv0, change=True, x_right=True)
                bv0 = self.layer0.apply_white(bv0, tran=True, x_right=True)
                
                # bv1_new = bv1 * R1^{-1} * P1 * R1
                bv1 = T.repeat(bv1.reshape((1, -1)), self.n0, 0)
                bv1 = self.layer1.apply_white(bv1, inv=True)
                bv1 = self.layer1.apply_std(bv1, change=True)
                bv1 = self.layer1.apply_white(bv1)
            
                # Delta_0.T = R0.T * p0 * delta_0
                dc0 = self.layer0.apply_std(dc0, x_right=True)
                dc0 = self.layer0.apply_white(dc0, tran=True, x_right=True)
                dc0 = T.repeat(dc0.reshape((-1, 1)), self.n1, 1)
                
                # Delta_1 = delta_1  * p1 * R1
                dc1 = self.layer1.apply_std(dc1)
                dc1 = self.layer1.apply_white(dc1)
                dc1 = T.repeat(dc1.reshape((1, -1)), self.n0, 0)
                
                # W_vel -= Delta_0.T * bv1_new
                W_vel -= dc0*bv1
                
                # W_vel -= bv0_new.T * Delta_1
                W_vel -= bv0*dc1
            
        elif mode == 'white':
            
            # W_vel = R0_new.T * W_vel_init * R1_new
            W_vel = self.layer1.apply_white(W_vel, new=True)
            W_vel = self.layer0.apply_white(W_vel, new=True, tran=True, x_right=True)
        
        return W_vel

    def update_weight_gauge(self, mode, updates=None):
        self.W_new = self.gauged_W(mode)
        if updates is None:
            self.updates = OrderedDict({self.W : self.W_new})
        else:
            updates[self.W] = self.W_new
            return updates
        
#%%
#%%
class Layer(NeuralNetParts):
    def __init__(self,
                 name,
                 output_dim,
                 input_dict=None,
                 transposed_dict=None,
                 IS_persist=False,
                 IS_centered=False,
                 std_eps=None,
                 white_type=None,
                 white_eps=None,
                 dropout_p=0.0,
                 batch_size=None,
                 theano_rng=None,
                 init_b=None,
                 init_persist=None,
                 init_c=None,
                 init_std=None,
                 init_wh=None,
                 init_wh_inv=None,
                 data_mean=None,
                 data_std=None,
                 data_cov=None,
                 activation=T.nnet.sigmoid):
        
        super(Layer, self).__init__(name)
        
        self.my_index = int(self.name[1:])
        self.output_dim = output_dim
        
        self.IS_persist = IS_persist
        self.IS_centered = IS_centered
        self.IS_std = (std_eps is not None)
        self.IS_whitened = (white_type is not None) and (white_eps is not None)
        self.std_eps = std_eps
        self.white_type = white_type
        self.white_eps = white_eps
        self.IS_datanorm = self.IS_centered or self.IS_std or self.IS_whitened
       
        if self.IS_whitened or self.IS_std:
            assert IS_centered
        if self.IS_std:
            assert std_eps is not None
        if self.IS_whitened:
            assert white_type is not None
            assert white_eps is not None
        
        self.batch_size = batch_size
        self.p = dropout_p
        self.IS_dropout = 1.0*(self.p > 0.0)
        if (0. < self.p < 1.) or self.IS_persist:
            assert self.batch_size is not None
        
        if theano_rng is None:
            theano_rng = make_theano_rng()
        self.theano_rng = theano_rng
        
        # creates a sorted input_dict
        self.input_dict = OrderedDict()
        if input_dict is not None:
            for k in np.sort(input_dict.keys()):
                self.input_dict[k] = input_dict[k]
            
        # creates a sorted transposed_dict
        self.transposed_dict = OrderedDict()
        if transposed_dict is not None:
            for k in np.sort(transposed_dict.keys()):
                self.transposed_dict[k] = transposed_dict[k]
            
        # assign layer properties
        len_in = len(self.input_dict)
        len_tran = len(self.transposed_dict)
        if len_in > 0 and len_tran > 0:
            self.layer_type = 'middle'
            self.direction = 'both'
            # need to compensate for reduced inputs when only going single direction
            self.z_up_adj = 1.0*(len_in+len_tran)/len_in
            self.z_down_adj = 1.0*(len_in+len_tran)/len_tran
        elif len_in > 0 and len_tran == 0:
            self.layer_type = 'output'
            self.direction = 'up'
        elif len_in == 0 and len_tran > 0:
            self.layer_type = 'input'
            self.direction = 'down'
        else:
            raise NotImplementedError
            
        self.activation = activation
            
        self.transposed_index = []
        self.W_T_ls = []
        for key, W in six.iteritems(self.transposed_dict):
            self.W_T_ls.append(W.T)
            self.transposed_index.append(key)
        
        self.input_index = []
        self.W_ls = []
        for key, W in six.iteritems(self.input_dict):
            self.W_ls.append(W)
            self.input_index.append(key)
        
        # create bias
        if init_b is None:
            if data_mean is not None:
                # tip from ref [2]
                p = np.clip(data_mean, EPS, 1-EPS)
                init_b = np.log(p/(1-p))
            else:            
                init_b = np.zeros(output_dim, dtype=FLOATX)
                
        self.b = make_shared(init_b, name=self.name + '_b')
        self.trainable_weights = self.b
       
        if self.IS_centered:
            if init_c is None:
                if data_mean is not None:
                    init_c = np.clip(data_mean, EPS, 1-EPS)
                else:            
                    # assuming these are Bernoulli variables
                    init_c = 0.5*np.ones(output_dim)
                        
            self.c = make_shared(init_c, name=self.name + '_center')
            self.non_trainable_weights = self.c
            
        if self.IS_std:
            if init_std is None:
                if data_std is not None:
                    init_std = data_std+100*EPS
                else:           
                    init_std = np.ones(output_dim)
                        
            self.std = make_shared(init_std, name=self.name + '_std')
            self.non_trainable_weights = self.std
            
        if self.IS_whitened:
            
            if data_cov is None:
                data_cov =  np.identity(output_dim)
            if init_wh is None:
                init_wh, init_wh_inv = self.update_white(cov=data_cov)
                        
            self.white = make_shared(init_wh, name=self.name + '_white')
            self.non_trainable_weights = self.white
            
            self.white_inv = make_shared(init_wh_inv,
                                         name=self.name + '_white_inv')
            self.non_trainable_weights = self.white_inv
            
            self.cov = make_shared(data_cov, name=self.name + '_cov')
            self.non_trainable_weights = self.cov
                        
        # persistant state of layer
        if self.IS_persist:
            if init_persist is None:
                if data_mean is not None:
                    # clipping data_mean to let each neuron have some variation
                    eps = 0.05
                    dm = np.clip(data_mean, eps, 1-eps)
                    dm *= np.mean(data_mean)/np.mean(dm) # keep same mean prob
                    #dm *= np.mean(dm)/np.mean(data_mean) # keep same mean prob
                    init_persist = np.repeat(dm.reshape((1, -1)), batch_size, 0)
                else:            
                    init_persist = 0.5*np.ones((batch_size, output_dim))    
            
            self._persist_chain = make_shared(init_persist,
                                              name=self.name+'_persist_chain')
            self.non_trainable_weights = self._persist_chain
        
        if self.IS_dropout:
            
            p = ((1-self.p)*np.ones((self.batch_size, self.output_dim)))
            self._p_matrix = p.astype(FLOATX)
            
            sample = binomial_sample(self.theano_rng, self._p_matrix).eval()
            self._dropout = make_shared(sample, name=self.name + '_dropout')
            self.non_trainable_weights = self._dropout      


    def init_layers(self, layer_in_ls, layer_out_ls):
        """ This allows layer to know who is connected to it """
        self.layer_in_ls = layer_in_ls
        self.layer_out_ls = layer_out_ls
        assert len(self.layer_in_ls) == len(self.W_ls)
        assert len(self.layer_out_ls) == len(self.W_T_ls)

    #%%
    @property
    def persist_chain(self):
        if self.IS_persist:
            return self._persist_chain
        else:
            return None
 
    
    @persist_chain.setter
    def persist_chain(self, persist_chain):
        if self.IS_persist:
            updates = OrderedDict({self._persist_chain : persist_chain})
            self.updates = updates
    
    #%%        
    def update_dropout(self):
        
        if 0. < self.p < 1:
            s = binomial_sample(self.theano_rng, self._p_matrix)
            updates = OrderedDict({self._dropout : s})
            self.updates = updates
        
    def update_center(self, prob_data, lr):
        if self.IS_centered:
            pd = prob_data[self.my_index]
            self.c_new = (1-lr)*self.c + lr*T.mean(pd, axis=0)
            self.updates = OrderedDict({self.c : self.c_new})
    
    def update_std(self, std_data, lr):
        if self.IS_std:
            sd = std_data[self.my_index]
            n = self.batch_size
            self.std_new = (1-lr)*self.std + lr*T.sqrt(T.sum(sd**2, axis=0)/(n-1))
            self.updates = OrderedDict({self.std : self.std_new})
    
    def update_cov(self, center_data, lr):
        if self.IS_whitened:
            cd = center_data[self.my_index]
            n = T.cast(self.batch_size, FLOATX)
            cov_update = T.dot(cd.T, cd)/(n-1)
            self.cov_new = (1-lr)*self.cov + lr*cov_update
            self.updates = OrderedDict({self.cov : self.cov_new})
    
    def update_white(self, cov=None, updates=None, eps=None):        
        if self.IS_whitened:
            if cov is None:
                cov = self.cov

            if eps is None:
                eps = self.white_eps
                
            if self.white_type == 'PCA':
                white_new = my_data.make_PCA_matrix(cov, eps)
            elif self.white_type == 'ZCA':
                white_new = my_data.make_ZCA_matrix(cov, eps)
            else:
                raise NotImplementedError, 'Do not know white_type: ' + self.white_type
                                
            if isinstance(white_new, np.ndarray):
                white_new_inv = np.linalg.inv(white_new)
            else:
                white_new_inv = nlinalg.MatrixInverse()(white_new)

            if isinstance(updates, OrderedDict):
                self.white_new = white_new
                self.white_new_inv = white_new_inv
                updates[self.white] = self.white_new
                updates[self.white_inv] = self.white_new_inv
                return updates
            else:
                return white_new, white_new_inv
    
    def update_bias_gauge(self, mode, updates=None):
        self.b_new = self.gauged_b(mode)
        
        if isinstance(updates, OrderedDict):
            updates[self.b] = self.b_new
            return updates
        else:
            self.updates = OrderedDict({self.b : self.b_new})    
    
    #%%
    def apply_dropout(self, x, IS_dropout):
        if (0. < self.p < 1.) and x is not None:
            x = ifelse(T.eq(IS_dropout, 0),
                       x,
                       x*self._dropout/T.cast(1.0-self.p, dtype=FLOATX)
                       )
        return x
 
    def apply_center(self, x, new=False):
        if self.IS_centered and x is not None:
            if new and hasattr(self, 'c_new'):
                c = self.c_new
            else:
                c = self.c
            x -= c   
        return x
    
    def apply_std(self, x, new=False, change=False, inv=False, x_right=False):
        if self.IS_std and x is not None:
            
            std_eps = T.cast(self.std_eps, dtype=FLOATX) 
            div = T.cast(1.0 + 2*std_eps, dtype=FLOATX)
            
            if change:
                # TODO: can I make this control sequence clearer?
                if hasattr(self, 'std_new'):
                    norm = (self.std + std_eps)/(self.std_new + std_eps)
                else:
                    return x
                
            elif new and hasattr(self, 'std_new'):
                norm = div/(self.std_new + std_eps)

            else:
                norm = div/(self.std + std_eps)
                    
            if inv:
                norm = 1.0/norm
                
            if x.ndim > 1 and x_right:
                norm = T.repeat(norm.reshape((-1, 1)), x.shape[1], 1)
            elif x.ndim > 1 and not x_right:
                norm = T.repeat(norm.reshape((1, -1)), x.shape[0], 0)
                
            x *= norm
                                
        return x
            
    def apply_white(self, x, tran=False, new=False, inv=False, x_right=False):
        
        if self.IS_whitened and (x is not None):
            
            if not hasattr(self, 'white_new'):
                new = False
                # TODO: should I do this or throw an error?
            
            if new and inv:
                W = self.white_new_inv
            elif new and not inv:
                W = self.white_new
            elif not new and inv:
                W = self.white_inv
            elif not new and not inv:
                W = self.white

            if tran:
                W = W.T
            
            if x_right:
                x = T.dot(W, x)
            else:
                x = T.dot(x, W)
            
        return x
    
    def apply_datanorm(self, x, tran=False, new=False, change=False, inv=False, x_right=False):
        x = self.apply_center(x, new=new)
        x = self.apply_std(x, new=new, change=change, inv=inv, x_right=x_right)
        x = self.apply_white(x, tran=tran, new=new, inv=inv, x_right=x_right)            
            
        return x
    
    #%%
    def grad_b(self, prob_data, prob_model):
        
        pd = prob_data[self.my_index]
        pm = prob_model[self.my_index]
        
        g = -(T.mean(pd, axis=0) - T.mean(pm, axis=0))

        return g
    
    def gauged_b(self, mode):
        assert mode in ['AIS', 'center_std', 'white']
        
        if self.IS_centered and mode in ['AIS', 'center_std']:
            
            layer_ls = self.layer_in_ls + self.layer_out_ls
            center_ls = [None]*len(layer_ls)
            for i, layer in enumerate(layer_ls):
                if mode == 'AIS':
                    c = -layer.c
                elif mode == 'center_std':
                    c = - layer.apply_center(layer.c, new=True)
                c = layer.apply_std(c)
                c = layer.apply_white(c)
                center_ls[i] = c
            b = self.get_input(center_ls, full_input=False, gauged=False)
            
        else:
            b = self.b
        
            
        # b_init = R * b_mod
        b = self.apply_white(b, x_right=True)
        
        if mode == 'AIS':    
            # b = p * b_init
            b = self.apply_std(b, x_right=True)
           
        elif mode == 'center_std':
            # b = R^{-1} * P^{-1} * b_init
            b = self.apply_std(b, change=True, inv=True, x_right=True)
            b = self.apply_white(b, inv=True, x_right=True)
        
        elif mode == 'white':
            # b = R_new^{-1} * b_init
            b = self.apply_white(b, new=True, inv=True, x_right=True)
        
        return b
        
    
    def gauged_b_velocity(self, b_vel, mode):
        assert mode in ['center_std', 'white']
                
        # b_vel_init = b_vel * R^{-1}
        b_vel = self.apply_white(b_vel, inv=True)
        
        if mode == 'center_std':    
            # b_vel = b_vel_init * P * R
            b_vel = self.apply_std(b_vel, change=True)
            b_vel = self.apply_white(b_vel)
            
        elif mode == 'white':
            # b_vel = b_vel_init * R_new
            b_vel = self.apply_white(b_vel, new=True)
        
        return b_vel
    
    #%%
    def needed_inputs(self, mean_field=True, direction=None):
        
        index = []
        if direction is None:
            direction = self.direction
        
        if direction in ['both', 'up']:
            index += self.input_index
        if not mean_field:
            index += [self.my_index]
        if direction in ['both', 'down']:
            index += self.transposed_index
            
        return index
    
    #%%
    def prep_input(self, full_input_ls, mean_field=True, direction=None):
        
        if direction is None:
            direction = self.direction
            
        index = self.needed_inputs(mean_field, direction)
        
        input_ls = [full_input_ls[i] for i in index]
        
        return input_ls

    #%%
    def get_input(self, input_ls, mean_field=True,
                  direction=None, full_input=True, gauged=True):
        
        if direction is None:
            direction = self.direction
        assert direction in ['both', 'up', 'down']
        
        if full_input:
            input_ls = self.prep_input(input_ls, mean_field, direction)
        
        # Controls which interactions to include    
        IS_up, IS_down = True, True
        if direction == 'up' or self.direction == 'up':
            IS_down = False            
        elif direction == 'down' or self.direction == 'down':
            IS_up = False
        
        start, stop = 0, 0
        current_W_ls, current_input = [], []
        
        if IS_up:
            stop = len(self.W_ls)
            current_input += input_ls[start:stop]
            current_W_ls += self.W_ls
        if not mean_field:
            input_self = input_ls[stop]
            stop += 1
        if IS_down:
            current_input += input_ls[stop:]
            current_W_ls += self.W_T_ls
              
        # Calculate activation input
        z = self.b
        for data, W in zip(current_input, current_W_ls):
            z += T.dot(data, W)
            if not mean_field:
                if self.IS_datanorm:
                    raise NotImplementedError
                z += -0.5*T.dot(data-data**2, W**2)*(input_self-0.5)
                
        # need to compensate for reduced input        
        if (self.layer_type == 'middle') and (direction == 'up'):
            z *= self.z_up_adj  
        elif (self.layer_type == 'middle') and (direction == 'down'):
            z *= self.z_down_adj
            
        if self.IS_std and gauged:
            z = self.apply_std(z)
            
        # TODO: check this
        if self.IS_whitened and gauged:
            z = self.apply_white(z, tran=True)
            
        return z

    #%%
    def get_output(self, input_ls, beta=1.0, mean_field=True, direction=None,
                   full_input=True, gauged=True):
        
        beta = T.cast(beta, FLOATX)
        z = self.get_input(input_ls, mean_field, direction, full_input, gauged)
        prob = self.activation(beta*z)   
        
        return prob
  